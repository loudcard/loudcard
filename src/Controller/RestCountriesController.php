<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\Mailer\Email;

use App\Controller\AppController;
// In a controller or table method.
use Cake\ORM\TableRegistry;




/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class RestCountriesController extends AppController
{
    public $component = array('RequestHandler');
    public $paginate = [
    'page' => 1,
    'limit' => 5,
    'maxLimit' => 100,
    'fields' => [],
    'sortWhitelist' => []
    ];
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);        
        $this->loadModel("Countries");
    }

    /**
     * Function to list all countries
     *
     * @return Json countries List
     */
    public function index() {        
        $queryArr = $this->Common->getQueryStrings(); 
        if(isset($queryArr)){
           extract($queryArr);
        }
        
        if(isset($type) && $type!='') {
            $type = $type;
        }else {
            $type = '';
        }
        
        $countries_list = [];
        $status         = 'fail';
        $mesage         = 'no data found';

        switch ($type) {
            case 'allcountries': {
                $countries = $this->Countries->find('all')->select(['id','name'])->order('name')->all();
                if(isset($countries) && $countries!=null) {
                    $countries_list = $countries;
                    $status         = 'success';
                    $mesage         = 'countries found';
                }
            }break;
            default : {

            }break;
        }
        
       $this->set([
            'status'     =>  $status,
            'message'    =>  $mesage,
            'data'       =>  $countries_list,            
            '_serialize' => ['status','message','data']
        ]);
    }
 


    /**
     * Function to add a new country
     *
     * @return Json message

     *      */
    public function add() {
        $action = $this->request->data['action'];        
        $data = [];
        $this->set([
            'data'=>$data,
            '_serialize' => ['data']
        ]);

    }

    public function view($id)
    {
        $queryArr=$this->Common->getQueryStrings();   
        
        if($queryArr)
            extract($queryArr); 
        
        $data = [];
        $this->set([
            'data'=>$data,
            '_serialize' => ['data']
            ]);
    }


    public function edit($id)
    {
        $this->loadModel('Countries');
        $data = [];

        $this->set([
            'status' => 'success',
            'data' => $data,
            '_serialize' => ['message', 'errorType']
            ]);
    }


    
    

    public function delete($id)
    {
        $this->loadModel('Countries');
        $queryArr=$this->Common->getQueryStrings();        
        extract($queryArr);

        $messages = [];
        $this->set([
            'messages'   => $messages,
            '_serialize' => ['message']
            ]); 
    }
}
