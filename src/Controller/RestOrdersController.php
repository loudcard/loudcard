<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Core\Exception\Exception;
use App\Controller\AppController;
// In a controller or table method.
use Cake\ORM\TableRegistry;




/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class RestOrdersController extends AppController
{
    public $component = array('RequestHandler');
    public $paginate = [
    'page' => 1,
    'limit' => 5,
    'maxLimit' => 100,
    'fields' => [],
    'sortWhitelist' => []
    ];
    
    public $img_type_array = array('image/png', 'image/jpg', 'image/jpeg', 'image/gif');
    public $audio_type_array = array('audio/wav','audio/x-m4a','audio/m4a');
    
    
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event); 
        /*$this->response->cors($this->request)
        ->allowOrigin(['*'])
        ->allowMethods(['GET', 'POST','PUT','PATCH','DELETE','OPTIONS'])
        ->allowHeaders(['X-CSRF-Token','Content-Type, Authorization, X-Requested-With, Accept'])
        ->allowCredentials()
        ->exposeHeaders(['Link'])
        ->maxAge(300)
        ->build();*/
        $this->loadModel("Orders");
        $this->loadModel('UsersImages');
        $this->loadModel('UsersAudio');
        $this->loadModel("Templates");
        $this->loadModel("ShippingAddress");
    }

    /**
     * Function to list all countries
     *
     * @return Json countries List
     */
    public function index() {
        $queryArr = $this->Common->getQueryStrings(); 
        if(isset($queryArr)){
           extract($queryArr);
        }
        
        if(isset($type) && $type!='') {
            $type = $type;
        }else {
            $type = '';
        }
        
        $states_list = [];
        $status         = 'fail';
        $mesage         = 'no data found';       
        
        $this->set([
            'status'     =>  $status,
            'message'    =>  $mesage,
            'data'       =>  $states_list,            
            '_serialize' => ['status','message','data']
        ]);
    }
 


    /**
     * Function to add a new country
     *
     * @return Json message

     *      */
    public function add() {        
        $data       = [];   
        $status     = 'fail';
        $message    = 'no action performed'; 	   
        try {
            $user_id        =   '';
            $template_id    =   '';
            $imagefileName  =   '';
            $audiofileName  =   '';
            $quantity       =   0;
            $price          =   0;
            $shipping_cost  =   0;
            $total_amount   =   0;
            $send_to        =   '';
            $payment_from   =   '';
            $card_type      =   '';
            $address_array  =   [];
            $created_on     =   date("Y-m-d H:i:s");
            
            
            if(isset($this->request->data['user_id']) && !empty($this->request->data['user_id']) && $this->request->data['user_id']!=0) {
                $user_id        =  $this->request->data['user_id'];
            }else {
                throw new Exception("empty user");
            }
            
            if(isset($this->request->data['template_id']) && !empty($this->request->data['template_id']) && $this->request->data['template_id']!=0) {
                $template_id    =  $this->request->data['template_id'];
            }else {
                throw new Exception("empty template");
            }
            
            if(isset($this->request->data['price']) && !empty($this->request->data['price'])){
                $price  = $this->request->data['price'];
            }else {
                throw new Exception("empty price");
            }
            
            if(isset($this->request->data['shipping_cost']) && !empty($this->request->data['shipping_cost'])){
                $shipping_cost  = $this->request->data['shipping_cost'];
            }else {
                throw new Exception("empty shipping cost");
            }
            
            if(isset($this->request->data['total_amount']) && !empty($this->request->data['total_amount'])){
                $total_amount  = $this->request->data['total_amount'];
            }else {
                throw new Exception("empty total amount");
            }
            
            if(isset($this->request->data['send_to']) && $this->request->data['send_to']!=''){
                //to_receipent,to_me
                $send_to  = $this->request->data['send_to'];
            }else {
                throw new Exception("empty send to");
            }
            
            if($send_to=='to_receipent' || $send_to=='to_me' ) {

                if(isset($this->request->data['address']) && $this->request->data['address']!='') {
                 $address_array  =  json_decode(stripslashes($this->request->data['address']));                   
                }else {
                    throw new Exception("empty addresses 2");
                }
            }    
            else {
                throw new Exception("invalid send to");
            }


            
            
            if(isset($this->request->data['payment_from']) && $this->request->data['payment_from']!=''){
                $payment_from  = $this->request->data['payment_from'];
            }
            
            if(isset($this->request->data['card_type']) && $this->request->data['card_type']!=''){
                $card_type  = $this->request->data['card_type'];
            }
            
            if(isset($this->request->data['card_type']) && $this->request->data['card_type']!=''){
                $card_type  = $this->request->data['card_type'];
            }
            
            if(isset($this->request->data['quantity']) && $this->request->data['quantity']!='') {
                $quantity = $this->request->data['quantity'];
            }
            
            
            $save_order =     $address_array; 
            /*pr($save_order);die();*/
            
            /*uploading image*/
           /* if(isset($_FILES['image']) && count($_FILES['image'])>0) {
                if(in_array($_FILES['image']['type'], $this->img_type_array)) {
                    $tmp_name = $_FILES["image"]["tmp_name"];
                    $name = $_FILES["image"]["name"];
                    //$uploads_dir = $_SERVER['DOCUMENT_ROOT'].'/loudcard/webroot/img/user_images';
                    $uploads_dir = $_SERVER['DOCUMENT_ROOT'].'/loudcards/webroot/img/user_images';
                    $nameArr = pathinfo($name);
                    $remove_spaces_from_image = preg_replace("/[^A-Za-z0-9\_\-\.]/", '', $this->request->data['image']['name']);
                    $imagefileName = time().uniqid().$remove_spaces_from_image;
                    if(move_uploaded_file($tmp_name, "$uploads_dir/$imagefileName")) { 
                                              
                    }else {
                        throw new Exception('image not uploaded');
                    }
                }else {
                    throw new Exception('image not supported.please try again');
                }
            }else {
                throw new Exception('no image to upload.please try againg');
            }
            
            /*uploading audio*/
        /*    if(isset($_FILES['audio']) && count($_FILES['audio'])>0) {
                if(in_array($_FILES['audio']['type'], $this->audio_type_array)) {
                    $tmp_name = $_FILES["audio"]["tmp_name"];
                    $name = $_FILES["audio"]["name"];
                    //$audio_uploads_dir = $_SERVER['DOCUMENT_ROOT'].'/loudcard/webroot/img/user_audio';
                    $audio_uploads_dir = $_SERVER['DOCUMENT_ROOT'].'/loudcards/webroot/img/user_audio';
                    $nameArr = pathinfo($name);
                    $remove_spaces_from_image = preg_replace("/[^A-Za-z0-9\_\-\.]/", '', $this->request->data['audio']['name']);
                    $audiofileName = time() . uniqid() . $remove_spaces_from_image;
                    if(move_uploaded_file($tmp_name, "$audio_uploads_dir/$audiofileName")) { 
                                              
                    }else {
                        throw new Exception('image not uploaded');
                    }
                }else {
                    throw new Exception('audio not supported.please try again');
                }
            }else {
                throw new Exception('no audio to upload.please try againg');
            }
            
            
            /*Creating user image*/
        /*    $user_file_entity = $this->UsersImages->newEntity();
            $user_file_entity->user_id = $user_id;            
            //$user_file_entity->template_id = $template_id;  
            $user_file_entity->user_image = $imagefileName;
            $user_file_entity->created_on           = $created_on;
            $save_user_image = $this->UsersImages->save($user_file_entity); 
            if($save_user_image) {
                $message = 'image created successfully';
                $user_image_id = $save_user_image->id;
            }else {
                throw new Exception('image not added');
            }
            
            
            /*Creating user audio*/
        /*    $user_audio_entity = $this->UsersAudio->newEntity();
            $user_audio_entity->user_id = $user_id;
            //$user_audio_entity->template_id = $template_id;            
            $user_audio_entity->user_audio = $audiofileName;
            $user_audio_entity->created_on           = $created_on;
            $save_audio = $this->UsersAudio->save($user_audio_entity);
            if($save_audio) {
                $message = 'audio created successfully';
                $audio_id = $save_audio->id;                
            }else {
                throw new Exception('audio not added');
            }
            
            
            //$get_price = $this->Templates->find()->where(['id' => $template_id])->first();
            
            /*Creating user order*/
        /*    $Orders_table                       = $this->Orders->newEntity();
            $Orders_table->user_id              = $user_id;
            $Orders_table->template_id          = $template_id;
            $Orders_table->user_image_id        = $user_image_id;
            $Orders_table->user_audio_id        = $audio_id;
            $Orders_table->quantity             = $quantity;
            $Orders_table->price                = $price;
            $Orders_table->shipping_cost        = $shipping_cost;
            $Orders_table->total_amount         = $total_amount;
            $Orders_table->send_to              = $send_to;           //to_receipent,to_me
            $Orders_table->payment_from         = $payment_from; //paypal,credit card
            $Orders_table->card_type            = $payment_from; //Visa            
            $Orders_table->created_on           = $created_on;
            $Orders_table->order_created_from   = 'mobile';
            $save_order = $this->Orders->save($Orders_table);
            if($save_order) {
                $status = 'success';
                $message = 'order created successfully';
                $order_id = $save_order->id;
            }else {
                throw new Exception('order not created');
            }
            
            /*Creating order shipping addresses*/
        /*    if($address_array!='') {
                foreach($address_array as $key=>$shiping_address) {
                    $shiping_address['order_id']    = $order_id;
                    $shiping_address['user_id']     = $user_id;
                    $shiping_address['created_on']  = $created_on;
                    $shipping_to                    = $this->ShippingAddress->newEntity($shiping_address);
                    if($this->ShippingAddress->save($shipping_to)){
		    }else {
			throw new Exception('address not added in database');	
                    }
                    
                }
            }else {
		throw new Exception('address empty 3');	
            }
            
            
            
            
            /*$maildata = array();
            $maildata['username'] =  @$this->Auth->User('first_name').'  '.@$this->Auth->User('last_name');
            $maildata['template_name'] = $this->request->session()->read("GuestUser.template_name");
            $maildata['tag_name'] = $this->request->session()->read("GuestUser.tag_name");
            $maildata['price'] = $this->request->session()->read("GuestUser.price");
            $maildata['type'] = "sign up";
            $email = new Email();
            $send_email = $email->template('detail')
                ->emailFormat('html')
                ->to('ljaswal@teqmavens.com')
                ->from(['ljaswal@teqmavens.com' => 'loudcard'])
                ->subject('Loudcard: Order detail')
                ->attachments([
                    'photo.jpg' => [
                        'file' => WWW_ROOT . 'img/user_images/' . $this->request->session()->read('GuestUser.image_name'),
                    ],
                    'audio.wav' => [
                       'file' => WWW_ROOT . 'img/user_audio/' . $this->request->session()->read('GuestUser.audio_name'),
                    ]
                ])
                ->viewVars($maildata)
                ->send();  
             * 
             */
            $this->set([
            'status'     =>  $status,
            'message'    =>  $message,
            'data'       =>  $save_order, 
            'request_data'=> [$_REQUEST,$_FILES],    
            '_serialize' => ['status','message','data','request_data']
            ]);
        
        } catch (Exception $ex) {
            $this->set([
            'status'     =>  $status,
            'message'    =>  $ex->getMessage(),
            'data'       =>  [],            
            'request_data'=> [$_REQUEST,$_FILES],            
            '_serialize' => ['status','message','data','request_data']
            ]);            
            
        }

    }

    public function view($id)
    {
        $queryArr=$this->Common->getQueryStrings();   
        
        if($queryArr)
            extract($queryArr); 
        
        $data = [];
        $this->set([
            'data'=>$data,
            '_serialize' => ['data']
            ]);
    }


    public function edit($id)
    {
        $data       = [];   
        $status     = 'fail';
        $message    = 'no action performed';        
        try {            
            $transaction_id =   '';            
            $modified_on    =   date("Y-m-d H:i:s");
            $order          =   [];            
            if(empty($id)) {
                throw new Exception("empty order");
            }
            
            if(isset($this->request->data['transaction_id']) && $this->request->data['transaction_id']!='') {
                $transaction_id    =  $this->request->data['transaction_id'];
            }else {
                throw new Exception("empty transaction");
            }
            
            if(isset($this->request->data['status']) && $this->request->data['status']!='') {
                $status    =  $this->request->data['status'];
            }else {
                throw new Exception("empty status");
            }
            
            $order = $this->Orders->get($id);
            
            if(isset($order) && count($order)>0 && $order!=null) {
                $order = $this->Orders->get($id);
            }else {
                throw new Exception("order not found");
            }
            
            $order->transaction_id  = $transaction_id;
            $order->status          = $status;
            $order->modified_on     = $modified_on;
            
            if($this->Orders->save($order)) {
                $status     = 'success';
                $message    = 'order updated';
            }else {
                throw new Exception("order not updated");
            }
            
            $this->set([
                'status'     =>  $status,
                'message'    =>  $message,
                'data'       =>  $order, 
                'request_data'=> [$this->request->data],    
                '_serialize' => ['status','message','data','request_data']
            ]);
        } catch (Exception $ex) {
            $this->set([
            'status'     =>  $status,
            'message'    =>  $ex->getMessage(),
            'data'       =>  [],            
            'request_data'=> [$_REQUEST,$_FILES],
            '_serialize' => ['status','message','data','request_data']
            ]);            
            
        }    
    }


    
    

    public function delete($id)
    {
        $this->loadModel('Orders');
        $queryArr=$this->Common->getQueryStrings();        
        extract($queryArr);

        $messages = [];
        $this->set([
            'messages'   => $messages,
            '_serialize' => ['message']
            ]); 
    }
}
