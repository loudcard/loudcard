<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Core\Exception\Exception;
use App\Controller\AppController;
// In a controller or table method.
use Cake\ORM\TableRegistry;

require_once(ROOT .DS. 'vendor' . DS . 'braintree_php' . DS . 'lib' . DS . 'Braintree.php');
use Braintree;
use DOMPDF;



/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class RestPaymentsController extends AppController
{
    public $component = array('RequestHandler');
    
    public $paginate = [
    'page' => 1,
    'limit' => 5,
    'maxLimit' => 100,
    'fields' => [],
    'sortWhitelist' => []
    ];
    
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event); 
        /*$this->response->cors($this->request)
        ->allowOrigin(['*'])
        ->allowMethods(['GET', 'POST','PUT','PATCH','DELETE','OPTIONS'])
        ->allowHeaders(['X-CSRF-Token','Content-Type, Authorization, X-Requested-With, Accept'])
        ->allowCredentials()
        ->exposeHeaders(['Link'])
        ->maxAge(300)
        ->build();*/
        $this->loadModel("Orders");
        $this->loadModel('UsersImages');
        $this->loadModel('UsersAudio');
        $this->loadModel("Templates");
        $this->loadModel("ShippingAddress");

	 $environment = Configure::read('BarinTree_environment');
        $merchantId = Configure::read('BarinTree_merchant_id');
        $publicKey = Configure::read('BarinTree_public_key');
        $privateKey = Configure::read('BarinTree_private_key');

	   Braintree\Configuration::environment($environment);
        Braintree\Configuration::merchantId($merchantId);
        Braintree\Configuration::publicKey($publicKey);
        Braintree\Configuration::privateKey($privateKey);
	
        /*Braintree\Configuration::environment('sandbox');
        Braintree\Configuration::merchantId('v8xyfcdmtn7sbsyb');
        Braintree\Configuration::publicKey('qnrhmh45ptfps3dz');
        Braintree\Configuration::privateKey('816fa5fd7e897d24013cf9a593969365');*/
        
    }

    /**
     * Function to list all countries
     *
     * @return Json countries List
     */
    public function index() {
        $queryArr = $this->Common->getQueryStrings(); 
        if(isset($queryArr)){
           extract($queryArr);
        }
        
        if(isset($type) && $type!='') {
            $type = $type;
        }else {
            $type = '';
        }
        
        $data           = [];
        $status         = 'fail';
        $mesage         = 'no data found';
        
        switch($type) {
            case 'gettoken':{
                $data['token'] = Braintree\ClientToken::generate();
                if($data['token']!='') {
                    $status = 'success';
                    $mesage = 'new token created';
                }else {
                    $mesage = 'token not created';
                }                
            }break;
            default : {                
            }break;            
        }
        
        $this->set([
            'status'     =>  $status,
            'message'    =>  $mesage,
            'data'       =>  $data,            
            '_serialize' => ['status','message','data']
        ]);
    }
 


    /**
     * Function to add a new country
     *
     * @return Json message

     *      */
    public function add() {
        $data       = [];   
        $status     = 'fail';
        $message    = 'no action performed'; 
        
         try {
             
             $nonceFromTheClient    = '';
             $amount                = 0;
             $order_id              = 0;
             $template_code         = '';
             $quantity              = 0;
             $shipping              = 0;
             $user_id               = 0;
             
             
             
                if(isset($this->request->data['client_nonce']) && $this->request->data['client_nonce']!='') {
                $nonceFromTheClient = $this->request->data['client_nonce'];
            }else {
                throw new Exception("empty nonce");
            }
            
            if(isset($this->request->data['amount']) && $this->request->data['amount']!='') {
                $amount = $this->request->data['amount'];
            }else {
                throw new Exception("empty amount");
            }
            
            if(isset($this->request->data['order_id']) && !empty($this->request->data['order_id']) && $this->request->data['order_id']!=0) {
                $order_id = $this->request->data['order_id'];
            }else {
                throw new Exception("empty order");
            }
            
            if(isset($this->request->data['template_code']) && $this->request->data['template_code']!='') {
                $template_code = $this->request->data['template_code'];
            }
            
            if(isset($this->request->data['shipping']) && $this->request->data['shipping']!='') {
                $shipping = $this->request->data['shipping'];
            }
            
            if(isset($this->request->data['user_id']) && $this->request->data['user_id']!='') {
                $user_id = $this->request->data['user_id'];
            }
            
            /*$result = Braintree\Transaction::sale([
                'amount'                => $amount,
                'orderId'               => $order_id,
                'paymentMethodNonce'    => $nonceFromTheClient,
                'options'               => [ 
                    'submitForSettlement' => true 
                ],
                'customFields'    => [
                    'template_code' => $template_code,
                    'shipping'      => $shipping,
                    'user_id'       => $user_id,                    
                ]     
            ]);*/
            
            $result = Braintree\Transaction::sale([
                'amount'                => $amount,
                'orderId'               => $order_id,
                'paymentMethodNonce'    => $nonceFromTheClient,
                'options'               => [ 
                    'submitForSettlement' => true 
            ]]);
            
           
            
            if ($result->success) {                
             /*        require_once(ROOT . DS . 'vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php');
                      $dompdf = new DOMPDF();
                        $SITEURL = Configure::read('SITEURL');
                        $project_name = Configure::read('project_name');
                        $support_email = Configure::read('Support');
                        $FROM_EMAIL = Configure::read('FROM_EMAIL');
                        $AdminEmail = Configure::read('AdminEmail');
                         $downloaded_path = $SITEURL . 'img' . DS . 'orderpdf' . DS . 'pdf' . time() . ".pdf";
                        $folder_path = WWW_ROOT . 'img' . DS . 'orderpdf' . DS . 'pdf' . time() . ".pdf";
                        $logo = WWW_ROOT . 'img' . DS . 'logo.png';
                        $day = date("D");
                        $d_m_y = date("M j, Y");
                        $sent_time = $day . ", " . $d_m_y;
                $order_data = $this->Orders->find()->where(['id' => $order_id])->first();
                if($order_data['send_to']=="to_me"){
                    $shipped_to = "To Me";
                }
                if($order_data['send_to']=="to_receipent"){
                    $shipped_to = "To Recipient";
                }
                $user_data =  $this->Users->find()->where(['id' => $order_data['user_id']])->first();
                $template_name = $this->Templates->find()->where(['id' => $order_data['template_id']])->first(); 
                $image_name = $this->UsersImages->find()->where(['id' => $order_data['user_image_id']])->first(); 
                $audio_name = $this->UsersAudio->find()->where(['id' => $order_data['user_audio_id']])->first(); 
                $total = $order_data['price']*$order_data['quantity'];
                $shipping_cost = $order_data['shipping_cost'];
                $sales_tax = $order_data['total_sales_tax'];
                $grand_total = $order_data['total_amount'];
                $html = '<html>
    <head></head>

    <table align="center" style="text-align: center; font-size:12px;" width="100%" border="0">
        <tr align="left">

            <td align="left" style="font-size:10px;">
                ' . date("n/j/Y") . '
            </td>
            <td colspan="4" align="left" style="font-size:10px;">
                From - ' . $FROM_EMAIL . '
            </td>
        </tr>
        <tr align="left">  <td><img align="left" src=' . $logo . ' width="80"></td>
            <td align="right" colspan="4" style="font-weight:bold;">&lt;' . $user_data['email'] . '&gt;</td>
        </tr>
        <tr>
            <td  colspan="5" style="color:lightgrey;">
                <hr>
            </td>
        </tr>
        <tr align="left">
            <td  colspan="4">
                Subject: <b>Order Detail</b>
            </td>
        </tr>
        <tr align="left">
            <td  colspan="4">
                
            </td>
        </tr>
         <tr>
            <td  colspan="5" style="color:lightgrey;">
                <hr>
            </td>
        </tr>
          <tr align="left">
            <td  colspan="2">
                From: <b>'.$project_name.'</b> &lt;' . $FROM_EMAIL . '&gt;
            </td>
            
             <td colspan="3" align="right">
             ' . $sent_time . '
                
            </td>
        </tr>
          <tr align="left">
            <td  colspan="4">
                To: ' . $user_data['email'] . '
            </td>
        </tr>';
                                $html .='<tr align="left">
            <td  colspan="4">
              Shipped '.@$shipped_to.'  
            </td>
        </tr>
         <tr>
            <td  colspan="4">
                
            </td>
        </tr>
         <tr>
            <td  colspan="4">
                
            </td>
        </tr>
                
        
        <tr>
            <td  colspan="4">
                
            </td>
        </tr>
         <tr>
            <td  colspan="4">
                
            </td>
        </tr>
         <tr>
            <td  colspan="4">
                
            </td>
        </tr>
        <tr align="left">
            <td  colspan="4">
                
            </td>
            <td align="left" colspan="4">
            </td>
        </tr>

    </table>';
    $get_shipp_address = $this->ShippingAddress->find()->where(['order_id' => $order_data['id']])->hydrate(false)->toArray();
        if(!empty($get_shipp_address)){
            foreach ($get_shipp_address as $key => $ship_adr) {
            $this->loadModel("Countries");
            $this->loadModel("States");   
        $get_countries = $this->Countries->find()->where(['id' => $ship_adr['country_id']])->first();

    $html.='<div style="width:30%; float:left; margin:10px 0px;border:1px solid grey"">
  <h4 style="font-weight:bold;font-size:12px;background-color:lightgrey;">Address'.$i.'</h4>
  <p style="font-size:10px;">'.$ship_adr['first_name']." ".$ship_adr['last_name'].'</p>
  <p style="font-size:10px;">'.$ship_adr['address_one']." ".@$ship_adr['address_two'].'</p>
  <p style="font-size:10px;">'.$ship_adr['city']." , ".'';
      if(!empty($ship_adr['state_id'])){
        $get_state = $this->States->find()->where(['id' => $ship_adr['state_id']])->first();
        $html .= @$get_state['name'];
         }
         $html.='</p>
  <p style="font-size:10px;">'.$get_countries['name'].', '.$ship_adr['zip'].'</p>
</div>';
$i++;
            }
    $html.='</div>';        
        }   

        $html.='<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px; border:1px solid #f1f1f1; padding:5px;">
  <tr>
    
    <td valign="top" style="text-align:left; padding-left:8px; font-weight:500; height:25px;">'.$template_name['name'].' ('.$template_name['code'].')</td>
    <td valign="top" style="text-align:right; padding-left:8px; font-weight:500">$'.$get_price['price'].'</td>
  </tr>
  <tr>
    <td valign="top" style="text-align:left; padding-left:8px; color:#333;">Quantity</td>
    <td valign="top" style="text-align:right; padding-left:8px; color:#333;">'.$order_data['quantity'].'</td>
  </tr>
</table>';

   $html.='<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px; text-align:right; color:#9f9f9f;">
  <tr>
    <td style="height:20px;">Items total</td>
    <td>$'.number_format($total,2).'</td>
  </tr>
  <tr>
    <td style="height:20px;">Shipping</td>
    <td>$'.number_format($shipping_cost,2).'</td>
  </tr>
  <tr>
    <td style="height:20px;">Sales Tax</td>
    <td>$'.number_format($sales_tax,2).'</td>
  </tr>

</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px; text-align:right; color:#9f9f9f; border-top:1px solid #f2f2f2; border-bottom:1px solid #f2f2f2;">

 <tr>
    <td style="color:#333;">Order Total</td>
    <td style="color:#333;">$'.number_format($grand_total,2).'</td>
  </tr>
  </table>';
                  

$html.='</html>
';


                                $dompdf->load_html($html);
                                $dompdf->render();

                                $output = $dompdf->output();
                                file_put_contents($folder_path, $output);
  
                   $maildata = array();
                $url = $SITEURL;
               $mail_content ="";
               $mail_content.='<p>Hi '.$this->Auth->user('first_name').',</p>
                        
                <p>Thank you for shopping at Lizicards. We&#39;ll soon be mailing your greeting card(s).</P> 

                <p>Please find attached the receipt.</p>  

                <p>If you have any questions, please email us: '.$FROM_EMAIL.'.</p>             
                 
                 <p>Many thanks, <br/>Lizicards Team</p>       
                      </td>                  
              </tr>
              
                <tr>
                
                
                  <td align="center">   <a href='.$url.' style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Send a Greeting Card </a> </td>
                  
                
                  </tr>';  
               
               $maildata['heading'] = "Lizicards: Order Confirmation";   
               $maildata['url'] = $url;  
               $maildata['data'] = $mail_content;
    
             $email = new Email();
                        $send_email = $email->template('allmail')
                        ->emailFormat('html')
                        ->to(array($this->Auth->user("email"),$FROM_EMAIL,$support_email))
                        ->from([$FROM_EMAIL => $project_name])
                        ->subject('LiziCards: Order Confirmation')
                        ->viewVars($maildata)
                        ->attachments([
                'invoice.pdf' => [
                'file' => $folder_path,
                ]])          
                        ->send();

            $email_audio_image = new Email();
            $email_audio_image->template('allmail')
            ->emailFormat('html')
            ->from([$FROM_EMAIL => $project_name])
            ->to($AdminEmail)
            ->subject('LiziCards Uploded Image and Audio')
            ->viewVars($maildata)
            ->attachments([
                'invoice.pdf' => [
                'file' => $folder_path,
                ],
                'photo.jpg' => [
                'file' => WWW_ROOT . 'img/user_images/' . $image_name['user_image'],
                ],
                'audio.wav' => [
                'file' => WWW_ROOT . 'img/user_audio/' . $audio_name['user_audio'],
                ]
                ])
            ->send();  */ 
                $status = 'success';
                $data['transaction_id'] = $result->transaction->id;
                
                $update_array = [
                  'transaction_id' =>  $data['transaction_id'],
                    'status'       => 'completed'
                ];                
                $update_order  =  $this->Common->sendAPIRequest("rest_temp_orders/$order_id.json", "PUT", $update_array);                
                $message = "payment done successfully";
            } else if ($result->transaction) { 
                $data['error_code'] = $result->transaction->processorResponseCode;
                $message = $result->transaction->processorResponseText;                
            } else {
                $message = "validation errors";
                $errors = $result->errors->deepAll();                
                if(isset($errors) && count($errors)>0) {
                    foreach($errors as $key=>$val) {
                        $data['error_data'][$key] = $val;
                    }
                }
            }
            $this->set([
                'status'     =>  $status,
                'message'    =>  $message,
                'data'       =>  $data,            
                'request_data'=> [$_REQUEST,$_FILES],
                'braintree_response' => $result,           
                '_serialize' => ['status','message','data','request_data','braintree_response']
            ]);
            
         }catch(Exception $ex){
            $this->set([
            'status'     =>  $status,
            'message'    =>  $ex->getMessage(),
            'data'       =>  [],            
            'request_data'=> [$_REQUEST,$_FILES],            
            '_serialize' => ['status','message','data','request_data']
            ]);
        }
        
    }

    public function view($id)
    {
        $queryArr=$this->Common->getQueryStrings();   
        
        if($queryArr)
            extract($queryArr); 
        
        $data = [];
        $this->set([
            'data'=>$data,
            '_serialize' => ['data']
        ]);
    }


    public function edit($id)
    {
        $data       = [];   
        $status     = 'fail';
        $message    = 'no action performed';        
        $this->set([
            'status'     =>  $status,
            'message'    =>  $message,
            'data'       =>  $data,            
            'request_data'=> [$_REQUEST,$_FILES],
            '_serialize' => ['status','message','data','request_data']
        ]);            
    }


    
    

    public function delete($id)
    {
        $this->loadModel('Orders');
        $queryArr=$this->Common->getQueryStrings();        
        extract($queryArr);

        $messages = [];
        $this->set([
            'messages'   => $messages,
            '_serialize' => ['message']
        ]); 
    }
}
