<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\Mailer\Email;
use App\Controller\AppController;
use Cake\Core\Exception\Exception;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
// In a controller or table method.
use Cake\ORM\TableRegistry;
use Imagick;
use CropAvatar;
use PaypalPro;
use DOMPDF;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class UsersController extends AppController
{

    public function checkemailuniqueness() {
        $this->autoRender = false;

        if (!empty($this->request->query['email'])) {
            $users_table = TableRegistry::get('Users');
            $query = $users_table->find()->where(['email' => $this->request->query['email'], 'status <=' => '1'])->count();
            if ($query > 0) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }

    public function register(){
        try {
            // pr($this->request->data);            

            if (empty($this->request->data['first_name'])) {
                throw new Exception("Please enter first_name.");
            }
            if (empty($this->request->data['last_name'])) {
                throw new Exception("Please enter last name");
            }
            if (isset($this->request->data['password']) && empty($this->request->data['password'])) {
                throw new Exception("Please enter password");
            }
	     $check_user_email = $this->Users->find("all", array("conditions" => array("Users.email" => $this->request->data['email'])))->first();
            if(!empty($check_user_email)){
               if($check_user_email['type']==0){
                throw new Exception("Email restricted. Please enter other email.");
               }
            }
            if($this->request->data['type']==1){
                if (empty($this->request->data['email']) || !filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                    throw new Exception("Please enter valid email");
                }
                $check_user_email = $this->Users->find("all", array("conditions" => array("Users.email" => $this->request->data['email'],"Users.status <= " => 1)))->count();
                if($check_user_email > 0){
                    throw new Exception("Email already exists.");
                }
                else{
                     $this->request->data["created_on"] = date("Y-m-d H:i:s");
                     $this->request->data["modified_on"] = date("Y-m-d H:i:s");
                     $store_data = $this->Users->newEntity($this->request->data);
                    if ($this->Users->save($store_data)) {
                        $user_id = $store_data->id;
                        $this->loadModel('UsersImages');
                        $this->loadModel('UsersAudio');
                        $user_file_entity = $this->UsersImages->newEntity();
                        $user_file_entity->user_id = $user_id;
                        $user_file_entity->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";
                        $user_file_entity->user_image = !empty($this->request->session()->read('GuestUser.image_name'))?$this->request->session()->read('GuestUser.image_name'):"";                        
                        $save_user_image = $this->UsersImages->save($user_file_entity); 
                        $user_image_id = $save_user_image->id;
                        $this->request->session()->write("GuestUser.user_image_id",$user_image_id);
                        $user_audio_entity = $this->UsersAudio->newEntity();
                        $user_audio_entity->user_id = $user_id;
                        $user_audio_entity->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";                       
                        $user_audio_entity->user_audio = !empty($this->request->session()->read('GuestUser.audio_name'))?$this->request->session()->read('GuestUser.audio_name'):"";
                        $save_audio = $this->UsersAudio->save($user_audio_entity);
                        $audio_id = $save_audio->id;
                        $this->request->session()->write("GuestUser.audio_id",$audio_id);
                        $authUser = $this->Users->get($user_id)->toArray();                         
                        $auth = $this->Auth->setUser($authUser); 

                        $this->loadModel("Templates");
                        $get_price = $this->Templates->find()->where(['id' => $this->request->session()->read('GuestUser.template_id')])->first();

                        $this->loadModel("Orders");
                        $Orders_table = $this->Orders->newEntity();
                        $Orders_table->user_id = $this->Auth->User("id");
                        $Orders_table->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";
                        $Orders_table->user_image_id = $user_image_id;
                        $Orders_table->user_audio_id = $audio_id;
                        $Orders_table->price = $get_price['price'];
                        $Orders_table->created_on = date("Y-m-d H:i:s");
                        $save_order = $this->Orders->save($Orders_table);
                        $order_id = $save_order->id;
                        $this->request->session()->write("GuestUser.order_id",$order_id);

                        $this->request->session()->write('GuestUser.page_number',6);

                       $from_email = Configure::read('FROM_EMAIL');
               $project_name = Configure::read('project_name');
               $url = Configure::read('SITEURL');
               $maildata = array();
               
               $mail_content ="";
               $mail_content.='<p>Hi '.$this->request->data['first_name'].',</P>
                        
                <p>Thank you for your registering at Lizicards.</P> 
                <p>Send a special message to your friends and loved ones with Lizicards greeting cards.</p> 
                <p>If you have any questions, please email us '.$from_email.'.</p>    
                        <p>Many thanks, <br/> Lizicards Team</p>
                        
                      </td>                  
              </tr>
              
                <tr>
                
                
                  <td align="center">   <a href='.$url.' style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Send a greeting card </a> </td>
                  
                
                  </tr>';  
               $maildata['name'] = $this->request->data['first_name']." ".$this->request->data['last_name'];
               $maildata['email'] = $this->request->data['email']; 
               $maildata['heading'] = "Lizicards Registration.";   
               $maildata['url'] = $url;  
               $maildata['data'] = $mail_content;
	       $maildata['subject'] = $project_name.': Registration';
		
		$this->Common->send_mail($maildata);

               /*$email = new Email();
               $mail = $email->template('allmail')
                       ->emailFormat('html')
                       ->to($maildata['email'])
                       ->from([$from_email => $project_name])
                       ->subject($project_name.' :Registration')
                       ->viewVars($maildata)
                       ->send();  */
                         $this->request->data['action'] = "register";
                       $returnArrayBody = $this->Common->sendAPIRequest("rest_users.json", "post", $this->request->data);    
                            $result['status'] = "success";
                        $result['msg'] = "Users registered successfully.";
                        echo json_encode($result);

                   

                    }
                    else{
                        throw new Exception("Error Processing Request", 1);
                        
                    }

                }
            }

            if($this->request->data['type']==2){
               $this->request->data['action'] = "guestregister";
                 $this->loadModel("Templates");
                        $get_price = $this->Templates->find()->where(['id' => $this->request->session()->read('GuestUser.template_id')])->first();
                if (empty($this->request->data['email']) || !filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                    throw new Exception("Please enter valid email");
                }
                $check_user_email = $this->Users->find("all", array("conditions" => array("Users.email" => $this->request->data['email'],"Users.status <= " => 1)))->first();
                if(count($check_user_email) > 0){
                   $dat = $this->Users->get($check_user_email['id']); 
                   $this->request->data["modified_on"] = date("Y-m-d H:i:s");                
                   $store_data = $this->Users->patchEntity($dat,$this->request->data);
                   $update = $this->Users->save($store_data);
                   if($update){                    
                    $authUser = $this->Users->get($check_user_email['id'])->toArray();
                    $auth = $this->Auth->setUser($authUser);   
                 $this->loadModel('UsersImages');
                $this->loadModel('UsersAudio');
                $user_file_entity = $this->UsersImages->newEntity();
                $user_file_entity->user_id = $this->Auth->User("id");
                $user_file_entity->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";
                $user_file_entity->user_image = !empty($this->request->session()->read('GuestUser.image_name'))?$this->request->session()->read('GuestUser.image_name'):"";                        
                $save_user_image = $this->UsersImages->save($user_file_entity); 
                $user_image_id = $save_user_image->id;
                $this->request->session()->write("GuestUser.user_image_id",$user_image_id);
                $user_audio_entity = $this->UsersAudio->newEntity();
                $user_audio_entity->user_id = $this->Auth->User("id");
                $user_audio_entity->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";                       
                $user_audio_entity->user_audio = !empty($this->request->session()->read('GuestUser.audio_name'))?$this->request->session()->read('GuestUser.audio_name'):"";
                $save_audio = $this->UsersAudio->save($user_audio_entity);
                $audio_id = $save_audio->id;
                $this->request->session()->write("GuestUser.audio_id",$audio_id);
                $this->loadModel("Orders");
                $Orders_table = $this->Orders->newEntity();
                $Orders_table->user_id = $this->Auth->User("id");
                $Orders_table->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";
                $Orders_table->user_image_id = $user_image_id;
                $Orders_table->user_audio_id = $audio_id;
                $Orders_table->price = $get_price['price'];
                $Orders_table->created_on = date("Y-m-d H:i:s");
                $save_order = $this->Orders->save($Orders_table);
                $order_id = $save_order->id;
                $this->request->session()->write("GuestUser.order_id",$order_id); 
                $this->request->session()->write('GuestUser.page_number',6);

                 
                $returnArrayBody = $this->Common->sendAPIRequest("rest_users.json", "post", $this->request->data);
                    $result['status'] = "success";
                    $result['msg'] = "You are already registered as a normal user.";
                    echo json_encode($result);
                }else{
                  throw new Exception("Something went wrong. Please try again later");  
              }
                    // throw new Exception("Email already exists");
          }
          else{
            $this->loadModel("Templates");
                        $get_price = $this->Templates->find()->where(['id' => $this->request->session()->read('GuestUser.template_id')])->first();
            $hashPswdObj = new DefaultPasswordHasher;
            $password = uniqid();
            // $authenticate_password = $hashPswdObj->hash($password);
            $this->request->data['password'] = $password;
            $this->request->data["created_on"] = date("Y-m-d H:i:s");
            $this->request->data["modified_on"] = date("Y-m-d H:i:s");
            $store_data = $this->Users->newEntity($this->request->data);
            if ($this->Users->save($store_data)) {
                $returnArrayBody = $this->Common->sendAPIRequest("rest_users.json", "post", $this->request->data);
                $user_id = $store_data->id;
                $authUser = $this->Users->get($user_id)->toArray();                         
                $auth = $this->Auth->setUser($authUser); 
                $this->loadModel('UsersImages');
                $this->loadModel('UsersAudio');
                $user_file_entity = $this->UsersImages->newEntity();
                $user_file_entity->user_id = $user_id;
                $user_file_entity->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";
                $user_file_entity->user_image = !empty($this->request->session()->read('GuestUser.image_name'))?$this->request->session()->read('GuestUser.image_name'):"";                        
                $save_user_image = $this->UsersImages->save($user_file_entity); 
                $user_image_id = $save_user_image->id;
                $this->request->session()->write("GuestUser.user_image_id",$user_image_id);
                $user_audio_entity = $this->UsersAudio->newEntity();
                $user_audio_entity->user_id = $user_id;
                $user_audio_entity->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";                       
                $user_audio_entity->user_audio = !empty($this->request->session()->read('GuestUser.audio_name'))?$this->request->session()->read('GuestUser.audio_name'):"";
                $save_audio = $this->UsersAudio->save($user_audio_entity);
                $audio_id = $save_audio->id;
                $this->request->session()->write("GuestUser.audio_id",$audio_id);

                $this->loadModel("Orders");
                $Orders_table = $this->Orders->newEntity();
                $Orders_table->user_id = $this->Auth->User("id");
                $Orders_table->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";
                $Orders_table->user_image_id = $user_image_id;
                $Orders_table->user_audio_id =  $audio_id;
                $Orders_table->price =  $get_price['price'];

                $Orders_table->created_on = date("Y-m-d H:i:s");
                $save_order = $this->Orders->save($Orders_table);
                $order_id = $save_order->id;
                $this->request->session()->write("GuestUser.order_id",$order_id);  
                $this->request->session()->write('GuestUser.page_number',6);
               $from_email = Configure::read('FROM_EMAIL');
               $project_name = Configure::read('project_name');
               $url = Configure::read('SITEURL');
               $maildata = array();
               $mail_content="";

             
               $mail_content.= '<p>Hi '.$this->request->data['first_name'].',</P>
                        
               <p>Thank you for your registering at Lizicards as a guest.</P> 
                
               <p>Send a special message to your friends and loved ones with Lizicards greeting cards.</p> 
                 <p>If you have any questions, please email us '.$from_email.'.</p>  
                <p>Many thanks, <br/> Lizicards Team</p>
                
                      </td>
                  
              </tr>
              
                <tr>
                
                
                  <td align="center">   <a href="'.$url.'" style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Send a greeting card </a> </td>
                  
                
                  </tr>';
               $maildata['name'] = $this->request->data['first_name']." ".$this->request->data['last_name'];
               $maildata['email'] = $this->request->data['email'];
               $maildata['password'] = $password;   
               $maildata['url'] = $url;  
               $maildata['data'] = $mail_content;
               $maildata['heading'] = "Lizicards Registration.";  
               $maildata['subject'] = $project_name.': Registration';
        
                 $this->Common->send_mail($maildata);

               /*$email = new Email();
               $mail = $email->template('allmail')
                       ->emailFormat('html')
                       ->to($maildata['email'])
                       ->from([$from_email => $project_name])
                       ->subject($project_name.': Registration')
                       ->viewVars($maildata)
                       ->send();   */                
                $result['status'] = "success";
                $result['msg'] = "Users registered successfully.";
                echo json_encode($result);
            } else {
                throw new Exception("Something went wrong. Please try again later");
            }

        }
                     
    }


} catch (Exception $ex) {
    $result['status'] = "error";
    $result["msg"] = $ex->getMessage();
    echo json_encode($result);
}
exit();
}


      /*
     * function to login with facebook
     */

      public function facebookLogin() {
        $this->autoRender = false;
        try {
            // pr($this->request->data); die;
            $this->request->data['action'] = "facebook_register";
             
             $this->loadModel("Templates");
                        $get_price = $this->Templates->find()->where(['id' => $this->request->session()->read('GuestUser.template_id')])->first();
            if (!empty($this->request->data['userdata'])) {
                $this->loadModel("Users");
                $check_user_email = $this->Users->find("all", array("conditions" => array("Users.email" => $this->request->data['userdata']['email'])))->first();

               if(count($check_user_email) > 0){
                   $dat = $this->Users->get($check_user_email['id']); 
                   $this->request->data['first_name'] = $this->request->data['userdata']['first_name'];
                   $this->request->data['last_name'] = $this->request->data['userdata']['last_name'];
                   $this->request->data['email'] = $this->request->data['userdata']['email']; 
                $this->request->data['fb_id'] = $this->request->data['userdata']['id'];  
                $this->request->data["modified_on"] = date("Y-m-d H:i:s");                            
                   $store_data = $this->Users->patchEntity($dat,$this->request->data);
                   $update = $this->Users->save($store_data);

                   if($update){                    
                    $authUser = $this->Users->get($check_user_email['id'])->toArray();
                    $auth = $this->Auth->setUser($authUser);  

                $this->loadModel('UsersImages');
                $this->loadModel('UsersAudio');
                $user_file_entity = $this->UsersImages->newEntity();
                $user_file_entity->user_id = $this->Auth->User("id");
                $user_file_entity->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";
                $user_file_entity->user_image = !empty($this->request->session()->read('GuestUser.image_name'))?$this->request->session()->read('GuestUser.image_name'):"";                        
                $save_user_image = $this->UsersImages->save($user_file_entity); 
                $user_image_id = $save_user_image->id;
                $this->request->session()->write("GuestUser.user_image_id",$user_image_id);
                $user_audio_entity = $this->UsersAudio->newEntity();
                $user_audio_entity->user_id = $this->Auth->User("id");
                $user_audio_entity->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";                       
                $user_audio_entity->user_audio = !empty($this->request->session()->read('GuestUser.audio_name'))?$this->request->session()->read('GuestUser.audio_name'):"";
                $save_audio = $this->UsersAudio->save($user_audio_entity);
                $audio_id = $save_audio->id;
                $this->request->session()->write("GuestUser.audio_id",$audio_id);
                $this->loadModel("Orders");
                $Orders_table = $this->Orders->newEntity();
                $Orders_table->user_id = $this->Auth->User("id");
                $Orders_table->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";
                $Orders_table->user_image_id = $user_image_id;
                $Orders_table->user_audio_id = $audio_id;
                $Orders_table->price = $get_price['price'];
                $Orders_table->created_on = date("Y-m-d H:i:s");
                $save_order = $this->Orders->save($Orders_table);
                $order_id = $save_order->id;
                $this->request->session()->write("GuestUser.order_id",$order_id);   
                $this->request->session()->write('GuestUser.page_number',6);
                    $result['status'] = "success";
                    $result['msg'] = "Registered Successfully.";
                    echo json_encode($result);
                }else{
                  throw new Exception("Something went wrong. Please try again later");  
              }
                    // throw new Exception("Email already exists");
          } else {

                    
//                if ($mail) {
                    $this->request->data['first_name'] = $this->request->data['userdata']['first_name'];
                   $this->request->data['last_name'] = $this->request->data['userdata']['last_name'];
                   $this->request->data['email'] = $this->request->data['userdata']['email'];
                 $this->request->data['fb_id'] = $this->request->data['userdata']['id'];
                     $hashPswdObj = new DefaultPasswordHasher;
                    $password = uniqid();
                    // $authenticate_password = $hashPswdObj->hash($password);
                     $this->request->data['password'] = $password;
                    $this->request->data['type'] = 3;
                    $this->request->data['status'] = "1";
                    $this->request->data["created_on"] = date("Y-m-d H:i:s");
                    $this->request->data["modified_on"] = date("Y-m-d H:i:s");
                    $newEntity = $this->Users->newEntity($this->request->data);
                    $insert_user = $this->Users->save($newEntity);
                    $returnArrayBody = $this->Common->sendAPIRequest("rest_users.json", "post", $this->request->data);
                    if ($insert_user) {
                        $user_id = $insert_user->id;

                        $authUser = $this->Users->get($user_id)->toArray();
                         $auth = $this->Auth->setUser($authUser);  

                $this->loadModel('UsersImages');
                $this->loadModel('UsersAudio');
                $user_file_entity = $this->UsersImages->newEntity();
                $user_file_entity->user_id = $this->Auth->User("id");
                $user_file_entity->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";
                $user_file_entity->user_image = !empty($this->request->session()->read('GuestUser.image_name'))?$this->request->session()->read('GuestUser.image_name'):"";                        
                $save_user_image = $this->UsersImages->save($user_file_entity); 
                $user_image_id = $save_user_image->id;
                $this->request->session()->write("GuestUser.user_image_id",$user_image_id);
                $user_audio_entity = $this->UsersAudio->newEntity();
                $user_audio_entity->user_id = $this->Auth->User("id");
                $user_audio_entity->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";                       
                $user_audio_entity->user_audio = !empty($this->request->session()->read('GuestUser.audio_name'))?$this->request->session()->read('GuestUser.audio_name'):"";
                $save_audio = $this->UsersAudio->save($user_audio_entity);
                $audio_id = $save_audio->id;
                $this->request->session()->write("GuestUser.audio_id",$audio_id);
                $this->loadModel("Orders");
                $Orders_table = $this->Orders->newEntity();
                $Orders_table->user_id = $this->Auth->User("id");
                $Orders_table->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";
                $Orders_table->user_image_id = $user_image_id;
                $Orders_table->user_audio_id = $audio_id;
                 $Orders_table->price = $get_price['price'];
                $Orders_table->created_on = date("Y-m-d H:i:s");
                $save_order = $this->Orders->save($Orders_table);
                $order_id = $save_order->id;
                $this->request->session()->write("GuestUser.order_id",$order_id);  
                $this->request->session()->write('GuestUser.page_number',6);
               $from_email = Configure::read('FROM_EMAIL');
               $project_name = Configure::read('project_name');
               $url = Configure::read('SITEURL');
               $maildata = array();
               $mail_content="";               
               $mail_content.='<p>Hi '.$this->request->data['userdata']['first_name'].',</P>
                        
                <p>Thank you for your registering at Lizicards</P>
                <p>These are the login credentials.</p>

                <p>Username: <b>'.$this->request->data['userdata']['email'].'</b></p>
                <p>Password: <b>'.$password.'</b></p>

               <p>Send a special message to your friends and loved ones with Lizicards greeting cards.</p> 
                  <p>If you have any questions, please email us '.$from_email.'.</p> 
               <p>Many thanks, <br/> Lizicards Team</p>
                
                      </td>
                  
              </tr>
              
                <tr>
                
                
                  <td align="center">   <a href="'.$url.'" style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Send a greeting card </a> </td>
                  
                
                  </tr>';
               $maildata['name'] = $this->request->data['userdata']['first_name']." ".$this->request->data['userdata']['last_name'];
               $maildata['email'] = $this->request->data['userdata']['email'];
               $maildata['password'] = $password;   
               $maildata['url'] = $url;  
               $maildata['data'] = $mail_content;
               $maildata['heading'] = "Lizicards Registration."; 
                $maildata['subject'] = $project_name.': Registration';
        
               $this->Common->send_mail($maildata);    
              /* $email = new Email();
               $mail = $email->template('allmail')
                       ->emailFormat('html')
                       ->to($maildata['email'])
                       ->from([$from_email => $project_name])
                       ->subject($project_name.':Successfully Register')
                       ->viewVars($maildata)
                       ->send();*/
                        $result['status'] = "success";
                        $result['error'] = "Register successfully.";
                        echo json_encode($result);
                    } else {
                        throw new Exception("Something went wrong.Please try again later");
                    }
               // } else {
               //     $result['status'] = "error";
               //     $result['error'] = "Unable to send email.Please try again later.";
               // }
                }
                 
            }
            $returnArrayBody = $this->Common->sendAPIRequest("rest_users.json", "post", $this->request->data);
        } catch (Exception $ex) {
            $result['status'] = "error";
            $result['msg'] = $ex->getMessage();
            echo json_encode($result);
        }
    }

    public function address(){

       if(empty($this->Auth->user("id"))){
        $this->request->session()->write("GuestUser.page_number",5);
         exit();
       }
        // $this->request->session()->write("GuestUser.template_id",$this->request->data['template_id']);        
        if(!empty($this->request->data['send']) && $this->request->data['send'] == "to_me"){   
            $this->request->session()->write("card_data",$this->request->data);
            $this->request->session()->write("type",$this->request->data['send']);
        }

        if(!empty($this->request->data['send']) && $this->request->data['send'] == "to_receipent"){   
            $this->request->session()->write("card_data",$this->request->data);
            $this->request->session()->write("type",$this->request->data['send']);
        }
        // pr($this->request->session()->read());
        $this->request->session()->write('GuestUser.page_number',8);

            
        
             $this->loadModel("Orders");
  
            $template_data = $this->Orders->find('all')->contain(['Templates','UsersAudio','UsersImages'])->where(['Orders.template_id' => $this->request->session()->read('GuestUser.template_id'),'Orders.user_id' => $this->Auth->user("id"),'Orders.id' => $this->request->session()->read("GuestUser.order_id")])->hydrate(false)->first();
           
            $this->loadModel("TemplatesImages");

            $template_images = $this->TemplatesImages->find()->contain(['Templates'])->where(['TemplatesImages.status' => 1,'TemplatesImages.show_preview' => 1,'TemplatesImages.template_id' => $template_data['template_id']])->hydrate(false)->first();
            $preview_image = $template_images['images'];

            $this->set(compact('template_data','preview_image'));
           
            
       
        
    }

    public function addMoreReceipent(){

        $countries = TableRegistry::get("countries");       
        $country_list = $countries->find("list", ['keyField' => 'id','valueField' => 'name'])->toArray();

        $state = TableRegistry::get("states");
        // $state_list = $state->find("list", ['keyField' => 'id','valueField' => 'name'])->where(['country_id' => 231])->order('name')->toArray();
        $state_list = array();
            $get_states = $state->find("all")->where(['country_id' => 231])->order('name')->toArray();
            foreach ($get_states as $stlist):
                $id = $stlist['id'];
                $title = $stlist["state_abbrivation"]." - ". $stlist['name'];
                $state_list[$id] = $title;
            endforeach;

        $city = TableRegistry::get("cities");
        $city_list = $city->find("list", ['keyField' => 'id','valueField' => 'name'])->where(['state_id' => 3919])->order('name')->toArray();
        // pr($state_list); die;
        $this->set('count',$this->request->data['count']);
        $this->set(compact('country_list','state_list','city_list'));
    }

    public function checkout(){
        if(empty($this->Auth->user("id"))){
        $this->request->session()->write("GuestUser.page_number",5);
        exit();
       }
        $this->request->session()->write('GuestUser.page_number',9);

         $this->loadModel("Orders");
  
            $template_data = $this->Orders->find('all')->contain(['Templates','UsersAudio','UsersImages'])->where(['Orders.template_id' => $this->request->session()->read('GuestUser.template_id'),'Orders.user_id' => $this->Auth->user("id"),'Orders.id' => $this->request->session()->read("GuestUser.order_id")])->hydrate(false)->first();
            
            $this->loadModel("TemplatesImages");

            $template_images = $this->TemplatesImages->find()->contain(['Templates'])->where(['TemplatesImages.status' => 1,'TemplatesImages.show_preview' => 1,'TemplatesImages.template_id' => $template_data['template_id']])->hydrate(false)->first();
            $preview_image = $template_images['images'];

            $this->set(compact('template_data','preview_image'));
    }

     public function payment(){
        $SITEURL = Configure::read('SITEURL');
            $user_id = $this->Auth->user('id');
            //$paypal_email = 'merchant@lizicards.com';
            $paypal_email = Configure::read('Paypal_MerChant_Email');
            $return_url = $SITEURL."users/success";
            $cancel_url = $SITEURL;
            $notify_url = $SITEURL."users/success"; 

            if (!isset($_POST["txn_id"]) && !isset($_POST["txn_type"])) {  
                // pr($this->)
                // Firstly Append paypal account to querystring               
                // Redirect to paypal IPN
                $querystring = "?business=".urlencode($paypal_email)."&";
                $querystring .= "cmd=".urlencode('_xclick')."&";
                $querystring .= "item_name=".urlencode($this->request->data['item_name'])."&";
                $querystring .= "item_number=".urlencode($this->request->data['item_number'])."&";
                $querystring .= "quantity=".urlencode($this->request->data['quantity'])."&";
                $querystring .= "shipping=".urlencode($this->request->data['shipping'])."&";
                $querystring .= "tax=".urlencode($this->request->data['tax'])."&";
                $querystring .= "amount=".urlencode($this->request->data['amount'])."&";
                $querystring .= "no_shipping=".urlencode($this->request->data['no_shipping'])."&";
                $querystring .= "currency_code=".urlencode('USD')."&";
                $querystring .= "_method=".urlencode('POST')."&";                
                $querystring .= "custom=".urlencode($user_id)."&";
                $querystring .= "return=".urlencode($return_url)."&";
                $querystring .= "cancel_return=".urlencode($cancel_url)."&";
                $querystring .= "notify_url=" . urlencode($notify_url);              
                
                 header('location:https://www.paypal.com/cgi-bin/webscr' . $querystring);
                exit();
            }
    }

    public function success($share=null){ 	
   
    // if(empty($this->request->session()->read("GuestUser.template_id"))){
    //     return $this->redirect("/");
    //     exit();
    // }     
        $this->viewBuilder()->layout("thankyou_page");

     $quantity = 0;
    $shipping_cost = 0; 
    $sales_tax = 0;
    $total = 0;
    $grand_total=0;
    $master_shipping_type = '';
    $sal_tax = Configure::read('Sales_tax');     
    $this->loadModel("Orders");
   $check_order = $this->Orders->find()->where(['id' => $_REQUEST['item_number'],"transaction_id" => $_REQUEST['tx']])->first();
   $shiping_table = TableRegistry::get("shipping_address");
   $check_address_exists = $shiping_table->find()->where(['order_id' => $check_order['id']])->count();
    

        $this->loadModel("Templates");
       $date = date("Y-m-d H:i:s");
        $previous_order_status = $this->Orders->find()->where(['id' => $_REQUEST['item_number'],'user_id' => $this->Auth->user("id"),'status' => 'pending','transaction_id !=' => $_REQUEST['tx']])->count();
        if($previous_order_status>0){
             $get_price = $this->Orders->find()->where(['id' => $this->request->session()->read("GuestUser.order_id")])->first();
                $template_name = $this->Templates->find()->where(['id' => $get_price['template_id']])->first();
             require_once(ROOT . DS . 'vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php');
                      $dompdf = new DOMPDF();
                        $SITEURL = Configure::read('SITEURL');
                        $project_name = Configure::read('project_name');
                        $support_email = Configure::read('Support');
                        $FROM_EMAIL = Configure::read('FROM_EMAIL');
                        $AdminEmail = Configure::read('AdminEmail');
                         $downloaded_path = $SITEURL . 'img' . DS . 'orderpdf' . DS . 'pdf' . time() . ".pdf";
                        $folder_path = WWW_ROOT . 'img' . DS . 'orderpdf' . DS . 'pdf' . time() . ".pdf";
                        $logo = WWW_ROOT . 'img' . DS . 'logo.png';
                        $day = date("D");
                        $d_m_y = date("M j, Y");
                        $sent_time = $day . ", " . $d_m_y;

                     
        $shipped_to = "";
    if(!empty($this->request->session()->read("card_data.send")) && $this->request->session()->read("card_data.send") == "to_me"){
      $quantity = $this->request->session()->read("card_data.quantity");
      $total = $get_price['price']*$quantity;
        $shipped_to = "To Me";        
        $state = TableRegistry::get("states");
        $countries = TableRegistry::get("countries");
        $get_state = $state->find()->where(['id' => $this->request->session()->read("card_data.state_id")])->first();
         if($get_state['country_id']==231){
                $state_name = $get_state['state_abbrivation'];
              }else{
                $state_name = $get_state['name'];
              }
        $get_countries = $countries->find()->where(['id' => $this->request->session()->read("card_data.country_id")])->first();
       if($this->request->session()->read("card_data.country_id")!=231){
                $shipping_cost+=5;
                $type = "outside_us_shipping";
               }else{
                $shipping_cost = $this->request->session()->read("card_data.shipping");
                if($this->request->session()->read("card_data.state_id")==3943){
                 $add_shipping = $total+$shipping_cost; 
                $sales_tax = $add_shipping*$sal_tax/100;                
               }  
               }

         if($this->request->session()->read("card_data.shipping")==1){
          $type = "us_shipping";
         }if($this->request->session()->read("card_data.shipping")==3){
         $type = "us_expedited_shipping";
         }
         

      $master_shipping_type =  $type;                   
               
      if($check_address_exists==0){         
      
      
      $ship_address = $shiping_table->newEntity();
      $ship_address->user_id = $this->Auth->user("id");
      $ship_address->order_id = $this->request->session()->read("GuestUser.order_id");
      $ship_address->first_name = $this->request->session()->read("card_data.first_name");
      $ship_address->last_name = $this->request->session()->read("card_data.last_name");
      $ship_address->address_one = $this->request->session()->read("card_data.address_one");
      
      $ship_address->address_two = $this->request->session()->read("card_data.address_two");
      $ship_address->city = $this->request->session()->read("card_data.city");
      $ship_address->state_id = $this->request->session()->read("card_data.state_id");
      $ship_address->sale_tax = number_format($sales_tax,2);
      $ship_address->zip = $this->request->session()->read("card_data.zip");
      $ship_address->country = $this->request->session()->read("card_data.country_id");
      $ship_address->shipping_type = $type;
      $ship_address->created_on = date("Y-m-d H:i:s");
      $ship_address->modified_on = date("Y-m-d H:i:s");
      $shiping_table->save($ship_address);
      
    }
      

    }
    // pr($this->request->session()->read()); die;   
   if(!empty($this->request->session()->read("card_data.send")) && $this->request->session()->read("card_data.send") == "to_receipent"){
     $shipped_to = "To Recipient";
      $quantity =  count($this->request->session()->read("card_data.data"));     
      $get_indiviual_state_quantity =0;
      $get_quantity_for_saleTax=0; 
      $calculate_ship_cost =0;    
      $master_shipping_flag_array = []; 
      foreach ($this->request->session()->read("card_data.data") as $key => $value) {
        // pr($value);
        if(!empty($value['shipping']) && $value['shipping']==1){
          $type = "us_shipping";
         }
         if(!empty($value['shipping']) && $value['shipping']==3){
          $type = "us_expedited_shipping";
         }    
        if($value['country_id']!=231){
                $shipping_cost+=5;
                $type = "outside_us_shipping";
               }else{
                $shipping_cost += $value['shipping'];
                if($value['state_id']==3943){
                $calculate_ship_cost+= $value['shipping'];  
                $get_indiviual_state_quantity = 1;           
                $get_quantity_for_saleTax+=1;
             
               $get_total = $get_price['price']*$get_quantity_for_saleTax+$calculate_ship_cost;

               $get_individual_total = $get_price['price']*$get_indiviual_state_quantity;
               $get_individual_sales_tax = $get_individual_total*$sal_tax/100;
                 $sales_tax = $get_total*$sal_tax/100;               
               
               }else{
                $get_individual_sales_tax = "0.00";
                  }
               }
          
          if(!in_array($type,$master_shipping_flag_array)) {
            $master_shipping_flag_array[] = $type ;
          }
        $shiping_table = TableRegistry::get("shipping_address");
         $check_address_exists = $shiping_table->find()->where(['order_id' => $check_order['id']])->count();  

        if($check_address_exists != count($this->request->session()->read("card_data.data")) && !empty($this->request->session()->read("GuestUser.order_id"))){
      $ship_address = $shiping_table->newEntity();
      $ship_address->user_id = $this->Auth->user("id");
      $ship_address->order_id = $this->request->session()->read("GuestUser.order_id");
      $ship_address->first_name = $value["first_name"];
      $ship_address->last_name = $value["last_name"];
      $ship_address->address_one = $value["address_one"]; 
      
      $ship_address->address_two =  $value["address_two"]; 
      $ship_address->city = $value["city"]; 
      $ship_address->state_id = !empty($value["state_id"])?$value["state_id"]:""; 
      $ship_address->sale_tax = number_format($get_individual_sales_tax,2); 
      $ship_address->zip = $value["zip"]; 
      $ship_address->country = $value["country_id"];
      $ship_address->shipping_type = $type; 
      $ship_address->created_on = date("Y-m-d H:i:s");
      $ship_address->modified_on = date("Y-m-d H:i:s");
      $shiping_table->save($ship_address);    
         }  
      } 

    if(count($master_shipping_flag_array)>1) {
      $master_shipping_type  = 'mixed';

    }else if(count($master_shipping_flag_array)==1) {
      $master_shipping_type = $master_shipping_flag_array[0];

    }else {
      $master_shipping_type = '';
    }

  }
  


  
   $update_data = $this->Orders->query();
          $res = $update_data->update()
          ->set(['quantity' => $quantity,'shipping_cost' => $shipping_cost,'master_shipping_type' => $master_shipping_type,'total_sales_tax' => number_format($sales_tax,2),'total_amount' => $_REQUEST['amt'],'send_to' => $this->request->session()->read('card_data.send'),'transaction_id' => $_REQUEST['tx'],'payment_from' => 'paypal','invoice' => $downloaded_path,'status' => $_REQUEST['st'],"modified_on" => $date])
           ->where(['id' => $_REQUEST['item_number'],'user_id' => $this->Auth->user("id")])->execute();
  
   $total= $get_price['price']*$quantity;        
  $grand_total += $total+$shipping_cost+$sales_tax;

                                $html = '<html>
    <head></head>

    <table align="center" style="text-align: center; font-size:12px;" width="100%" border="0">
        <tr align="left">

            <td align="left" style="font-size:10px;">
                ' . date("n/j/Y") . '
            </td>
            <td colspan="4" align="left" style="font-size:10px;">
                From - ' . $FROM_EMAIL . '
            </td>
        </tr>
        <tr align="left">  <td><img align="left" src=' . $logo . ' width="80"></td>
            <td align="right" colspan="4" style="font-weight:bold;">&lt;' . $this->Auth->user('email') . '&gt;</td>
        </tr>
        <tr>
            <td  colspan="5" style="color:lightgrey;">
                <hr>
            </td>
        </tr>
        <tr align="left">
            <td  colspan="4">
                Subject: <b>Order Detail</b>
            </td>
        </tr>
        <tr align="left">
            <td  colspan="4">
                
            </td>
        </tr>
         <tr>
            <td  colspan="5" style="color:lightgrey;">
                <hr>
            </td>
        </tr>
          <tr align="left">
            <td  colspan="2">
                From: <b>'.$project_name.'</b> &lt;' . $FROM_EMAIL . '&gt;
            </td>
            
             <td colspan="3" align="right">
             ' . $sent_time . '
                
            </td>
        </tr>
          <tr align="left">
            <td  colspan="4">
                To: ' . $this->Auth->user('email') . '
            </td>
        </tr>';
                                $html .='<tr align="left">
            <td  colspan="4">
              Shipped '.@$shipped_to.'  
            </td>
        </tr>
         <tr>
            <td  colspan="4">
                
            </td>
        </tr>
         <tr>
            <td  colspan="4">
                
            </td>
        </tr>
                
        
        <tr>
            <td  colspan="4">
                
            </td>
        </tr>
         <tr>
            <td  colspan="4">
                
            </td>
        </tr>
         <tr>
            <td  colspan="4">
                
            </td>
        </tr>
        <tr align="left">
            <td  colspan="4">
                
            </td>
            <td align="left" colspan="4">
            </td>
        </tr>

    </table>';

if(!empty($this->request->session()->read("card_data.send")) && $this->request->session()->read("card_data.send") == "to_me"){
    $html.='<div style="width:30%; float:left; margin:10px 0px;border:1px solid grey"">
  <h4 style="font-weight:bold;font-size:12px;background-color:lightgrey;">Address</h4>
  <p style="font-size:10px;">'.$this->request->session()->read("card_data.first_name")." ".$this->request->session()->read("card_data.last_name").'</p>
  <p style="font-size:10px;">'.$this->request->session()->read("card_data.address_one")." ".@$this->request->session()->read("card_data.address_two")." ".$this->request->session()->read("card_data.city").'</p>
  <p style="font-size:10px;">'.$state_name.", ".$this->request->session()->read("card_data.zip").", ".@$get_countries['name'].'</p>
</div>';
}
if(!empty($this->request->session()->read("card_data.send")) && $this->request->session()->read("card_data.send") == "to_receipent"){
    $i=1;
    $html.='<div style="width:100%;float:left;">';
    foreach ($this->request->session()->read("card_data.data") as $ship_adr) {
        $state = TableRegistry::get("states");
        $countries = TableRegistry::get("countries");        
        $get_countries = $countries->find()->where(['id' => $ship_adr['country_id']])->first();

    $html.='<div style="width:30%; float:left; margin:10px 0px;border:1px solid grey"">
  <h4 style="font-weight:bold;font-size:12px;background-color:lightgrey;">Address'.$i.'</h4>
  <p style="font-size:10px;">'.$ship_adr['first_name']." ".$ship_adr['last_name'].'</p>
  <p style="font-size:10px;">'.$ship_adr['address_one']." ".@$ship_adr['address_two']." ".$ship_adr['city'].'</p>
  <p style="font-size:10px;">';
      if(!empty($ship_adr['state_id'])){
        $get_state = $state->find()->where(['id' => $ship_adr['state_id']])->first();
         if($get_state['country_id']==231){
                $state_name = $get_state['state_abbrivation'];
              }else{
                $state_name = $get_state['name'];
              }
        $html .= @$state_name.", ";
         }
         $html.= @$ship_adr['zip'].', '.$get_countries['name'].'</p>
</div>';
$i++;}
$html.='</div>';
}

$html.='<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px; border:1px solid #f1f1f1; padding:5px;">
  <tr>
    
    <td valign="top" style="text-align:left; padding-left:8px; font-weight:500; height:25px;">'.$template_name['name'].'</td>
    <td valign="top" style="text-align:right; padding-left:8px; font-weight:500">$'.$get_price['price'].'</td>
  </tr>
  <tr>
    <td valign="top" style="text-align:left; padding-left:8px; color:#333;">Quantity</td>
    <td valign="top" style="text-align:right; padding-left:8px; color:#333;">'.$quantity.'</td>
  </tr>
</table>';

   $html.='<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px; text-align:right; color:#9f9f9f;">
  <tr>
    <td style="height:20px;">Items total:</td>
    <td>$'.number_format($total,2).'</td>
  </tr>
  <tr>
    <td style="height:20px;">Shipping Fee:</td>
    <td>$'.number_format($shipping_cost,2).'</td>
  </tr>
  <tr>
    <td style="height:20px;">Sales Tax:</td>
    <td>$'.number_format($sales_tax,2).'</td>
  </tr>

</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px; text-align:right; color:#9f9f9f; border-top:1px solid #f2f2f2; border-bottom:1px solid #f2f2f2;">

 <tr>
    <td style="color:#333;">Order Total:</td>
    <td style="color:#333;">$'.number_format($grand_total,2).'</td>
  </tr>
  </table>';
                  

$html.='</html>
';


                                $dompdf->load_html($html);
                                $dompdf->render();

                                $output = $dompdf->output();
                                file_put_contents($folder_path, $output);

                      $maildata = array();
                $url = $SITEURL;
               $mail_content ="";
               $mail_content.='<p>Hi '.$this->Auth->user('first_name').',</P>
                        
                <p>Thank you for shopping at Lizicards. We&#39;ll soon be mailing your greeting card(s).</P> 

                <p>Please find attached the receipt.</p> 
                <p>If you have any questions, please email us: '.$FROM_EMAIL.'.</p> 

                <p>Many thanks, <br/> Lizicards Team</p>             
                        
                      </td>                  
              </tr>
              
                <tr>
                
                
                  <td align="center">   <a href='.$url.' style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Send a Greeting Card </a> </td>
                  
                
                  </tr>';  
               $maildata['email'] = $this->Auth->user("email");
               $maildata['from_email'] = $FROM_EMAIL;
               $maildata['support_email'] = $support_email;
               $maildata['heading'] = "Lizicards: Order Confirmation";   
               $maildata['url'] = $url;  
               $maildata['data'] = $mail_content; 
               $maildata['subject'] = $project_name.': Order Confirmation'; 
               $maildata['invoice'] =  $folder_path;  
	       $maildata['transaction_id'] = $_REQUEST['tx'];
               $maildata['super_impose_image'] =  WWW_ROOT . 'img/user_images/' . $this->request->session()->read('GuestUser.image_name');
               $maildata['audio'] =  WWW_ROOT . 'img/user_audio/' . $this->request->session()->read('GuestUser.audio_name');  


               $this->Common->send_mail_attchment($maildata);    

          
            //              $email = new Email();
            //             $send_email = $email->template('allmail')
            //             ->emailFormat('html')
            //             ->to(array($this->Auth->user("email"),$FROM_EMAIL,$support_email))
            //             ->from([$FROM_EMAIL => $project_name])
            //             ->subject('LiziCards: Order Confirmation')
            //             ->viewVars($maildata)
            //             ->attachments([
            //     'invoice.pdf' => [
            //     'file' => $folder_path,
            //     ]])          
            //             ->send();

            // $email_audio_image = new Email();
            // $email_audio_image->template('allmail')
            // ->emailFormat('html')
            // ->from([$FROM_EMAIL => $project_name])
            // ->to($AdminEmail)
            // ->subject('LiziCards Uploded Image and Audio')
            // ->viewVars($maildata)
            // ->attachments([
            //     'invoice.pdf' => [
            //     'file' => $folder_path,
            //     ],
            //     'photo.jpg' => [
            //     'file' => WWW_ROOT . 'img/user_images/' . $this->request->session()->read('GuestUser.image_name'),
            //     ],
            //     'audio.wav' => [
            //     'file' => WWW_ROOT . 'img/user_audio/' . $this->request->session()->read('GuestUser.user_audio'),
            //     ]
            //     ])
            // ->send();      
  
       }

           $this->request->session()->delete("GuestUser");

           $template_data = $this->Orders->find('all')->contain(['Templates','UsersAudio','UsersImages'])->where(['Orders.id' => $_REQUEST['item_number'],'Orders.user_id' => $this->Auth->user("id"),'Orders.transaction_id' => $_REQUEST['tx']])->hydrate(false)->first();

           $template_name = $template_data['template']['name'];

            $this->loadModel("TemplatesImages");

            $template_images = $this->TemplatesImages->find()->contain(['Templates'])->where(['TemplatesImages.status' => 1,'TemplatesImages.show_preview' => 1,'TemplatesImages.template_id' => $template_data['template_id']])->hydrate(false)->first();
            $preview_image = $template_images['images'];

      $this->request->session()->write("get_preview_image",$preview_image);
             $this->request->session()->write("get_template_name",$template_name);

            $this->set(compact('template_data','preview_image','template_name'));
            
    }

    public function shiipingOption(){

    }

    public function shiipingOptionMore(){
      $this->set('count',$this->request->data['count']);
    }

    public function showCountry(){
        // pr($this->request->data); die;
        $state = TableRegistry::get("states");
      // $state_list = $state->find("list", ['keyField' => 'id','valueField' => 'name'])->where(['country_id' => $this->request->data['val']])->order('name')->toArray();
         $state_list = array();
            $get_states = $state->find("all")->where(['country_id' => $this->request->data['val']])->order('name')->toArray();

            foreach ($get_states as $stlist):
                $id = $stlist['id'];
                $title = ($this->request->data['val']==231)?$stlist["state_abbrivation"]." - ". $stlist['name']:$stlist['name'];
                $state_list[$id] = $title;
            endforeach;

        $this->set(compact('state_list'));
    }

    public function showCountryMore(){
        // pr($this->request->data); die;
        $state = TableRegistry::get("states");
    // $state_list = $state->find("list", ['keyField' => 'id','valueField' => 'name'])->where(['country_id' => $this->request->data['val']])->order('name')->toArray();
          $state_list = array();
            $get_states = $state->find("all")->where(['country_id' => $this->request->data['val']])->order('name')->toArray();

            foreach ($get_states as $stlist):
                $id = $stlist['id'];
                $title = ($this->request->data['val']==231)?$stlist["state_abbrivation"]." - ". $stlist['name']:$stlist['name'];
                $state_list[$id] = $title;
            endforeach;
         $this->set('count',$this->request->data['count']);

        $this->set(compact('state_list'));
    }

    public function paymentProcess() {
        $this->autoRender = false;
        if ($this->request->is("post")) {
	 ini_set('memory_limit', '-1');
            require_once(ROOT . DS . 'vendor' . DS . 'paypal_pro' . DS . 'PaypalPro.php');
            $nameArray = explode(' ', $_POST['name_on_card']);
            $firstName = !empty($nameArray[0]) ? $nameArray[0] : "";
            $lastName = !empty($nameArray[1]) ? $nameArray[1] : "";
            $city = '';
            $zipcode = '';
            $countryCode = '';
            $apiUsername = Configure::read('Paypal_Pro_Username');
            $apiPassword = Configure::read('Paypal_Pro_Password');
            $apiSignature = Configure::read('Paypal_Pro_Signature');


            $configure_paypal['apiUsername'] = $apiUsername;
            $configure_paypal['apiPassword'] = $apiPassword;
            $configure_paypal['apiSignature'] = $apiSignature;

            $paypal = new PaypalPro($configure_paypal);
            $paypalParams = array(
                'paymentAction' => 'Sale',
                'amount' => $this->request->data['payable_amount'],
                'currencyCode' => 'USD',
                'creditCardType' => $this->request->data['card_type'],
                'creditCardNumber' => trim(str_replace(" ", "", $this->request->data['card_number'])),
                'expMonth' => $this->request->data['expiry_month'],
                'expYear' => $this->request->data['expiry_year'],
                'cvv' => $this->request->data['cvv'],
                'firstName' => $firstName,
                'lastName' => $lastName,
                'city' => $city,
                'zip' => $zipcode,
                'countryCode' => $countryCode,
                );
            // pr($paypalParams);

            $response = $paypal->paypalCall($paypalParams);

            $paymentStatus = strtoupper($response["ACK"]);
             // pr($response); die;
            if ($paymentStatus == "SUCCESS") {
              $quantity = 0;
                $shipping_cost = 0; 
                $sales_tax = 0;
                $total = 0;
                $grand_total=0;
                $sal_tax = Configure::read('Sales_tax');
             $this->loadModel("Orders");
             $check_order = $this->Orders->find()->where(['template_id' => $this->request->session()->read("GuestUser.template_id"),"transaction_id" => $response['TRANSACTIONID']])->first();
             $shiping_table = TableRegistry::get("shipping_address");
             $check_address_exists = $shiping_table->find()->where(['order_id' => $check_order['id']])->count();
             


  $this->loadModel("Templates");
  $date = date("Y-m-d H:i:s");
  $template_price = $this->Templates->find()->select(['price','id'])->where(['Templates.id' => $this->request->session()->read("GuestUser.template_id")])->first();
  $previous_order_status = $this->Orders->find()->where(['template_id' => $this->request->session()->read("GuestUser.template_id"),'user_id' => $this->Auth->user("id"),'status' => 'pending','transaction_id !=' => $response['TRANSACTIONID']])->count();
  if($previous_order_status>0){
   $get_price = $this->Orders->find()->where(['id' => $this->request->session()->read("GuestUser.order_id")])->first();
   $template_name = $this->Templates->find()->where(['id' => $get_price['template_id']])->first();
             require_once(ROOT . DS . 'vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php');
                      $dompdf = new DOMPDF();
                        $SITEURL = Configure::read('SITEURL');
                        $project_name = Configure::read('project_name');
                        $support_email = Configure::read('Support');
                        $FROM_EMAIL = Configure::read('FROM_EMAIL');
                        $AdminEmail = Configure::read('AdminEmail');
                         $downloaded_path = $SITEURL . 'img' . DS . 'orderpdf' . DS . 'pdf' . time() . ".pdf";
                        $folder_path = WWW_ROOT . 'img' . DS . 'orderpdf' . DS . 'pdf' . time() . ".pdf";
                        $logo = WWW_ROOT . 'img' . DS . 'logo.png';
                        $day = date("D");
                        $d_m_y = date("M j, Y");
                        $sent_time = $day . ", " . $d_m_y;
                        $shipped_to = "";
                        if(!empty($this->request->session()->read("card_data.send")) && $this->request->session()->read("card_data.send") == "to_me"){
                    $shipped_to = "To Me";
                    $quantity = $this->request->session()->read("card_data.quantity");
                    $total = $get_price['price']*$quantity;
        $state = TableRegistry::get("states");
        $countries = TableRegistry::get("countries");
        $get_state = $state->find()->where(['id' => $this->request->session()->read("card_data.state_id")])->first();
         if($get_state['country_id']==231){
                $state_name = $get_state['state_abbrivation'];
              }else{
                $state_name = $get_state['name'];
              }
        $get_countries = $countries->find()->where(['id' => $this->request->session()->read("card_data.country_id")])->first();        
               if($this->request->session()->read("card_data.country_id")!=231){
                $shipping_cost+=5;
                $type = "outside_us_shipping";
            }else{
                $shipping_cost = $this->request->session()->read("card_data.shipping");
                 if($this->request->session()->read("card_data.state_id")==3943){
                 $add_shipping = $total+$shipping_cost; 
                $sales_tax = $add_shipping*$sal_tax/100;                
                 }  
            }

            if($this->request->session()->read("card_data.shipping")==1){
          $type = "us_shipping";
         }if($this->request->session()->read("card_data.shipping")==3){
         $type = "us_expedited_shipping";
         }
         
           $master_shipping_type =  $type;

            if($check_address_exists==0){         
              

              $ship_address = $shiping_table->newEntity();
              $ship_address->user_id = $this->Auth->user("id");
              $ship_address->order_id = $this->request->session()->read("GuestUser.order_id");
              $ship_address->first_name = $this->request->session()->read("card_data.first_name");
              $ship_address->last_name = $this->request->session()->read("card_data.last_name");
              $ship_address->address_one = $this->request->session()->read("card_data.address_one");

              $ship_address->address_two = $this->request->session()->read("card_data.address_two");
              $ship_address->city = $this->request->session()->read("card_data.city");
              $ship_address->state_id = $this->request->session()->read("card_data.state_id");
              $ship_address->sale_tax = number_format($sales_tax,2);
              $ship_address->zip = $this->request->session()->read("card_data.zip");
              $ship_address->country = $this->request->session()->read("card_data.country_id");
               $ship_address->shipping_type = $type;
              $ship_address->created_on = date("Y-m-d H:i:s");
              $ship_address->modified_on = date("Y-m-d H:i:s");
              $shiping_table->save($ship_address);

          }


      }
      if(!empty($this->request->session()->read("card_data.send")) && $this->request->session()->read("card_data.send") == "to_receipent"){
        $shipped_to = "To Recipient";
          $quantity =  count($this->request->session()->read("card_data.data"));     
          $get_indiviual_state_quantity =0;
          $get_quantity_for_saleTax=0;
          $calculate_ship_cost =0;
          $master_shipping_flag_array = []; 
          foreach ($this->request->session()->read("card_data.data") as $key => $value) {
            if(!empty($value['shipping']) && $value['shipping']==1){
          $type = "us_shipping";
         }
         if(!empty($value['shipping']) && $value['shipping']==3){
          $type = "us_expedited_shipping";
         }    
            if($value['country_id']!=231){
                $shipping_cost+=5;
                $type = "outside_us_shipping";
            }else{
                $shipping_cost += $value['shipping'];
                if($value['state_id']==3943){
                $calculate_ship_cost+= $value['shipping'];  
                $get_indiviual_state_quantity = 1;           
                $get_quantity_for_saleTax+=1;
             
               $get_total = $get_price['price']*$get_quantity_for_saleTax+$calculate_ship_cost;

               $get_individual_total = $get_price['price']*$get_indiviual_state_quantity;
               $get_individual_sales_tax = $get_individual_total*$sal_tax/100;
                 $sales_tax = $get_total*$sal_tax/100;               
               
               }else{
                $get_individual_sales_tax = "0.00";
                  }
            }

            if(!in_array($type,$master_shipping_flag_array)) {
            $master_shipping_flag_array[] = $type ;
          }
            $shiping_table = TableRegistry::get("shipping_address");
            $check_address_exists = $shiping_table->find()->where(['order_id' => $check_order['id']])->count();  

            if($check_address_exists != count($this->request->session()->read("card_data.data")) && !empty($this->request->session()->read("GuestUser.order_id"))){
              $ship_address = $shiping_table->newEntity();
              $ship_address->user_id = $this->Auth->user("id");
              $ship_address->order_id = $this->request->session()->read("GuestUser.order_id");
              $ship_address->first_name = $value["first_name"];
              $ship_address->last_name = $value["last_name"];
              $ship_address->address_one = $value["address_one"]; 

              $ship_address->address_two =  $value["address_two"]; 
              $ship_address->city = $value["city"]; 
              $ship_address->state_id = $value["state_id"]; 
              $ship_address->sale_tax = number_format($get_individual_sales_tax,2); 
              $ship_address->zip = $value["zip"]; 
              $ship_address->country = $value["country_id"];
              $ship_address->shipping_type = $type;  
              $ship_address->created_on = date("Y-m-d H:i:s");
              $ship_address->modified_on = date("Y-m-d H:i:s");
              $shiping_table->save($ship_address);    
          }  
      }
      if(count($master_shipping_flag_array)>1) {
      $master_shipping_type  = 'mixed';

    }else if(count($master_shipping_flag_array)==1) {
      $master_shipping_type = $master_shipping_flag_array[0];

    }else {
      $master_shipping_type = '';
    }
  }
   $update_data = $this->Orders->query();
   $res = $update_data->update()
   ->set(['quantity' => $quantity,'shipping_cost' => $shipping_cost,'master_shipping_type' => $master_shipping_type,'total_sales_tax' => number_format($sales_tax,2),'total_amount' => $response['AMT'],'send_to' => $this->request->session()->read('card_data.send'),'transaction_id' => $response['TRANSACTIONID'],'payment_from' => 'credit card','invoice' => $downloaded_path,'card_type' => $this->request->data["card_type"],'status' => "Completed","modified_on" => $date])
   ->where(['id' => $this->request->session()->read("GuestUser.order_id"),'user_id' => $this->Auth->user("id")])->execute();
        $total= $get_price['price']*$quantity;        
         $grand_total += $total+$shipping_cost+$sales_tax;;                         


                                $html = '<html>
    <head></head>

    <table align="center" style="text-align: center; font-size:12px;" width="100%" border="0">
        <tr align="left">

            <td align="left" style="font-size:10px;">
                ' . date("n/j/Y") . '
            </td>
            <td colspan="4" align="left" style="font-size:10px;">
                From - ' . $FROM_EMAIL . '
            </td>
        </tr>
        <tr align="left">  <td><img align="left" src=' . $logo . ' width="80"></td>
            <td align="right" colspan="4" style="font-weight:bold;">&lt;' . $this->Auth->user('email') . '&gt;</td>
        </tr>
        <tr>
            <td  colspan="5" style="color:lightgrey;">
                <hr>
            </td>
        </tr>
        <tr align="left">
            <td  colspan="4">
                Subject: <b>Order Detail</b>
            </td>
        </tr>
        <tr align="left">
            <td  colspan="4">
                
            </td>
        </tr>
         <tr>
            <td  colspan="5" style="color:lightgrey;">
                <hr>
            </td>
        </tr>
          <tr align="left">
            <td  colspan="2">
                From: <b>'.$project_name.'</b> &lt;' . $FROM_EMAIL . '&gt;
            </td>
            
             <td colspan="3" align="right">
             ' . $sent_time . '
                
            </td>
        </tr>
          <tr align="left">
            <td  colspan="4">
                To: ' . $this->Auth->user('email') . '
            </td>
        </tr>';
                                $html .='<tr align="left">
            <td  colspan="4">
              Shipped '.@$shipped_to.'  
            </td>
        </tr>
         <tr>
            <td  colspan="4">
                
            </td>
        </tr>
         <tr>
            <td  colspan="4">
                
            </td>
        </tr>
                
        
        <tr>
            <td  colspan="4">
                
            </td>
        </tr>
         <tr>
            <td  colspan="4">
                
            </td>
        </tr>
         <tr>
            <td  colspan="4">
                
            </td>
        </tr>
        <tr align="left">
            <td  colspan="4">
                
            </td>
            <td align="left" colspan="4">
            </td>
        </tr>

    </table>';

   if(!empty($this->request->session()->read("card_data.send")) && $this->request->session()->read("card_data.send") == "to_me"){
    $html.='<div style="width:30%; float:left; margin:10px 0px;border:1px solid grey"">
  <h4 style="font-weight:bold;font-size:12px;background-color:lightgrey;">Address</h4>
  <p style="font-size:10px;">'.$this->request->session()->read("card_data.first_name")." ".$this->request->session()->read("card_data.last_name").'</p>
  <p style="font-size:10px;">'.$this->request->session()->read("card_data.address_one")." ".@$this->request->session()->read("card_data.address_two")." ".$this->request->session()->read("card_data.city").'</p>
  <p style="font-size:10px;">'.$state_name.", ".$this->request->session()->read("card_data.zip").", ".@$get_countries['name'].'</p>
</div>';
}
if(!empty($this->request->session()->read("card_data.send")) && $this->request->session()->read("card_data.send") == "to_receipent"){
    $i=1;
    $html.='<div style="width:100%;float:left;">';
    foreach ($this->request->session()->read("card_data.data") as $ship_adr) {
        $state = TableRegistry::get("states");
        $countries = TableRegistry::get("countries");
        $get_countries = $countries->find()->where(['id' => $ship_adr['country_id']])->first();

    $html.='<div style="width:30%; float:left; margin:10px 0px;border:1px solid grey"">
  <h4 style="font-weight:bold;font-size:12px;background-color:lightgrey;">Address'.$i.'</h4>
  <p style="font-size:10px;">'.$ship_adr['first_name']." ".$ship_adr['last_name'].'</p>
  <p style="font-size:10px;">'.$ship_adr['address_one']." ".@$ship_adr['address_two']." ".$ship_adr['city'].'</p>
  <p style="font-size:10px;">';
      if(!empty($ship_adr['state_id'])){
        $get_state = $state->find()->where(['id' => $ship_adr['state_id']])->first();
        if($get_state['country_id']==231){
                $state_name = $get_state['state_abbrivation'];
              }else{
                $state_name = $get_state['name'];
              }
        $html .= @$state_name.", ";
         }
         $html.= @$ship_adr['zip'].', '.$get_countries['name'].'</p>
</div>';
$i++;}
$html.='</div>';
}

$html.='<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px; border:1px solid #f1f1f1; padding:5px;">
  <tr>
    
    <td valign="top" style="text-align:left; padding-left:8px; font-weight:500; height:25px;">'.$template_name['name'].'</td>
    <td valign="top" style="text-align:right; padding-left:8px; font-weight:500">$'.$get_price['price'].'</td>
  </tr>
  <tr>
    <td valign="top" style="text-align:left; padding-left:8px; color:#333;">Quantity</td>
    <td valign="top" style="text-align:right; padding-left:8px; color:#333;">'.$quantity.'</td>
  </tr>
</table>';

   $html.='<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px; text-align:right; color:#9f9f9f;">
  <tr>
    <td style="height:20px;">Items total</td>
    <td>$'.number_format($total,2).'</td>
  </tr>
  <tr>
    <td style="height:20px;">Shipping</td>
    <td>$'.number_format($shipping_cost,2).'</td>
  </tr>
  <tr>
    <td style="height:20px;">Sales Tax</td>
    <td>$'.number_format($sales_tax,2).'</td>
  </tr>

</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px; text-align:right; color:#9f9f9f; border-top:1px solid #f2f2f2; border-bottom:1px solid #f2f2f2;">

 <tr>
    <td style="color:#333;">Order Total</td>
    <td style="color:#333;">$'.number_format($grand_total,2).'</td>
  </tr>
  </table>';
                  

$html.='</html>
';


                                $dompdf->load_html($html);
                                $dompdf->render();

                                $output = $dompdf->output();
                                file_put_contents($folder_path, $output);
  
                   $maildata = array();
                $url = $SITEURL;
               $mail_content ="";
               $mail_content.='<p>Hi '.$this->Auth->user('first_name').',</p>
                        
                <p>Thank you for shopping at Lizicards. We&#39;ll soon be mailing your greeting card(s).</P> 

                <p>Please find attached the receipt.</p>  

                <p>If you have any questions, please email us: '.$FROM_EMAIL.'.</p>             
                 
                 <p>Many thanks, <br/>Lizicards Team</p>       
                      </td>                  
              </tr>
              
                <tr>
                
                
                  <td align="center">   <a href='.$url.' style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Send a Greeting Card </a> </td>
                  
                
                  </tr>';  
               
                $maildata['email'] = $this->Auth->user("email");
               $maildata['from_email'] = $FROM_EMAIL;
               $maildata['support_email'] = $support_email;
               $maildata['heading'] = "Lizicards: Order Confirmation";   
               $maildata['url'] = $url;  
               $maildata['data'] = $mail_content; 
               $maildata['subject'] = $project_name.': Order Confirmation'; 
               $maildata['invoice'] =  $folder_path;  
	       $maildata['transaction_id'] = $response['TRANSACTIONID'];
               $maildata['super_impose_image'] =  WWW_ROOT . 'img/user_images/' . $this->request->session()->read('GuestUser.image_name');
               $maildata['audio'] =  WWW_ROOT . 'img/user_audio/' . $this->request->session()->read('GuestUser.audio_name');  


               $this->Common->send_mail_attchment($maildata);    

    
            //  $email = new Email();
            //             $send_email = $email->template('allmail')
            //             ->emailFormat('html')
            //             ->to(array($this->Auth->user("email"),$FROM_EMAIL,$support_email))
            //             ->from([$FROM_EMAIL => $project_name])
            //             ->subject('LiziCards: Order Confirmation')
            //             ->viewVars($maildata)
            //             ->attachments([
            //     'invoice.pdf' => [
            //     'file' => $folder_path,
            //     ]])          
            //             ->send();

            // $email_audio_image = new Email();
            // $email_audio_image->template('allmail')
            // ->emailFormat('html')
            // ->from([$FROM_EMAIL => $project_name])
            // ->to($AdminEmail)
            // ->subject('LiziCards Uploded Image and Audio')
            // ->viewVars($maildata)
            // ->attachments([
            //     'invoice.pdf' => [
            //     'file' => $folder_path,
            //     ],
            //     'photo.jpg' => [
            //     'file' => WWW_ROOT . 'img/user_images/' . $this->request->session()->read('GuestUser.image_name'),
            //     ],
            //     'audio.wav' => [
            //     'file' => WWW_ROOT . 'img/user_audio/' . $this->request->session()->read('GuestUser.audio_name'),
            //     ]
            //     ])
            // ->send();  

  

}

$this->request->session()->write("order_id",$this->request->session()->read("GuestUser.order_id"));
$this->request->session()->delete("GuestUser");


        $data['status'] = 1;
    }else{
        $data['status'] = 0;
        }
        echo json_encode($data);
    }
    }

public function thanks(){
     $this->viewBuilder()->layout("thankyou_page");
      $this->loadModel("Orders");
     $template_data = $this->Orders->find('all')->contain(['Templates','UsersAudio','UsersImages'])->where(['Orders.user_id' => $this->Auth->user("id"),'Orders.id' => $this->request->session()->read('order_id')])->hydrate(false)->first();

           $template_name = $template_data['template']['name'];

            $this->loadModel("TemplatesImages");

            $template_images = $this->TemplatesImages->find()->contain(['Templates'])->where(['TemplatesImages.status' => 1,'TemplatesImages.show_preview' => 1,'TemplatesImages.template_id' => $template_data['template_id']])->hydrate(false)->first();
            $preview_image = $template_images['images'];
      
            $this->request->session()->write("get_preview_image",$preview_image);
             $this->request->session()->write("get_template_name",$template_name);

            $this->set(compact('template_data','preview_image','template_name'));
}

public function login() {
        $this->autoRender = false; 
         if (!$this->Auth->user() && empty($this->request->is('post'))) {
            $this->redirect("/");
        }       
        if ($this->request->is("ajax")) {
            try {
                $url = Configure::read('SITEURL');
                $username = $this->request->data['email'];
                $findUser = $this->Users->find('all', ['conditions' => ['Users.email' => $username, 'Users.type >' => 0]])->first();
                if ($findUser) {
                    
                    $user = $this->Auth->identify();
                    
                    if ($user) {
                         $this->loadModel('UsersImages');
                        $this->loadModel('UsersAudio');
                        $user_file_entity = $this->UsersImages->newEntity();
                        $user_file_entity->user_id = $user['id'];
                        $user_file_entity->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";
                        $user_file_entity->user_image = !empty($this->request->session()->read('GuestUser.image_name'))?$this->request->session()->read('GuestUser.image_name'):"";                        
                        $save_user_image = $this->UsersImages->save($user_file_entity); 
                        $user_image_id = $save_user_image->id;
                        $this->request->session()->write("GuestUser.user_image_id",$user_image_id);
                        $user_audio_entity = $this->UsersAudio->newEntity();
                        $user_audio_entity->user_id = $user['id'];
                        $user_audio_entity->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";                       
                        $user_audio_entity->user_audio = !empty($this->request->session()->read('GuestUser.audio_name'))?$this->request->session()->read('GuestUser.audio_name'):"";
                        $save_audio = $this->UsersAudio->save($user_audio_entity);
                        $audio_id = $save_audio->id;
                        $this->request->session()->write("GuestUser.audio_id",$audio_id);
                        $this->Auth->setUser($user);
                        $this->loadModel("Templates");
                        $get_price = $this->Templates->find()->where(['id' => $this->request->session()->read('GuestUser.template_id')])->first();

                        $this->loadModel("Orders");
                        $Orders_table = $this->Orders->newEntity();
                        $Orders_table->user_id = $this->Auth->User("id");
                        $Orders_table->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";
                        $Orders_table->user_image_id = $user_image_id;
                        $Orders_table->user_audio_id = $audio_id;
                        $Orders_table->price = $get_price['price'];
                        $Orders_table->created_on = date("Y-m-d H:i:s");
                        $save_order = $this->Orders->save($Orders_table);
                        $order_id = $save_order->id;
                        $this->request->session()->write("GuestUser.order_id",$order_id);

                        $this->request->session()->write('GuestUser.page_number',6);

                        $result['status'] = "success";
                        $result['msg'] = '';                       

                        echo json_encode($result);
                    } else {
                        throw new Exception("Either your email and/or password is  incorrect.");
                    }
                } else {
                    throw new Exception("Either your email and/or password is  incorrect.");
                }
            } catch (Exception $ex) {
                $result['status'] = "error";
                $result['msg'] = $ex->getMessage();
                echo json_encode($result);
            }
        }
    }

     /*
    Function to request new password
    */
    public function forgotPassword() {
        $project_name = Configure::read('project_name');
        $pageDescription = 'Forgot Password | ' . $project_name;

        $this->set(compact('pageDescription'));
        $this->viewBuilder()->layout('admin');
        // $this->autoRender = false;
        if($this->request->is("post")){
            try {
                if (empty($this->request->data['email'])) {
                    throw new Exception("Please enter your email Id.");             
                }
                
                /*$result['status'] = "success";
                $result['response'] = 'test response';
                echo json_encode($result);
                exit;*/
              
               if (!empty($this->request->data['email'])) {
                    $check_email = $this->Users->find()->where(['email' => $this->request->data['email'],"type !=" => "0"])->first();

                    if (!empty($check_email)) {
                        $activation_key = md5(uniqid());
                        $activationlink = Configure::read('SITEURL') . 'users/reset-password/' . $activation_key;
                        $toEmail = $check_email->email;
//                    
                        $FROM_EMAIL = Configure::read('FROM_EMAIL');

                        $project_name = Configure::read('project_name');
//echo $project_name; die;
                        $url = Configure::read('SITEURL');
                        $mail_content="";
                        $maildata = array();

                       

                        $mail_content .= '<p>We got a request to change the password for the account with the username <strong style="font-weight: bold;">'.$check_email["email"].'</strong>.</P>
                        
                <p>If you don&#39;t want to reset your password, you can ignore this email.</P>
                
                <p>If you request this change, Please click the link below to reset the password or copy and paste the link on the browser <b>'.$activationlink.'</b></P> 

                <p>Many thanks, <br/> Lizicards Team</p>           
                
                      </td>                 
              </tr>              
                <tr>           
                
                  <td align="center">   <a href='.$activationlink.' style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Reset Your Password </a> </td>                 
                
                  </tr>';

                   // $maildata['name'] = $check_email['first_name']." " .$check_email['last_name'];
                        // $maildata['email'] = $check_email["email"];
                        // $maildata['reset_activationKey'] = $activation_key;
                        $maildata['email'] = $this->request->data['email'];
                        $maildata['heading'] = "Forget your password? Let's get you a new one.";
                        $maildata['url'] = $url;
                        $maildata['data'] = $mail_content;
                        $maildata['subject'] = $project_name.': Reset Password';
        
                        
                        // $maildata['reset_link'] = $activationlink;
//                    $hashPswdObj = new DefaultPasswordHasher;
//                    $password = $hashPswdObj->hash($maildata['password']);
                       /* $email = new Email();
                        $send_email = $email->template('allmail')
                        ->emailFormat('html')
                        ->to($this->request->data['email'])
                        ->from([$FROM_EMAIL => $project_name])
                        ->subject('LiziCards: Reset Password')
                        ->viewVars($maildata)
                        ->send();*/
                    //  echo "jkdsfhkj"; die;
                            $user = $this->Users->newEntity();
// $update_password = $this->Users->updateAll(array("password" => $password), array("username" => $this->request->data['username']));
                            $user = $this->Users->patchEntity($check_email, ['reset_activation_key' => $activation_key]);
                            if($this->Users->save($user)){
                             $this->Common->send_mail($maildata);   
                            $result['status'] = "success";
                            $result['response'] = '<div class="alert alert-success">An email has been sent to <b>' . $check_email["email"] . '</b> so you can reset your password</div>';
                            echo json_encode($result);
                            exit();
                        } else {
                            throw new Exception("Something went wrong. Please try again later.");
                        }
                    } else {
                        throw new Exception("Invalid email. Please enter a valid email address");
                    }
                }
            } catch (Exception $e) {
                $result['status'] = "error";
                $result['response'] = $e->getMessage();
                echo json_encode($result);
                exit;
            }
        }

    }

     /*
     * function to reset the password
     */

    public function resetpassword($key = null,$template_id=null) {

        $project_name = Configure::read('project_name');
        $pageDescription = 'Reset Password|  ' . $project_name;

        $this->set(compact('pageDescription'));
        $this->viewBuilder()->layout('inner_page');
        $user = $this->Users->find('all', array(
            'conditions' => array(
                'Users.reset_activation_key' => $key,
                ),
            ))->first();

        if ($this->request->is("post")) {
            try {
                if (empty($user)) {
                    throw new Exception("It seems that your token has been expired");
                }
                if (empty($this->request->data['password'])) {
                    throw new Exception("Please enter password");
                }
                if (empty($this->request->data['password_confirm'])) {
                    throw new Exception("Please confirm your password");
                }

                if ($this->request->data['password_confirm'] != $this->request->data['password']) {
                    throw new Exception("Password does not match");
                }
                $update_password = $this->Users->patchEntity($user, ['password' => $this->request->data['password'], 'reset_activation_key' => ""]);
                if ($this->Users->save($update_password)) {
                    $message = "Password updated successfully.";
                    $this->Flash->success(__($message));
                    $this->redirect("/users/reset-password");
                }
            } catch (Exception $ex) {
                $message = $ex->getMessage();
                $this->Flash->error(__($message));
            }
        }
    }


public function dashboard() {
     $this->viewBuilder()->layout("thankyou_page");  
    
        $this->loadModel("Orders");
           $project_name = Configure::read('project_name');
        $pageDescription = 'Order List | ' . $project_name;

        $this->set(compact('pageDescription'));
         $this->loadModel("Orders");
         $template_data = $this->Orders->find()->contain(['Templates','Users'])->where(['Orders.user_id' => $this->Auth->user("id")])->hydrate(false)->order(["Orders.id" => "DESC"])->toArray();
        
        $this->set(compact('template_data'));

    }

    public function viewOrders($id=null){
       $this->viewBuilder()->layout("thankyou_page");  

        $project_name = Configure::read('project_name');
        $pageDescription = 'View Order | ' . $project_name;
        $this->loadModel("Orders");

        $template_data = $this->Orders->find('all')->contain(['Templates','UsersAudio','UsersImages'])->where(['Orders.id' => $id,'Orders.user_id' => $this->Auth->user("id")])->hydrate(false)->first();
        // pr($template_data);
           
            $this->loadModel("TemplatesImages");

            $template_images = $this->TemplatesImages->find()->contain(['Templates'])->where(['TemplatesImages.status' => 1,'TemplatesImages.show_preview' => 1,'TemplatesImages.template_id' => $template_data['template_id']])->hydrate(false)->first();
            $preview_image = $template_images['images'];

            $shipping_table = TableRegistry::get("shipping_address");
            $get_address = $shipping_table->find()->where(["order_id" => $id])->toArray();

            $this->set(compact('template_data','preview_image','get_address'));
           
         $this->set(compact('pageDescription'));
    }

    public function terms(){
        $this->viewBuilder()->layout("thankyou_page");  
    }

   public function privacy(){
        $this->viewBuilder()->layout("thankyou_page");  
    }
    public function shippingReturn(){
      $this->viewBuilder()->layout("thankyou_page");    
    }
    public function cookie(){
      $this->viewBuilder()->layout("thankyou_page");    
    }
        
         public function contact() {
            $this->viewBuilder()->layout("thankyou_page");  
        $project_name = Configure::read('project_name');
        $pageDescription = 'Contact US | ' . $project_name;
    
   
        if ($this->request->is("post") || $this->request->is("put")) {
            try {
                if (empty($this->request->data['name'])) {
                    throw new Exception("Please enter your name.");
                }
                if (empty($this->request->data['email']) || !filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                    throw new Exception("Please enter valid email");
                }
                // if (empty($this->request->data['subject'])) {
                //     throw new Exception("Please enter subject");
                // }
                if (empty($this->request->data['message'])) {
                    throw new Exception("Please enter your message");
                }

                $contact_table = TableRegistry::get("contact");
                
                    $this->request->data["created_on"] = date("Y-m-d H:i:s");
                    $this->request->data["modified_on"] = date("Y-m-d H:i:s");
                    $new_entity = $contact_table->newEntity($this->request->data);
                    if ($contact_table->save($new_entity)) {
                        $maildata = array();
                $url = Configure::read('SITEURL');
               $mail_content ="";
               $mail_content.='<p>Hi Admin,</P>
                        
                <p>You have received a email from '.$this->request->data['name'].'('.$this->request->data['email'].')</P> 

                <p><b>Below is the user message.</b></p>                       
               <p> '.$this->request->data['message'].'</p>

               <p>Many thanks, <br/> Lizicards Team</p>
      
                      </td>                  
              </tr>
              
                <tr>
                
                
                  <td align="center">   <a href='.$url.' style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Send a greeting card </a> </td>
                  
                
                  </tr>';  
               
               $maildata['heading'] = "Lizicards: User Feedback";   
               $maildata['url'] = $url;  
               $maildata['data'] = $mail_content;     
                    $maildata['name'] = $this->request->data['name'];
                    $maildata['email'] = $this->request->data['email'];
                    // $maildata['subject'] = $this->request->data['subject'];
                    $maildata['message'] = $this->request->data['message'];

                    $Admin_email = Configure::read('AdminEmail');
                    $project_name = Configure::read('project_name');
                    $email = new Email();
                    $mail = $email->template('allmail')
                            ->emailFormat('html')
                            ->to($Admin_email)
                            ->from([$this->request->data['email'] => $project_name])
                            ->subject('Lizicards: Contact us query')
                            ->viewVars($maildata)
                            ->send();
                       $message = "Thanks for reaching out.  We’ll be in touch soon!";
                       $this->Flash->success(__($message));
//                        return $this->redirect("/users/contact");
                        return $this->redirect("/users/contact");
                        die;
                    } else {
                        throw new Exception("Something went wrong. Please try again later");
                    }
                
            } catch (Exception $ex) {
                $message = $ex->getMessage();
                $this->Flash->error($message);
            }
        }

        
        $this->set(compact('pageDescription'));
    }  
    
public function logout() {
        $this->request->session()->delete("GuestUser");
        $this->request->session()->delete("card_data");
        $u_id = $this->request->session()->read('Auth.User.id');
        $session = $this->request->session()->delete($u_id);
        return $this->redirect($this->Auth->logout());
    }
    
    /* This function is used to test email */
    public function testmyem() {
        $url = Configure::read('SITEURL');
 	$maildata = array();               
        $mail_content ="";
	$mail_content.='<p>Hi demo ,</P>';
	$maildata['heading'] = "Lizicards Registration.";   
        $maildata['url'] = $url;  
        $maildata['data'] = $mail_content;
        /*$email = new Email();
        $send_email = $email->template('allmail')
        ->emailFormat('html')
        ->to($this->request->data['email'])
        ->from([$FROM_EMAIL => $project_name])
        ->subject('LiziCards: Reset Password')
        ->viewVars($maildata)
        ->send();*/
        $email = new Email();
         $send_email = $email->template('allmail')
         ->emailFormat('html')
        ->from(['orders@lizicards.com' => 'Lizicards'])
        ->to('ljaswal@teqmavens.com')
        ->subject('Test Message with cake php')
	->viewVars($maildata)
        ->send();
        var_dump($email);
        
        die('hello');
    }  
	
    public function whatis() {
      $this->viewBuilder()->layout("thankyou_page"); 
      if($this->request->session()->check("whatIsGetStarted")){
        $this->request->session()->delete("whatIsGetStarted");
      }  
      $title = "Introducing";
      $this->set(compact("title")); 
    }


public function whatIsGetStarted(){
  if(!empty($this->request->session()->read("whatIsGetStarted"))){
    $number = $this->request->session()->read("whatIsGetStarted")+1;
  }else{
    $number = 1;
  }
  $this->request->session()->write("whatIsGetStarted",$number);
  echo $this->request->session()->read("whatIsGetStarted");
  exit();  

}
  
    


}
