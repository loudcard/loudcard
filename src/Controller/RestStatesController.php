<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\Mailer\Email;

use App\Controller\AppController;
// In a controller or table method.
use Cake\ORM\TableRegistry;




/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class RestStatesController extends AppController
{
    public $component = array('RequestHandler');
    public $paginate = [
    'page' => 1,
    'limit' => 5,
    'maxLimit' => 100,
    'fields' => [],
    'sortWhitelist' => []
    ];
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);                
        $this->loadModel("States");
    }

    /**
     * Function to list all countries
     *
     * @return Json countries List
     */
    public function index() {        
        $queryArr = $this->Common->getQueryStrings(); 
        if(isset($queryArr)){
           extract($queryArr);
        }
        
        if(isset($type) && $type!='') {
            $type = $type;
        }else {
            $type = '';
        }
        
        $states_list = [];
        $status         = 'fail';
        $mesage         = 'no data found';

        switch ($type) {
            case 'countriesstates': {
                if(isset($country_id) && !empty($country_id) && $country_id!=0) {
                    $state_data = array('231','38','101','166','181','44','13');
                    if(in_array($country_id, $state_data)){
                    $states = $this->States->find('all')->select(['id','name','state_abbrivation'])->where(['country_id'=>$country_id])->order('name')->all();
                    if(isset($states) && $states!=null) {                        
                        foreach ($states as $key => $value) {
                            //pr($value->state_abbrivation);die();
                            if($value->state_abbrivation==null || $value->state_abbrivation=='') {
                                $states_list[] = ['id'=>$value->id,'name'=>$value->name,'state_abbrivation'=>''] ;
                            }else {
                                $states_list[] = ['id'=>$value->id,'name'=>$value->name,'state_abbrivation'=>$value->state_abbrivation] ;
                            }                            
                        }
                        $status         = 'success';
                        $mesage         = 'states found';
                    }
                 }
                }else {
                    $mesage         = 'empty country id';
                }    
            }break;
            default : {

            }break;
        }
        
       $this->set([
            'status'     =>  $status,
            'message'    =>  $mesage,
            'data'       =>  $states_list,            
            '_serialize' => ['status','message','data']
        ]);
    }
 


    /**
     * Function to add a new country
     *
     * @return Json message

     *      */
    public function add() {
        $action = $this->request->data['action'];        
        $data = [];
        $this->set([
            'data'=>$data,
            '_serialize' => ['data']
        ]);

    }

    public function view($id)
    {
        $queryArr=$this->Common->getQueryStrings();   
        
        if($queryArr)
            extract($queryArr); 
        
        $data = [];
        $this->set([
            'data'=>$data,
            '_serialize' => ['data']
            ]);
    }


    public function edit($id)
    {
        $this->loadModel('States');
        $data = [];

        $this->set([
            'status' => 'success',
            'data' => $data,
            '_serialize' => ['message', 'errorType']
            ]);
    }


    
    

    public function delete($id)
    {
        $this->loadModel('States');
        $queryArr=$this->Common->getQueryStrings();        
        extract($queryArr);

        $messages = [];
        $this->set([
            'messages'   => $messages,
            '_serialize' => ['message']
            ]); 
    }
}
