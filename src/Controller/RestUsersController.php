<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Core\Exception\Exception;

use App\Controller\AppController;
// In a controller or table method.
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;




/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class RestUsersController extends AppController
{
    //public $component = array('RequestHandler');

   public function beforeFilter(Event $event)
   {
    parent::beforeFilter($event);
    $this->loadModel("Users");


}

// public function index(){
//     echo "hellodfsdfsdfddfsdf"; die;
// }


    /**
     * Function to add a new user
     *
     * @return Json message

     *      */
    public function add() {
        if(isset($this->request->data['action']) && $this->request->data['action']!='') {
            $action = $this->request->data['action'];
            unset($this->request->data['action']);
        }else {
            $action = '';
        }
      
     
     $status = "fail";  
     $userData = [];  
     $from_email = Configure::read('FROM_EMAIL');
     $project_name = Configure::read('project_name');
     $url = Configure::read('SITEURL');
     $maildata = array(); 
     $message = 'no action performed';
     switch ($action) {

        case 'register':
        try{
            // echo "helo"; die;
            // pr($this->request->data); die;
            if (empty($this->request->data['first_name'])) {
                throw new Exception("Please enter first_name.");
            }
            if (empty($this->request->data['last_name'])) {
                throw new Exception("Please enter last name");
            }
            if (empty($this->request->data['password'])) {
                throw new Exception("Please enter password");
            }

            if (empty($this->request->data['email']) || !filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                throw new Exception("Please enter valid email");
            }
            $check_user_email = $this->Users->find("all", array("conditions" => array("Users.email" => $this->request->data['email'])))->first();
            if(!empty($check_user_email)){
               if($check_user_email['type']==0){
                throw new Exception("Email restricted. Please enter other email.");
               }
            }
            $check_user_email = $this->Users->find("all", array("conditions" => array("Users.email" => $this->request->data['email'],"Users.status <= " => 1)))->count();
            if($check_user_email > 0){
                throw new Exception("Email already exists.");
            }else{
		
                $this->request->data['type'] = 1;
                $userData = $this->Users->newEntity($this->request->data);
                $this->Users->save($userData);                 
               
               $mail_content ="";
               $mail_content.='<p>Hi '.$this->request->data['first_name'].',</P>
                        
                <p>Thank you for your registering at Lizicards.</P> 
                <p>Send a special message to your friends and loved ones with Lizicards greeting cards.</p> 
                <p>If you have any questions, please email us '.$from_email.'.</p>              
                 <p>Many thanks, <br/> Lizicards Team</p>       
                      </td>                  
              </tr>
              
                <tr>
                
                
                  <td align="center">   <a href='.$url.' style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Send a greeting card </a> </td>
                  
                
                  </tr>';  
               $maildata['name'] = $this->request->data['first_name']." ".$this->request->data['last_name'];
               $maildata['email'] = $this->request->data['email']; 
               $maildata['heading'] = "Lizicards Registration.";   
               $maildata['url'] = $url;  
               $maildata['data'] = $mail_content;
 	       $maildata['subject'] = $project_name.': Registration';
		
		$this->Common->send_mail($maildata);

               /*$email = new Email();
               $mail = $email->template('allmail')
                       ->emailFormat('html')
                       ->to($maildata['email'])
                       ->from([$from_email => $project_name])
                       ->subject($project_name.' :Registration')
                       ->viewVars($maildata)
                       ->send();*/
                $status = "success";
                $userData['id'] = $userData->id; 
                $message = 'Registration has been completed.';
               

            }


            $this->set([
               'status' => $status,       
               'message' => $message,
               'userData' => $userData,
               '_serialize' => ['status','message', 'userData']
               ]);

        }catch(Exception $ex) {
             // $this->Flash->error(__($ex->getMessage()));
          $this->set([
            'status' => $status, 
            'message' => $ex->getMessage(),
            'userData' => '',
            '_serialize' => ['status','message','userdata']
            ]);

      }
      break;

      case 'guestregister':
      try{
        if (empty($this->request->data['first_name'])) {
            throw new Exception("Please enter first_name.");
        }
        if (empty($this->request->data['last_name'])) {
            throw new Exception("Please enter last name");
        }          

        if (empty($this->request->data['email']) || !filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
            throw new Exception("Please enter valid email");
        }
        $check_user_email = $this->Users->find("all", array("conditions" => array("Users.email" => $this->request->data['email'])))->first();
            if(!empty($check_user_email)){
               if($check_user_email['type']==0){
                throw new Exception("Email restricted. Please enter other email.");
               }
            }
        $check_user_email = $this->Users->find("all", array("conditions" => array("Users.email" => $this->request->data['email'],"Users.status <= " => 1)))->hydrate(false)->first();
        if(count($check_user_email) > 0){  

         $dat = $this->Users->get($check_user_email['id']);  
                   // pr($dat);die;               
         $userData = $this->Users->patchEntity($dat,$this->request->data);
         $this->Users->save($userData);
         $userData['id'] = $check_user_email['id']; 
         $status = "success"; 
         $message = 'You are already registered as a normal user.';
     }else{

        $this->request->data['type'] = 2;
        $hashPswdObj = new DefaultPasswordHasher;
        $password = uniqid();
        // $this->request->data['password'] = $hashPswdObj->hash(uniqid());
         $this->request->data['password'] = $password;
        $userData = $this->Users->newEntity($this->request->data);
        $this->Users->save($userData);
        $userData['id'] = $userData->id;         
               $mail_content="";             
               $mail_content ='<p>Hi '.$this->request->data['first_name'].',</P>
                        
               <p>Thank you for your registering at Lizicards as a guest.</P>                 

               <p>Send a special message to your friends and loved ones with Lizicards greeting cards.</p> 
                 <p>If you have any questions, please email us '.$from_email.'.</p> 

                 <p>Many thanks, <br/> Lizicards Team</p> 
                
                      </td>
                  
              </tr>
              
                <tr>
                
                
                  <td align="center">   <a href="'.$url.'" style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Send a greeting card </a> </td>
                  
                
                  </tr>';
               $maildata['name'] = $this->request->data['first_name']." ".$this->request->data['last_name'];
               $maildata['email'] = $this->request->data['email'];
               $maildata['password'] = $password;   
               $maildata['url'] = $url;  
               $maildata['data'] = $mail_content;
               $maildata['heading'] = "Lizicards Registration."; 
	       $maildata['subject'] = $project_name.': Registration';
		
		$this->Common->send_mail($maildata);
                
               $email = new Email();
               /*$mail = $email->template('allmail')
                       ->emailFormat('html')
                       ->to($maildata['email'])
                       ->from([$from_email => $project_name])
                       ->subject($project_name.': Registration')
                       ->viewVars($maildata)
                       ->send();*/
        $status = "success"; 
        $message = 'Registration has been completed.';

    }
   $this->set([
               'status' => $status,       
               'message' => $message,
               'userData' => $userData,
               'maildata' => $maildata, 
               '_serialize' => ['status','message', 'userData','maildata']
               ]);

}catch(Exception $ex) {
             // $this->Flash->error(__($ex->getMessage()));
  $this->set([
            'status' => $status, 
            'message' => $ex->getMessage(),
            'userData' => [],
            'maildata' => [],
            '_serialize' => ['status','message','userdata','maildata']
            ]);

}
break;
case 'facebook_register':
try{
    if (!empty($this->request->data['userdata'])) {
        $this->loadModel("Users");
        $check_user_email = $this->Users->find("all", array("conditions" => array("Users.email" => $this->request->data['userdata']['email'])))->first();


        $check_user_email = $this->Users->find("all", array("conditions" => array("Users.email" => $this->request->data['userdata']['email'],"Users.status <= " => 1)))->hydrate(false)->first();
        if(count($check_user_email) > 0){  

            $dat = $this->Users->get($check_user_email['id']); 
            $this->request->data['first_name'] = $this->request->data['userdata']['first_name'];
            $this->request->data['last_name'] = $this->request->data['userdata']['last_name'];
            $this->request->data['email'] = $this->request->data['userdata']['email']; 
            $this->request->data['fb_id'] = $this->request->data['userdata']['id'];                               
            $userData = $this->Users->patchEntity($dat,$this->request->data);
            $this->Users->save($userData);
            $userData['id'] = $check_user_email['id']; 
             $status = "success"; 
            $message = 'Register Successfully.';
        }else{
            $this->request->data['first_name'] = $this->request->data['userdata']['first_name'];
            $this->request->data['last_name'] = $this->request->data['userdata']['last_name'];
            $this->request->data['email'] = $this->request->data['userdata']['email'];
            $this->request->data['fb_id'] = $this->request->data['userdata']['id']; 
            $hashPswdObj = new DefaultPasswordHasher;
            // $this->request->data['password'] = $hashPswdObj->hash(uniqid());  
            $password = uniqid();
             $this->request->data['password'] = $password;
            $this->request->data['type'] = 3;
            $this->request->data['status'] = "1";
            $userData = $this->Users->newEntity($this->request->data);
            $this->Users->save($userData);
            $userData['id'] = $userData->id; 
             $mail_content="";               
               $mail_content.='<p>Hi '.$this->request->data['userdata']['first_name'].',</P>
                        
                <p>Thank you for your registering at Lizicards</P>
                <p>These are the login credentials.</p>

                <p>Username: <b>'.$this->request->data['userdata']['email'].'</b></p>
                <p>Password: <b>'.$password.'</b></p>

               <p>Send a special message to your friends and loved ones with Lizicards greeting cards.</p> 
                  <p>If you have any questions, please email us '.$from_email.'.</p> 

                  <p>Many thanks, <br/> Lizicards Team</p>
                
                      </td>
                  
              </tr>
              
                <tr>
                
                
                  <td align="center">   <a href="'.$url.'" style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Send a greeting card </a> </td>
                  
                
                  </tr>';
               $maildata['name'] = $this->request->data['userdata']['first_name']." ".$this->request->data['userdata']['last_name'];
               $maildata['email'] = $this->request->data['userdata']['email'];
               $maildata['password'] = $password;   
               $maildata['url'] = $url;  
               $maildata['data'] = $mail_content;
               $maildata['heading'] = "Lizicards Registration.";  
	       $maildata['subject'] = $project_name.': Registration';
		
		$this->Common->send_mail($maildata);   
               /*$email = new Email();
               $mail = $email->template('allmail')
                       ->emailFormat('html')
                       ->to($maildata['email'])
                       ->from([$from_email => $project_name])
                       ->subject($project_name.':Successfully Register')
                       ->viewVars($maildata)
                       ->send();*/
             $status = "success";                  
            $message = 'Registration has been completed.';

        }
       $this->set([
               'status' => $status,       
               'message' => $message,
               'userData' => $userData,
               '_serialize' => ['status','message', 'userData']
               ]);
    }


}catch(Exception $ex) {
             // $this->Flash->error(__($ex->getMessage()));
   $this->set([
            'status' => $status, 
            'message' => $ex->getMessage(),
            'userData' => '',
            '_serialize' => ['status','message','userData']
            ]);

}
break;
        case 'authmobileusers' : {
            $status             = 'fail';
            $user_data          = [];            
            $message            = '';
            $email              = @$this->request->data['email'];
            $password           = @$this->request->data['password'];
            if(isset($email) && isset($password)) {                
                $user            = $this->Users->find('all')->where(['email'=>$email])->first();
                if(isset($user) && count($user)>0) {                    
                    if($user->status==1){
                        if($user->type!=0) {
                            if ((new DefaultPasswordHasher)->check($password, $user->password)) {
                                $status = 'success';
                                $message = "login successful";                            
                                unset($user->password);                        
                                $user_data = $user;
                            }else {
                                $message = "invalid password";
                            }
                        }else {
                            $message = "restricted access";   
                        }                        
                    }else {
                        $message = "either your account is inactive or deleted.";
                    }    
                }else {
                    $message = "email does not exists";
                }
            }else {
                $message = "email password can not be empty";                
            }
            $this->set([
                'status'        => $status,
                'userData'      => $user_data,                  
                'message'       => $message,
                '_serialize'    => ['status','userData','message']
            ]);
        }break;
        case 'resetpasswordmobileusers' : {
            $pageDescription = 'Forgot Password | ' . $project_name;
            $email           = '';            
            try {
                if (isset($this->request->data['email']) && $this->request->data['email']!='') {
                    $email = $this->request->data['email'];
                }else {
                    throw new Exception("Please enter your email.");
                }
                
                $check_email = $this->Users->find()->where(['email' => $email,"type !=" => "0"])->first();
                if (!empty($check_email)) {
                        $activation_key         = md5(uniqid());
                        $activationlink         = Configure::read('SITEURL') . 'users/reset-password/' . $activation_key;
                        $toEmail                = $check_email->email;
                        $FROM_EMAIL             = Configure::read('FROM_EMAIL');
                        $project_name           = Configure::read('project_name');
                        $url                    = Configure::read('SITEURL');
                        $mail_content           = "";
                        $maildata               = array();
                        $mail_content           = '<p>We got a request to change the password for the account with the username <strong style="font-weight: bold;">'.$check_email["email"].'</strong>.</P>                        
                            <p>If you don&#39;t want to reset your password, you can ignore this email.</P>
                            <p>If you request this change, Please click the link below to reset the password or copy and paste the link on the browser <b>'.$activationlink.'</b></P> 
                            <p>Many thanks, <br/> Lizicards Team</p>           
                                  </td>                 
                          </tr>              
                            <tr>           
                              <td align="center">   <a href='.$activationlink.' style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Reset Your Password </a> </td>                 
                              </tr>';
                        $maildata['heading']    = "Forget your password? Let's get you a new one.";
                        $maildata['url']        = $url;
                        $maildata['data']       = $mail_content; 
                        $maildata['email'] = $this->request->data['email']; 
                  			$maildata['subject'] = $project_name.': Reset Password';
                  		
                  			$this->Common->send_mail($maildata);                      
                      //  $email                  = new Email();
                        $status     		= "success";
                        $message    		= "email sending success";                        
                        /*$send_email             = $email->template("allmail')
                        ->emailFormat('html')
                        ->to($this->request->data['email'])
                        ->from([$FROM_EMAIL => $project_name])
                        ->subject('LiziCards: Reset Password')
                        ->viewVars($maildata)
                        ->send();
                        if ($send_email) {
                            $user       = $this->Users->newEntity();
                            $user       = $this->Users->patchEntity($check_email, ['reset_activation_key' => $activation_key]);
                            $this->Users->save($user);
                            $status     = "success";
                            $message    = 'An email has been sent to ' . $check_email["email"] . '. Kindly check your email and reset password';
                            
                        } else {
                           $message = "Something went wrong. Please try again later.";
                        }*/
                } else {
                    $message = "Email does not exists.";
                }   
                $this->set([
                'status'        => $status,
                'userData'      => [],                  
                'message'       => $message,
                '_serialize'    => ['status','userData','message']
                ]);   
                
            } catch (Exception $e) {                
                $this->set([
                'status'        => $status,
                'userData'      => [],                  
                'message'       => $e->getMessage(),
                '_serialize'    => ['status','userData','message']
                ]);
                
            }
       
            
        }break;
        
        default:{
            $this->set([
                'status'        => $status,
                'userData'      => [],                  
                'message'       => $message,
                '_serialize'    => ['status','userData','message']
            ]);
        }
        break;
}

}

public function view($id)
{
    $queryArr=$this->Common->getQueryStrings();   

    if($queryArr)
        extract($queryArr); 

    $data = [];
    $this->set([
        'data'=>$data,
        '_serialize' => ['data']
        ]);
}


public function edit($id)
{
    $this->loadModel('Users');
    $data = [];

    $this->set([
        'status' => 'success',
        'data' => $data,
        '_serialize' => ['message', 'errorType']
        ]);
}





public function delete($id)
{
    $this->loadModel('Users');
    $queryArr=$this->Common->getQueryStrings();        
    extract($queryArr);

    $messages = [];
    $this->set([
        'messages'   => $messages,
        '_serialize' => ['message']
        ]); 
}
}
