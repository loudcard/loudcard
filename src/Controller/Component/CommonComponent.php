<?php
namespace App\Controller\Component;
use Cake\Core\Configure;

use Cake\Controller\Component;
use Cake\Mailer\Email;
use Cake\Network\Http\Client;

// In a controller or table method.
use Cake\ORM\TableRegistry;

class CommonComponent extends Component
{
	public function getRandomString($count) {
        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $count);
        return $randomString;
    }

    public function sendAPIRequest($linkEnd, $postType, $dataArray=array())
    {
        // Send POST request to add the user data
        $http = new Client();           
        $link =  Configure::read('SITEURL').$linkEnd;
        if(!empty($dataArray)):
            $data = json_encode($dataArray);
        
        $response = $http->$postType($link, $data, ['type' => 'json']);
        else:
            $response = $http->$postType($link);
        endif;
        
        return $returnArrayBody = json_decode($response->body, true);
    }
    
    public function getQueryStrings(){
        $queryString = explode('?', $_SERVER['REQUEST_URI']);
        $queryString = array_filter($queryString);
        if(isset($queryString[1]) && !empty($queryString[1])){
            $queryString = explode('&', $queryString[1]);
            $queryArr=array();
            foreach ($queryString as $key=>$value){
                $valueArr = explode('=', $value);
                $queryArr[$valueArr[0]]=$valueArr[1];

            }
            return $queryArr;
        }
    }
    
    /****
     * Function to return data with respect to md5 format id
     * ****/
    public function getData($tablename,$id){

        $table_obj = TableRegistry::get($tablename);
        $userDetails = $table_obj->find('all')->where(['md5(md5(id))' => $id])->first();      
        return $userDetails;
    }
    

    public function send_mail($email_data) {
        // pr($email_data); die;
         $from_email = Configure::read('FROM_EMAIL');
        $project_name = Configure::read('project_name');
        $email = new Email();
        $mail = $email->template('allmail')
        ->emailFormat('html')
        ->to($email_data['email'])
        ->from([$from_email => $project_name])
        ->subject($email_data['subject'])
        ->viewVars($email_data)
        ->send();  
    }  

    public function send_mail_attchment($email_data){
                 $project_name = Configure::read('project_name');
                 $AdminEmail = Configure::read('AdminEmail');
                      $email = new Email();
                        $send_email = $email->template('allmail')
                        ->emailFormat('html')
                        ->to(array($email_data['email'],$email_data['from_email']))
                        ->from([$email_data['from_email'] => $project_name])
                        ->subject($email_data['subject'])
                        ->viewVars($email_data)
                        ->attachments([
                'invoice.pdf' => [
                'file' => $email_data['invoice'],
                ]])          
                        ->send();

            $email_audio_image = new Email();
            $email_audio_image->template('allmail')
            ->emailFormat('html')
            ->from([$email_data['email'] => $project_name])
            ->to($email_data['from_email'])
            ->subject('LiziCards Uploded Image and Audio')
            ->viewVars($email_data)
            ->attachments([
                $email_data['transaction_id'].'.pdf' => [
                'file' => $email_data['invoice'],
                ],
                $email_data['transaction_id'].'.jpg' => [
                'file' => $email_data['super_impose_image'],
                ],
                $email_data['transaction_id'].'.wav' => [
                'file' => $email_data['audio'],
                ]
                ])
            ->send();      
    }  
}
