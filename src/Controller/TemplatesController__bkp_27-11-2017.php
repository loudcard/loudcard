<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\Mailer\Email;
use App\Controller\AppController;
use Cake\Core\Exception\Exception;
// In a controller or table method.
use Cake\ORM\TableRegistry;
use Imagick;
use CropAvatar;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class TemplatesController extends AppController
{
    public $paginate = [
    // Other keys here.
    'maxLimit' => 20
    ];

    /*
    function to display the template at homepage
    */
    public function index()
    { 
        // if(!$this->Auth->user("id")){
            $this->request->session()->delete("GuestUser");
            $this->request->session()->delete("card_data");
        // }
        $page_number=1;
        if(!empty($this->request->query['page'])){
            $this->set(compact($this->request->query['page']));
        }

        $promotion_table = TableRegistry::get("promotional_code");
        $date = date("Y-m-d");
        $promotion = $promotion_table->find()->where(['status' => 1,'start_date <=' => $date, 'end_date >=' => $date])->hydrate(false)->first();      


        $this->loadModel("Tags");
        $tag_list = $this->Tags->find()->select(['id','name'])->where(['Tags.status' => 1])->hydrate(false)->toArray();           
        $templates = $this->Templates->find()->contain(['Tags'])->where(['Templates.status' => 1,'Tags.status' => 1,'Tags.name' => 'Christmas'])->order("Templates.id desc");
        if(!empty($this->request->session()->read("GuestUser.page_number"))){
             $this->request->session()->write('GuestUser.page_number',2);
        }
        $templates =  $this->paginate($templates);

        // $pagination =  $this->Paginator->request->params['paging']['Templates'];

        

        // pr($pagination);die();






        /*$count_total_templates = count($templates);
        $total_pages = ceil($count_total_templates/$per_page);
        // pr($templates);
        $folder_path = WWW_ROOT . 'img' . DS . 'templates' . DS . $templates[1]['templates'];
        $degrees = 90;

        $image_size = getimagesize($folder_path);
        $width = $image_size[0];
        $height = $image_size[1];

        if($width < $height){

        }else{
            header('Content-type: image/jpeg');
    //Create an image from our original JPEG image.
            $sourceCopy = imagecreatefromjpeg($folder_path);
    //Rotate this image by 90 degrees.
            $rotatedImg = imagerotate($sourceCopy, $degrees, 0);
    //Replace original image with rotated / landscape version.
            imagejpeg($rotatedImg, $folder_path);
        }*/
        $this->set(compact('templates','page_number','tag_list','total_pages','promotion'));

    }
    /*
    function to search the template
    */

    public function searchTemplate()
    {

        $templates = $this->Templates->find()->where(['Templates.name like' => '%'.$this->request->data["template_name"].'%'])->hydrate(false)->order("Templates.id desc")->toArray();
        $this->set(compact('templates'));

    }

    public function templateImages(){
        $this->loadModel("TemplatesImages");
        $templates_images= $this->TemplatesImages->find()->contain('Templates')->where(['TemplatesImages.template_id' => $this->request->data['id']])->hydrate(false)->toArray();     
        $this->set(compact('templates_images',$this->request->data['name'],$this->request->data['image_name']));
    }

    public function uploadImg(){
        $this->autoRender = false;
        // pr($this->request->data);
        if(!empty($this ->request->data['set_uer_image']) && count($this->request->data['set_uer_image']) > 0){
            $extension = array("jpg", "jpeg");
            try{
                if($this->request->data['set_uer_image']["size"] > 15000000) {
                    throw new Exception('Your picture must be less than 15 MB');
                }
                if (!empty($this->request->data['set_uer_image']['name'])) {
                    $image_name = $this->request->data['set_uer_image']['name'];
                    $remove_spaces_from_image = preg_replace("/[^A-Za-z0-9\_\-\.]/", '', $this->request->data['set_uer_image']['name']);
                    $file_name = time() . uniqid() . $remove_spaces_from_image;
                    $file_tmp = $this->request->data['set_uer_image']['tmp_name'];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $ext = strtolower($ext);
                    $path = WWW_ROOT . 'img' . DS . 'user_images' . DS . $file_name;
                    if (in_array($ext, $extension)) {
                        if (move_uploaded_file($file_tmp, $path)) {
                            $degrees = 90;
                            $image_size = getimagesize($path);
                            $width = $image_size[0];
                            $height = $image_size[1];
                            if($width < $height){
                            }else{
                                header('Content-type: image/jpeg');
                                //Create an image from our original JPEG image.
                                $sourceCopy = imagecreatefromjpeg($path);
                                //Rotate this image by 90 degrees.
                                $rotatedImg = imagerotate($sourceCopy, $degrees, 0);
                                //Replace original image with rotated / landscape version.
                                imagejpeg($rotatedImg, $path);
                            }
                            // if($width < 1622 || $height < 1217){
                            //     unlink($path);
                            //     throw new Exception('Your picture must have at least 1,622x1,217 pixels.');
                            // }
                            // $resource = new Imagick($path);
                            // $imageResolution = $resource->getImageResolution();
                            // if($imageResolution['x'] < 1622 || $imageResolution['y'] < 1217){
                            //     throw new Exception('Your picture must have at least 1,622x1,217 pixels.');
                            // }
                            $this->loadModel("TemplatesImages");
                            $update_data = $this->TemplatesImages->query();
                            $res = $update_data->update()
                            ->set(['user_upload' => $file_name])
                            ->where(['id' => $this->request->data['id']])->execute();
                            // $this->TemplatesImages->save($save_data);
                            $result['status'] = "success";
                            $result['image_name'] = $file_name;                            
                            $result['dimension'] = "addclass";                           
                            echo json_encode($result);
                            exit();
                        } else {
                            throw new Exception('Server error.Image Not uploaded.Please try again after some time');
                        }
                    } else {
                        throw new Exception('Picture must be JPG or JPEG.');
                    }
                }      

            }catch (Exception $ex) {
                $result['status'] = "error";
                $result['msg'] = $ex->getMessage();
                echo json_encode($result);
                exit();
            }
            
            // pr($this->request->data['set_uer_image']);
        }
    }

    public function showUploadButton(){
        $this->autoRender = false;
        $this->loadModel("TemplatesImages");
        $get_data = $this->TemplatesImages->find()->where(['id' => $this->request->data['id']])->hydrate(false)->first();

        if($get_data['show_preview'] == 1){           
            $response['show'] = "success";  
            $response['preview'] = $get_data['show_preview'];         
        }else{
            $response['show'] = "error";
            $response['preview'] = $get_data['show_preview'];
        }
        echo json_encode($response);
    }

    /*
    Function to show card by tags
    */
    public function showCards(){

        $templates = $this->Templates->find()->contain(['Tags'])->where(['Templates.tag_id' => $this->request->data['id'],'Templates.status' => 1])->order("Templates.id desc")->hydrate(false)->toArray(); 

        $this->set(compact('templates'));

    }

    /*
//     Function to show card by tags
//     */
    public function viewDetail(){

        $this->loadModel("TemplatesImages");
        $this->loadModel("Tags");
        //['Templates.id','Templates.name','Templates.templates','Templates.price','TemplatesImages.id','TemplatesImages.template_id','TemplatesImages.images','TemplatesImages.user_upload','TemplatesImages.show_preview','Tags.id','Tags.name']

        $templates_images = $this->Templates->find('all')->contain(['TemplatesImages','Tags'])->where(['Templates.id' => $this->request->data['id'],'Templates.status' => 1])->hydrate(false)->first();     
        // pr($templates_images); die;
        $this->set(compact('templates_images',$this->request->data['data_counter']));


    }

    /*
    Function to upload user 
    */

    public function uploadPhoto($id = null) {      
        // pr($this->request->session()->read());
        /* creating new temporary session if not exists */
	if(empty($this->request->session()->read("GuestUser.page_number"))){
            $this->request->session()->write("GuestUser.page_number",2);
        }
        $session = $this->request->session();
        if ($session->check('GuestUser.unique_id')) {
            $session->write('GuestUser.template_id', $id);
        }else {
            $unique_session_id = md5(time().$this->Common->getRandomString(5));
            $session->write('GuestUser.unique_id', $unique_session_id);
            $session->write('GuestUser.template_id', $id);
        }

        $this->request->session()->write("GuestUser.template_id",$id);
        
        //pr($session->read('GuestUser'));

        $page_number=2;
        
        $this->viewBuilder()->layout("inner_page"); 


        $templates_images = $this->Templates->find('all')->contain(['TemplatesImages','Tags'])->where(['Templates.id' => $id,'Templates.status' => 1])->hydrate(false)->first(); 
        

        $countries = TableRegistry::get("countries");       
        $country_list = $countries->find("list", ['keyField' => 'id','valueField' => 'name'])->order('name')->toArray();

        $state = TableRegistry::get("states");
        $state_list = array();
            $get_states = $state->find("all")->where(['country_id' => 231])->order('name')->toArray();
            foreach ($get_states as $stlist):
                $id = $stlist['id'];
                $title = $stlist["state_abbrivation"]." - ". $stlist['name'];
                $state_list[$id] = $title;
            endforeach;
        // $state_list = $state->find("list", ['keyField' => 'id','valueField' => 'name'])->where(['country_id' => 231])->order('name')->toArray();

        $city = TableRegistry::get("cities");
        $city_list = $city->find("list", ['keyField' => 'id','valueField' => 'name'])->where(['state_id' => 3919])->order('name')->toArray();

        

        $this->request->session()->write("GuestUser.template_name",$templates_images['name']);
        $this->request->session()->write("GuestUser.tag_name",$templates_images['tag']['name']);
        $this->request->session()->write("GuestUser.price",$templates_images['price']);


        if($this->request->is("Ajax")){
	ini_set('memory_limit', '-1');

            if(!empty($this ->request->data['avatar_file']) && count($this->request->data['avatar_file']) > 0) {
                $extension = array("jpg","jpeg");
                try{
                    if($this->request->data['avatar_file']["size"] > 15000000) {
                        throw new Exception('Your picture must be less than 15 MB');
                    }
                    if (!empty($this->request->data['avatar_file']['name'])) {
                        $image_name = $this->request->data['avatar_file']['name'];
                        $remove_spaces_from_image = preg_replace("/[^A-Za-z0-9\_\-\.]/", '', $this->request->data['avatar_file']['name']);
                        $file_name = time() . uniqid() . $remove_spaces_from_image;
                        $crop_name = uniqid() . time() . $remove_spaces_from_image;
                        $file_tmp = $this->request->data['avatar_file']['tmp_name'];
                        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                        $ext = strtolower($ext);
                        $path = WWW_ROOT . 'img' . DS . 'user_images' . DS . $file_name;
                        if (in_array($ext, $extension)) {
                            if (move_uploaded_file($file_tmp, $path)) {
                               $crop_url = WWW_ROOT . 'img' . DS . 'user_images' . DS . $crop_name;
                               $src_img = imagecreatefromjpeg($path);

                               $avtar_data = (array)$_POST['avatar_data'];
                               $data = json_decode($avtar_data[0]);

                                // pr($myimage_manuf->x); 
                               $size = getimagesize($path);
                            $size_w = $size[0]; // natural width
                            $size_h = $size[1]; // natural height

                            $max_width_fourth_case =  1440;
                              $max_height_fourth_case = 2076;

                              $max_width_third_case =  1080;
                              $max_height_third_case = 1557;

                              $max_width_second_case =  720;
                              $max_height_second_case = 1038;

                              $max_width =  360;
                              $max_height = 519;
                              

                            if($size_w > $max_width_third_case && $size_h > $max_height_third_case) {
                                $img_width = $max_width_fourth_case;
                                $img_height = $max_height_fourth_case;
                            }

                            else if(($size_h < $max_height_fourth_case && $size_h > $max_height_third_case) && ($size_w >= $max_width_third_case && $size_w < $max_width_fourth_case)) {
                                $img_width = $max_width_third_case;
                                $img_height = $max_height_third_case;
                            }
                            else if(($size_h < $max_height_third_case && $size_h > $max_height_second_case) && ($size_w > $max_width_second_case && $size_w < $max_width_third_case)) {

                                $img_width = $max_width_second_case;
                                $img_height = $max_height_second_case;
                            }

                            else if(($size_h > $max_height && $size_h < $max_height_second_case) && ($size_w > $max_width && $size_w < $max_width_second_case)) {

                                $img_width = $max_width;
                                $img_height = $max_height;
                            }
                           
                            else{
                                $img_width = 360;
                                $img_height = 519;
                            }

                            // $aspect_ratio = $size_w/$size_h;
                            // echo $aspect_ratio; die;
                            // if($size_w < 1622 || $size_h < 1217){
                            //     // unlink($path);
                            //     throw new Exception('Your picture must have at least 1,622 x 1,217 pixels.');
                            // }
                            $src_img_w = $size_w;
                            $src_img_h = $size_h;
                            // $degrees = ($src_img_w > $src_img_h)?90:$data->rotate;
                            $degrees = $data->rotate;
                            // Rotate the source image
                            if (is_numeric($degrees) && $degrees != 0) {
        // PHP's degrees is opposite to CSS's degrees
                                $new_img = imagerotate( $src_img, -$degrees, imagecolorallocatealpha($src_img, 0, 0, 0, 127) );

                                imagedestroy($src_img);
                                $src_img = $new_img;

                                $deg = abs($degrees) % 180;
                                $arc = ($deg > 90 ? (180 - $deg) : $deg) * M_PI / 180;

                                $src_img_w = $size_w * cos($arc) + $size_h * sin($arc);
                                $src_img_h = $size_w * sin($arc) + $size_h * cos($arc);

                        // Fix rotated image miss 1px issue when degrees < 0
                                $src_img_w -= 1;
                                $src_img_h -= 1;
                            }

                            $tmp_img_w = $data->width;
                            $tmp_img_h = $data->height;

                            // $dst_img_w = 1080;
                            // $dst_img_h = 1557;
                            $dst_img_w = $img_width;
                            $dst_img_h = $img_height;

                            $src_x = $data->x;
                            $src_y = $data->y;

                            if ($src_x <= -$tmp_img_w || $src_x > $src_img_w) {
                                $src_x = $src_w = $dst_x = $dst_w = 0;
                            } else if ($src_x <= 0) {
                                $dst_x = -$src_x;
                                $src_x = 0;
                                $src_w = $dst_w = min($src_img_w, $tmp_img_w + $src_x);
                            } else if ($src_x <= $src_img_w) {
                                $dst_x = 0;
                                $src_w = $dst_w = min($tmp_img_w, $src_img_w - $src_x);
                            }

                            if ($src_w <= 0 || $src_y <= -$tmp_img_h || $src_y > $src_img_h) {
                                $src_y = $src_h = $dst_y = $dst_h = 0;
                            } else if ($src_y <= 0) {
                                $dst_y = -$src_y;
                                $src_y = 0;
                                $src_h = $dst_h = min($src_img_h, $tmp_img_h + $src_y);
                            } else if ($src_y <= $src_img_h) {
                                $dst_y = 0;
                                $src_h = $dst_h = min($tmp_img_h, $src_img_h - $src_y);
                            }

      // Scale to destination position and size
                            // $ratio = $tmp_img_w / $dst_img_w;
                            $ratio = $tmp_img_w / $tmp_img_h;
                            $dst_x /= $ratio;
                            $dst_y /= $ratio;
                            $dst_w /= $ratio;
                            $dst_h /= $ratio;

                            $dst_w = $img_width;
                            $dst_h = $img_height;

                            $dst_img = imagecreatetruecolor($dst_img_w, $dst_img_h);

      // Add transparent background to destination image
                            imagefill($dst_img, 0, 0, imagecolorallocatealpha($dst_img, 0, 0, 0, 127));
                            imagesavealpha($dst_img, true);

                            $result = imagecopyresampled($dst_img, $src_img, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
                            imagejpeg($dst_img, $crop_url, 100);
                            imagedestroy($src_img);
                            imagedestroy($dst_img);

                            unlink($path);

                            $session->write('GuestUser.image_name', $crop_name);

                            /*$templates_table = TableRegistry::get("Templates");
                            $template_data = $templates_table->get($this->request->data['card_id']);
                            $template_data->user_upload = $crop_name;
                            $templates_table->save($template_data);*/

                            // pr($templates_table); die;
                            
                            // $new_width = round($myimage_manuf->width);
                            // echo $new_width;
                            // $new_height = round($myimage_manuf->height);
                            // $x_axis = round($myimage_manuf->x);
                            // $y_axis = round($myimage_manuf->y);
                            // $crop_url = WWW_ROOT . 'img' . DS . 'user_images' . DS . $crop_name.$this->request->data['avatar_file']['name'];
                            //  $result = imagecopyresampled($dst_img, $src_img, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
                            // $truecolor = imagecreatetruecolor($new_width, $new_height);
                            // imagecopyresampled($truecolor, $newfile, 0, 0, $x_axis,$y_axis,$new_width,$new_height);
                            // imagejpeg($truecolor,$crop_url,100);

                            // if($width < $height){
                            // }else{
                            //     header('Content-type: image/jpeg');
                            //     //Create an image from our original JPEG image.
                            //     $sourceCopy = imagecreatefromjpeg($path);
                            //     //Rotate this image by 90 degrees.
                            //     $rotatedImg = imagerotate($sourceCopy, $myimage_manuf->, 0);
                            //     //Replace original image with rotated / landscape version.
                            //     imagejpeg($rotatedImg, $path);
                            // }
                            // if($width < 1622 || $height < 1217){
                            //     unlink($path);
                            //     throw new Exception('Your picture must have at least 1,622x1,217 pixels.');
                            // }
                            // $resource = new Imagick($path);
                            // $imageResolution = $resource->getImageResolution();
                            // if($imageResolution['x'] < 1622 || $imageResolution['y'] < 1217){
                            //     throw new Exception('Your picture must have at least 1,622x1,217 pixels.');
                            // }
                            // $this->loadModel("TemplatesImages");
                            // $update_data = $this->TemplatesImages->query();
                            // $res = $update_data->update()
                            // ->set(['user_upload' => $file_name])
                            // ->where(['id' => $this->request->data['id']])->execute();
                            // $this->TemplatesImages->save($save_data);
                            //mytestcode
                            $status['status'] = "success";
                            $status['image_name'] = $crop_name;                            
                            echo json_encode($status);
                            exit();

                        } else {
                            throw new Exception('Server error.Image Not uploaded.Please try again after some time');
                        }
                    } else {
                        throw new Exception('Picture must be JPG or JPEG.');
                    }
                }      

            }catch (Exception $ex) {
                $status['status'] = "error";
                $status['msg'] = $ex->getMessage();
                echo json_encode($status);
                exit();

            }
            
            // pr($this->request->data['set_uer_image']);
        }
        // pr($this->request->data);
        //    //pr($_FILES['avatar_file']); 
        //    //pr($_POST['avatar_src']);
        // $myimage_manu = (array)$_POST['avatar_data'];
        // $myimage_manuf = json_decode($myimage_manu[0]);


        // pr($myimage_manuf->x); 
           //echo ($myimage_manu->x);

            // $response = array(
            //   'state'  => 200,
            //   'message' => $crop -> getMsg(),
            //   'result' => $crop -> getResult()
            //   );

            // echo json_encode($response);
    }

    $this->set(compact('templates_images','page_number',"country_list",'state_list','id','city_list'));
}
public function recordAudio(){
        // pr($this->request->data); die;
    $page_number=$this->request->data['page_number'];

        //$this->viewBuilder()->layout("audio_page");


    $templates_images = $this->Templates->find('all')->contain(['TemplatesImages','Tags'])->where(['Templates.id' => $this->request->data['id'],'Templates.status' => 1])->hydrate(false)->first(); 
    if(empty($templates_images['user_upload']))
    {
       $message = 'Please upload your picture';
       $this->Flash->error(__($message));

   }
   $this->set(compact('templates_images','page_number'));
}

public function save($id = null){
   
    if(!empty($this->request->data['audio-filename']) && !empty($this->request->data['audio-blob']['name'])){

        $file_name = $this->request->data['audio-filename'];
        $file_tmp = $this->request->data['audio-blob']['tmp_name'];
        $path = WWW_ROOT . 'img' . DS . 'user_audio' . DS . $file_name;
        if (move_uploaded_file($file_tmp, $path)) {
            $this->request->session()->write('GuestUser.audio_name', $file_name);
            // $email = new Email('default');
            // $email->from(['ljaswal@teqmavens.com' => 'LiziCards'])
            // ->to('ljaswal@teqmavens.com')
            // ->subject('LiziCards Uploded Image and')
            // ->attachments([
            //     'photo.jpg' => [
            //     'file' => WWW_ROOT . 'img/user_images/' . $this->request->session()->read('GuestUser.image_name'),
            //     ],
            //     'audio.wav' => [
            //     'file' => WWW_ROOT . 'img/user_audio/' . $this->request->session()->read('GuestUser.audio_name'),
            //     ]
            //     ])
            // ->send('My message');
            $this->request->session()->write("GuestUser.template_id",$id);
            // pr($this->request->session()->read("GuestUser")); die;
            /*$templates_table = TableRegistry::get("Templates");
            $template_data = $templates_table->get($id);
            $template_data->audio_message = $file_name;
            $templates_table->save($template_data);*/
            exit();
        }


    }
}

public function setpageNumber(){
    clearstatcache();    
    $SITEURL = Configure::read('SITEURL'); 
    if(!empty($this->request->data['next_btn'])){
        if(!empty($this->request->data['type'])){
            $this->request->session()->write("type",$this->request->data['type']);
            $result['type'] = $this->request->session()->read("type");
	    $result['user_first_name'] = $this->Auth->user("first_name");
            $result['user_last_name'] = $this->Auth->user("last_name");
        }
        if(!empty($this->request->data['form_type'])){
            $this->request->session()->write("form_type",$this->request->data['form_type']);
            $result['form_type'] = $this->request->session()->read("form_type");
        }
        if($this->request->data['page_number'] == 2){
             $result['audio'] = !empty($this->request->session()->read('GuestUser.audio_name'))?"yes":"no";  
            
          if(empty($this->request->session()->read('GuestUser.image_name'))){
            $result['status'] = "error";
            $result['msg'] = "Please upload image"; 
            echo json_encode($result);
            exit();
        } 
    } 

    if($this->request->data['page_number'] == 3){  
        if(!empty($this->request->session()->read('GuestUser.audio_name'))){
            $user_audio   = 'img/user_audio/'.$this->request->session()->read('GuestUser.audio_name'); 
            $result['media'] = '<audio preload="auto" src='.$SITEURL.$user_audio.' controls=""></audio>';
        }
      if(empty($this->request->session()->read('GuestUser.audio_name'))){
        $result['status'] = "error";
        $result['msg'] = "Please upload audio message";

    } 
    

} 

 if($this->request->data['page_number'] == 4){  
        if(!empty($this->request->session()->read('GuestUser.audio_name'))){
            $user_audio   = 'img/user_audio/'.$this->request->session()->read('GuestUser.audio_name'); 
            $result['media'] = '<audio preload="auto" src='.$SITEURL.$user_audio.' controls=""></audio>';
        }
      if(empty($this->request->session()->read('GuestUser.audio_name'))){
        $result['status'] = "error";
        $result['msg'] = "Please upload audio message";

    } 
    if(!empty($this->Auth->user("id"))){
        $this->loadModel("Orders");
        $this->loadModel('UsersImages');
        $this->loadModel('UsersAudio');
       $this->loadModel("Templates");
                        $get_price = $this->Templates->find()->where(['id' => $this->request->session()->read('GuestUser.template_id')])->first();
        // pr($this->request->session()->read());
        // $previous_order_status = $this->Orders->find()->where(['template_id' => $this->request->session()->read('GuestUser.template_id'),'user_id' => $this->Auth->user("id"),'status' => 'pending'])->count();
        // pr($this->request->session()->read());
        $previous_order_status = $this->Orders->find()->where(['id' => $this->request->session()->read('GuestUser.order_id'),'user_id' => $this->Auth->user("id"),'status' => 'pending'])->first();
        if(count($previous_order_status)==0){

            $user_file_entity = $this->UsersImages->newEntity();
            $user_file_entity->user_id = $this->Auth->User("id");
            $user_file_entity->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";
            $user_file_entity->user_image = !empty($this->request->session()->read('GuestUser.image_name'))?$this->request->session()->read('GuestUser.image_name'):"";                        
            $save_user_image = $this->UsersImages->save($user_file_entity); 
            $user_image_id = $save_user_image->id;
            $this->request->session()->write("GuestUser.user_image_id",$user_image_id);
            $user_audio_entity = $this->UsersAudio->newEntity();
            $user_audio_entity->user_id = $this->Auth->User("id");
            $user_audio_entity->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";                       
            $user_audio_entity->user_audio = !empty($this->request->session()->read('GuestUser.audio_name'))?$this->request->session()->read('GuestUser.audio_name'):"";
            $save_audio = $this->UsersAudio->save($user_audio_entity);
            $audio_id = $save_audio->id;
            $this->request->session()->write("GuestUser.audio_id",$audio_id);
            $this->loadModel("Orders");
            $Orders_table = $this->Orders->newEntity();
            $Orders_table->user_id = $this->Auth->User("id");
            $Orders_table->template_id = !empty($this->request->session()->read('GuestUser.template_id'))?$this->request->session()->read('GuestUser.template_id'):"";
            $Orders_table->user_image_id = $user_image_id;
            $Orders_table->user_audio_id = $audio_id;
            $Orders_table->price = $get_price['price'];
            $Orders_table->created_on = date("Y-m-d H:i:s");
            $save_order = $this->Orders->save($Orders_table);
            $order_id = $save_order->id;
            $this->request->session()->write("GuestUser.order_id",$order_id);
        }else{

            $update_data = $this->Orders->query();
            $res = $update_data->update()
            ->set(['template_id' => $this->request->session()->read('GuestUser.template_id'),'price' => $get_price['price']])
            ->where(['id' => $this->request->session()->read('GuestUser.order_id')])->execute();

            $update_images = $this->UsersImages->query();
            $res_img = $update_images->update()
            ->set(['user_image' => $this->request->session()->read('GuestUser.image_name'),'template_id' => $this->request->session()->read('GuestUser.template_id')])
            ->where(['id' => $previous_order_status['user_image_id']])->execute();

            $update_audio = $this->UsersAudio->query();
            $res_audio = $update_audio->update()
            ->set(['user_audio' => $this->request->session()->read('GuestUser.audio_name'),'template_id' => $this->request->session()->read('GuestUser.template_id')])
            ->where(['id' => $previous_order_status['user_audio_id']])->execute();

        }
    } 

} 

if($this->request->data['page_number'] == 5){
  if(empty($this->Auth->User("id"))) {
    $result['status'] = "error";
    $result['msg'] = ($this->request->session()->read("form_type")=="sign_up")?"Oops! Please do registration first.":"Oops! Please do login first.";  
    echo json_encode($result);
    exit();
} 
} 

$page_number = $this->request->data['page_number']+1;
$result['form_type'] = $this->request->session()->read("form_type");
$this->request->session()->write('GuestUser.page_number',$page_number);
$result['page_number'] = $this->request->session()->read('GuestUser.page_number');
if(!empty($this->Auth->User("id")) && $this->request->data['page_number'] == 4){        
    $this->request->session()->write('GuestUser.page_number',$page_number+1); 
    $result['page_number'] = $this->request->session()->read('GuestUser.page_number');
}


}


if(!empty($this->request->data['prev_btn'])){          
    $page_number = $this->request->data['page_number']-1;
    $this->request->session()->write('GuestUser.page_number',$page_number);
    $result['page_number'] = $this->request->session()->read('GuestUser.page_number');

    if(!empty($this->Auth->User("id")) && $this->request->data['page_number'] == 6){         
        $this->request->session()->write('GuestUser.page_number',$page_number-1);    
        $result['page_number'] = $this->request->session()->read('GuestUser.page_number');
        $result['logged_in'] = "yes";
    }

    if(!empty($this->Auth->User("id")) && $this->request->data['page_number'] == 6){ 
        $this->request->session()->delete("type");  
    }
}

echo json_encode($result);
exit();
}

public function showAudioRecorded(){

}
public function showAudioWithoutPlaybtn(){

}
public function showAudioRecordedPreview(){

}

public function showSampleVideos(){

}

}
