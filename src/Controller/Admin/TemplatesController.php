<?php
namespace App\Controller\Admin;
use Cake\Core\Configure;
use App\Controller\AppController;
use Cake\Core\Exception\Exception;
use Cake\Event\Event;
use Cake\Mailer\Email;
// In a controller or table method.
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

class TemplatesController extends AppController {


	public function index() {
		$this->redirect('/admin/templates/manage');
	}


/*
   Function to manage users
    */
   Public function manage(){
     $this->viewBuilder()->layout("admin_inner");
     $project_name = Configure::read('project_name');
     $pageDescription = 'Manage templates | ' . $project_name;

     $templates = $this->Templates->find()->contain(['Tags'])->where(['Templates.status' => 1,'Tags.status' => 1])->hydrate(false)->order("Templates.id desc")->toArray();       
     $this->set(compact('pageDescription','templates'));
   }


   public function add(){
    $this->viewBuilder()->layout("admin_inner");
    $project_name = Configure::read('project_name');
    $pageDescription = 'Add template | ' . $project_name;

    $tag = TableRegistry::get("tags");       
    $tag_list = $tag->find("list", ['keyField' => 'id','valueField' => 'name'])->toArray();
    $this->set(compact('tag_list','pageDescription'));
    if($this->request->is("post")){
     try{
            // pr($this->request->data); die;
       if(empty($this->request->data['code'])){
        throw new Exception("Please enter template unique code");            
      }
       if(empty($this->request->data['name'])){
        throw new Exception("Please enter template name");            
      }
      if(empty($this->request->data['price'])){
        throw new Exception("Please enter template price");            
      }

      if(empty($this->request->data['templates']['name'])){
        throw new Exception("Please upload template image");            
      }
      $check_template_code = $this->Templates->find()->where(['code' => $this->request->data['code']])->count();
      if($check_template_code > 0){
        throw new Exception('Template code already exists');
      }

      $extension = array("jpg", "jpeg");
      if($this->request->data['templates']["size"] > 15000000) {
        throw new Exception('Template must be less than 15 MB');
      }
                // echo "hello"; die;
      if (!empty($this->request->data['templates']['name'])) {
        $image_name = $this->request->data['templates']['name'];
        $remove_spaces_from_image = preg_replace("/[^A-Za-z0-9\_\-\.]/", '', $this->request->data['templates']['name']);
        $file_name = time() . uniqid() . $remove_spaces_from_image;
        $file_tmp = $this->request->data['templates']['tmp_name'];
        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
        $ext = strtolower($ext);
        $path = WWW_ROOT . 'img' . DS . 'templates' . DS . $file_name;

        if(!isset($this->request->data['images'])){
          if (in_array($ext, $extension)) {
            if (move_uploaded_file($file_tmp, $path)) {
              $this->request->data['templates'] = $file_name;
              $this->request->data['created_on'] = date("Y-m-d H:i:s");
              $this->request->data['modified_on'] = date("Y-m-d H:i:s");
              $this->loadModel("Templates");
              $newEntity = $this->Templates->newEntity($this->request->data);
              $save = $this->Templates->save($newEntity);
            }else{
             throw new Exception('Server error.Image Not uploaded.Please try again after some time');
           }
         }else{
          throw new Exception('Picture must be JPG or JPEG.');
        }
        $result['status'] = "success";
        $result['msg']= "Template added successfully";
        echo json_encode($result);
      }

      if(isset($this->request->data['images'])){

        $i=1;
        foreach ($this->request->data['images'] as $key => $value) {
          if(!empty($this->request->data['show_preview'][$key])){
            if(empty($value['name'])){
              throw new Exception("Please upload image required to customize", 1);
            }

          }
          if($value['size'] > 15000000){
            throw new Exception("Image must be less than 15 MB", 1);                            
          }
          if(!isset($this->request->data['show_preview'])){
            throw new Exception("Please selct image to customize", 1);   
          }

          if($i==1){
            move_uploaded_file($file_tmp, $path);
            $this->request->data['templates'] = $file_name;
            $this->request->data['created_on'] = date("Y-m-d H:i:s");
            $this->request->data['modified_on'] = date("Y-m-d H:i:s");
            $this->loadModel("Templates");
            $newEntity = $this->Templates->newEntity($this->request->data);
            $save = $this->Templates->save($newEntity);
          }


          $template_image_name = $value['name'];
          $remove_spaces_from_template_image_name = preg_replace("/[^A-Za-z0-9\_\-\.]/", '', $value['name']);
          $template_file_name = time() . uniqid() . $remove_spaces_from_template_image_name;
          $template_file_tmp = $value['tmp_name'];
          $extn = pathinfo($template_file_name, PATHINFO_EXTENSION);
          $exten = strtolower($extn);
          $image_path = WWW_ROOT . 'img' . DS . 'template_images' . DS . $template_file_name;
          if (in_array($exten, $extension)) {

            if(move_uploaded_file($template_file_tmp, $image_path)){
             $template_images = TableRegistry::get("templates_images");
             $image_newEntity = $template_images->newEntity();
             $image_newEntity->template_id = $save->id;
             $image_newEntity->images = $template_file_name;
             $image_newEntity->show_preview = (!empty($this->request->data['show_preview'][$key]))?$this->request->data['show_preview'][$key]:0;
             $image_newEntity->created_on = date("Y-m-d H:i:s");
             $image_newEntity->modified_on = date("Y-m-d H:i:s");
             $template_images->save($image_newEntity);                          

           }

         }
         $i++; }
         $result['status'] = "success";
         $result['msg']= "Template added successfully";
         echo json_encode($result);
       }

     }



   }catch (Exception $ex) {
    $result['status'] = "error";
    $result["msg"] = $ex->getMessage();
    echo json_encode($result);
  }
  exit();
}
}

public function checkcodeuniqueness() {
        $this->autoRender = false;

        if (!empty($this->request->query['code'])) {
            $query = $this->Templates->find()->where(['code' => $this->request->query['code']])->count();
            if ($query > 0) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }

public function addMoreImages(){
  // PR($this->request->data); die;
  $this->set('count',$this->request->data['counter']);

}



/*
   Function to add users
    */
   Public function edit($id = null){
     $this->viewBuilder()->layout("admin_inner");

     $project_name = Configure::read('project_name');
     $pageDescription = 'Edit users | ' . $project_name;

     $this->set(compact('pageDescription','id'));
     if(!empty($id)){

      $get_template_data = $this->Templates->get($id);


      $check_id = $this->Templates->exists(['id' => $id]);

            // $check_id = $this->Users->find()->where(['id' => $id])->first();  

      if(!$check_id){                
        $message = 'Wrong template ID passed';
        $this->Flash->error(__($message));
        return $this->redirect("/admin/templates/manage");
        die;
      }
    }
    $tag = TableRegistry::get("tags");       
    $tag_list = $tag->find("list", ['keyField' => 'id','valueField' => 'name'])->toArray();
    $templates = $this->Templates->find()->contain(['Tags'])->where(['Templates.id' => $id,])->hydrate(false)->first();
    $this->loadModel("TemplatesImages");
    $templates_images= $this->TemplatesImages->find()->contain('Templates')->where(['TemplatesImages.template_id' => $id])->hydrate(false)->toArray();     
    
    if($this->request->is("post") || $this->request->is('put')){
      try{
         $get_template_data = $this->Templates->get($this->request->data['id']);
 // pr($this->request->data); die;
           // pr($this->request->data); die;
        if(empty($this->request->data['code'])){
        throw new Exception("Please enter template unique code");            
      } 
       if(empty($this->request->data['name'])){
        throw new Exception("Please enter template name");            
      }
      if(empty($this->request->data['price'])){
        throw new Exception("Please enter template price");            
      }

   

       if(!isset($this->request->data['images'])){
           if(!empty($this->request->data['templates']['name'])){
         // echo "hello"; die;
        if($this->request->data['templates']["size"] > 15000000) {
          throw new Exception('Template must be less than 15 MB');
        }

        $image_name = $this->request->data['templates']['name'];
        $remove_spaces_from_image = preg_replace("/[^A-Za-z0-9\_\-\.]/", '', $this->request->data['templates']['name']);
        $file_name = time() . uniqid() . $remove_spaces_from_image;
        $file_tmp = $this->request->data['templates']['tmp_name'];
        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
        $ext = strtolower($ext);
        $path = WWW_ROOT . 'img' . DS . 'templates' . DS . $file_name;
        $extension = array("jpg", "jpeg");
        if (in_array($ext, $extension)) {

         // unlink(WWW_ROOT . 'img' . DS . 'templates' . DS . $get_template_data['templates']);   
         if (move_uploaded_file($file_tmp, $path)) {
          $this->request->data['templates'] = $file_name;                 
                   // echo "hello"; die;
        } }else{
        throw new Exception('Picture must be JPG or JPEG.');
      }
             
}else{
        unset($this->request->data['templates']);
      }
        
        $this->request->data['modified_on'] = date("Y-m-d H:i:s");
        $template_data = $this->Templates->patchEntity($get_template_data,$this->request->data);
        if($this->Templates->save($template_data)){               
          $message = "Template updated successfully";
          $this->Flash->success($message);
        }
     
       $result['status'] = "success";
        $result['msg']= "Template updated successfully";
        echo json_encode($result);
    }



    if(isset($this->request->data['images'])){
     // pr($this->request->data);
      $i=1;
      $extension = array("jpg", "jpeg");

          if(!empty($this->request->data['templates']['name'])){
         // echo "hello"; die;
        if($this->request->data['templates']["size"] > 15000000) {
          throw new Exception('Template must be less than 15 MB');
        }
         $image_name = $this->request->data['templates']['name'];
         $remove_spaces_from_image = preg_replace("/[^A-Za-z0-9\_\-\.]/", '', $this->request->data['templates']['name']);
         $file_name = time() . uniqid() . $remove_spaces_from_image;
         $file_tmp = $this->request->data['templates']['tmp_name'];
         $ext = pathinfo($file_name, PATHINFO_EXTENSION);
         $ext = strtolower($ext);
         $path = WWW_ROOT . 'img' . DS . 'templates' . DS . $file_name;
         

         if (in_array($ext, $extension)) {

           // unlink(WWW_ROOT . 'img' . DS . 'templates' . DS . $get_template_data['templates']);   
           if (move_uploaded_file($file_tmp, $path)) {
            $this->request->data['templates'] = $file_name;                 
                   // echo "hello"; die;
          }
          
        }else{
          throw new Exception('Picture must be JPG or JPEG.');
        }
      }else{
        unset($this->request->data['templates']);
      }
      // echo "hello"; die;
          $this->request->data['cover_text'] = $this->request->data['cover_text'];
          $this->request->data['inside_text'] = $this->request->data['inside_text'];
          $this->request->data['card_details'] = $this->request->data['card_details'];
          $this->request->data['modified_on'] = date("Y-m-d H:i:s");
          // pr($get_template_data); die;
          $template_data = $this->Templates->patchEntity($get_template_data,$this->request->data);
          $this->Templates->save($template_data);              
            // $message = "Template updated successfully";
            // $this->Flash->success($message);
         



      foreach ($this->request->data['images'] as $key => $value) {

        
      $template_images = TableRegistry::get("templates_images");
      if(!empty($this->request->data['show_preview'][$key])){
        $get_show_preview = array_intersect_key($this->request->data['tempid'], $this->request->data['show_preview']);
       $show_preview=array_values($get_show_preview);
       // echo $show_preview; die;
        $update_data = $template_images->query();
        $change_preview_to_zero = $update_data->update()
            ->set(['show_preview' => 0])->where(['template_id' => $this->request->data['id']])->execute(); 
            $res = $update_data->update()
            ->set(['show_preview' => 1])->where(['id' => $show_preview[0]])->execute(); 

      }

      if($value['size'] > 15000000){
        throw new Exception("Image must be less than 15 MB", 1);                            
      }
      if(!isset($this->request->data['show_preview'])){
        throw new Exception("Please selct image to customize", 1);   
      }

       

       

        if(!empty($value['name'])){  

         $template_image_name = $value['name'];
         $remove_spaces_from_template_image_name = preg_replace("/[^A-Za-z0-9\_\-\.]/", '', $value['name']);
         $template_file_name = time() . uniqid() . $remove_spaces_from_template_image_name;
         $template_file_tmp = $value['tmp_name'];
         $extn = pathinfo($template_file_name, PATHINFO_EXTENSION);
         $exten = strtolower($extn);
          if (in_array($exten, $extension)) {
         $image_path = WWW_ROOT . 'img' . DS . 'template_images' . DS . $template_file_name;
         move_uploaded_file($template_file_tmp, $image_path);
           $check_id = $template_images->find()->where(['id' => $this->request->data['tempid'][$key]])->count();
           if($check_id>0){
            
            $modified_on = date("Y-m-d H:i:s");
            $update_data = $template_images->query();
            $res = $update_data->update()
            ->set(['images' => $template_file_name,'modified_on' => $modified_on])->where(['template_id' => $this->request->data['id'],'id' => $this->request->data['tempid'][$key]])->execute();
          }else{
         
            $insert_data = $template_images->newEntity();
            $insert_data->template_id = $this->request->data['id'];
            $insert_data->images = $template_file_name;
            $insert_data->show_preview = (!empty($this->request->data['show_preview'][$key]))?$this->request->data['show_preview'][$key]:0;
            $insert_data->created_on = date("Y-m-d H:i:s");
            $insert_data->modified_on = date("Y-m-d H:i:s");
            $template_images->save($insert_data);
          }
        }


      }
      $i++; }
      $result['status'] = "success";
         $result['msg']= "Template updated successfully";
         echo json_encode($result);
      
    }





    // $this->redirect('/admin/templates/manage');


  }catch (Exception $ex) {
   $result['status'] = "error";
    $result["msg"] = $ex->getMessage();
    echo json_encode($result);
 }
 exit();
}
$this->set(compact('tag_list','pageDescription','templates',"get_template_data",'templates_images'));


}

public function checkcodeuniquenessEdit() {
                 $this->autoRender = false;
        // pr($this->request);
        if (!empty($this->request->data['code'])) {
           
            $query = $this->Templates->find()->where(['code' => $this->request->data['code'], 'id NOT IN' => $this->request->data['id']])->count();
            //echo $query; die;
            if ($query > 0) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }
/*
     * function to soft delete the users (Change status to 2 in the database)
     */

public function deleteTemplateImage() {
  $this->autoRender = false;
  $template_images = TableRegistry::get("templates_images");
  if(!empty($this->request->data['id'])){
   $deleteTemplate_images = $template_images->deleteAll(['id' =>  $this->request->data['id']]);
   if ($deleteTemplate_images) {
    $result["status"] = "success";
    $result['msg'] = "Template image has been deleted";
  } else {
    $result["status"] = "error";
    $result['msg'] = "Something went wrong. Please try again later";

  }
}else{
 $result["status"] = "success";
 $result['msg'] = "Template image has been deleted";
}
echo json_encode($result);
}


/*
     * function to soft delete the template (Change status to 2 in the database)
     */

    public function deleteTemplate() {
        $this->autoRender = false;
        $template_table = TableRegistry::get("templates");
        $fetch_record = $template_table->get($this->request->data['id']);
        $fetch_record->status = "0";
        if ($template_table->save($fetch_record)) {
            $result["status"] = "success";
            $result['msg'] = "Template has been deleted sucessfully.";
        } else {
            $result["status"] = "error";
            $result['msg'] = "Something went wrong. Please try again later.";
        }
        echo json_encode($result);
    }

    public function promotion(){

       $this->viewBuilder()->layout("admin_inner");

     $project_name = Configure::read('project_name');
     $pageDescription = 'Promotion Code | ' . $project_name;

      $promotion_table = TableRegistry::get("promotional_code");
      $promotion = $promotion_table->find()->where(['status IS NOT' => 2])->hydrate(false)->toArray();      
     $this->set(compact('pageDescription','id','promotion'));


    }

    public function editCode($id = null){
      $this->viewBuilder()->layout("admin_inner");

      $project_name = Configure::read('project_name');
      $pageDescription = 'Edit Promotion | ' . $project_name;
      $promotion_table = TableRegistry::get("promotional_code");
      if(!empty($id)){
      $get_promotion = $promotion_table->get($id);
    }
     
      if($this->request->is("post") || $this->request->is("put")){
        try{
          $get_promotion = $promotion_table->get($this->request->data['id']);
          
          if(empty($this->request->data['text'])){
            throw new Exception("Please enter promotion code short description", 1);
            
          }
           if(empty($this->request->data['code'])){
            throw new Exception("Please enter promotion code", 1);
            
          }
           if(empty($this->request->data['start_date'])){
            throw new Exception("Please enter promotion code start date", 1);
            
          }
           if(empty($this->request->data['end_date'])){
            throw new Exception("Please enter promotion code end date", 1);
            
          }
          // pr($this->request->data); die;
          if(isset($this->request->data['status'])){
           $check_promotion = $promotion_table->find()->where(['status' => 1 , 'id !=' => $this->request->data['id']])->count();
           if($check_promotion>0){
            $update_code_status = $promotion_table->query();
        $change_status_to_zero = $update_code_status->update()
            ->set(['status' => 0])->where(['id !=' => $this->request->data['id']])->execute(); 
              }
           $this->request->data['status'] = $this->request->data['status'];
          }else{
            $this->request->data['status'] = 0;
          }
          $this->request->data['modified_on'] = date("Y-m-d H:i:s");
          $update_code = $promotion_table->patchEntity($get_promotion,$this->request->data);
          $promotion_table->save($update_code);
          $result['status'] = "success";
        $result['msg']= "Promotion updated successfully";
        echo json_encode($result);

        }catch (Exception $ex) {
         $result['status'] = "error";
         $result["msg"] = $ex->getMessage();
         echo json_encode($result);
       }
       exit();
     }

      $this->set(compact('pageDescription','get_promotion','id'));
   }

public function addCode(){

  $this->viewBuilder()->layout("admin_inner");

      $project_name = Configure::read('project_name');
      $pageDescription = 'Add Promotion | ' . $project_name;
      $promotion_table = TableRegistry::get("promotional_code");

      if($this->request->is("post")){
        try{
              if(empty($this->request->data['text'])){
            throw new Exception("Please enter promotion code short description", 1);
            
          }
           if(empty($this->request->data['code'])){
            throw new Exception("Please enter promotion code", 1);
            
          }

          if(empty($this->request->data['start_date'])){
            throw new Exception("Please enter promotion code start date", 1);
            
          }
           if(empty($this->request->data['end_date'])){
            throw new Exception("Please enter promotion code end date", 1);
            
          }
          if(isset($this->request->data['status'])){
            $update_code_status = $promotion_table->query();
        $change_status_to_zero = $update_code_status->update()
            ->set(['status' => 0])->execute(); 
           $this->request->data['status'] = $this->request->data['status'];
          }
          $this->request->data['created_on'] = date("Y-m-d H:i:s");
          $this->request->data['modified_on'] = date("Y-m-d H:i:s");
          $newEntity = $promotion_table->newEntity($this->request->data);
          $save = $promotion_table->save($newEntity);
           $result['status'] = "success";
        $result['msg']= "Promotion added successfully";
        echo json_encode($result);
        }catch (Exception $ex) {
         $result['status'] = "error";
         $result["msg"] = $ex->getMessage();
         echo json_encode($result);
       }
        exit();
      }

       $this->set(compact('pageDescription'));

}

 public function deletePromotion() {
        $this->autoRender = false;
         $promotion_table = TableRegistry::get("promotional_code");
        $fetch_record = $promotion_table->get($this->request->data['id']);
        $fetch_record->status = "2";
        if ($promotion_table->save($fetch_record)) {
            $result["status"] = "success";
            $result['msg'] = "Promotion has been deleted sucessfully.";
        } else {
            $result["status"] = "error";
            $result['msg'] = "Something went wrong. Please try again later.";
        }
        echo json_encode($result);
    }


}
