<?php
namespace App\Controller\Admin;
use Cake\Core\Configure;
use App\Controller\AppController;
use Cake\Core\Exception\Exception;
use Cake\Event\Event;
use Cake\Mailer\Email;
// In a controller or table method.
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use PHPExcel;
use PHPExcel_Writer_Excel2007;

class UsersController extends AppController {

	public function beforeFilter(Event $event) {
		parent::beforeFilter($event);
		$this->Auth->allow(['forgotPassword','resetpassword']);		
	}

	public function index() {
		$this->redirect('/admin/users/manage');
	}

    /*
    function to admin login
    */

    public function login(){
    	
    	$project_name = Configure::read('project_name');
    	$pageDescription = 'Admin Login | ' . $project_name;

    	$this->set(compact('pageDescription'));
    	if ($this->Auth->user()) {
    		$this->redirect("/admin/users/manage");
    	}
    	$this->viewBuilder()->layout('admin');
    	if ($this->request->is('post')) {


    		$user = $this->Auth->identify();

    		 
    		if (isset($this->request->data['remember_me'])) {
    			if ($this->request->data['remember_me'] == "1") {

    				$cookie = array();
    				$cookie['remember_me'] = $this->request->data['remember_me'];
    				$cookie['username'] = $this->request->data['email'];
    				$cookie['password'] = $this->request->data['password'];
    				$this->Cookie->write('rememberMe', $cookie, true, "1 week");
    				unset($this->request->data['remember_me']);
    			} else {
    				$this->Cookie->delete('rememberMe');
    			}
    		} else {
    			$this->Cookie->delete('rememberMe');
    		}

    		if ($user) {

    			$this->Auth->setUser($user);
			
			

    			return $this->redirect('/admin/users/manage');

    		}
    		$this->Flash->error('Your username or password is incorrect.', 'alert alert-danger');
    	}
    }
    /*
    Show the dashboard
    */
    Public function dashboard(){
    	$project_name = Configure::read('project_name');
    	$pageDescription = 'Dashboard | ' . $project_name;

    	$this->set(compact('pageDescription'));

    }

    /*
    Function to request new password
    */
    public function forgotPassword() {
    	$project_name = Configure::read('project_name');
    	$pageDescription = 'Forgot Password | ' . $project_name;

    	$this->set(compact('pageDescription'));
    	$this->viewBuilder()->layout('admin');
        // $this->autoRender = false;
    	if($this->request->is("post")){
    		try {
    			if (empty($this->request->data['email'])) {
    				throw new Exception("Please enter your email Id.");    			
    			}

    			if (!empty($this->request->data['email'])) {

    				$check_email = $this->Users->find()->where(['email' => $this->request->data['email'],"type" => "0"])->first();

    				if (!empty($check_email)) {
    					$activation_key = md5(uniqid());
    					$activationlink = Configure::read('SITEURL') . 'admin/users/reset-password/' . $activation_key;
    					$toEmail = $check_email->email;
//                    
    					
                        $FROM_EMAIL = Configure::read('FROM_EMAIL');

                        $project_name = Configure::read('project_name');
//echo $project_name; die;
                        $url = Configure::read('SITEURL');
                        $mail_content="";
                        $maildata = array();

                        $mail_content .= '<p>We got a request to change the password for the account with the username <strong style="font-weight: bold;">'.$check_email["email"].'</strong>.</P>
                        
                <p>If you don&#39;t want to reset your password, you can ignore this email.</P>
                
                <p>If you request this change, Please click the link below to reset the password or copy and paste the link on the browser <b>'.$activationlink.'</b></P> 

                <p>Many thanks, <br/> Lizicards Team</p>           
                
                      </td>                 
              </tr>              
                <tr>           
                
                  <td align="center">   <a href='.$activationlink.' style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Reset Your Password </a> </td>                 
                
                  </tr>';

                   // $maildata['name'] = $check_email['first_name']." " .$check_email['last_name'];
                        // $maildata['email'] = $check_email["email"];
                        // $maildata['reset_activationKey'] = $activation_key;
                       $maildata['email'] = $this->request->data['email'];
                        $maildata['heading'] = "Forget your password? Let's get you a new one.";
                        $maildata['url'] = $url;
                        $maildata['data'] = $mail_content;
                        $maildata['subject'] = $project_name.': Reset Password';
//                    $hashPswdObj = new DefaultPasswordHasher;
//                    $password = $hashPswdObj->hash($maildata['password']);
    					
    					
                    //  echo "jkdsfhkj"; die;
    						$user = $this->Users->newEntity();
// $update_password = $this->Users->updateAll(array("password" => $password), array("username" => $this->request->data['username']));
    						$user = $this->Users->patchEntity($check_email, ['reset_activation_key' => $activation_key]);
    						if($this->Users->save($user)){
                             $this->Common->send_mail($maildata);     
    						$result['status'] = "success";
    						$result['response'] = 'An email has been sent to ' . $check_email["email"] . ' so you can reset your password';
    						echo json_encode($result);
    						exit();
    					} else {
    						throw new Exception("Something went wrong. Please try again later.");
    					}
    				} else {
    					throw new Exception("Invalid email. Please enter a valid email address");
    				}
    			}
    		} catch (Exception $e) {
    			$result['status'] = "error";
    			$result['response'] = $e->getMessage();
    			echo json_encode($result);
    			exit;
    		}
    	}

    }

    /*
     * function to reset the password
     */

    public function resetpassword($key = null) {
    	$project_name = Configure::read('project_name');
    	$pageDescription = 'Reset Password|  ' . $project_name;

    	$this->set(compact('pageDescription'));
        $this->viewBuilder()->layout('admin');
    	$user = $this->Users->find('all', array(
    		'conditions' => array(
    			'Users.reset_activation_key' => $key,
    			),
    		))->first();

    	if ($this->request->is("post")) {
    		try {
    			if (empty($user)) {
    				throw new Exception("It seems that your token has been expired");
    			}
    			if (empty($this->request->data['password'])) {
    				throw new Exception("Please enter password");
    			}
    			if (empty($this->request->data['password_confirm'])) {
    				throw new Exception("Please confirm your password");
    			}

    			if ($this->request->data['password_confirm'] != $this->request->data['password']) {
    				throw new Exception("Password does not match");
    			}
    			$update_password = $this->Users->patchEntity($user, ['password' => $this->request->data['password'], 'reset_activation_key' => ""]);
    			if ($this->Users->save($update_password)) {
    				$message = "Password updated successfully";
    				$this->Flash->success($message);
    				$this->redirect("/admin/Users/login");
    			}
    		} catch (Exception $ex) {
    			$message = $ex->getMessage();
    			$this->Flash->error(__($message));
    		}
    	}
    }


/*
   Function to manage users
    */
    Public function manage(){
    	$this->viewBuilder()->layout("admin_inner");
        $project_name = Configure::read('project_name');
        $pageDescription = 'Manage users | ' . $project_name;

        $get_userdetail = $this->Users->find('all')->hydrate(false)->where(['Users.type >' => 0,'Users.status !=' => 2])->order(['Users.first_name'])->toArray();       
        $this->set(compact('pageDescription','get_userdetail'));
    }



/*
   Function to edit users
    */
    Public function edit($id = null){
    	$this->viewBuilder()->layout("admin_inner");
        $project_name = Configure::read('project_name');
        $pageDescription = 'Edit users | ' . $project_name;

        $this->set(compact('pageDescription'));
        if(!empty($id)){

            $get_user_data = $this->Users->get($id);


            $check_id = $this->Users->exists(['id' => $id]);
          
            // $check_id = $this->Users->find()->where(['id' => $id])->first();  
         
            if(!$check_id){                
                $message = 'Wrong user ID passed';
                $this->Flash->error(__($message));
                return $this->redirect("/admin/users/manage");
                die;
            }
        }

        if($this->request->is("post") || $this->request->is('put')){
            try{
              
                 if(empty($this->request->data['first_name'])){
                    throw new Exception("Please enter first name");                    
                 }
                 if(empty($this->request->data['last_name'])){
                    throw new Exception("Please enter last name");                    
                 }
                 if(empty($this->request->data['email'])){
                    throw new Exception("Please enter email address");                    
                 }
                 if(!empty($this->request->data['email'])){
                    $where = '';
                    if(empty($id)){
                        $where = ['email' => $this->request->data['email'],'status' => 1];
                    }else{
                      $where = ['email' => $this->request->data['email'],'id !=' => $id,'status' => 1];
                    }
                    $check_email = $this->Users->find()->where($where)->count();  
                    if($check_email > 0){
                        throw new Exception("Email address already exists"); 
                    }           
                 }
                
            $usersData = $this->Users->patchEntity($get_user_data,$this->request->data);
           		 if($this->Users->save($usersData)){               
                    $message = "User updated successfully";
                    $this->Flash->success($message);
                  }
                  $this->redirect('/admin/users/manage');
         

            }catch (Exception $ex) {
                 $message = $ex->getMessage();
                $this->Flash->error($message);
            }
        }

        $this->set(compact("get_user_data"));

        
    }

    Public function view($id = null){
        $this->viewBuilder()->layout("admin_inner");
        $project_name = Configure::read('project_name');
        $pageDescription = 'View users | ' . $project_name;

        $this->set(compact('pageDescription'));
        if(!empty($id)){

            $get_user_data = $this->Users->get($id);


            $check_id = $this->Users->exists(['id' => $id]);
          
            // $check_id = $this->Users->find()->where(['id' => $id])->first();  
         
            if(!$check_id){                
                $message = 'Wrong user ID passed';
                $this->Flash->error(__($message));
                return $this->redirect("/admin/users/manage");
                die;
            }
        }

      
        $this->set(compact("get_user_data"));

        
    }

    /*
     * function to check email uniqueness when add new user
     */

    public function checkemailuniqueness() {
        $this->autoRender = false;
       
        if (!empty($this->request->data['email'])) {
            $users_table = TableRegistry::get('Users');
           
            $where = '';
             if(empty($this->request->data['id'])){
               
                        $where = ['email' => $this->request->data['email'],'status' => 1];
                    }else{
                       
                      $where = ['email' => $this->request->data['email'],'status' => 1,'id !=' => $this->request->data['id']];
                    }
            $query = $users_table->find()->where($where)->count();
            if ($query > 0) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }

/*
     * function to soft delete the users (Change status to 2 in the database)
     */

    public function deleteUser() {
        $this->autoRender = false;
        $users = TableRegistry::get("users");
        $fetch_record = $users->get($this->request->data['id']);
        $fetch_record->status = "2";
        if ($users->save($fetch_record)) {
            $result["status"] = "success";
            $result['msg'] = "User has been deleted sucessfully.";
        } else {
            $result["status"] = "error";
            $result['msg'] = "Something went wrong. Please try again later.";
        }
        echo json_encode($result);
    }


    /*
     * function to change the users staus (Activate deactivate)
     */

    public function changeStatus() {
        $this->autoRender = false;

        $users = TableRegistry::get("users");
        $fetch_record = $users->get($this->request->data['id']);
        $fetch_record->status = ($this->request->data['status']==1)?"0":"1";

        if ($users->save($fetch_record)) {
            $result['new_status'] = ($this->request->data['status']==1)?"0":"1";
            $result["status"] = "success";
            $result['msg'] = "User status has been changed sucessfully.";
        } else {
            $result["status"] = "error";
            $result['msg'] = "Something went wrong. Please try again later.";
        }
        echo json_encode($result);
    }



     /*
     * function to logout from the admin
     */

     public function logout() {
     	$u_id = $this->request->session()->read('Auth.Admin.id');     	
     $message = "Logged out successfully";
    $this->Flash->success($message);
     	return $this->redirect($this->Auth->logout());
     }

   
     /*
   Function to edit admin profile
    */
    Public function profile($id = null){

        $project_name = Configure::read('project_name');
        $pageDescription = 'Admin Profile | ' . $project_name;

        $this->set(compact('pageDescription'));
       

            $get_user_data = $this->Users->get($id);


            $check_id = $this->Users->exists(['id' => $id]);
          
            // $check_id = $this->Users->find()->where(['id' => $id])->first();  
         
            if(!$check_id){                
                $message = 'Wrong user ID passed';
                $this->Flash->error(__($message));
                return $this->redirect("/admin/users/profile/".$id."");
                die;
            }
        

        if($this->request->is("post") || $this->request->is('put')){
            try{
              
                 if(empty($this->request->data['first_name'])){
                    throw new Exception("Please enter first name");                    
                 }
                 if(empty($this->request->data['last_name'])){
                    throw new Exception("Please enter last name");                    
                 }
                 if(empty($this->request->data['username'])){
                    throw new Exception("Please enter email address");                    
                 }
                 if(!empty($this->request->data['username'])){
                    $where = '';
                   
                    $where = ['username' => $this->request->data['username'],'id !=' => $id,'status' => 1];                  
                    
                    $check_email = $this->Users->find()->where($where)->count();  
                    if($check_email > 0){
                        throw new Exception("Email address already exists"); 
                    }           
                 }
                 
               
              
            $usersData = $this->Users->patchEntity($get_user_data,$this->request->data);
            if($this->Users->save($usersData)){
		  $UserLogs_table = TableRegistry::get('user_logs');
             $query = $UserLogs_table->query();
           $query->update()
              ->set(['first_name' => $this->request->data['first_name'],'last_name' => $this->request->data['last_name'],'username' => $this->request->data['username']])
              ->where(['user_id' => $this->Auth->user("id"),'current_login' =>1])
              ->execute();
               	   $this->request->session()->write('Auth.User.first_name',$this->request->data['first_name']);
                 $this->request->session()->write('Auth.User.last_name',$this->request->data['last_name']);
                    $message = "Admin updated successfully";
                    $this->Flash->success($message);
                     return $this->redirect('/admin/users/profile/'.$id.'');
                  }
                
         

            }catch (Exception $ex) {
                 $message = $ex->getMessage();
                $this->Flash->error($message);
            }
        }

       
        $this->set(compact('get_user_data','id'));
// echo "helo"; die;
    }


       /*
   Function to change password
    */
    public function updatePassword()
    {
        if($this->request->is('post')){
            $userObj = TableRegistry::get('Users');
            $userData =  $userObj->get($this->request->data['id']);

            $user =  $userObj->patchEntity($userData,$this->request->data,['validate' => 'password']);
            $error = $user->errors();
            if(empty($error)){
                $this->Flash->success('Password Updated Successfully');
                $userObj->save($user);
               return $this->redirect('/admin/users/update_password');
            } 
            $this->set('User',$user); 

        } else {
            $this->set('User','');
        }
         $project_name = Configure::read('project_name');
        $pageDescription = 'Update Password | ' . $project_name;

        $this->set(compact('pageDescription'));
    }
	
      /*
   Function to view the login history
    */
   public function logs(){
        
       $this->loadModel("UserLogs");
       $paginationCountChange = (isset($_GET['limt']) && $_GET['limt']!=''&& $_GET['limt']!='undefined') ? $_GET['limt'] : 50;
        $pagestr = (isset($_GET['page']) && $_GET['page']!=''&& $_GET['page']!='undefined') ? $_GET['page'] : 1;
        $sortstr = (isset($_GET['sort']) && $_GET['sort']!=''&& $_GET['sort']!='undefined') ? $_GET['sort'] : 'UserLogs.first_name';
        $directionstr = (isset($_GET['direction']) && $_GET['direction']!=''&& $_GET['direction']!='undefined') ? $_GET['direction'] : "asc";
         $options = []; 
        if(isset($_GET['q']) && $_GET['q']!='') {
            $options['or'] =[
                                'UserLogs.first_name like'=>'%'.$_GET['q'].'%',
                                'UserLogs.last_name like'=>'%'.$_GET['q'].'%',
                                'UserLogs.username'=>'%'.$_GET['q'].'%',
                                'UserLogs.role like'=>'%'.$_GET['q'].'%',
                                'UserLogs.created like'=>'%'.$_GET['q'].'%',
                            ];
        }
         $query = $this->UserLogs->find()->select()->where($options)->hydrate(false)->order(["UserLogs.created" => "desc"]);

         $this->paginate =['page'=>$pagestr,'limit' => $paginationCountChange,'sort'=>$sortstr, 'direction'=>$directionstr ];
        $user_log_list = $this->paginate($query);

        $pagination = $this->Paginator->request->params['paging']['UserLogs'];
        $this->set(compact('user_log_list','paginationCountChange','sortstr','directionstr','pagestr','pagination'));
        $this->set('pagination_url', 'admin/users/logs');
        $project_name = Configure::read('project_name');
        $pageDescription = 'User Logs | ' . $project_name;
     
        $this->set(compact('pageDescription'));
        if ($this->request->is('ajax')){
            $this->viewBuilder()->layout(false);
            $this->render('/Element/admin/users/logs');
        }
    }

    public function orders(){

         $this->viewBuilder()->layout("admin_inner");
           $project_name = Configure::read('project_name');
        $pageDescription = 'Order List | ' . $project_name;

        $this->set(compact('pageDescription'));
         $this->loadModel("Orders");
         $template_data = $this->Orders->find()->contain(['Templates','Users'])->where(['Orders.user_id IS NOT' => NULL])->hydrate(false)->order(["Orders.id" => "DESC"])->toArray();

         //    $template_datas = $this->Orders->find('all')->contain(['Templates','UsersAudio','UsersImages','ShippingAddress'])->hydrate(false)->toArray();
         // $orders_id = array();
         // foreach ($template_datas as $key => $value) {
         //    $orders_id[] = $value['id'];
         //     foreach ($value['shipping_address'] as $key => $shp) {
                
         //         if(in_array($shp['order_id'], $orders_id)){
         //         echo "hello";
         //     }
         //     }
         // }
        
        
        $this->set(compact('template_data'));
        

    }

    public function viewOrders($id=null){
      $this->viewBuilder()->layout("admin_inner");  

        $project_name = Configure::read('project_name');
        $pageDescription = 'View Order | ' . $project_name;
        $this->loadModel("Orders");

        $template_data = $this->Orders->find('all')->contain(['Templates','UsersAudio','UsersImages'])->where(['Orders.id' => $id])->hydrate(false)->first();
        // pr($template_data);
           
            $this->loadModel("TemplatesImages");

            $template_images = $this->TemplatesImages->find()->contain(['Templates'])->where(['TemplatesImages.status' => 1,'TemplatesImages.show_preview' => 1,'TemplatesImages.template_id' => $template_data['template_id']])->hydrate(false)->first();
            $preview_image = $template_images['images'];

            $shipping_table = TableRegistry::get("shipping_address");
            $get_address = $shipping_table->find()->where(["order_id" => $id])->toArray();

            $this->set(compact('template_data','preview_image','get_address'));
           
         $this->set(compact('pageDescription','id'));
    }

     public function downloadMedia($id=null,$type=null)

    {   $this->loadModel("Orders");
        $template_data = $this->Orders->find('all')->contain(['UsersAudio','UsersImages'])->where(['md5(Orders.id)' => $id])->hydrate(false)->first();        
        if($type=="audio"){
        $actual_file_name = WWW_ROOT . 'img/user_audio/' . $template_data['users_audio']['user_audio'];
        $download_file_name = (strtolower($template_data['status'])=="completed")?$template_data['transaction_id'].".wav":$template_data['users_audio']['user_audio'];
        $mime = 'application/wav';

        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: ' . $mime);
        header('Content-Disposition: attachment; filename="'. $download_file_name .'"'); // Set it here!
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($actual_file_name));
        header('Connection: close');
        readfile($actual_file_name);
       
        }

        if($type=="image"){
        $actual_file_name = WWW_ROOT . 'img/user_images/' . $template_data['users_image']['user_image'];
        $download_file_name = (strtolower($template_data['status'])=="completed")?$template_data['transaction_id'].".jpg":$template_data['users_image']['user_image'];
        $mime = 'application/jpg';

        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: ' . $mime);
        header('Content-Disposition: attachment; filename="'. $download_file_name .'"'); // Set it here!
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($actual_file_name));
        header('Connection: close');
        readfile($actual_file_name);
       
        }
         exit();
    }

    public function exportCsv_from_list(){
       
            clearstatcache(); 
                ini_set('memory_limit', '-1');

       $SITEURL = Configure::read('SITEURL');

            $counter=1;
           
                    // $this->autoRender = false;
        $this->loadModel("Orders");
        // pr($this->request->data);





        $where = "";
        if(!empty($this->request->data['from_date']) && empty($this->request->data['to_date'])){
            // echo "one";
            // $where = ['DATE_FORMAT(Orders.created_on,"%Y-%m-%d") >=' => $this->request->data['from_date'],'Orders.status' => strtolower($this->request->data['status'])];
            $where = ['Orders.created_on >' => 'DATE_SUB(CURDATE(),INTERVAL 1 DAY)','Orders.status' => strtolower('completed')];
            

            $filename = WWW_ROOT . 'img/export_data_from-'.$this->request->data['from_date']; 

            if(!file_exists($filename)){  

                mkdir($filename);
                chmod($filename, 0777);
                mkdir($filename."/audio");
                chmod($filename."/audio", 0777);
                mkdir($filename."/images");
                chmod($filename."/images", 0777);
                mkdir($filename."/excel");
                chmod($filename."/excel", 0777);
            }else{
                // echo "hello";
                while(is_dir($filename))
                {
                    $count_number_in_string = preg_match_all( "/[0-9]/", $filename );
                    $counter++;
                    if($count_number_in_string>=9){
                        $explode = explode("-", $filename);
                        $end = end($explode);
                        $filename= substr($filename, 0, -1)."-".$counter;
                    }else{
                     $filename = $filename."-".$counter; 
                 }


             }
                 // die;
             mkdir($filename);
             chmod($filename, 0777);
             mkdir($filename."/audio");
             chmod($filename."/audio", 0777);
             mkdir($filename."/images");
             chmod($filename."/images", 0777);
             mkdir($filename."/excel");
             chmod($filename."/excel", 0777);
         }



     }
           
        if(empty($this->request->data['from_date']) && !empty($this->request->data['to_date'])){
            // echo "two";
            $where = ['DATE_FORMAT(Orders.created_on,"%Y-%m-%d") <=' => $this->request->data['to_date'],'Orders.status' => strtolower($this->request->data['status'])];

             $filename = WWW_ROOT . 'img/export_data_before-'.$this->request->data['to_date'];     
            if(!file_exists($filename)){  

                mkdir($filename);
                chmod($filename, 0777);
                mkdir($filename."/audio");
                chmod($filename."/audio", 0777);
                mkdir($filename."/images");
                chmod($filename."/images", 0777);
                mkdir($filename."/excel");
                chmod($filename."/excel", 0777);
            }else{
                while(is_dir($filename))
            {
                    $count_number_in_string = preg_match_all( "/[0-9]/", $filename );
                    $counter++;
                    if($count_number_in_string>=9){
                        $explode = explode("-", $filename);
                        $end = end($explode);
                        $filename= substr($filename, 0, -1)."-".$counter;
                    }else{
                     $filename = $filename."-".$counter; 
                 }

                 
             }
                 mkdir($filename);
                chmod($filename, 0777);
                mkdir($filename."/audio");
                chmod($filename."/audio", 0777);
                mkdir($filename."/images");
                chmod($filename."/images", 0777);
                mkdir($filename."/excel");
                chmod($filename."/excel", 0777);
            }
        }

        if(!empty($this->request->data['from_date']) && !empty($this->request->data['to_date'])){
            // echo "three";
            $where = ['DATE_FORMAT(Orders.created_on,"%Y-%m-%d") >=' => $this->request->data['from_date'],'DATE_FORMAT(Orders.created_on,"%Y-%m-%d") <=' => $this->request->data['to_date'],'Orders.status' => strtolower($this->request->data['status'])];

            $filename = WWW_ROOT . 'img/export_data_from-'.$this->request->data['from_date']."-to-".$this->request->data['to_date'];     
            if(!file_exists($filename)){  

                mkdir($filename);
                chmod($filename, 0777);
                mkdir($filename."/audio");
                chmod($filename."/audio", 0777);
                mkdir($filename."/images");
                chmod($filename."/images", 0777);
                mkdir($filename."/excel");
                chmod($filename."/excel", 0777);
            }else{
                while(is_dir($filename))
            {
                    $count_number_in_string = preg_match_all( "/[0-9]/", $filename );
                    $counter++;
                    if($count_number_in_string>=9){
                        $explode = explode("-", $filename);
                        $end = end($explode);
                        $filename= substr($filename, 0, -1)."-".$counter;
                    }else{
                     $filename = $filename."-".$counter; 
                 }

                 
             }
                 mkdir($filename);
                chmod($filename, 0777);
                mkdir($filename."/audio");
                chmod($filename."/audio", 0777);
                mkdir($filename."/images");
                chmod($filename."/images", 0777);
                mkdir($filename."/excel");
                chmod($filename."/excel", 0777);
            }
        }

         if(empty($this->request->data['from_date']) && empty($this->request->data['to_date'])){
            // echo "three";
            $where = ['Orders.status' => strtolower($this->request->data['status'])];

             $filename = WWW_ROOT . 'img/export_data-'.$this->request->data['status'];     
            if(!file_exists($filename)){  

                mkdir($filename);
                chmod($filename, 0777);
                mkdir($filename."/audio");
                chmod($filename."/audio", 0777);
                mkdir($filename."/images");
                chmod($filename."/images", 0777);
                mkdir($filename."/excel");
                chmod($filename."/excel", 0777);
            }else{
                while(is_dir($filename))
            {
                    $count_number_in_string = preg_match_all( "/[0-9]/", $filename );
                    $counter++;
                    if($count_number_in_string>=9){
                        $explode = explode("-", $filename);
                        $end = end($explode);
                        $filename= substr($filename, 0, -1)."-".$counter;
                    }else{
                     $filename = $filename."-".$counter; 
                 }

                 
             }
                 mkdir($filename);
                chmod($filename, 0777);
                mkdir($filename."/audio");
                chmod($filename."/audio", 0777);
                mkdir($filename."/images");
                chmod($filename."/images", 0777);
                mkdir($filename."/excel");
                chmod($filename."/excel", 0777);
            }
        }
         $template_data = $this->Orders->find('all')->contain(['Templates','UsersAudio','UsersImages','ShippingAddress','Users'])->where($where)->hydrate(false)->toArray();
         pr($template_data); die;
          // pr($template_data); die;
         // die;
         // echo count($template_data);
         echo date('H:i:s') . " Create new PHPExcel object\n";
        require_once(ROOT . DS . 'vendor' . DS . 'PHPExcel' . DS . 'Classes' . DS . 'PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        $row =1;
        $col=0;
        $first_row = array("Order Id","Transaction ID","Sender First Name","Sender Last name","Receiver First Name","Receiver Last Name","Card Id","Card Name","Image","Audio","Image Link","Audio Link","Order Created From","Order Date","Send Type","Address One","Address Two","State","City","Zip","Country","Shipping Type","Quantity","Price","Item Total","Shipping Cost","Sales Tax","Grand Total","Status");

        $styleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 10,
        'name'  => 'Verdana',

    ));

         $contentstyleArray = array(
    'font'  => array(
        'Roman'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 10,
        'name'  => 'Liberation Serif',

    ));


        // pr($first_row); die;
          $objPHPExcel->getActiveSheet()->getStyle('1:1')->applyFromArray($styleArray);
          

        for($col=0; $col<29;$col++){
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $first_row[$col]);
        }
       
         $data_row = 2;
             $data_col=0;

            



       
         foreach ($template_data as $key => $value) {
            $orders_id[] = $value['id'];
             // pr($value);
            
             foreach ($value['shipping_address'] as $data => $shp) {
                
                 if(in_array($shp['order_id'], $orders_id)){
                    // pr($data);
                    $audio_path = WWW_ROOT . 'img/user_audio/' . $value['users_audio']['user_audio'];
                    $image_path = WWW_ROOT . 'img/user_images/' . $value['users_image']['user_image'];
                    $new_audio_path = $filename."/audio/".$value['users_audio']['user_audio'];
                    $new_image_path = $filename."/images/".$value['users_image']['user_image'];
                    if(file_exists($audio_path)){
                    copy($audio_path,$new_audio_path);
                    }
                    if(file_exists($image_path)){
                    copy($image_path,$new_image_path);
                    }
                    $price = "$".number_format($value['price'],2);
                    $item_total = $value['quantity']*$value['price'];
                    
                    $grand_total = $item_total+$shp['sale_tax']+$value['shipping_cost'];
                    $shipping_cost = "$".number_format($value['shipping_cost'],2);
                    $item_total = "$".number_format($item_total,2);
                    $grand_total = "$".number_format($grand_total,2);

                    $state = TableRegistry::get("states");
                    $countries = TableRegistry::get("countries");
                     $get_state = $state->find()->where(['id' => $shp['state_id']])->first();
                     $get_countries = $countries->find()->where(['id' => $shp['country']])->first();
                 if($get_state['country_id']==231){
                        $state_name = $get_state['state_abbrivation'];
                      }else{
                        $state_name = $get_state['name'];
                      }
                      // pr($get_state); 
                    $send_to = "";
                    if(!empty($value['send_to']) && $value['send_to']=="to_me"){
                     // echo $value['id'];
                        $send_to = "send to me";
                    }else if(!empty($value['send_to']) && $value['send_to']=="to_receipent"){
                         $send_to = "directly to recipient";
                    }

                    $shipping_text = 'invalid';
                    if($shp['shipping_type']=='us_shipping') {
                        $shipping_text = 'First Class : $1';
                    }else if($shp['shipping_type']=='outside_us_shipping') {
                        $shipping_text = 'International shipping : $5';                        
                    }else if($shp['shipping_type']=='us_expedited_shipping'){
                        $shipping_text = 'Expedited shipping : $3';
                    }
                    if(!empty($shp['sale_tax'])){
                        $sale_tax = "$".number_format($shp['sale_tax'],2);
                    }
                    else{
                        $sale_tax = "$0.00";
                    }
                   
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $data_row, $value['id']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $data_row, $value['transaction_id']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $data_row, mb_convert_encoding($value['user']['first_name'],"UTF-7"));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $data_row, mb_convert_encoding($value['user']['last_name'],"UTF-7"));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $data_row, mb_convert_encoding($shp['first_name'],"UTF-7"));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $data_row, mb_convert_encoding($shp['last_name'],"UTF-7"));
                     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $data_row, $value['template']['code']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $data_row, $value['template']['name']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $data_row, (file_exists($image_path)?$value['users_image']['user_image']:""));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $data_row, (file_exists($audio_path)?$value['users_audio']['user_audio']:""));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $data_row, (file_exists($image_path)?$SITEURL."img/user_images/".$value['users_image']['user_image']:""));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $data_row, (file_exists($audio_path)?$SITEURL."img/user_audio/".$value['users_audio']['user_audio']:""));
                     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $data_row, @$value['order_created_from']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $data_row, date("Y-m-d",strtotime($value['created_on']))." at ".date("H:i:s",strtotime($value['created_on'])));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $data_row, $send_to);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $data_row, $shp['address_one']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $data_row, @$shp['address_two']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $data_row, $state_name);
                     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $data_row, mb_convert_encoding($shp['city'],"UTF-7"));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $data_row, $shp['zip']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $data_row, $get_countries['name']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, $data_row, $shipping_text);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, $data_row, $value['quantity']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(23, $data_row, $price);
                     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(24, $data_row, $item_total);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(25, $data_row, $shipping_cost);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(26, $data_row, $sale_tax);
                     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(27, $data_row, $grand_total);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(28, $data_row, ucfirst($value['status']));

                    $objPHPExcel->getActiveSheet()->getStyle($data_row.":".$data_row)->applyFromArray($contentstyleArray);

                   
                    $data_row++;
                   
                   
                     // $output .="\n";
             }
             }

         }

        
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save($filename."/excel/orders.xlsx");

chmod($filename."/excel/orders.xlsx", 0777);

// Echo done


         /// Zip the data /////////////////////////
         
         $source = $filename;
         $destination= $filename.".zip";
    

         if (!extension_loaded('zip') || !file_exists($source)) {
        return false;
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
        return false;
    }

    $source = str_replace('\\', '/', realpath($source));

    if (is_dir($source) === true)
    {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

        foreach ($files as $file)
        {
            $file = str_replace('\\', '/', $file);

            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;

            $file = realpath($file);

            if (is_dir($file) === true)
            {
                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
            }
            else if (is_file($file) === true)
            {
                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
            }
        }
    }
    else if (is_file($source) === true)
    {
        $zip->addFromString(basename($source), file_get_contents($source));
    }

     $zip->close();

    
/////////////////////////////////////////////////////////////////////////////////

return $this->redirect('/admin/users/orders');
exit();        
    }




    public function exportCsv(){
        
            clearstatcache(); 
                ini_set('memory_limit', '-1');

       $SITEURL = Configure::read('SITEURL');

            $counter=1;
           
                    // $this->autoRender = false;
        $this->loadModel("Orders");
        // pr($this->request->data);





        $where = "";
       
            // echo "one";
            // $where = ['DATE_FORMAT(Orders.created_on,"%Y-%m-%d") >=' => $this->request->data['from_date'],'Orders.status' => strtolower($this->request->data['status'])];
            $where = ['Orders.created_on BETWEEN CURDATE() - INTERVAL 4 DAY AND CURDATE()','Orders.status' =>strtolower('completed')];
            

            $filename = WWW_ROOT . 'img/export_data_before-'.date("Y-m-d"); 

            if(!file_exists($filename)){  

                mkdir($filename);
                chmod($filename, 0777);
                mkdir($filename."/audio");
                chmod($filename."/audio", 0777);
                mkdir($filename."/images");
                chmod($filename."/images", 0777);
                mkdir($filename."/excel");
                chmod($filename."/excel", 0777);
            }else{
                // echo "hello";
                while(is_dir($filename))
                {
                    $count_number_in_string = preg_match_all( "/[0-9]/", $filename );
                    $counter++;
                    if($count_number_in_string>=9){
                        $explode = explode("-", $filename);
                        $end = end($explode);
                        $filename= substr($filename, 0, -1)."-".$counter;
                    }else{
                     $filename = $filename."-".$counter; 
                 }


             }
                 // die;
             mkdir($filename);
             chmod($filename, 0777);
             mkdir($filename."/audio");
             chmod($filename."/audio", 0777);
             mkdir($filename."/images");
             chmod($filename."/images", 0777);
             mkdir($filename."/excel");
             chmod($filename."/excel", 0777);
         }



         $template_data = $this->Orders->find('all')->contain(['Templates','UsersAudio','UsersImages','ShippingAddress','Users'])->where($where)->hydrate(false)->toArray();
       
          // pr($template_data); die;
         // die;
         // echo count($template_data);
       //  echo date('H:i:s') . " Create new PHPExcel object\n";
        require_once(ROOT . DS . 'vendor' . DS . 'PHPExcel' . DS . 'Classes' . DS . 'PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        $row =1;
        $col=0;
        $first_row = array("Order Id","Transaction ID","Sender First Name","Sender Last name","Receiver First Name","Receiver Last Name","Card Id","Card Name","Image","Audio","Image Link","Audio Link","Order Created From","Order Date","Send Type","Address One","Address Two","State","City","Zip","Country","Shipping Type","Quantity","Price","Item Total","Shipping Cost","Sales Tax","Grand Total","Status");

        $styleArray = array(
    'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 10,
        'name'  => 'Verdana',

    ));

         $contentstyleArray = array(
    'font'  => array(
        'Roman'  => true,
        'color' => array('rgb' => '000000'),
        'size'  => 10,
        'name'  => 'Liberation Serif',

    ));


        // pr($first_row); die;
          $objPHPExcel->getActiveSheet()->getStyle('1:1')->applyFromArray($styleArray);
          

        for($col=0; $col<29;$col++){
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $first_row[$col]);
        }
       
         $data_row = 2;
             $data_col=0;

            



       
         foreach ($template_data as $key => $value) {
            $orders_id[] = $value['id'];
             // pr($value);
            
             foreach ($value['shipping_address'] as $data => $shp) {
                
                 if(in_array($shp['order_id'], $orders_id)){
                    // pr($data);
                    $audio_path = WWW_ROOT . 'img/user_audio/' . $value['users_audio']['user_audio'];
                    $image_path = WWW_ROOT . 'img/user_images/' . $value['users_image']['user_image'];
                    $new_audio_path = $filename."/audio/".$value['users_audio']['user_audio'];
                    $new_image_path = $filename."/images/".$value['users_image']['user_image'];
                    if(file_exists($audio_path)){
                    copy($audio_path,$new_audio_path);
                    }
                    if(file_exists($image_path)){
                    copy($image_path,$new_image_path);
                    }
                    $price = "$".number_format($value['price'],2);
                    $item_total = $value['quantity']*$value['price'];
                    
                    $grand_total = $item_total+$shp['sale_tax']+$value['shipping_cost'];
                    $shipping_cost = "$".number_format($value['shipping_cost'],2);
                    $item_total = "$".number_format($item_total,2);
                    $grand_total = "$".number_format($grand_total,2);

                    $state = TableRegistry::get("states");
                    $countries = TableRegistry::get("countries");
                     $get_state = $state->find()->where(['id' => $shp['state_id']])->first();
                     $get_countries = $countries->find()->where(['id' => $shp['country']])->first();
                 if($get_state['country_id']==231){
                        $state_name = $get_state['state_abbrivation'];
                      }else{
                        $state_name = $get_state['name'];
                      }
                      // pr($get_state); 
                    $send_to = "";
                    if(!empty($value['send_to']) && $value['send_to']=="to_me"){
                     // echo $value['id'];
                        $send_to = "send to me";
                    }else if(!empty($value['send_to']) && $value['send_to']=="to_receipent"){
                         $send_to = "directly to recipient";
                    }

                    $shipping_text = 'invalid';
                    if($shp['shipping_type']=='us_shipping') {
                        $shipping_text = 'First Class : $1';
                    }else if($shp['shipping_type']=='outside_us_shipping') {
                        $shipping_text = 'International shipping : $5';                        
                    }else if($shp['shipping_type']=='us_expedited_shipping'){
                        $shipping_text = 'Expedited shipping : $3';
                    }
                    if(!empty($shp['sale_tax'])){
                        $sale_tax = "$".number_format($shp['sale_tax'],2);
                    }
                    else{
                        $sale_tax = "$0.00";
                    }
                   
                    
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $data_row, $value['id']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $data_row, $value['transaction_id']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $data_row, mb_convert_encoding($value['user']['first_name'],"UTF-7"));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $data_row, mb_convert_encoding($value['user']['last_name'],"UTF-7"));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $data_row, mb_convert_encoding($shp['first_name'],"UTF-7"));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $data_row, mb_convert_encoding($shp['last_name'],"UTF-7"));
                     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $data_row, $value['template']['code']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $data_row, $value['template']['name']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $data_row, (file_exists($image_path)?$value['users_image']['user_image']:""));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $data_row, (file_exists($audio_path)?$value['users_audio']['user_audio']:""));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $data_row, (file_exists($image_path)?$SITEURL."img/user_images/".$value['users_image']['user_image']:""));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $data_row, (file_exists($audio_path)?$SITEURL."img/user_audio/".$value['users_audio']['user_audio']:""));
                     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $data_row, @$value['order_created_from']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $data_row, date("Y-m-d",strtotime($value['created_on']))." at ".date("H:i:s",strtotime($value['created_on'])));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $data_row, $send_to);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $data_row, $shp['address_one']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $data_row, @$shp['address_two']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $data_row, $state_name);
                     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $data_row, mb_convert_encoding($shp['city'],"UTF-7"));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $data_row, $shp['zip']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(20, $data_row, $get_countries['name']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(21, $data_row, $shipping_text);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(22, $data_row, $value['quantity']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(23, $data_row, $price);
                     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(24, $data_row, $item_total);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(25, $data_row, $shipping_cost);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(26, $data_row, $sale_tax);
                     $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(27, $data_row, $grand_total);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(28, $data_row, ucfirst($value['status']));

                    $objPHPExcel->getActiveSheet()->getStyle($data_row.":".$data_row)->applyFromArray($contentstyleArray);

                   
                    $data_row++;
                   
                   
                     // $output .="\n";
             }
             }

         }

           
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save($filename."/excel/orders.xlsx");

chmod($filename."/excel/orders.xlsx", 0777);

// Echo done


         /// Zip the data /////////////////////////
         
         $source = $filename;
         $destination= $filename.".zip";


         if (!extension_loaded('zip') || !file_exists($source)) {
		
        return false;
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
        return false;
    }

    $source = str_replace('\\', '/', realpath($source));

    if (is_dir($source) === true)
    {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

        foreach ($files as $file)
        {
            $file = str_replace('\\', '/', $file);

            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;

            $file = realpath($file);

            if (is_dir($file) === true)
            {
                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
            }
            else if (is_file($file) === true)
            {
                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
            }
        }
    }
    else if (is_file($source) === true)
    {
        $zip->addFromString(basename($source), file_get_contents($source));
    }
	
     $zip->close();

    
/////////////////////////////////////////////////////////////////////////////////

//return $this->redirect('/admin/users/orders');
exit();        
    }

  
}
 
