<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */

    public $components = ['Pagination','Common'];

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');        
        $SITEURL = Configure::read('SITEURL'); 
	$fb_id = Configure::read('Facebook_id'); 
	$sal_tax = Configure::read('Sales_tax'); 
        $Project_Name = Configure::read('project_name');   
        $this->set(compact("Project_Name","Project_Name","sal_tax"));

        $this->loadComponent('Cookie');
        if ($this->Cookie->read('rememberMe') != null) {
            $remembered_data = $this->Cookie->read('rememberMe');
            $this->set(compact('remembered_data'));
        }

         if ($this->request->prefix == 'admin') {     
            $table = $controller = 'Users';
            $user = array($table.'.type' => "0", 'Users.status' => "1");   
            $this->loadComponent('Auth', [
                 'authenticate' => array(
                    'Form' => array(
                        'scope' => $user,
                        'fields' => array("username" => "email", "password" =>"password")
                        
                    )
                ),   
                'loginRedirect' => [
                    'controller' => 'Users',
                    'action' => 'dashboard'
                ],
                'logoutRedirect' => [
                    'controller' => 'Users',
                    'action' => 'login'
                ],
                'storage' => [
                    'className' => 'Session',
                    'key' => 'Auth.Admin',               
                ]
            ]);

           



        }else{
            // echo "hello"; 
            $table = $controller = 'Users';
            $user = array($table.'.type >' => "0", 'Users.status' => "1");            
             $this->loadComponent('Auth', [
            'authenticate' => array(
                    'Form' => array(
                        'scope' => $user,
                        'fields' => array("username" => "email", "password" =>"password")
                        
                    )
                ),    
            'loginRedirect' => [
                'controller' => 'templates',
                'action' => 'upload-photo'
            ],
            'logoutRedirect' => [
                'controller' => 'Templates',
                'action' => 'index',                
            ],
            'storage' => [
                    'className' => 'Session',
                    'key' => 'Auth.User',               
                ],
        ]);

        }
       

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see http://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
       $this->set(compact("SITEURL",'Project_Name','fb_id'));
    }

    public function beforeFilter(Event $event)
    {
         if(in_array($this->request->params['controller'],array('RestTemplates','RestUsers','RestCountries','RestStates','RestOrders','RestTempOrders','RestPayments'))){
            
            // For RESTful web service requests, we check the name of our contoller
            $this->Auth->allow();
           
        }else{
        $this->Auth->allow(['index', 'templateImages', 'showCards','viewDetail', 'uploadPhoto', 'save', 'setpageNumber','checkemailuniqueness','register','facebookLogin',"address","checkout","normalLogin",'forgotPassword','resetpassword','terms','shippingReturn','cookie','contact','privacy','showAudioRecorded','showAudioWithoutPlaybtn','showAudioRecordedPreview','showSampleVideos','testmyem','whatis','whatIsGetStarted','exportCsv']);
         }
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
            ) {
            $this->set('_serialize', true);
    }
}

    public function delete_template_session(){
        $this->request->session()->delete("GuestUser.unique_id");
        $this->request->session()->delete("GuestUser.template_id");
        $this->request->session()->delete("GuestUser.template_name");
        $this->request->session()->delete("GuestUser.tag_name");
        $this->request->session()->delete("GuestUser.price");
        $this->request->session()->delete("GuestUser.image_name");
        $this->request->session()->delete("GuestUser.audio_name");
        return true;
    }
}
