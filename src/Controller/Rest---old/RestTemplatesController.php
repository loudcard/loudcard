<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Rest;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\Mailer\Email;

use App\Controller\AppController;
// In a controller or table method.
use Cake\ORM\TableRegistry;




/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class TemplatesController extends AppController
{
    public $component = array('RequestHandler');
    public $paginate = [
    'page' => 1,
    'limit' => 5,
    'maxLimit' => 100,
    'fields' => [],
    'sortWhitelist' => []
    ];
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->loadModel("Tags");

        
    }

    /**
     * Function to list all users
     *
     * @return Json Users List
     */
    public function index() {  die("hello");      

        $queryArr = $this->Common->getQueryStrings(); 

        if(isset($queryArr)){
           extract($queryArr);
       }
       if(isset($types)) {
        $type = $types;
    }else {
        $type = '';
    }

    switch ($type) {
        case 'tags':
        if(isset($search)){
          $like = ['Tags.name like' => '%'.$search.'%','Tags.status' => 1];  
      }else{
        $like =['Tags.status' => 1];
    }   
    $tag_list = $this->Tags->find()->select(['id','name'])->where($like)->hydrate(false)->toArray();     
    $this->set([
        'status' => 'success',
        'data'=>$tag_list,
        '_serialize' => ['data']
        ]);
    break;

    
        case 'templates':
        if(isset($search) && isset($tag_id)){
            // echo "one"; die;
          $like = ['Tags.name like' => '%'.$search.'%','Templates.status' => 1,'Tags.status' => 1,'Templates.tag_id' => $tag_id];  
      }elseif(isset($search) && empty($tag_id)){
             // echo "two"; die;
           $like = ['Tags.name like' => '%'.$search.'%','Templates.status' => 1,'Tags.status' => 1];           
             
      }

      elseif(empty($search) && isset($tag_id)){
         // echo "three"; die;
        $like =['Templates.status' => 1,'Tags.status' => 1,'Templates.tag_id' => $tag_id];
    }   
    $templates_temp = $this->Templates->find()->contain(['Tags','TemplatesImages'])->where($like)->order("Templates.id desc"); 
    $templates      = [];

    if(!empty($templates_temp) && $templates_temp!=NULL){
    foreach ($templates_temp as $key => $template) {
         $cover_text = strip_tags($template['cover_text']);
            $cover_text = html_entity_decode($cover_text);
            $cover_text = urldecode($cover_text);
            $cover_text = preg_replace("/[^ \w]+/", "", $cover_text);
            $template->cover_text = $cover_text;
            $inside_text = strip_tags($template['inside_text']);
            $inside_text = html_entity_decode($inside_text);
            $inside_text = urldecode($inside_text);
            $inside_text = preg_replace("/[^ \w]+/", "", $inside_text);
            $template->inside_text = $cover_text;
            $card_details = strip_tags($template['card_details']);
            $card_details = html_entity_decode($card_details);
            $card_details = urldecode($card_details);
            $card_details = preg_replace("/[^ \w]+/", "", $card_details);
            $template->card_details = $card_details;

        
        // $template->cover_text = strip_tags($template['cover_text']);
        // $template->inside_text = strip_tags($template['inside_text']);
        // $template->card_details = strip_tags($template['card_details']); 
        $templates[] = $template;        
    }
}
    $this->set([
        'status' => 'success',
        'data'=>$templates,
        '_serialize' => ['data']
        ]);

    break;

    default:

    $templates = $this->Templates->find()->contain(['Tags','TemplatesImages'])->where(['Templates.status' => 1,'Tags.status' => 1])->order("Templates.id desc");
    $templates =  $this->paginate($templates);
    $templates_final      = [];
    if(!empty($templates) && $templates!=NULL){
        foreach ($templates as $key => $template) {
            $cover_text = strip_tags($template['cover_text']);
            $cover_text = html_entity_decode($cover_text);
            $cover_text = urldecode($cover_text);
            $cover_text = preg_replace("/[^ \w]+/", "", $cover_text);
            $template->cover_text = $cover_text;
            $inside_text = strip_tags($template['inside_text']);
            $inside_text = html_entity_decode($inside_text);
            $inside_text = urldecode($inside_text);
            $inside_text = preg_replace("/[^ \w]+/", "", $inside_text);
            $template->inside_text = $cover_text;
            $card_details = strip_tags($template['card_details']);
            $card_details = html_entity_decode($card_details);
            $card_details = urldecode($card_details);
            $card_details = preg_replace("/[^ \w]+/", "", $card_details);
            $template->card_details = $card_details;
             
            $templates_final[] = $template;        
        }
    }



    $this->set([
        'status' => 'success',
        'data'=>$templates_final,
        '_serialize' => ['data']
        ]);
    break;

}




}
 


    /**
     * Function to add a new user
     *
     * @return Json message

     *      */
    public function add() {
        $action = $this->request->data['action'];        
        $data = [];

    }

    public function view($id)
    {
        $queryArr=$this->Common->getQueryStrings();   
        
        if($queryArr)
            extract($queryArr); 
        
        $data = [];
        $this->set([
            'data'=>$data,
            '_serialize' => ['data']
            ]);
    }


    public function edit($id)
    {
        $this->loadModel('Users');
        $data = [];

        $this->set([
            'status' => 'success',
            'data' => $data,
            '_serialize' => ['message', 'errorType']
            ]);
    }


    
    

    public function delete($id)
    {
        $this->loadModel('Users');
        $queryArr=$this->Common->getQueryStrings();        
        extract($queryArr);

        $messages = [];
        $this->set([
            'messages'   => $messages,
            '_serialize' => ['message']
            ]); 
    }
}
