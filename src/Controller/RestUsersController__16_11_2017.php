<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Core\Exception\Exception;

use App\Controller\AppController;
// In a controller or table method.
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;




/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class RestUsersController extends AppController
{
    //public $component = array('RequestHandler');

   public function beforeFilter(Event $event)
   {
    parent::beforeFilter($event);
    $this->loadModel("Users");


}

// public function index(){
//     echo "hellodfsdfsdfddfsdf"; die;
// }


    /**
     * Function to add a new user
     *
     * @return Json message

     *      */
    public function add() {
        // pr($this->request->data); die;
     $action = $this->request->data['action'];   
     $status = "fail";  
     $userdata = [];  
     $from_email = Configure::read('FROM_EMAIL');
     $project_name = Configure::read('project_name');
     $url = Configure::read('SITEURL');
     $maildata = array(); 
     switch ($action) {

        case 'register':
        try{
            // echo "helo"; die;
            // pr($this->request->data); die;
            if (empty($this->request->data['first_name'])) {
                throw new Exception("Please enter first_name.");
            }
            if (empty($this->request->data['last_name'])) {
                throw new Exception("Please enter last name");
            }
            if (empty($this->request->data['password'])) {
                throw new Exception("Please enter password");
            }

            if (empty($this->request->data['email']) || !filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
                throw new Exception("Please enter valid email");
            }
            $check_user_email = $this->Users->find("all", array("conditions" => array("Users.email" => $this->request->data['email'],"Users.status <= " => 1)))->count();
            if($check_user_email > 0){
                throw new Exception("Email already exists.");
            }else{

                $this->request->data['type'] = 1;
                $userData = $this->Users->newEntity($this->request->data);
                $this->Users->save($userData);                 
               
               $mail_content ="";
               $mail_content.='<p>Hi '.$this->request->data['first_name'].' ,</P>
                        
                <p>Thank you for your registering at Lizicards.</P> 
                <p>Send a special message to your friends and loved ones with Lizicards greeting cards.</p> 
                <p>If you have any questions, please email us '.$from_email.'.</p>              
                 <p>Many thanks, <br/> Lizicards Team</p>       
                      </td>                  
              </tr>
              
                <tr>
                
                
                  <td align="center">   <a href='.$url.' style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Send a greeting card </a> </td>
                  
                
                  </tr>';  
               $maildata['name'] = $this->request->data['first_name']." ".$this->request->data['last_name'];
               $maildata['email'] = $this->request->data['email']; 
               $maildata['heading'] = "Lizicards Registration.";   
               $maildata['url'] = $url;  
               $maildata['data'] = $mail_content;


               $email = new Email();
               $mail = $email->template('allmail')
                       ->emailFormat('html')
                       ->to($maildata['email'])
                       ->from([$from_email => $project_name])
                       ->subject($project_name.' :Registration')
                       ->viewVars($maildata)
                       ->send();
                $status = "success";
                $userData['id'] = $userData->id; 
                $message = 'Registration has been completed.';
               

            }


            $this->set([
               'status' => $status,       
               'message' => $message,
               'userData' => $userData,
               '_serialize' => ['status','message', 'userData']
               ]);

        }catch(Exception $ex) {
             // $this->Flash->error(__($ex->getMessage()));
          $this->set([
            'status' => $status, 
            'message' => $ex->getMessage(),
            'userData' => '',
            '_serialize' => ['status','message','userdata']
            ]);

      }
      break;

      case 'guestregister':
      try{
        if (empty($this->request->data['first_name'])) {
            throw new Exception("Please enter first_name.");
        }
        if (empty($this->request->data['last_name'])) {
            throw new Exception("Please enter last name");
        }          

        if (empty($this->request->data['email']) || !filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL)) {
            throw new Exception("Please enter valid email");
        }
        $check_user_email = $this->Users->find("all", array("conditions" => array("Users.email" => $this->request->data['email'],"Users.status <= " => 1)))->hydrate(false)->first();
        if(count($check_user_email) > 0){  

         $dat = $this->Users->get($check_user_email['id']);  
                   // pr($dat);die;               
         $userData = $this->Users->patchEntity($dat,$this->request->data);
         $this->Users->save($userData);
         $userData['id'] = $check_user_email['id']; 
         $status = "success"; 
         $message = 'You are already registered as a normal user.';
     }else{

        $this->request->data['type'] = 2;
        $hashPswdObj = new DefaultPasswordHasher;
         $password = uniqid();
        // $this->request->data['password'] = $hashPswdObj->hash(uniqid());
         $this->request->data['password'] = $password;
        $userData = $this->Users->newEntity($this->request->data);
        $this->Users->save($userData);
        $userData['id'] = $userData->id;         
               $mail_content="";             
               $mail_content.='<p>Hi '.$this->request->data['first_name'].',</P>
                        
               <p>Thank you for your registering at Lizicards as a guest.</P>                 

               <p>Send a special message to your friends and loved ones with Lizicards greeting cards.</p> 
                 <p>If you have any questions, please email us '.$from_email.'.</p> 

                 <p>Many thanks, <br/> Lizicards Team</p> 
                
                      </td>
                  
              </tr>
              
                <tr>
                
                
                  <td align="center">   <a href="'.$url.'" style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Send a greeting card </a> </td>
                  
                
                  </tr>';
               $maildata['name'] = $this->request->data['first_name']." ".$this->request->data['last_name'];
               $maildata['email'] = $this->request->data['email'];
               $maildata['password'] = $password;   
               $maildata['url'] = $url;  
               $maildata['data'] = $mail_content;
               $maildata['heading'] = "Lizicards Registration.";  
               $email = new Email();
               $mail = $email->template('allmail')
                       ->emailFormat('html')
                       ->to($maildata['email'])
                       ->from([$from_email => $project_name])
                       ->subject($project_name.': Registration')
                       ->viewVars($maildata)
                       ->send(); 
        $status = "success"; 
        $message = 'Registration has been completed.';

    }
   $this->set([
               'status' => $status,       
               'message' => $message,
               'userData' => $userData,
               '_serialize' => ['status','message', 'userData']
               ]);

}catch(Exception $ex) {
             // $this->Flash->error(__($ex->getMessage()));
  $this->set([
            'status' => $status, 
            'message' => $ex->getMessage(),
            'userData' => '',
            '_serialize' => ['status','message','userdata']
            ]);

}
break;
case 'facebook_register':
try{
    if (!empty($this->request->data['userdata'])) {
        $this->loadModel("Users");
        $check_user_email = $this->Users->find("all", array("conditions" => array("Users.email" => $this->request->data['userdata']['email'])))->first();


        $check_user_email = $this->Users->find("all", array("conditions" => array("Users.email" => $this->request->data['email'],"Users.status <= " => 1)))->hydrate(false)->first();
        if(count($check_user_email) > 0){  

            $dat = $this->Users->get($check_user_email['id']); 
            $this->request->data['first_name'] = $this->request->data['userdata']['first_name'];
            $this->request->data['last_name'] = $this->request->data['userdata']['last_name'];
            $this->request->data['email'] = $this->request->data['userdata']['email']; 
            $this->request->data['fb_id'] = $this->request->data['userdata']['id'];                               
            $userData = $this->Users->patchEntity($dat,$this->request->data);
            $this->Users->save($userData);
            $userData['id'] = $check_user_email['id']; 
             $status = "success"; 
            $message = 'Register Successfully.';
        }else{
            $this->request->data['first_name'] = $this->request->data['userdata']['first_name'];
            $this->request->data['last_name'] = $this->request->data['userdata']['last_name'];
            $this->request->data['email'] = $this->request->data['userdata']['email'];
            $this->request->data['fb_id'] = $this->request->data['userdata']['id']; 
            $hashPswdObj = new DefaultPasswordHasher;
            // $this->request->data['password'] = $hashPswdObj->hash(uniqid());  
            $password = uniqid();
             $this->request->data['password'] = $password;
            $this->request->data['type'] = 3;
            $this->request->data['status'] = "1";
            $userData = $this->Users->newEntity($this->request->data);
            $this->Users->save($userData);
            $userData['id'] = $userData->id; 
             $mail_content="";               
               $mail_content.='<p>Hi '.$this->request->data['userdata']['first_name'].',</P>
                        
                <p>Thank you for your registering at Lizicards</P>
                <p>These are the login credentials.</p>

                <p>Username: <b>'.$this->request->data['userdata']['email'].'</b></p>
                <p>Password: <b>'.$password.'</b></p>

               <p>Send a special message to your friends and loved ones with Lizicards greeting cards.</p> 
                  <p>If you have any questions, please email us '.$from_email.'.</p> 

                  <p>Many thanks, <br/> Lizicards Team</p>
                
                      </td>
                  
              </tr>
              
                <tr>
                
                
                  <td align="center">   <a href="'.$url.'" style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: &#39;Open Sans&#39;, sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Send a greeting card </a> </td>
                  
                
                  </tr>';
               $maildata['name'] = $this->request->data['userdata']['first_name']." ".$this->request->data['userdata']['last_name'];
               $maildata['email'] = $this->request->data['userdata']['email'];
               $maildata['password'] = $password;   
               $maildata['url'] = $url;  
               $maildata['data'] = $mail_content;
               $maildata['heading'] = "Lizicards Registration.";     
               $email = new Email();
               $mail = $email->template('allmail')
                       ->emailFormat('html')
                       ->to($maildata['email'])
                       ->from([$from_email => $project_name])
                       ->subject($project_name.':Successfully Register')
                       ->viewVars($maildata)
                       ->send();
             $status = "success";                  
            $message = 'Registration has been completed.';

        }
       $this->set([
               'status' => $status,       
               'message' => $message,
               'userData' => $userData,
               '_serialize' => ['status','message', 'userData']
               ]);
    }


}catch(Exception $ex) {
             // $this->Flash->error(__($ex->getMessage()));
   $this->set([
            'status' => $status, 
            'message' => $ex->getMessage(),
            'userData' => '',
            '_serialize' => ['status','message','userdata']
            ]);

}
break;
        // $this->request->data['type'] = 1;
        // $userData = $this->Users->newEntity($this->request->data);
        //             $errors = $userData->errors();
        //             $errorType = 0;

        //             if(count($errors) <= 0){
        //                 if($this->Users->exists(array("email" => $this->request->data['email']))){
        //                     $message = 'Duplicate Email';
        //                 }
        //                 elseif ($this->Users->save($userData)) {
        //                     $userData['id'] = $userData->id; 
        //                     /*calling the credits function , sign_up is action key in actionsetting table*/

        //                  $message = 'Registration has been completed.';
        //                     $errorType = 1;

        //                     //calling log function 
        //                 } else {
        //                     $message = "Error";
        //                 }
        //                 $this->Flash->error(__($message));
        //             }else {
        //                 $message = "Error";
        //             }



break;

}

}

public function view($id)
{
    $queryArr=$this->Common->getQueryStrings();   

    if($queryArr)
        extract($queryArr); 

    $data = [];
    $this->set([
        'data'=>$data,
        '_serialize' => ['data']
        ]);
}


public function edit($id)
{
    $this->loadModel('Users');
    $data = [];

    $this->set([
        'status' => 'success',
        'data' => $data,
        '_serialize' => ['message', 'errorType']
        ]);
}





public function delete($id)
{
    $this->loadModel('Users');
    $queryArr=$this->Common->getQueryStrings();        
    extract($queryArr);

    $messages = [];
    $this->set([
        'messages'   => $messages,
        '_serialize' => ['message']
        ]); 
}
}
