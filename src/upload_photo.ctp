    <div class="photo-frame">      
      <div class="row pre_next_btn">
            <!-- <div class="col-lg-8">
                <h1>CARD: <span><?php if(!empty($templates_images['name'])){echo $templates_images['name'];} ?></span></h1>
              </div> --> 
              <div class="col-lg-6">
                <a href="javascript:void(0)" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==2 || empty($this->request->session()->read("GuestUser.page_number")) )?"none":"block"; ?>" data-type="<?php echo @$this->request->session()->read("card_data.send");?>"  data-page_number="<?php echo !empty($this->request->session()->read("GuestUser.page_number"))?$this->request->session()->read("GuestUser.page_number"):$page_number;?>" class="btn btn-lg btn-default lead-btn pull-right back pull-left prev_btn"><i class="fa fa-arrow-left myarrow" aria-hidden="true"></i>Back  </a>
              </div>     
              <div class="col-lg-6 next_btn_div" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")>=5 || empty($this->request->session()->read("GuestUser.image_name")) )?"none":"block"; ?>">
                <a href="javascript:void(0)" data-page_number="<?php echo !empty($this->request->session()->read("GuestUser.page_number"))?$this->request->session()->read("GuestUser.page_number"):$page_number;?>" class="btn btn-lg btn-default lead-btn pull-right next_btn">Next <i class="fa fa-arrow-right myarrow" aria-hidden="true"></i> </a>
              </div>
            </div>
            <?php
            //pr($this->request->session()->read('GuestUser'));
            ?>
            <center><?php echo $this->Flash->render();?></center>
            <div class="uplaod-card">
              <div class="row rowdata" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")>=4)?"none":"block"; ?>">        
                <div class="col-lg-12 myrow">
                  <div class="uplaod-card-img">

                    <?php 
                    $main_background = '../img/upload-img-text.png';
                    $user_image      = 'upload-img-dummy.png';
                    if(isset($templates_images['templates_images']) && count($templates_images['templates_images'])>0  ) {
                      foreach($templates_images['templates_images'] as $key=>$val) {
                        if($val['show_preview']==1) {
                          $main_background = $SITEURL.'/img/template_images/'.$val['images'];
                          break;
                        }
                      }
                    }

                    if($this->request->session()->check('GuestUser.image_name')) {
                      $user_image      = 'user_images/'.$this->request->session()->read('GuestUser.image_name');
                    }


                    ?>
                    <style>
                      .card-img {
                        background: rgba(0, 0, 0, 0) url("<?php echo $main_background; ?>") no-repeat scroll center center;
                      }
                    </style>
                    <div class="card-img">
                      <div class="row">
                        <div class="col-lg-6">                
                        </div>
                        <div class="col-lg-6 mycol">
                          <div class="card-orignal-image">                    
                            <?php echo $this->Html->image($user_image,['class'=>'user-inner-image user_card_image']); ?> 

                            <div class="add-photo-button" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==2 || empty($this->request->session()->read("GuestUser.page_number")))?"block":"none"; ?>">
                              <a href="javascript:void(0);" class="btn btn-lg btn-default lead-btn avatar-view">Add Your Photo </a></div>
                              <div class="my-add-audio" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==3)?"block":"none"; ?>">
                                <div class="record-your-voice recordrtc">
                                  <input type="hidden" class="recording-media" value="record-audio">
                                  <button class="btn btn-lg btn-default record-btn">Start Recording</button><div class="timer" id="timer" style="display: none;">30 sec remaining</div>
                                  <div id="record_img" style="text-align: center; display: none;">
                                  </div>
                                  <br>
                                  <video controls muted="" fullscreen=false style="display: none;"></video>
                                  <div style="text-align: center; display: none;">
                                    <button  id="save-to-disk">Save To Disk</button>
                                    <button class="btn btn-lg btn-default upload-btn" id="upload-to-server">Upload To Server</button>
                                  </div>
                                  <?php  if($this->request->session()->check('GuestUser.audio_name')) {

                                    $user_audio   = 'img/user_audio/'.$this->request->session()->read('GuestUser.audio_name'); ?>
                                    <audio preload="auto" src="<?php echo $SITEURL.$user_audio;?>" controls=""></audio>

                                    <?php  } ?>


                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <!-- Cropping modal -->
                          <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <form class="avatar-form" action="#" enctype="multipart/form-data" method="post">
                                  <div class="modal-header upload-photo-popup">
                                    <button type="button" class="close my-close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title" id="avatar-modal-label">Upload Your Image</h4>
                                  </div>
                                  <div class="modal-body">
                                    <div class="avatar-body">

                                      <!-- Upload image and data -->
                                      <div class="avatar-upload">
                                        <input type="hidden" name="card_id" value="<?php echo $templates_images['id'];?>">
                                        <input type="hidden" class="avatar-src" name="avatar_src">
                                        <input type="hidden" class="avatar-data" name="avatar_data">
                                        <label for="avatarInput"></label>
                                        <input type="file" class="avatar-input my-new-button" id="avatarInput" name="avatar_file">
                                        <!--<div class="fileUpload btn btn-primary" id="avatarInput" type="file" class="avatar-input" name="avatar_file">
    									<span>Browse...</span>
    									<input type="file" class="upload" />
										</div>-->
                                      </div>

                                      <!-- Crop and preview -->
                                      <div class="row">
                                        <div class="col-md-9">
                                          <div class="avatar-wrapper"></div>
                                        </div>
                             <!--  <div class="col-md-3">
                                <div class="avatar-preview preview-lg"></div>
                                <div class="avatar-preview preview-md"></div>
                                <div class="avatar-preview preview-sm"></div>
                              </div> -->
                            </div>

                            <div class="row avatar-btns" style="display:none;">
                              <div class="col-md-9">
                                <div class="btn-group">
                                  <button type="button" class="btn btn-primary rotate-button" data-method="rotate" data-option="-90" title="Rotate -90 degrees">Rotate Left</button>
                                 <!--  <button type="button" class="btn btn-primary" data-method="rotate" data-option="-15">-15deg</button>
                                  <button type="button" class="btn btn-primary" data-method="rotate" data-option="-30">-30deg</button>
                                  <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45">-45deg</button> -->
                                </div>
                                <div class="btn-group">
                                  <button type="button" class="btn btn-primary rotate-button" data-method="rotate" data-option="90" title="Rotate 90 degrees">Rotate Right</button>
                                  <!-- <button type="button" class="btn btn-primary" data-method="rotate" data-option="15">15deg</button>
                                  <button type="button" class="btn btn-primary" data-method="rotate" data-option="30">30deg</button>
                                  <button type="button" class="btn btn-primary" data-method="rotate" data-option="45">45deg</button> -->
                                </div>
                              </div>
                              <div class="col-md-3">
                                <button type="submit" class="btn btn-primary btn-block avatar-save done-button">Done</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div> -->
                      </form>
                    </div>
                  </div>
                </div><!-- /.modal -->
              </div>             
            </div>
          </div>
        </div>
        <!--- Registration Form -->
        
        <div class="row registration_form" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==4 && empty($this->request->session()->read("Auth.User.id")))?"block":"none"; ?>">
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

            <div class="sign-up-col">
              <i class="fa fa-pencil sign-up-icon" aria-hidden="true"></i><h1>Sign Up Now</h1>
              <p>By Joining you agree to our <a href="#"> Terms & Privacy Policy</a>. Plus, get order updates, sale alerts and more in your inbox. </p>

              <form role="form" id="normal_user_form" method="post">
                <div class="form-group">
                  <input name="first_name" class="form-control" style="border-radius:0px" id="" placeholder="First Name" type="text">
                </div>
                <div class="form-group">
                  <input name="last_name" class="form-control" style="border-radius:0px" id="" placeholder="Last Name" type="text">
                </div>

                <div class="form-group">
                  <input name="email" class="form-control" style="border-radius:0px" id="" placeholder="Email" type="email">
                </div>

                <div class="form-group">
                  <input name="password" class="form-control" style="border-radius:0px" id="" placeholder="Password" type="password">
                </div>
                <input name="type" value="1" type="hidden">
                <div class="form-group">
                  <button type="submit" class="btn btn-default submit-btn normal_user_submit">Join Now</button>
                </div>
                <div class="form-group" align="center">
                  <label class="already-member"> <a href="#">i'm already a Loud Card member</a> </label>
                </div>

              </form>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="or">OR</div>
            <div class="facebook-login">
              <a href="javascript:void(0)" onclick="fblogin()"><?php echo $this->Html->image("facbook-login.png");?></a>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="sign-up-col guest">
              <h1>Guest Checkout</h1>


              <form role="form" id="guest_user_form" method="post">
                <div class="form-group">
                  <input name="first_name" class="form-control" style="border-radius:0px" id="" placeholder="First Name" type="text">
                </div>
                <div class="form-group">
                  <input name="last_name" class="form-control" style="border-radius:0px" id="" placeholder="Last Name" type="text">
                </div>

                <div class="form-group">
                  <input name="email" class="form-control" style="border-radius:0px" id="" placeholder="Email" type="email">
                </div>
                <input name="type" value="2" type="hidden">

                <div class="form-group">
                  <button type="submit" class="btn btn-default guest-btn">Continue as Guest</button>
                </div>

              </form>
            </div>
          </div>
        </div>


        <!-- Select sending type-->

        <div class="row sending_type" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==5 && !empty($this->request->session()->read("Auth.User.id")))?"block":"none"; ?>">
          <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
            <div class="mail-card-reciept mail-card-reciept-receipent" align="center">
              <?php echo $this->Html->image("mail-reciept.png");?>
              <p>Post the card <br/>
              directly to my friend(s).</p>
            </div>

          </div>
          
          <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          <div class="or">OR</div>
          </div>

          <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" align="center">

            <div class="mail-card-reciept me mail-card-reciept-me" align="center">
              <?php echo $this->Html->image("mail-to-me.png");?>
             <p>Post the card <br/>
             to me.</p>
            </div>
            
          </div>
        </div>


        <!--Mail card to me -->

        <div class="row card_to_me_text" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==6 && !empty($this->request->session()->read("Auth.User.id")) && $this->request->session()->read("type")== "to_me")?"block":"none"; ?>">
          <div class="col-lg-12">
            <div class="mail-reciptant card-to-me">
              <?php echo $this->Html->image("mail-to-me.png");?>
              <span>Post</span> the card to me</h1>
            </div>
          </div>
        </div>


        <div class="row card_to_me" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==6 && !empty($this->request->session()->read("Auth.User.id")) && $this->request->session()->read("type")== "to_me")?"block":"none"; ?>">
          <form role="form" action="" method="post" id="card_to_me_form"> 
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">

              <div class="mail-reciptant-col">


              <input type="hidden" value="<?php echo $id;?>" name="template_id">
                <input type="hidden" class="send_to_me" name="send" value="<?php echo $this->request->session()->read("type");?>">
                <div class="form-group">
                  <input class="form-control" tabindex="1" name="first_name" value="<?php echo $this->request->session()->read("Auth.User.first_name");?>" style="border-radius:0px" id="" placeholder="First Name" type="text">
                </div>
                <div class="form-group">
                  <input class="form-control" tabindex="3" name="address_one" style="border-radius:0px" id="" placeholder="Address 1" type="text">
                </div>

                 <div class="row">
               <div class="col-lg-12">
              <div class="form-group country_name_list">
                <?php echo $this->Form->input('country_id', ['class' => 'form-control country_name_list_action', "tabindex" => "5", 'id' =>'country_name_list', 'label' => false,'options' => $country_list,'default' => 231]); ?>
                </div>
              </div>
              </div>

               <div class="row" id="country_name_lists">
                  <div class="col-lg-6">
                  <input class="form-control" name="city" style="border-radius:0px" id="" placeholder="City" type="text">
                  <!-- <?php echo $this->Form->input('city_id', ['class' => 'form-control', "tabindex" => "7", 'label' => false,'options' => $city_list]); ?> -->
                 </div>
                 <div class="col-lg-6">
                  <div class="form-group">
                    <input class="form-control" name="zip" tabindex="8" style="border-radius:0px" id="" placeholder="Zip" type="mail">
                  </div>
                </div>
              </div>

              <div class="row show_shiiping_option">
                <div class="col-md-12 col-sm-12 col-xs-12 radio-button">
                  <input checked="checked" class="radio-shipping" type="radio" name="shipping" value="0"> Free US shipping (average 4-5 days) : <strong>Free</strong><br>
                  <input class="radio-shipping" type="radio" name="shipping" value="3"> Expedited shipping (average 2-3 days) : <strong>$3</strong><br>
                  <input class="radio-shipping" type="radio" name="shipping" value="5"> Next day rush order (next day delivery if ordered before 12pm) : <strong>$5</strong>
                </div>
              </div>


              
             
              <div class="form-group">
                <button type="submit" class="btn btn-default submit-btn">Add to Cart</button>
              </div>       
              

            </div>
          </div>

          <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">

            <div class="mail-reciptant-col">



              <div class="form-group">
                <input class="form-control" tabindex="2" name="last_name" value="<?php echo $this->request->session()->read("Auth.User.last_name");?>" style="border-radius:0px" id="" placeholder="Last Name" type="text">
              </div>
              <div class="form-group">
                <input class="form-control" tabindex="4" name="address_two" style="border-radius:0px" id="" placeholder="Address (Optional)" type="text">
              </div>
              
              <div class="form-group show_state_list">
                <?php echo $this->Form->input('state_id', ['class' => 'form-control', "tabindex" => "6", 'label' => false,'options' => $state_list]); ?>
      <!-- <select class="form-control" id="">
        <option>State</option>
      </select> -->

    </div>
    <div class="form-group">
    </div>
    <div class="row">
    <div class="col-lg-6">
              <div class="count-input">             
                <a class="incr-btn decrease_btn" data-action="decrease" href="javascript:void(0)">–</a>
                <input class="quantity" tabindex="9" readonly="readonly" type="text" name="quantity" value="1"/>
                <a class="incr-btn increase_btn" data-action="increase" href="javascript:void(0)">&plus;</a>
        </div>
      </div>
    </div>

  </div>


</div>
</form>
</div>
<!-- Mail card to receipent -->
<div class="row mail_receipent" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==6 && !empty($this->request->session()->read("Auth.User.id")) && $this->request->session()->read("type")== "to_receipent")?"block":"none"; ?>">
  <div class="col-lg-12">
    <div class="mail-reciptant">
      <?php echo $this->Html->image("mail-reciept.png");?>
      <h1><span>Post</span> the card directly to my friend(s).</h1>
    </div>
  </div>
</div>


<div class="form-row mail_receipent mail-reciptant-col" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==6 && !empty($this->request->session()->read("Auth.User.id")) && $this->request->session()->read("type")== "to_receipent")?"block":"none"; ?>">
  <form role="form" action="" method="post" id="card_to_receipent_form">
  <input type="hidden" value="<?php echo $id;?>" name="template_id">
   <input type="hidden" class="send_to_receipent" name="send" value="<?php echo $this->request->session()->read("type");?>">

   <div class="row">
    <div class="col-md-5 col-sm-6 col-xs-12"> 
      <div class="form-group">
        <input class="form-control  first_name" name="data[0][first_name]" style="border-radius:0px" id="" placeholder="First Name" type="text">
      </div>
    </div>
    <div class="col-md-5 col-sm-6 col-xs-12"> <div class="form-group">
      <input class="form-control last_name" name="data[0][last_name]" style="border-radius:0px" id="" placeholder="Last Name" type="text">
    </div> </div>    
  </div>

  <div class="row">
    <div class="col-md-5 col-sm-6 col-xs-12"> 
      <div class="form-group">
        <input class="form-control address_one" name="data[0][address_one]" style="border-radius:0px" id="" placeholder="Address 1" type="text">
      </div>
    </div>
    <div class="col-md-5 col-sm-6 col-xs-12">  <div class="form-group">
      <input class="form-control" name="data[0][address_two]" style="border-radius:0px" id="" placeholder="Address (Optional)" type="text">
    </div> </div>    
  </div>

  <div class="row">

    <div class="col-md-5 col-sm-6 col-xs-12">   <div class="form-group">
   <?php echo $this->Form->input('data[0][country_id]', ['class' => 'form-control country_fees', 'id' => '0', 'label' => false,'options' => $country_list,'default' => 231]); ?>
 </div> </div>    
   <div class="col-md-5 col-sm-6 col-xs-12">   <div class="form-group show_state_list0">
     <?php echo $this->Form->input('data[0][state_id]', ['class' => 'form-control', 'label' => false,'options' => $state_list]); ?>

   </div> </div>    
 </div>

  <div class="row" id='country_fees0'>

 <div class="col-md-5 col-sm-6 col-xs-12">

    <div class="form-group">
    <input class="form-control city" name="data[0][city]" style="border-radius:0px" id="" placeholder="City" type="text">
     <!-- <?php echo $this->Form->input('data[0][city_id]', ['class' => 'form-control city', 'label' => false,'options' => $city_list]); ?> -->
   </div>      
   </div>
  <div class="col-md-5 col-sm-6 col-xs-12"> 
    <div class="form-group">
      <input class="form-control zip" name="data[0][zip]" style="border-radius:0px" id="" placeholder="Zip" type="text">
    </div>
  </div>
      
</div>

  <div class="row show_shiiping_option">
                <div class="col-md-12 col-sm-12 col-xs-12 radio-button">
                  <input checked="checked" class="radio-shipping" type="radio" name="shipping" value="0"> Free US shipping (average 4-5 days) : <strong>Free</strong><br>
                  <input class="radio-shipping" type="radio" name="shipping" value="3"> Expedited shipping (average 2-3 days) : <strong>$3</strong><br>
                  <input class="radio-shipping" type="radio" name="shipping" value="5"> Next day rush order (next day delivery if ordered before 12pm) : <strong>$5</strong>
                </div>
              </div>




<div class="show_more_receipent"></div>

<div class="row">
  <div class="col-md-5 col-sm-6 col-xs-12"> 
    <div class="form-group">
      <button type="submit" class="btn btn-default submit-btn">Add to Cart</button>
    </div>  
  </div>
  <div class="col-md-6 col-sm-6 col-xs-12">   <div class="form-group"> <a href="javascript:void(0)" class="btn btn-default submit-btn add-form add_more_receipent">Send same card to one more person </a> <a href="javascript:void(0)" id="" class="btn btn-danger remove_receipent">Remove this Recipient</a> </div> </div>    
</div>

<style type="text/css"> .add-form {
  background: #413a7d !important;
} 
.btn.btn-danger.remove_receipent {
  display: none;
  border: medium none;
  color: #ffffff;
  font-size: 17px;
  padding: 14px 24px;
  position:relative;
  top:13px;
}

</style>      

</form>
</div>
<!-- Cart page -->

    <div class="cart-conatiner" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==7 && !empty($this->request->session()->read("Auth.User.id")))?"block":"none"; ?>">
      <div class="master-cart__full">
        
      </div> 
    </div>

     <!-- Payment page html (Checkout)-->

     <div class="chackout_page" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==8 && !empty($this->request->session()->read("Auth.User.id")))?"block":"none"; ?>">
     
      
    </div>




</div>

</div>

</div>

</div>
</div>
</div>
</div>
<?php echo $this->element("audio_footer");?>

<style>

  .record-your-voice.recordrtc > audio {
    vertical-align: bottom;
    width: 99%;
    font-size: 1.2em;
  }

  video {
    width: 10em !important;
  }
  hr{
    display: none;
  }

</style>
<?php echo $this->Html->script("recordRTC");?>
<?php echo $this->Html->script("gumadapter");?>

<script>
  (function() {
    var params = {},
    r = /([^&=]+)=?([^&]*)/g;
    function d(s) {
      return decodeURIComponent(s.replace(/\+/g, ' '));
    }
    var match, search = window.location.search;
    while (match = r.exec(search.substring(1))) {
      params[d(match[1])] = d(match[2]);
      if(d(match[2]) === 'true' || d(match[2]) === 'false') {
        params[d(match[1])] = d(match[2]) === 'true' ? true : false;
      }
    }
    window.params = params;
  })();
</script>

<script>
        // var initial = 2000;
        // var count = initial;
        //  var counter;
        var counter = 20;
        //  var interval = "";
        var recordingDIV = document.querySelector('.recordrtc');
        var recordingMedia = recordingDIV.querySelector('.recording-media');
        var recordingPlayer = recordingDIV.querySelector('video');
        var mediaContainerFormat = recordingDIV.querySelector('.media-container-format');
        recordingDIV.querySelector('button').onclick = function() {
          var button = this;
          if(button.innerHTML === 'Stop Recording') {  
            document.getElementById('record_img').style="display:none";
            document.getElementById('save-to-disk').style="display:none"; 
            setTimeout(function(){ $(document).find("#upload-to-server").trigger("click"); }, 2000);                          
            var audiolength=document.getElementsByTagName('audio').length;
            if(audiolength > 0){
              var audiolength=document.getElementsByTagName('audio')[0].remove();
              document.getElementsByTagName('hr')[0].remove();
            }
            button.disabled = true;
            button.disableStateWaiting = true;
            setTimeout(function() {
              button.disabled = false;
              button.disableStateWaiting = false;
            }, 2 * 1000);
            button.innerHTML = 'Start Recording';
            function stopStream() {

              if(button.stream && button.stream.stop) {
                button.stream.stop();
                button.stream = null;
                counter = 0;
                clearInterval(interval);
                $('#timer').hide();
              }
            }
            if(button.recordRTC) {
              if(button.recordRTC.length) {
                button.recordRTC[0].stopRecording(function(url) {
                  if(!button.recordRTC[1]) {
                    button.recordingEndedCallback(url);
                    stopStream();                                    
                    saveToDiskOrOpenNewTab(button.recordRTC[0]);
                    return;
                  }
                  button.recordRTC[1].stopRecording(function(url) {
                    button.recordingEndedCallback(url);
                    stopStream();
                  });
                });
              }
              else {
                button.recordRTC.stopRecording(function(url) {
                  button.recordingEndedCallback(url);
                  stopStream();
                  saveToDiskOrOpenNewTab(button.recordRTC);
                });
              }
            }
            return;
          }
          button.disabled = true;
          var commonConfig = {
            onMediaCaptured: function(stream) {                        
              button.stream = stream;
              if(button.mediaCapturedCallback) {
                button.mediaCapturedCallback();
              }
              button.innerHTML = 'Stop Recording';
              button.disabled = false;
              document.getElementById('upload-to-server').innerHTML="Upload To Server";
            },
            onMediaStopped: function() {

              button.innerHTML = 'Start Recording';
              if(!button.disableStateWaiting) {
                button.disabled = false;

              }
            },
            onMediaCapturingFailed: function(error) {
              if(error.name === 'PermissionDeniedError' && !!navigator.mozGetUserMedia) {
                InstallTrigger.install({
                  'Foo': {
                                    // https://addons.mozilla.org/firefox/downloads/latest/655146/addon-655146-latest.xpi?src=dp-btn-primary
                                    URL: 'https://addons.mozilla.org/en-US/firefox/addon/enable-screen-capturing/',
                                    toString: function () {
                                      return this.URL;
                                    }
                                  }
                                });
              }
              commonConfig.onMediaStopped();
            }
          };

          if(recordingMedia.value === 'record-audio'){                    
            captureAudio(commonConfig);
            button.mediaCapturedCallback = function() {
              button.recordRTC = RecordRTC(button.stream, {
                type: 'audio',
                bufferSize: typeof params.bufferSize == 'undefined' ? 0 : parseInt(params.bufferSize),
                sampleRate: typeof params.sampleRate == 'undefined' ? 44100 : parseInt(params.sampleRate),
                leftChannel: params.leftChannel || false,
                disableLogs: params.disableLogs || false,
                recorderType: webrtcDetectedBrowser === 'edge' ? StereoAudioRecorder : null

              });
              button.recordingEndedCallback = function(url) {
                var audio = new Audio();
                audio.src = url;
                audio.controls = true;
                recordingPlayer.parentNode.appendChild(document.createElement('hr'));
                recordingPlayer.parentNode.appendChild(audio);
                if(audio.paused) audio.play();
                audio.onended = function() {
                  audio.pause();
                  audio.src = URL.createObjectURL(button.recordRTC.blob);
                };
              };
              button.recordRTC.startRecording();
              $("#timer").show();
              counter = 30;
              $(document).find("audio").remove();
                        // counter = setInterval(timer, 10);                        
                        //    displayCount(initial);

                        interval = setInterval(function() {

                            // Display 'counter' wherever you want to display it.
                            if (counter == 1) {    
                                // alert(1);                            
                                $(".record-btn").trigger("click");
                                clearInterval(interval);
                                $("#timer").hide();
                                
                                
                              }
                              document.getElementById("timer").innerHTML=counter+" sec remaining";
                              counter--     
                            }, 1000);
                        document.getElementById('record_img').style="display:block";
                        document.getElementById('upload-to-server').style="display:none";
                      };
                    }

                  };

                  function captureAudio(config) {
                    captureUserMedia({audio: true}, function(audioStream) {
                      recordingPlayer.srcObject = audioStream;
                      recordingPlayer.play();
                      config.onMediaCaptured(audioStream);
                      audioStream.onended = function() {
                        config.onMediaStopped();                        
                      };
                    }, function(error) {
                      config.onMediaCapturingFailed(error);
                    });
                  }


                  function captureUserMedia(mediaConstraints, successCallback, errorCallback) {
                    navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
                  }
                  function setMediaContainerFormat(arrayOfOptionsSupported) {
                    var options = Array.prototype.slice.call(
                      mediaContainerFormat.querySelectorAll('option')
                      );
                    var selectedItem;
                    options.forEach(function(option) {
                      option.disabled = true;
                      if(arrayOfOptionsSupported.indexOf(option.value) !== -1) {
                        option.disabled = false;
                        if(!selectedItem) {
                          option.selected = true;
                          selectedItem = option;
                        }
                      }
                    });
                  }
                  recordingMedia.onchange = function() {
                    if(this.value === 'record-audio') {
                      setMediaContainerFormat(['WAV', 'WAV']);
                      return;
                    }

                    setMediaContainerFormat(['WebM', /*'Mp4',*/ 'Gif']);
                  };
                  if(webrtcDetectedBrowser === 'edge') {
                // webp isn't supported in Microsoft Edge
                // neither MediaRecorder API
                // so lets disable both video/screen recording options
                console.warn('Neither MediaRecorder API nor webp is supported in Microsoft Edge. You cam merely record audio.');
                recordingMedia.innerHTML = '<option value="record-audio">Audio</option>';
                setMediaContainerFormat(['WAV']);
              }
              if(webrtcDetectedBrowser === 'firefox') {
                // Firefox implemented both MediaRecorder API as well as WebAudio API
                // Their MediaRecorder implementation supports both audio/video recording in single container format
                // Remember, we can't currently pass bit-rates or frame-rates values over MediaRecorder API (their implementation lakes these features)
                // recordingMedia.innerHTML = '<option value="record-audio-plus-video">Audio+Video</option>'
                //                             + '<option value="record-audio-plus-screen">Audio+Screen</option>'
                //                             + recordingMedia.innerHTML;
              }
            // disabling this option because currently this demo
            // doesn't supports publishing two blobs.
            // todo: add support of uploading both WAV/WebM to server.
            if(false && webrtcDetectedBrowser === 'chrome') {
              recordingMedia.innerHTML = '<option value="record-audio-plus-video">Audio+Video</option>'
              + recordingMedia.innerHTML;
              console.info('This RecordRTC demo merely tries to playback recorded audio/video sync inside the browser. It still generates two separate files (WAV/WebM).');
            }
            function saveToDiskOrOpenNewTab(recordRTC) {               
              recordingDIV.querySelector('#save-to-disk').parentNode.style.display = 'block';
              recordingDIV.querySelector('#save-to-disk').onclick = function() {                    
                if(!recordRTC) return alert('No recording found.');
                recordRTC.save();
              };
                // recordingDIV.querySelector('#open-new-tab').onclick = function() {
                //     if(!recordRTC) return alert('No recording found.');
                //     window.open(recordRTC.toURL());
                // };
                recordingDIV.querySelector('#upload-to-server').disabled = false;
                recordingDIV.querySelector('#upload-to-server').onclick = function() {
                  if(!recordRTC) return alert('No recording found.');
                  this.disabled = true;
                  var button = this;
                  uploadToServer(recordRTC, function(progress, fileURL) {
                    if(progress === 'ended') {
                      button.disabled = false;
                      button.innerHTML = 'Click to download from server';
                      button.onclick = function() {
                        window.open(fileURL);
                      };
                      return;
                    }
                    button.innerHTML = progress;
                  });
                };
              }
              var listOfFilesUploaded = [];
              function uploadToServer(recordRTC, callback) {
                var blob = recordRTC instanceof Blob ? recordRTC : recordRTC.blob;
                var fileType = blob.type.split('/')[0] || 'audio';
                var fileName = (Math.random() * 1000).toString().replace('.', '');
                if (fileType === 'audio') {
                    // fileName += '.' + (!!navigator.mozGetUserMedia ? 'ogg' : 'wav');
                    fileName += '.' + (!!navigator.mozGetUserMedia ? 'wav' : 'wav');
                  } else {
                    fileName += '.webm';
                  }
                // create FormData
                var formData = new FormData();
                formData.append(fileType + '-filename', fileName);
                formData.append(fileType + '-blob', blob);
                callback('Uploading ' + fileType + ' recording to server.');
                makeXMLHttpRequest(path+'templates/save/<?php echo $templates_images["id"];?>', formData, function(progress) {
                  if (progress !== 'upload-ended') {
                    callback(progress);
                    return;
                  }
                  var initialURL = path+'img/user_audio/';                    
                  callback('ended', initialURL + fileName);
                    // to make sure we can delete as soon as visitor leaves
                    listOfFilesUploaded.push(initialURL + fileName);
                  });
              }
              function makeXMLHttpRequest(url, data, callback) {
                var request = new XMLHttpRequest();
                request.onreadystatechange = function() {
                  if (request.readyState == 4 && request.status == 200) {
                    callback('upload-ended');
                  }
                };
                request.upload.onloadstart = function() {
                  callback('Upload started...');
                };
                request.upload.onprogress = function(event) {
                  callback('Upload Progress ' + Math.round(event.loaded / event.total * 100) + "%");
                };
                request.upload.onload = function() {
                  callback('progress-about-to-end');
                };
                request.upload.onload = function() {
                  callback('progress-ended');
                };
                request.upload.onerror = function(error) {
                  callback('Failed to upload to server');
                  console.error('XMLHttpRequest failed', error);
                };
                request.upload.onabort = function(error) {
                  callback('Upload aborted.');
                  console.error('XMLHttpRequest aborted', error);
                };
                request.open('POST', url);
                request.send(data);
              }
              window.onbeforeunload = function() {
                recordingDIV.querySelector('button').disabled = false;
                recordingMedia.disabled = false;
                mediaContainerFormat.disabled = false;
                if(!listOfFilesUploaded.length) return;
                listOfFilesUploaded.forEach(function(fileURL) {
                  var request = new XMLHttpRequest();
                  request.onreadystatechange = function() {
                    if (request.readyState == 4 && request.status == 200) {
                      if(this.responseText === ' problem deleting files.') {
                        alert('Failed to delete ' + fileURL + ' from the server.');
                        return;
                      }
                      listOfFilesUploaded = [];
                      alert('You can leave now. Your files are removed from the server.');
                    }
                  };
                  request.open('POST', 'delete.php');
                  var formData = new FormData();
                  formData.append('delete-file', fileURL.split('/').pop());
                  request.send(formData);
                });
                return 'Please wait few seconds before your recordings are deleted from the server.';
              };

              /*Count down timer */

              function timer() {
                if (count <= 0) {
                  clearInterval(counter);
                  return;
                }
                count--;
                displayCount(count);
              }

              function displayCount(count) {
                var res = count / 100;
                if(res==0){
                  $(".record-btn").trigger("click");
                  clearInterval(counter);
                }        
                document.getElementById("timer").innerHTML = res.toPrecision(count.toString().length) + " secs";
              }
            </script>