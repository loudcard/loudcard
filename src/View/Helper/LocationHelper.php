<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         3.0.4
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;

class LocationHelper extends Helper {
    /*     * **
     * Function to get client name for particular bid
     * *** */

    public function country_name($id = null) {

        $country_name = TableRegistry::get('Countries');
        $fetch = $country_name->find()->select(['name'])->where(['id' => $id])->hydrate(false)->first();
        // debug($client_name); die;
        return $fetch;
    }

     public function state_name($id = null) {

        $state_name = TableRegistry::get('States');
        $fetch = $state_name->find()->select(['name','state_abbrivation','country_id'])->where(['id' => $id])->hydrate(false)->first();
        // debug($client_name); die;
        return $fetch;
    }

 

}
