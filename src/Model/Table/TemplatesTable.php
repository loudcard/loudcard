<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;


class TemplatesTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');	
    $this->hasMany('TemplatesImages', [
            'foreignKey' => 'template_id',
        ]);
     $this->hasMany('UsersAudio', [
            'foreignKey' => 'template_id',
        ]);
      $this->hasMany('UsersImages', [
            'foreignKey' => 'template_id',
        ]);
       $this->hasMany('Orders', [
            'foreignKey' => 'template_id',
        ]);
     $this->belongsTo('Tags', [
            'foreignKey' => 'tag_id',
        ]);

 
    }
}
?>
