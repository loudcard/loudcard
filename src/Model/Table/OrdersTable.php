<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;


class OrdersTable extends Table
{
    public function initialize(array $config)
    {
    	$this->belongsTo('Templates', [
            'foreignKey' => 'template_id',
        ]);
        $this->belongsTo('UsersAudio', [
            'foreignKey' => 'user_audio_id',
        ]);
        $this->belongsTo('UsersImages', [
            'foreignKey' => 'user_image_id',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
        ]);
         $this->hasMany('ShippingAddress', [
            'foreignKey' => 'order_id',
        ]);
        $this->addBehavior('Timestamp');	
    

 
    }
}
?>
