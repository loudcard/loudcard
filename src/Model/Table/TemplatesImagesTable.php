<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class TemplatesImagesTable extends Table
{
    public function initialize(array $config)
    {
        
        $this->addBehavior('Timestamp');
        $this->belongsTo('Templates', [
            'foreignKey' => 'template_id',
        ]);
     
    }
    
    
    
}
?>
