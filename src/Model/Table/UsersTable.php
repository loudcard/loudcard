<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');	
    

 
    }

        public function validationDefault(Validator $validator)
    {
        $validator = new Validator();
        $validator
            ->notEmpty('first_name', 'Please add your first name')
            ->notEmpty('email', 'Please add your Email')
            ->notEmpty('last_name', 'Please add your last name')
            ->notEmpty('password', 'Please add password')          
            ->add('email', '_empty', [
                'rule' => 'email',
                'message' => 'E-mail must be valid'
            ]);

        return $validator;
    }
}
?>
