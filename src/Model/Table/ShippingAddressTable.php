<?php
// src/Model/Table/UsersTable.php
namespace App\Model\Table;

use Cake\ORM\Table;


class ShippingAddressTable extends Table
{
    public function initialize(array $config)
    {
    	$this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
        ]);
        $this->addBehavior('Timestamp');	
    

 
    }
}
?>
