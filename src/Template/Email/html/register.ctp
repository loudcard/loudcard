<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Lizicards: Registration</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet"> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	
</head>

<body style="margin: 0; padding: 0;">

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#fff" >
	<tbody>
	<tr>
		<td width="100%" valign="top" align="center">
		
		
<table width="680" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#f2f2f2" style="">
							<tbody>
							
							<tr>
							
							<td>							
						<!-- Wrapper -->
							<table width="620" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border-radius:8px; margin-top: 30px;">
							<tbody>
							
							<tr>
								<td height="" width="100%" style="padding:20px;"> <a href="#"><img width="87" border="0" alt="" src="<?php echo $siteurl;?>img/mail_logo.png"></a> </td>
							</tr>
							
							<tr>						

											<td valign="middle" width="100%" style="text-align: left; font-family: 'Open Sans', sans-serif; font-size:17px; color: #60605e; line-height:28px; font-weight:normal;  padding:0 20px;">
											<h1 width="100%" style="text-align: left; font-family: 'Open Sans', sans-serif; font-size:30px; color: #60605e; line-height:38px; font-weight: 600; margin: 0 0 20px; padding: 0;">
															Registered to Lizicards.
														</h1>
									<p>Hi <?php echo $name;?>,</P>
												
								<p>Thanks for registration with LiziCards.Please click on link below to visit the site or copy and paste the link on the browser <b><?php echo $siteurl; ?></b>  </P>
								
								
												
								
												
											</td>
									
							</tr>
							
								<tr>
								
								
									<td align="center">	 	<a href="<?php echo $siteurl; ?>" style="padding:18px 40px; color: #ffffff; font-size:18px; text-decoration: none; line-height: 34px;  font-family: 'Open Sans', sans-serif; background:#3c387a;  display:inline-block;  border-radius: 8px;  font-weight: 600; letter-spacing: 1px; margin:30px 0;"> Visit Lizicards </a> </td>
									
								
									</tr>
			<tr>
<td align="center">	
           <a title="Facebook" style=" margin: 0 10px; color:#fff; background:#547bc0; padding:10px; display: inline-block; padding: 10px; width: 20px;  border-radius: 30px; " target="_blank" href="http://www.facebook.com/lizicards"><i class="fa fa-facebook"></i></a>
           <a title="instagram" style=" margin: 0 10px; color:#fff; background:#e64060; padding:10px; display: inline-block; padding: 10px; width: 20px;  border-radius: 30px;"  href="https://www.instagram.com/lizicards/"><i aria-hidden="true" class="fa fa-instagram"></i></a>
           <a title="Twitter" style=" margin: 0 10px; color:#fff; background:#78ccf3; padding:10px; display: inline-block; padding: 10px; width: 20px;  border-radius: 30px;"  href="https://twitter.com/lizicards"><i class="fa fa-twitter"></i></a>
    
</td>
									</tr>

						<tr>							
							<td align="center" height="30">	 </td>
								
									</tr>							
							
						</tbody></table><!-- End Wrapper 2 -->
						
						
						
						
								<table width="620" border="0" cellpadding="0" cellspacing="0" align="center" style="border-radius:8px; margin-top: 30px;">
							<tbody>
		
		                     <tr>
							
							<td align="center" style="text-align: center; font-family: 'Open Sans', sans-serif; font-size:17px; color: #30302e; line-height:28px; font-weight:normal;  padding:0 20px 40px; margin-bottom:50px;"> 

 © 2017 LiziCards Inc. Boston All Right Reserved <br>
 501 Silverside Road, Suite 105,Wilmington, Delaware 19809 USA 
<br> 
  <a style=" color: #30302e; margin:0 6px;" href="<?php echo $url;?>users/contact"> Contact Us </a> 
     <a style=" color: #30302e; margin:0 6px;" href="<?php echo $url;?>users/terms"> Terms & Conditions </a>
         <a style=" color: #30302e; margin:0 6px;" href="<?php echo $url;?>users/privacy"> Privacy Policy </a>
        
					 </td>
							</tr>
		
		</tbody></table>
		
		</td>
		
						 </td>
							</tr>
		
		</tbody></table>	
	
	</tr>
	
</tbody></table>


</body>

</html>



