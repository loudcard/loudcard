<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Lizicards: Forgot Password</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet"> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	
</head>

<body style="margin: 0; padding: 0;">

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#fff" >
	<tbody>
	<tr>
		<td width="100%" valign="top" align="center">
		
		
<table width="680" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#f2f2f2" style="">
							<tbody>
							
							<tr>
							
							<td>							
						<!-- Wrapper -->
							<table width="620" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="border-radius:8px; margin-top: 30px;">
							<tbody>
							
							<tr>
								<td height="" width="100%" style="padding:20px;"> <a href="<?php echo $url;?>"><img width="87" border="0" alt="" src="<?php echo $url;?>img/mail_logo.png"></a> </td>
							</tr>
							
							<tr>						

											<td valign="middle" width="100%" style="text-align: left; font-family: 'Open Sans', sans-serif; font-size:17px; color: #60605e; line-height:28px; font-weight:normal;  padding:0 20px;">
											<h1 width="100%" style="text-align: left; font-family: 'Open Sans', sans-serif; font-size:30px; color: #60605e; line-height:38px; font-weight: 600; margin: 0 0 20px; padding: 0;">
															<?php echo $heading;?>
														</h1>
							<?php echo $data;?>
							
			<tr>
		<td align="center">	
           <a title="Facebook" style=" margin: 0 10px; color:#fff;  padding:10px; display: inline-block; padding: 10px; width: 20px;  border-radius: 30px; " target="_blank" href="http://www.facebook.com/lizicards"><img src="<?php echo $url;?>img/fb.png"></a>
           <a title="instagram" style=" margin: 0 10px; color:#fff;  padding:10px; display: inline-block; padding: 10px; width: 20px;  border-radius: 30px;"  href="https://www.instagram.com/lizicards/"><img src="<?php echo $url;?>img/insta.png"></a>
           <a title="Twitter" style=" margin: 0 10px; color:#fff;  padding:10px; display: inline-block; padding: 10px; width: 20px;  border-radius: 30px;"  href="https://twitter.com/lizicards"><img src="<?php echo $url;?>img/twitter.png"></a>    
		</td>
									</tr>

						<tr>							
							<td align="center" height="30">	 </td>
								
									</tr>							
							
						</tbody></table><!-- End Wrapper 2 -->
						
						
						
						
								<table width="620" border="0" cellpadding="0" cellspacing="0" align="center" style="border-radius:8px; margin-top: 30px;">
							<tbody>
		
		                     <tr>
							
							<td align="center" style="text-align: center; font-family: 'Open Sans', sans-serif; font-size:17px; color: #30302e; line-height:28px; font-weight:normal;  padding:0 20px 40px; margin-bottom:50px;"> 

 © 2017 LiziCards. Boston. All Right Reserved. <br>
 745 Atlantic Ave, Boston, MA 02111, USA
<br> 
  <a style=" color: #30302e; margin:0 6px;" href="<?php echo $url;?>users/contact"> Contact Us </a> 
     <a style=" color: #30302e; margin:0 6px;" href="<?php echo $url;?>users/terms"> Terms & Conditions </a>
         <a style=" color: #30302e; margin:0 6px;" href="<?php echo $url;?>users/privacy"> Privacy Policy </a>
        
					 </td>
							</tr>
		
		</tbody></table>
		
		</td>
		
						 </td>
							</tr>
		
		</tbody></table>	
	
	</tr>
	
</tbody></table>


</body>

</html>






