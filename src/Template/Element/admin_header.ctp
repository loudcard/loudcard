<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=0">
  <title><?php echo $pageDescription ?></title>
  <?php
echo $this->Html->meta('icon','img/favicon.png');
?>
  <?php
    echo $this->Html->css('bootstrap-theme.min');
    echo $this->Html->css('bootstrap.min'); 
    echo $this->Html->css('font-awesome.min');
    echo $this->Html->css('custom.min');
    echo $this->Html->css('admincustom.css?'.date("is"));
    echo $this->Html->css('dataTables.bootstrap.min');
    echo $this->Html->css('buttons.bootstrap.min');
    echo $this->Html->css('fixedHeader.bootstrap.min');
    echo $this->Html->css('responsive.bootstrap.min');
    echo $this->Html->css('scroller.bootstrap.min');  
    echo $this->Html->script('jquery-1.10.2.min');
    echo $this->Html->script('custom.js?'.date("is"));
    echo $this->Html->script('sweetalert.min');
    echo $this->Html->css('sweetalert');

  ?>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script type="text/javascript">
    var path = '<?= $SITEURL; ?>';
  </script>  
</head>
<body class="nav-md">
  <div class="container body">
    <div class="main_container">
