<footer class="footer footer-clum">
    <div class="container">
    <div class="row">
        <div class="footer-logo"><?php echo $this->Html->image("logo.png",['atl' => "loudcards"]);?></div>
       <ul class="footer-menu">            
            <li><a href="<?php echo $SITEURL;?>users/terms"> Terms of Service </a></li>
            <li><a href="<?php echo $SITEURL;?>users/privacy"> Privacy Policy  </a></li>
            <li><a href="<?php echo $SITEURL;?>users/contact"> Contact Us </a></li>            
          </ul>
          <ul class="social-network social-circle">
            <li><a href="http://www.facebook.com/lizicards" target="_blank" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://www.instagram.com/lizicards/" target="_blank" class="icoinstagram" title="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <li><a title="pinterest" class="icoTwitter" target="_blank" href="https://pinterest.com/lizicards"><i class="fa fa-pinterest-p"></i></a></li>
    

          </ul>
      </div>
  </div>
    <div class="container">
    <div class="row copy">
        <div class=""> © 2017 LiziCards. Boston </div>
        <div class="pull-right">  </div>
      </div>
  </div>
  </footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 

<!-- Include all compiled plugins (below), or include individual files as needed --> 
<?php echo $this->Html->script("bootstrap.min");?>
<?php echo $this->Html->script("slider");?>
<?php echo $this->Html->script("sweetalert.min");?>
<?php echo $this->Html->script("custom");?>
<?php echo $this->Html->script("cropper.min");?>
<?php echo $this->Html->script("main");?>
<?php echo $this->Html->script("jquery.validate.min");?>
<?php echo $this->Html->script("validation");?>
<?php echo $this->Html->script("shieldui-all.min");?>
<?php echo $this->Html->Script('jquery.dataTables.min');?>
 <?php echo $this->Html->Script('dataTables.buttons.min');?>

</body>
  </html>
<script>
$(window).scroll(function() {

    if ($(this).scrollTop()>0)
     {
        $('.emr-ico').fadeOut();
     }
    else
     {
      $('.emr-ico').fadeIn();
     }
 });
 
 </script>
<script>
 $(window).scroll(function(){
        var wscroll = $(this).scrollTop();
        if(wscroll > 50){
         $(".navbar").addClass("shrink-nav");
        }
        else{
          $(".navbar").removeClass("shrink-nav");
        }
      });
 
  </script>
   <?php if(!empty($this->request->session()->read("GuestUser.page_number")) && $this->request->session()->read("GuestUser.page_number") == 8){  ?>
    <script type="text/javascript">
    $(document).ready(function(){   
   
       $.ajax({
            url: path + "users/address", //this is the submit URL
            type: 'POST', //or POST            
            success: function (data) {
            	 $(".container-wizard").hide();
              $(".master-cart__full").html(data);
               
              
              
                }
              
            // }
        });

    });
     

    </script>

    <?php } ?>

       <?php if(!empty($this->request->session()->read("GuestUser.page_number")) && $this->request->session()->read("GuestUser.page_number") == 9){  ?>
    <script type="text/javascript">
    $(document).ready(function(){ 
    // var options = {direction:'right',duration:500};  
    $(".cart-conatiner").hide();
        $(".next_btn").attr("data-page_number",9);
         $(".prev_btn").attr("data-page_number",9);      
       $.ajax({
            url: path + "users/checkout", //this is the submit URL
            type: 'POST', //or POST            
            success: function (data) {
             $(".chackout_page").show(); 
	   $(".container-wizard").hide(); 
            $(".chackout_page").html(data);
              
              
                }
              
            // }
        });

    });
     

    </script>

    <?php } ?>

    <script>
    var show_video = "";
   $(document).ready(function () {
        $('#user_list').DataTable({
          "pagingType": "full_numbers"        
        });
    });
</script>

<?php if($this->request->param("action")=="whatis"){?>
<script type="text/javascript">
  show_video = "yes";

</script>

<?php } ?>
<Script Language="JavaScript">
var nVer = navigator.appVersion;
var nAgt = navigator.userAgent;
var browserName  = navigator.appName;
var fullVersion  = ''+parseFloat(navigator.appVersion); 
var majorVersion = parseInt(navigator.appVersion,10);
var nameOffset,verOffset,ix;
var nav = window.navigator,  
ua = window.navigator.userAgent.toLowerCase();  
// In Opera, the true version is after "Opera" or after "Version"
if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
 browserName = "Opera";
 fullVersion = nAgt.substring(verOffset+6);
 if ((verOffset=nAgt.indexOf("Version"))!=-1) 
   fullVersion = nAgt.substring(verOffset+8);
}
// In MSIE, the true version is after "MSIE" in userAgent
else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
 browserName = "Microsoft Internet Explorer";
 fullVersion = nAgt.substring(verOffset+5);
}
// In Chrome, the true version is after "Chrome" 
else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
 browserName = "Chrome";
 fullVersion = nAgt.substring(verOffset+7);
}
// In Safari, the true version is after "Safari" or after "Version" 
else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
 browserName = "Safari";
 fullVersion = nAgt.substring(verOffset+7);
 if ((verOffset=nAgt.indexOf("Version"))!=-1) 
   fullVersion = nAgt.substring(verOffset+8);
}
// In Firefox, the true version is after "Firefox" 
else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
 browserName = "Firefox";
 fullVersion = nAgt.substring(verOffset+8);
}
// In most other browsers, "name/version" is at the end of userAgent 
else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < 
          (verOffset=nAgt.lastIndexOf('/')) ) 
{
 browserName = nAgt.substring(nameOffset,verOffset);
 fullVersion = nAgt.substring(verOffset+1);
 if (browserName.toLowerCase()==browserName.toUpperCase()) {
  browserName = navigator.appName;
 }
}
// trim the fullVersion string at semicolon/space if present
if ((ix=fullVersion.indexOf(";"))!=-1)
   fullVersion=fullVersion.substring(0,ix);
if ((ix=fullVersion.indexOf(" "))!=-1)
   fullVersion=fullVersion.substring(0,ix);

majorVersion = parseInt(''+fullVersion,10);
if (isNaN(majorVersion)) {
 fullVersion  = ''+parseFloat(navigator.appVersion); 
 majorVersion = parseInt(navigator.appVersion,10);
}

if(browserName=="Firefox" && majorVersion < 45 && show_video==""){
  document.location.href = path;
    // document.getElementById("show_browser_link").innerHTML= error_msg;
    

}
if(browserName=="Chrome"){
    $(document).find("#timeline_audio,#timeline_preview").hide();
    $(document).find("#timeline_audios,#timeline_preview").hide();
              $(document).find(".playbtn-left").removeClass("col-md-2");
              $(document).find(".playbtn-left").addClass("col-md-12");
            //  $(document).find(".playbtn-left").css("padding","0% 45%");
              $(document).find(".playbtn-left").removeClass("playbtn-left");
    }
if(browserName=="Chrome" && majorVersion < 45 && show_video==""){
    document.location.href = path;
}
if(browserName=="Safari" || browserName=="MSIE" || browserName=="Netscape"){
   document.location.href = path;
}
if(browserName=="Opera" && majorVersion < 45 && show_video==""){
  document.location.href = path;
}
if (ua.match(/iphone/i) !== null && show_video=="") {
document.location.href = path;
} 

if (ua.match(/android/i) !== null && show_video=="") { 
document.location.href = path;
}

</script>

