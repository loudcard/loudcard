<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title><?php echo @$Project_Name;?></title>

  <!-- Bootstrap -->
  <?php echo $this->Html->css('bootstrap.min'); ?>
   <?php echo $this->Html->css('font-awesome.min'); ?>
   <?php echo $this->Html->css('style'); ?>
   <?php echo $this->Html->css('responsive'); ?>
    <link href="<?php echo $this->request->webroot;?>fonts/stylesheet.css" rel="stylesheet" type="text/css">
        <?php echo $this->Html->css('animate'); ?>
         <?php echo $this->Html->css('sweetalert'); ?>        
     <?php echo $this->Html->css('cropper.min'); ?>
  <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700|Pacifico" rel="stylesheet"> 
   <?php echo $this->Html->css('custom'); ?>
   <?php echo $this->Html->css('all.min'); ?> 
    <?php echo $this->Html->script("recordRTC");?>
 <?php echo $this->Html->script("gumadapter");?>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
     <script type="text/javascript">
            var path = '<?= $SITEURL; ?>';
          
        </script>
  </head>
  <body>
<header> 
    
    <!-- Navigation -->
    <section class="main-menu">
    <nav class="navbar navbar-fixed-top navigation-top inner-nav" role="navigation">
        <div class="container-fluid"> 
        
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="<?php echo $SITEURL;?>"><?php echo $this->Html->image("logo.png",['alt' => 'loudcards']);?> </a> </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
            <li class="">
                <div class="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>2</span> </div>
              </li>
          </ul>
          </div>
      </div>
      </nav>
    
    
  </section>
  </header>
  
	<div class="inner-page-container">
    <div class="container-fluid">
    <div class="row">
    	<div class="col-sm-3">
        <div class="col-left">
        <h1><?php if(!empty($templates_images['name'])){echo $templates_images['name'];} ?><i class="fa fa-info-circle mycircle" aria-hidden="true"></i></h1>
        </div>
        
        <?php if(!empty($templates_images['templates_images'])){ ?>
        <ul class="col-left-img">
       <?php  foreach($templates_images['templates_images'] as $templates_images){?>
        <li><?php echo $this->Html->image("template_images/".$templates_images['images']);?></li>
        <?php } ?>       
        </ul>
        <?php } ?>
        
        <div class="select-another-card">
        <center> <a href="<?php echo $SITEURL;?>" class="btn btn-lg btn-default lead-btn"><i class="fa fa-arrow-left myarrow" aria-hidden="true"></i>Back  </a></center>
        </div>
        
        </div>
        
        
        
        
        <div class="col-sm-9">
        <div class="widget-body">
        <div class="row">
        <form id="wizard-1" novalidate>
            <div id="bootstrap-wizard-1" class="col-sm-12">
            <div class="form-bootstrapWizard">
                <ul class="bootstrapWizard form-wizard">
                
                <li class="<?php if(isset($page_number) && $page_number==1 || $page_number==2 || $page_number==3){echo "active";} ?>" data-target="#step1">
                                           <span class="step">1</span> <span class="title">Select your card</span>
                                        </li>
                                        <li class="<?php if(isset($page_number) && $page_number==2 || $page_number==3){echo "active";} ?>">
                                           <span class="step">2</span> <span class="title">Upload your Image</span>
                                        </li>
                                        <li class="step-three <?php if(isset($page_number) && $page_number==3){echo "active";} ?>">
                                            <span class="step">3</span> <span class="title">Record your Voice</span>
                                        </li>
                                        <li class="<?php if(isset($page_number) && $page_number==4){echo "active";} ?>">
                                            <span class="step">4</span> <span class="title">Print & Mail</span>
                                        </li>
              </ul>
                <div class="clearfix"></div>
              </div>
          </div>
          </form>
      </div>
      </div>
