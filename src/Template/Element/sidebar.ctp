<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="<?php echo $SITEURL;?>admin/" class="site_title"><?=$this->Html->image("logo.png",["class" => "img-circle logo_img","align" =>"left"]);?></i> <span><?php echo @$Project_Name;?></span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <!-- <div class="profile_pic">
        <?=$this->Html->image("img.jpg",["class" => "img-circle profile_img"]);?>

      </div> -->
      <div class="profile_info">
        <span>Welcome,</span>

        <h2><?=@ucfirst($this->request->session()->read('Auth.Admin.first_name'))." ".@ucfirst($this->request->session()->read('Auth.Admin.last_name'));?></h2>
	 <!-- <h2 style="font-size: smaller; margin-top: 10px; font-weight: bold;">Login Time (GMT+1) <b style="font-size: 15px;"> -->
   <?php
   // $datesArg = explode(' ', $get_login_time['created']);
   // echo date("h:i A",strtotime($datesArg[0]. ' ' .$datesArg[1]));
   ?></b></h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
       
        <ul class="nav side-menu">
         
            <li><a><i class="fa fa-user"></i> Users <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li><a href="<?=$SITEURL;?>admin/users/manage">Manage Users</a></li>
                
              </ul>
            </li>

             <li><a><i class="fa fa-gift"></i> Templates <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
               <li><a href="<?=$SITEURL;?>admin/templates/add">Add Template</a></li>                    
                <li><a href="<?=$SITEURL;?>admin/templates/manage">Manage Templates</a></li>
                
              </ul>
            </li>

            <li><a><i class="fa fa-first-order"></i> Order <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">

               <li><a href="<?=$SITEURL;?>admin/users/orders">Order List</a></li>                    
                
              </ul>
            </li>

             <li><a><i class="fa fa-tags"></i> Promotion Code <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu"> 
              <li><a href="<?=$SITEURL;?>admin/templates/add_code">Add Promotion</a></li>                                 
                <li><a href="<?=$SITEURL;?>admin/templates/promotion">Manage Promotion</a></li>
                
              </ul>
            </li>
	
           
            
                
               
               

                
                
              </ul>
            </div>
          </div>
          <!-- /sidebar menu -->

          
        </div>
      </div>
