<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title><?php echo @$Project_Name;?></title>
 <?php
echo $this->Html->meta('icon','img/favicon.png');
?>
  <!-- Bootstrap -->
 <?php echo $this->Html->css('bootstrap.min'); ?>
   <?php echo $this->Html->css('font-awesome.min'); ?>
   <?php echo $this->Html->css('style'); ?>
   <?php echo $this->Html->css('responsive'); ?>
  <link href="<?php echo $this->request->webroot;?>fonts/stylesheet.css" rel="stylesheet" type="text/css">
        <?php echo $this->Html->css('animate'); ?>
         <?php echo $this->Html->css('sweetalert'); ?>        
     <?php echo $this->Html->css('cropper.min'); ?>
  <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700|Pacifico" rel="stylesheet"> 
   <?php echo $this->Html->css('custom'); ?> 
   <?php echo $this->Html->css('all.min'); ?> 
   <?php echo $this->Html->css('dataTables.bootstrap.min');?>
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
     <script type="text/javascript">
            var path = '<?= $SITEURL; ?>';
      var fb_id = '<?= $fb_id; ?>';
          
        </script>
  </head>
   <div class="facebook_pixel">
 <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1878217502492295');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1878217502492295&ev=PageView&noscript=1"
/></noscript>



  </div>
  <body>
<header> 
   
    <!-- Navigation -->
    <section class="main-menu">
    <nav class="navbar navbar-fixed-top navigation-top inner-nav" role="navigation">
        <div class="container"> 
        <div class="row"> 
        <!-- Brand and toggle get grouped for better mobile display -->
       <div class="col-sm-6 col-xs-6"> <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="<?php echo $SITEURL;?>"><?php echo $this->Html->image("logo.png",['alt' => 'loudcards']);?> </a> </div>
       </div>
        <div class="col-sm-6 col-xs-6">  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
            <!-- <li class=""><div class="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>
            <?php if(!empty($this->request->session()->read('GuestUser.template_id'))){?>
            1 <?php } else{?> 
            0 <?php }?></span>
</div></li> -->
  <?php $display = "";
              if(!empty($this->request->session()->read('Auth.User.id'))){
                $display="block;";
              }else{
                $display= "none;";
              }

                ?>
             
            <!--<li class="after_login_menu" style="display:<?php echo $display;?>"><a href="<?php echo $SITEURL;?>users/dashboard">Dashboard</a></li>-->
            <li class="after_login_menu" style="display:<?php echo $display;?>"><a href="<?php echo $SITEURL;?>users/logout"> <i class="fa fa-sign-out" aria-hidden="true"></i>
 Sign out</a></li>
          </ul>
          </div>  </div>
      </div>  </div>
      </nav>
    
    
  </section>
  </header>
  
  <div class="inner-page-container">
    <div class="container">
    <div class="row">
      <div class="col-sm-3 col-md-3 col-xs-12 template_sidebar" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")>=5)?"none":($this->request->params['action']=="resetPassword" && empty($this->request->session()->read("GuestUser.page_number")))?"none":($this->request->params['action']=="dashboard")?"none":"block"; ?>">
     
          <h1><?php if(!empty($templates_images['name'])){echo $templates_images['name'];} ?> <i class="fa fa-info-circle mycircle" aria-hidden="true"></i></h1>
   
   
         <div class="fix-col-left">
    
 
        
     
          <?php
          
          if($this->request->session()->check('GuestUser.image_name')) {
                      $sidebar_user_image      = 'user_images/'.$this->request->session()->read('GuestUser.image_name');
                    }

           if(!empty($templates_images['templates_images'])){ $image_class="hide_rest"; ?>
        <ul class="col-left-img">
       <?php   foreach($templates_images['templates_images'] as $templates_images){ if($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==3){ if($templates_images['show_preview']==1){ $display="block"; $image_class=""; $class="show_preview";$div_class="mini_imposter"; 
        $main_background = $SITEURL.'/img/template_images/'.$templates_images['images'];
        $sidebar_user_image      = 'upload-img-dummys.jpg';
        if($this->request->session()->check('GuestUser.image_name')) {
                      $sidebar_user_image      = 'user_images/'.$this->request->session()->read('GuestUser.image_name');
                    }
        }else{$display="none";$class="";$div_class="";$sidebar_user_image = "template_images/".$templates_images['images'];}?>

        <li class="<?php echo $image_class;?>" style="display:<?php echo $display;?>">
        <div class="<?php echo $div_class;?>">
        <?php echo $this->Html->image($sidebar_user_image,["class" => $class]);?>
        </div>
        </li>
        <?php }

          if($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==2 || empty($this->request->session()->read("GuestUser.page_number"))){  if($templates_images['show_preview']==1){ 
            $sidebar_user_image      = 'upload-img-dummys.jpg';
            $main_background = $SITEURL.'/img/template_images/'.$templates_images['images'];
                 if($this->request->session()->check('GuestUser.image_name')) {
                      $sidebar_user_image      = 'user_images/'.$this->request->session()->read('GuestUser.image_name');
                    }
            $image_class = "";
            $class="show_preview";
            $div_class ="mini_imposter";
            // pr($this->request->session()->read());
                 }else{
                  $image_class = "hide_rest";
                  $class="";
                  $div_class ="";
                  $sidebar_user_image = "template_images/".$templates_images['images'];
                 }       
            ?>
         <li class="<?php echo $image_class;?>">
          <div class="<?php echo $div_class;?>">
         <?php echo $this->Html->image($sidebar_user_image,["class" => $class]);?>
         </div>
         </li> 
          
          <?php }

           if($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==4 || $this->request->session()->read("GuestUser.page_number")>=5){ 
            if($templates_images['show_preview']==1){
              $sidebar_user_image      = 'upload-img-dummys.jpg';
              $main_background = $SITEURL.'/img/template_images/'.$templates_images['images'];
                 if($this->request->session()->check('GuestUser.image_name')) {
                      $sidebar_user_image      = 'user_images/'.$this->request->session()->read('GuestUser.image_name');
                    }
            $image_class = "";
            $class="show_preview";
            $div_class = "mini_imposter";
                 }else{
                $class=""; 
                $div_class =""; 
                $sidebar_user_image = "template_images/".$templates_images['images'];
                 }       
            ?>
         <li class="<?php echo $image_class;?>">
          <div class="<?php echo $div_class;?>">
         <?php echo $this->Html->image($sidebar_user_image,["class" => $class]);?>
         </div>
         </li> 
          
          <?php }

         }?>       
        </ul>
        <?php } ?>
       <!--  href="<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==3 )?"javascript:void(0)":$SITEURL; ?>" data-type="<?php echo @$this->request->session()->read("card_data.send");?>"  data-page_number="<?php echo !empty($this->request->session()->read("GuestUser.page_number"))?$this->request->session()->read("GuestUser.page_number"):$page_number;?>" class="btn btn-lg btn-default lead-btn pull-right back pull-left prev_btn" -->
        <div class="select-another-card">
        <center> <a href="<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==3 || $this->request->session()->read("GuestUser.page_number")==4)?"javascript:void(0)":$SITEURL; ?>" data-type="<?php echo @$this->request->session()->read("card_data.send");?>"  data-page_number="<?php echo !empty($this->request->session()->read("GuestUser.page_number"))?$this->request->session()->read("GuestUser.page_number"):$page_number;?>" class="btn btn-lg btn-default lead-btn <?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==3 || $this->request->session()->read("GuestUser.page_number")==4)?"prev_btn":""; ?> sidebar_prev_btn"><?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")==3 )?"Back":(!empty($this->request->session()->read("GuestUser.page_number")) && $this->request->session()->read("GuestUser.page_number") ==4?"Back":"Back") ?></a></center>
        </div>
        
        </div>
       </div>
        
        <?php

        $width = "0%";
        $status = "";
        $done_step_two ='';
        $done_step_three = '';
        $done_step_four = '';
        $status_two ='';
        $status_three = '';
        $status_four = '';
        // pr($this->request->session()->read());
        if(empty($this->request->session()->read('GuestUser.page_number')) || $this->request->session()->read('GuestUser.page_number')==2)
        {
          $width = "33.3333%";
       
        $status_two = "active";
          

        }if($this->request->session()->check('GuestUser.page_number') && $this->request->session()->read('GuestUser.page_number')==3 || $this->request->session()->read('GuestUser.page_number')==4) {
          $width = "66.6667%";
          $status_three = "active";
          $done_step_two = "done";

        }if($this->request->session()->check('GuestUser.page_number') && $this->request->session()->read('GuestUser.page_number')>=5) {          
           $width = "100%";
          $status = "active";
          $done_step_two = "done";
          $done_step_three = "done";
          // $done_step_four = "done";
        }

   if($this->request->params['action']=="resetPassword" && empty($this->request->session()->read("GuestUser.page_number"))){
          $width = "100%";
          $status = "active";
          $done_step_two = "done";
          $done_step_three = "done";
        }
        ?>
        
        <div class="<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")>=5)?"col-sm-12":($this->request->params['action']=="resetPassword" && empty($this->request->session()->read("GuestUser.page_number")))?"col-sm-12":($this->request->params['action']=="dashboard")?"col-sm-12":"col-sm-9 col-xs-12";; ?> main_container_div">
  <?php if($this->request->params['action']!="dashboard" && $this->request->session()->read("GuestUser.page_number")!=5){ ?>
        <div class="widget-body">
    
      <div id="container-wizard" class="container-wizard" style="display:<?php echo ($this->request->params['action']=="viewOrders" || $this->request->params['action']=="resetPassword")?"none":"block";?>">
  <div class="step-wizard" style="display:<?php echo !empty($this->request->session()->read("GuestUser.page_number") && $this->request->session()->read("GuestUser.page_number")>=8)?"none":"block"; ?>">
    <div class="progress">
      <div class="progressbar empty"></div>
      <div id="prog" class="progressbar" style="width:<?php echo $width;?>"></div>
    </div>
    <ul>
      <li class="done">
        <a href="javascript:void(0)" id="step1">
          <span class="step">1</span>
          <span class="title">Select your Card</span>
        </a>
      </li>
      <li class="step-two <?php echo $status_two." ".$done_step_two;?>">
        <a href="javascript:void(0)" id="step2">
          <span class="step">2</span>
          <span class="title">Upload your Photo</span>
        </a>
      </li>
      <li class="step-three <?php echo $status_three." ".$done_step_three;?>">
        <a href="javascript:void(0)" id="step3">
          <span class="step">3</span>
          <span class="title">Record your Voice</span>
        </a>
      </li>
      <li class="step-four <?php echo $status." ".$done_step_four;?>">
        <a href="javascript:void(0)" id="step4">
          <span class="step">4</span>
          <span class="title">Address</span>
        </a>
      </li>
      
      <div class="clear"></div>
    </ul>
  </div>
  
  
</div>
     
      </div>
 <?php } ?>
<style type="text/css">
.mini_imposter{
  background: rgba(0, 0, 0, 0) url("<?php echo $main_background; ?>") no-repeat scroll center center / 100% auto;
height:100%;
}

.show_preview {
    float: right;
    height: 74px;
    position: relative;
    right: 3px;
    text-align: center;
    top: 14px;
    width: 51px;
}

@media (max-width:1280px){

 .show_preview {
    float: right;
    height: 53px;
    position: relative;
    right: 2px;
    text-align: center;
    top: 11px;
    width: 36px;
}
}
</style>