<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo @$title." ". @$Project_Name;?></title>
<?php
echo $this->Html->meta('icon','img/favicon.png');
?>
 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- Bootstrap -->
<?php echo $this->Html->css('bootstrap.min'); ?>
   <?php echo $this->Html->css('font-awesome.min'); ?>
   <?php echo $this->Html->css('style'); ?>
   <?php echo $this->Html->css('responsive'); ?>
  <link href="<?php echo $this->request->webroot;?>fonts/stylesheet.css" rel="stylesheet" type="text/css">
        <?php echo $this->Html->css('animate'); ?>
         <?php echo $this->Html->css('sweetalert'); ?>        
     <?php echo $this->Html->css('cropper.min'); ?>
  <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700|Pacifico" rel="stylesheet"> 
   <?php echo $this->Html->css('custom'); ?> 
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110007763-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-110007763-1');
</script>
     <script type="text/javascript">
            var path = '<?= $SITEURL; ?>';
          
        </script>
</head>

 <div class="facebook_pixel">
 <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1878217502492295');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1878217502492295&ev=PageView&noscript=1"
/></noscript>

<?php if($this->request->params("action")=="success"){?>
<script>
  fbq('track', 'Purchase');
</script>
<?php } ?>

  </div>
<body>
<header> 
  
  <!-- Navigation -->
  <section class="main-menu">
    <nav class="navbar navbar-fixed-top navigation-top inner-nav" role="navigation">
      <div class="container-fluid"> 
        
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand" href="<?php echo $SITEURL;?>"><?php echo $this->Html->image("logo.png",['alt' => 'loudcards']);?> </a> </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <!-- <li class=""><div class="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>
            <?php if(!empty($this->request->session()->read('GuestUser.template_id'))){?>
            1 <?php } else{?> 
            0 <?php }?></span>
</div></li> -->
             <?php $display = "";
              if(!empty($this->request->session()->read('Auth.User.id'))){
                $display="block;";
              }else{
                $display= "none;";
              }

                ?>
             
            <!--<li class="after_login_menu" style="display:<?php echo $display;?>"><a href="<?php echo $SITEURL;?>users/dashboard">Dashboard</a></li>-->
            <li class="after_login_menu" style="display:<?php echo $display;?>"><a href="<?php echo $SITEURL;?>users/logout"> <i class="fa fa-sign-out" aria-hidden="true"></i>
 Sign out</a></li>
          </ul>
        </div>
      </div>
    </nav>
  </section>
</header>

<?php $cls=""; if($this->request->params['action']=="dashboard" || $this->request->params['action']=="viewOrders"){
  $cls= "user_dashboard";

  }?>
<div class="inner-page-container">
  <div class="container <?php echo $cls;?>">
