      <footer>
      <!--     <div class="pull-right">
          </div> -->
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <?php
      echo $this->Html->Script('bootstrap.min');
      echo $this->Html->Script('custom.min');
      echo $this->Html->Script('jquery.validate.min'); 
      echo $this->Html->Script('jquery.dataTables.min');
      echo $this->Html->Script('dataTables.buttons.min');
      echo $this->Html->Script('admin_validation.js');

    ?>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	
  </body>
</html>
