<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
  <head>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '312355679116864');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=312355679116864&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title><?php echo @$Project_Name;?></title>
  <?php
echo $this->Html->meta('icon','img/favicon.png');
?>
<link rel="canonical" href="<?=!empty($SITEURL)?$SITEURL:'';?>" />
  <meta property="og:locale" content="en_US" /> 
 <meta property="og:type" content="article" />
 <meta property="og:title" content="<?=!empty($this->request->session()->read("get_template_name"))?$this->request->session()->read("get_template_name"):'';?>" />
<meta property="og:description" content="I've just seen a customized greeting card with my voice message and photo. Available in on IPhone or www.lizicards.com"/>
<meta property="og:url" content="<?=!empty($SITEURL)?$SITEURL:'';?>" />
<meta property="og:image" content="<?=!empty($this->request->session()->read("get_preview_image"))?$SITEURL.'img/template_images/'.$this->request->session()->read("get_preview_image"):'';?>" /> 
<meta property="fb:app_id" content="663418910527497" />  
  <!-- Bootstrap -->
  <?php echo $this->Html->css('bootstrap.min'); ?>
   <?php echo $this->Html->css('font-awesome.min'); ?>
   <?php echo $this->Html->css('style'); ?>
   <?php echo $this->Html->css('responsive'); ?>
  <link href="fonts/stylesheet.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand:400,700" rel="stylesheet">
        <?php echo $this->Html->css('animate'); ?>
         <?php echo $this->Html->css('sweetalert'); ?>        
     <?php echo $this->Html->css('cropper.min'); ?>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110007763-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-110007763-1');
</script>

     <script type="text/javascript">
            var path = '<?= $SITEURL; ?>';
	    var fb_id = '<?= $fb_id; ?>';
          
        </script>

       
  </head>
 <div class="facebook_pixel">
  <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1878217502492295');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1878217502492295&ev=PageView&noscript=1"
/></noscript>
  </div>
  <body>
<script>
  fbq('track', 'Purchase', {
    value: 1,
    currency: 'usd',
  });
</script>
<header> 
    
    <!-- Navigation -->
    <section class="main-menu">
 
    <nav class="navbar navbar-fixed-top navigation-top" role="navigation">
        <div class="container-fluid"> 
    
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="<?php echo $SITEURL;?>"><?php echo $this->Html->image("logo.png",["alt" => ""]);?> </a> </div>
        
     
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
             <!-- <li class=""><div class="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>
            <?php if(!empty($this->request->session()->read('GuestUser.template_id'))){?>
            1 <?php } else{?> 
            0 <?php }?></span>
</div></li> -->
<?php $display = "";
              if(!empty($this->request->session()->read('Auth.User.id'))){
                $display="block;";
              }else{
                $display= "none;";
              }

                ?>
             
            <!--<li class="after_login_menu" style="display:<?php echo $display;?>"><a href="<?php echo $SITEURL;?>users/dashboard">Dashboard</a></li>-->
            <li class="after_login_menu" style="display:<?php echo $display;?>"><a href="<?php echo $SITEURL;?>users/logout"> <i class="fa fa-sign-out" aria-hidden="true"></i>
 Sign out</a></li>

          </ul>
          </div>
    
     
       </div>

      </nav>
    
        <div class="header-bg">
    <div class="slider-container"> 
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators my">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
  
    <div class="item active">
    	<div class="row">
            <div class="col-md-7 col-sm-7 col-xs-7"> 
              <div class="slide-text slide_style_center">
                <h1 data-animation="animated zoomInRight" class="cover-heading">Paper Greeting Cards <br> Play Your Voice Messages When Opened</h1>
                
                
                <a href="javascript:void(0)" class="btn btn-lg btn-default lead-btn scrollto_homedesign">Get Started <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>  
                


                <div class="slide-button">
                <a href="<?php echo $SITEURL;?>whatis" class="btn btn-lg btn-default lead-btn watch_video">Watch video <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> 
                </div>
               

                </div>
            </div>
           <div class="col-md-5 col-sm-5 col-xs-5">  <?php echo $this->Html->image("slider-img.png",["alt" => "","class" =>"slide-image"]);?> </div> 
        </div>
    </div>
    
    <div class="item">
	
	<div class="row">
            <div class="col-md-7 col-sm-7 col-xs-7"> 
              <div class="slide-text slide_style_center">
                  <h1 data-animation="animated zoomInRight" class="cover-heading">Your Photo Personalizes <br> Your Greeting Card </h1>
                <p data-animation="animated fadeInLeft" class="lead"></p>
                <a href="javascript:void(0)" class="btn btn-lg btn-default lead-btn scrollto_homedesign">Get Started <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> 

                <div class="slide-button">
                <a href="<?php echo $SITEURL;?>whatis" class="btn btn-lg btn-default lead-btn watch_video">Watch video <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
                </div>
            </div>
           <div class="col-md-5 col-sm-5 col-xs-5">  <?php echo $this->Html->image("slider-img2.png",["alt" => "","class" =>"slide-image2"]);?>  </div> 
        </div>
	
	
	

    </div>
    
    <div class="item">
	
		<div class="row">
            <div class="col-md-7 col-sm-7 col-xs-7"> 
              <div class="slide-text slide_style_center">
                           <h1 data-animation="animated zoomInRight" class="cover-heading">Your Voice Message Plays <br> When Your Paper Card Is Opened </h1>

                <a href="javascript:void(0)" class="btn btn-lg btn-default lead-btn scrollto_homedesign">Get Started <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> 

                <div class="slide-button">
                <a href="<?php echo $SITEURL;?>whatis" class="btn btn-lg btn-default lead-btn watch_video">Watch video <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
                </div>
            </div>
           <div class="col-md-5 col-sm-5 col-xs-5">  <?php echo $this->Html->image("slider-img3.png",["alt" => "","class" =>"slide-image3"]);?>  </div> 
        </div>
	
	
 
    </div>   
    
    <div class="item">
    <div class="row">	
    
    
            <div class="col-md-7 col-sm-7 col-xs-7"> 
              <div class="slide-text slide_style_center">
                <h1 data-animation="animated zoomInRight" class="cover-heading"> We Print And Mail <br> Your Greeting Card For You</h1>

                <a data-animation="animated fadeInLeft" href="javascript:void(0)" class="btn btn-lg btn-default lead-btn scrollto_homedesign">Get Started <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> 

                <div class="slide-button">
                <a href="<?php echo $SITEURL;?>whatis" class="btn btn-lg btn-default lead-btn watch_video">Watch video <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                </div>
                </div>
            </div>
            
  <div class="col-md-5 col-sm-5 col-xs-5"> <?php echo $this->Html->image("slider-img4.png",["alt" => "","class" =>"slide-image4"]);?> </div>
        </div>  
    </div> 
  </div>

  <!-- Controls -->
  <!--<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>-->
</div>

</div>
</div> 
    

    
  </section>

  </header>
   <?php if(!empty($promotion)){?>
  <section class="promotional">
  <div class="container">
  <div class="row">
    <div class="col-lg-12">
     
        <h1><?php echo $promotion['text'];?></h1>
        <span>USE CODE: <?php echo $promotion['code'];?></span>
       
    </div>
  </div>
  </div>
  </section>
   <?php } ?>
	
 <section class="show_browser_link">
 
  </section>
    <section class="product-listing">
  <div class="container step_bar_design">
    <div class="row">
    
            <h1 class="page-head">Create Your Paper LiziCard in 4 Steps</h1>
       
    </div>
    <div class="widget-body">

                    <div class="row">
                       <div id="container-wizard">
  <div class="step-wizard">
    <div class="progress">
      <div class="progressbar empty"></div>
      <div id="prog" class="progressbar"></div>
    </div>
    <ul>
      <li class="<?php if(isset($page_number) && $page_number==1){echo "active";} ?>">
        <a href="javascript:void(0)" id="step1">
          <span class="step">1</span>
          <span class="title">Select your Card</span>
        </a>
      </li>
      <li class="">
        <a href="javascript:void(0)" id="step2">
          <span class="step">2</span>
          <span class="title">Upload your Photo</span>
        </a>
      </li>
      <li class="">
        <a href="javascript:void(0)" id="step3">
          <span class="step">3</span>
          <span class="title">Record Voice Message </span>
        </a>
      </li>
      <li class="">
        <a href="javascript:void(0)" id="step4">
          <span class="step">4</span>
          <span class="title">Enter Address</span>
        </a>
      </li>
      
      <div class="clear"></div>
    </ul>
  </div>
  
  
</div>
                    </div>

                </div>
