<!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?=$this->Html->image("img.png");?><?=@ucfirst($this->request->session()->read('Auth.Admin.first_name'))." ".@ucfirst($this->request->session()->read('Auth.Admin.last_name'));?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                  <!--  <li><a href="<?=$SITEURL;?>admin/users/profile/<?=$this->request->session()->read('Auth.User.id');?>"> Profile</a></li>
                    <li><a href="<?=$SITEURL;?>admin/users/update_password"> Change Password</a></li> -->
                    
                    <li><a href="<?=$SITEURL;?>admin/users/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
