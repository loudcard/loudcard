<section class="cus-Support">
  <div class="container-fluid voice2">
    <div class="row">
      <div class="col-md-7 col-sm-12 col-xs-12 right-sps">
        <div class="newsltr">
          <div class="home-newsletter">
            <h4> Sample Voice Messages to Help You Come up with Your Very Own Message </h4>
            <div class="voice-record">
              <div class="voice-post">
                <div class="row">
                  <div class="col-md-1">
                    <button id="pButton" class="play"></button>
                  </div>
                  <div class="col-md-11">
                    <p>Happy Fathers Day dad! We sure do miss you. Josh keeps on talking about the fun you two had at zoo. Please give all of our love from Boston to you and mom!

 </p>
                    <audio id="music" preload="true">
                      <source src="<?php echo $SITEURL;?>img/audio/sample_recording_N1.wav">
                    </audio>
                    <div id="audioplayer">
                      <div id="timeline">
                        <div id="playhead"></div>
                      </div>
                    </div>
                    <span class="posted-name">Scott whishing Happy Fathers Day to his Dad.</span> </div>
                </div>
              </div>
              <div class="voice-post">
                <div class="row">
                  <div class="col-md-1"><button id="pButton_two" class="play"></button></div>
                  <div class="col-md-11">
                    <p>Hi Mom and Dad, We wish you a Merry Christmas. We wish you a Merry Christmas. We wish you a Merry Christmas. And a happy New Year. We love you and can't wait to see you next Summer.

</p>
                    <audio id="music_two" preload="true">
                      <source src="<?php echo $SITEURL;?>img/audio/sample_recording_N2.wav">
                    </audio>
                    <div id="audioplayer">
                      <div id="timeline_two">
                        <div id="playhead_two"></div>
                      </div>
                    </div>
                    <span class="posted-name">Alexa's voice message to her parents from her husband, her daughter and herself.</span> </div>
                </div>
              </div>
              <div class="voice-post">
                <div class="row">
                  <div class="col-md-1"><button id="pButton_three" class="play"></button></div>
                  <div class="col-md-11">
                    <p>Ten little fingers and ten little toes. Oh my how our family does grow. We are thrilled to announce the arrival of Samantha Elle Thompson, our daughter, on May 2nd.

  </p>
                    <audio id="music_three" preload="true">
                      <source src="<?php echo $SITEURL;?>img/audio/sample_recording_N3.wav">
                    </audio>
                    <div id="audioplayer">
                      <div id="timeline_three">
                        <div id="playhead_three"></div>
                      </div>
                    </div>
                    <span class="posted-name">Jessica announcing the birth of her baby to multiple recipients.</span> </div>
                </div>
              </div>
            </div>
            <a class="btn btn-lg btn-default lead-btn scrollto_homedesign" href="javaScript:void(0)">Get Started</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>

  
    <section class="news-slide">
    <div class="container">
    <div class="row">
 

<div id="testimonial4" class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x" data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">
        <div class="testimonial4_header">
            <h4><?php echo @$Project_Name;?> in the News</h4>
        </div>
        
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="testimonial4_slide">
                    
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
                
                </div>
            </div>
            <div class="item">
                <div class="testimonial4_slide">
                    
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
                
                </div>
            </div>
            <div class="item">
                <div class="testimonial4_slide">
                    
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</p>
                    
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#testimonial4" role="button" data-slide="prev">
            <span class="fa fa-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#testimonial4" role="button" data-slide="next">
            <span class="fa fa-chevron-right"></span>
        </a> 
        <div class="clear"></div>
        <ol class="carousel-indicators">
            <li data-target="#testimonial4" data-slide-to="0" class="active"> <?php echo $this->Html->image("news1.jpg");?></li>
            <li data-target="#testimonial4" data-slide-to="1"> <?php echo $this->Html->image("news2.jpg");?> </li>
            <li data-target="#testimonial4" data-slide-to="2"> <?php echo $this->Html->image("news3.jpg");?></li>
        </ol>
        
        
        
    </div>
  
  
    
    </div>
   
  </div>
  </section>
  
  
  

  
  
<section class="cus-Support cus-Support2">
    <div class="container">
    <div class="row">
        <div class="col-md-6 col-sm-12 col-xs-12">
          <div class="app-img">  <?php echo $this->Html->image("iphone.png");?> </div>
      </div>
        <div class="col-md-6 col-sm-12 col-xs-12 right-sps">
        <div class="testimonial-block pull-right">
            <h3>Download <?php echo $Project_Name;?> iPhone App</h3>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
            <!-- Wrapper for slides -->
            <div class="row">
                <div class="col-md-10 col-sm-10 col-xs-12">
                <div class="carousel-inner">
                    <div class="item">
                    <div class="row">
                        <div class="col-sm-12">
                        <div class="testimonial-autr">
                            <div class="col-sm-2 col-md-3 col-xs-12"> <?php echo $this->Html->image("test2.png");?> </div>
                            <div class="col-sm-10 col-md-9 col-xs-12">
                            <h4>Byron R.</h4>
                           
                          </div>
                            <div class="clear"></div>
                            <p class="testimonial_para">

These really are top quality cards. My wife was THRILLED by the family photo and loving voice message I added.</p>
                          </div>
                      </div>
                      </div>
                  </div>
                    <div class="item active">
                    <div class="row">
                        <div class="col-sm-12">
                        <div class="testimonial-autr">
                            <div class="col-sm-2 col-md-3 col-xs-12"> <?php echo $this->Html->image("test-img.png");?></div>
                            <div class="col-sm-10 col-md-9 col-xs-12">
                             <h4>Jullia R </h4>
                          </div>
                            <div class="clear"></div>
                            <p class="testimonial_para">
Love how fast this Christmas card building process is. Took a couple minutes to get the design, pic and voice.</p>
                          </div>
                      </div>
                      </div>
                  </div>
                  </div>
              </div>
                <div class="col-md-2 col-sm-2 col-xs-12">
                <div class="controls testimonial_control"> <?php echo $this->Html->image("qto-ico.png",["class" =>"qto-ico"]);?> <a class="left testimonial_btn" href="#carousel-example-generic" data-slide="prev"><?php echo $this->Html->image("l-aro.png");?></a> <a class="right testimonial_btn" href="#carousel-example-generic" data-slide="next"> <?php echo $this->Html->image("r-aro.png");?> </a> </div>
              </div>
              </div>
             
              
              
          </div>
           <a href="https://itunes.apple.com/us/app/lizicard/id1312846884?ls=1&mt=8" target="_blank" class="app-btn"> <?php echo $this->Html->image("app-btn.png");?> </a>
          </div>
      </div>
      </div>
  </div>
  </section>

  
  
  
  
<footer class="footer footer-clum">
   
    <div class="container">
        <div class="row">
  
    <div class="footer-logo">  <?php echo $this->Html->image("logo.png",["alt" => ""]);?></div>
     

           <ul class="footer-menu">            
            <li><a href="<?php echo $SITEURL;?>users/terms"> Terms of Service </a></li>
            <li><a href="<?php echo $SITEURL;?>users/privacy"> Privacy Policy </a></li>
            <li><a href="<?php echo $SITEURL;?>users/contact"> Contact Us </a></li>            
          </ul>
    

              <ul class="social-network social-circle">
            <li><a href="http://www.facebook.com/lizicards" target="_blank" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://www.instagram.com/lizicards/" target="_blank" class="icoinstagram" title="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <li><a href="https://pinterest.com/lizicards" target="_blank" class="icoTwitter" title="Twitter"><i class="fa fa-pinterest-p"></i></a></li>
    

          </ul>
       
      </div>
      </div>

    <div class="container">
    <div class="row copy">
        <div class=""> © 2017 LiziCards. Boston </div>
        <div class="pull-right"> </div>
      </div>
  </div>
  </footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<?php echo $this->Html->script("bootstrap.min");?>
<?php echo $this->Html->script("slider");?>
<?php echo $this->Html->script("sweetalert.min");?>
<?php echo $this->Html->script("custom");?>
<?php echo $this->Html->script("cropper.min");?>
<?php echo $this->Html->script("main");?>
</body>
  </html>
<Script Language="JavaScript">
var nVer = navigator.appVersion;
var nAgt = navigator.userAgent;
var browserName  = navigator.appName;
var fullVersion  = ''+parseFloat(navigator.appVersion); 
var majorVersion = parseInt(navigator.appVersion,10);
var nameOffset,verOffset,ix;
var nav = window.navigator,  
ua = window.navigator.userAgent.toLowerCase();  
// In Opera, the true version is after "Opera" or after "Version"
if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
 browserName = "Opera";
 fullVersion = nAgt.substring(verOffset+6);
 if ((verOffset=nAgt.indexOf("Version"))!=-1) 
   fullVersion = nAgt.substring(verOffset+8);
}
// In MSIE, the true version is after "MSIE" in userAgent
else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
 browserName = "Microsoft Internet Explorer";
 fullVersion = nAgt.substring(verOffset+5);
}
// In Chrome, the true version is after "Chrome" 
else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
 browserName = "Chrome";
 fullVersion = nAgt.substring(verOffset+7);
}
// In Safari, the true version is after "Safari" or after "Version" 
else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
 browserName = "Safari";
 fullVersion = nAgt.substring(verOffset+7);
 if ((verOffset=nAgt.indexOf("Version"))!=-1) 
   fullVersion = nAgt.substring(verOffset+8);
}
// In Firefox, the true version is after "Firefox" 
else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
 browserName = "Firefox";
 fullVersion = nAgt.substring(verOffset+8);
}
// In most other browsers, "name/version" is at the end of userAgent 
else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < 
          (verOffset=nAgt.lastIndexOf('/')) ) 
{
 browserName = nAgt.substring(nameOffset,verOffset);
 fullVersion = nAgt.substring(verOffset+1);
 if (browserName.toLowerCase()==browserName.toUpperCase()) {
  browserName = navigator.appName;
 }
}
// trim the fullVersion string at semicolon/space if present
if ((ix=fullVersion.indexOf(";"))!=-1)
   fullVersion=fullVersion.substring(0,ix);
if ((ix=fullVersion.indexOf(" "))!=-1)
   fullVersion=fullVersion.substring(0,ix);

majorVersion = parseInt(''+fullVersion,10);
if (isNaN(majorVersion)) {
 fullVersion  = ''+parseFloat(navigator.appVersion); 
 majorVersion = parseInt(navigator.appVersion,10);
}

if(browserName=="Firefox" && majorVersion < 45){
  var error_msg = ' <div class="container-link"><div class="row"><div class="col-lg-12"><h1 class="browser_text">This website requires updated Mozilla Firefox browsers.To avoid this, please update your browser.</h1><a class="container-link-btn" href="https://www.mozilla.org/en-US/firefox/new/" target="_blank"><img src='+path+'img/firefox.png></a></div></div></div><div id="cspio-socialprofiles"> <a href="mailto:hello@lizicards.com" target="_blank"><i class="fa fa-envelope fa-2x"></i></a><a href="http://www.facebook.com/lizicards" target="_blank"><i class="fa fa-facebook-official fa-2x"></i></a><a href="http://www.pinterest.com/lizicards" target="_blank"><i class="fa fa-pinterest-square fa-2x"></i></a></div>';
  $(".step_bar_design,.cus-Support,.news-slide,.scrollto_homedesign,.navbar-collapse,.footer").remove();
   $(".show_browser_link").show();
   $(".show_browser_link").html(error_msg);
    // document.getElementById("show_browser_link").innerHTML= error_msg;
    

}
if(browserName=="Chrome" && majorVersion < 45){
    var error_msg = ' <div class="container-link"><div class="row"><div class="col-lg-12"><h1 class="browser_text">This website requires updated Google Chrome browsers.To avoid this, please update your browser.</h1><a class="container-link-btn" href="https://www.google.com/chrome/browser/desktop/index.html?brand=CHBD&gclid=Cj0KCQjwp_DPBRCZARIsAGOZYBQeAYfzhcXIN0Wl9p5-Xq5DNzHtcxPoKPmrT4uqysbZIwL-KoAtQGAaAlIJEALw_wcB&dclid=CIaGiPnDotcCFQgtjgod50YC-g" target="_blank"><img src='+path+'img/chrome.png></a></div></div></div><div id="cspio-socialprofiles"> <a href="mailto:hello@lizicards.com" target="_blank"><i class="fa fa-envelope fa-2x"></i></a><a href="http://www.facebook.com/lizicards" target="_blank"><i class="fa fa-facebook-official fa-2x"></i></a><a href="http://www.pinterest.com/lizicards" target="_blank"><i class="fa fa-pinterest-square fa-2x"></i></a></div>';
   $(".step_bar_design,.cus-Support,.news-slide,.scrollto_homedesign,.navbar-collapse,.footer").remove();
   $(".show_browser_link").show();
   $(".show_browser_link").html(error_msg);
}
if(browserName=="Safari" || browserName=="MSIE" || browserName=="Netscape"){
    var error_msg = ' <div class="container-link"><div class="row"><div class="col-lg-12"><h1 class="browser_text">This website requires Google Chrome or Mozilla Firefox browsers. Or please download our iPhone app.</h1><a class="container-link-btn" href="https://www.google.com/chrome/browser/desktop/index.html?brand=CHBD&gclid=Cj0KCQjwp_DPBRCZARIsAGOZYBQeAYfzhcXIN0Wl9p5-Xq5DNzHtcxPoKPmrT4uqysbZIwL-KoAtQGAaAlIJEALw_wcB&dclid=CIaGiPnDotcCFQgtjgod50YC-g" target="_blank"><img src='+path+'img/chrome.png></a><a class="container-link-btn" href="https://www.mozilla.org/en-US/firefox/new/" target="_blank"><img src='+path+'img/firefox.png></a><a class="container-link-btn" href="https://itunes.apple.com/us/app/lizicard/id1312846884?ls=1&mt=8" target="_blank"><img src='+path+'img/app-btn.png></a></div></div></div><div id="cspio-socialprofiles"> <a href="mailto:hello@lizicards.com" target="_blank"><i class="fa fa-envelope fa-2x"></i></a><a href="http://www.facebook.com/lizicards" target="_blank"><i class="fa fa-facebook-official fa-2x"></i></a><a href="http://www.pinterest.com/lizicards" target="_blank"><i class="fa fa-pinterest-square fa-2x"></i></a></div>';
    $(".step_bar_design,.cus-Support,.news-slide,.scrollto_homedesign,.navbar-collapse,.footer").remove();
   $(".show_browser_link").show();
   $(".show_browser_link").html(error_msg);
}
if(browserName=="Opera" && majorVersion < 45){
   var error_msg = ' <div class="container-link"><div class="row"><div class="col-lg-12"><h1 class="browser_text">This website requires updated Google Chrome browsers.To avoid this, please update your browser.</h1><a class="container-link-btn" href="http://www.opera.com/?utm_campaign=%2300%20-%20WW%20-%20Search%20-%20EN%20-%20Branded&gclid=Cj0KCQjwp_DPBRCZARIsAGOZYBTIIHHtuBWO9WB1OtBw9KRn4YBukd0KvQbUckiXkTM6OYhq-Vu6DsoaAr2lEALw_wcB" target="_blank"><img src='+path+'img/opera.png></a></div></div></div><div id="cspio-socialprofiles"> <a href="mailto:hello@lizicards.com" target="_blank"><i class="fa fa-envelope fa-2x"></i></a><a href="http://www.facebook.com/lizicards" target="_blank"><i class="fa fa-facebook-official fa-2x"></i></a><a href="http://www.pinterest.com/lizicards" target="_blank"><i class="fa fa-pinterest-square fa-2x"></i></a></div>';
   $(".step_bar_design,.cus-Support,.news-slide,.scrollto_homedesign,.navbar-collapse,.footer").remove();
   $(".show_browser_link").show();
   $(".show_browser_link").html(error_msg);
}

if (ua.match(/iphone/i) !== null) {
var error_msg = ' <div class="container-link"><div class="row"><div class="col-lg-12"><a class="container-link-btn" href="https://itunes.apple.com/us/app/lizicard/id1312846884?ls=1&mt=8" target="_blank"><img src='+path+'img/app-btn.png></a><h1 class="browser_text browser_text_data">To start with Lizicards, Please download the LiziCards App from the appstore</h1></div></div></div><div id="cspio-socialprofiles"> <a href="mailto:hello@lizicards.com" target="_blank"><i class="fa fa-envelope fa-2x"></i></a><a href="http://www.facebook.com/lizicards" target="_blank"><i class="fa fa-facebook-official fa-2x"></i></a><a href="http://www.pinterest.com/lizicards" target="_blank"><i class="fa fa-pinterest-square fa-2x"></i></a></div>';
   $(".step_bar_design,.cus-Support,.news-slide,.scrollto_homedesign,.navbar-collapse,.footer").remove();
   $(".show_browser_link").show();
   $(".show_browser_link").html(error_msg);
} 

if (ua.match(/android/i) !== null) { 
var error_msg = ' <div class="container-link"><div class="row"><div class="col-lg-12"><h1 class="browser_text">Lizicards app for Android will come soon. To start now open Lizicards in your PC for best user experience.</h1></div></div></div><div id="cspio-socialprofiles"> <a href="mailto:hello@lizicards.com" target="_blank"><i class="fa fa-envelope fa-2x"></i></a><a href="http://www.facebook.com/lizicards" target="_blank"><i class="fa fa-facebook-official fa-2x"></i></a><a href="http://www.pinterest.com/lizicards" target="_blank"><i class="fa fa-pinterest-square fa-2x"></i></a></div>';
   $(".step_bar_design,.cus-Support,.news-slide,.scrollto_homedesign,.navbar-collapse,.footer").remove();
   $(".show_browser_link").show();
   $(".show_browser_link").html(error_msg);
}


</script>
 <script>
var music = document.getElementById('music'); // id for audio element
var music_two = document.getElementById('music_two');
var music_three = document.getElementById('music_three');
var duration = music.duration; // Duration of audio clip, calculated here for embedding purposes
var duration_two = music_two.duration;
var duration_three = music_three.duration;

var pButton = document.getElementById('pButton'); // play button
var pButton_two = document.getElementById('pButton_two');
var pButton_three = document.getElementById('pButton_three');
var playhead = document.getElementById('playhead'); // playhead
var playhead_two = document.getElementById('playhead_two');
var playhead_three = document.getElementById('playhead_three');
var timeline = document.getElementById('timeline'); // timeline
var timeline_two = document.getElementById('timeline_two');
var timeline_three = document.getElementById('timeline_three');

// timeline width adjusted for playhead
var timelineWidth = timeline.offsetWidth - playhead.offsetWidth;
var timelineWidth_two = timeline_two.offsetWidth - playhead_two.offsetWidth;
var timelineWidth_three = timeline_three.offsetWidth - playhead_three.offsetWidth;

$(document).on("click","#pButton",function(){
// play button event listenter
play(music,pButton);
music_two.pause();       
pButton_two.className = "";
pButton_two.className = "play";  
music_three.pause();       
pButton_three.className = "";
pButton_three.className = "play";   
       
    
   
// pButton_two.addEventListener("click", play);
// pButton_three.addEventListener("click", play);

// timeupdate event listener
music.addEventListener("timeupdate", timeUpdate, false);
// makes timeline clickable
timeline.addEventListener("click", function(event) {
    moveplayhead(event);
    music.currentTime = duration * clickPercent(event);
}, false);

});

$(document).on("click","#pButton_two",function(){
// play button event listenter
play(music_two,pButton_two);
music.pause();       
pButton.className = "";
pButton.className = "play";  
music_three.pause();       
pButton_three.className = "";
pButton_three.className = "play"; 
// pButton_two.addEventListener("click", play);
// pButton_three.addEventListener("click", play);

// timeupdate event listener
music_two.addEventListener("timeupdate", timeUpdate_two, false);
// alert(music_two.duration);
// makes timeline clickable
timeline_two.addEventListener("click", function(event) {
    moveplayhead_two(event);
    music_two.currentTime = duration_two * clickPercent_two(event);
}, false);

});

$(document).on("click","#pButton_three",function(){
// play button event listenter
play(music_three,pButton_three);
music.pause();       
pButton.className = "";
pButton.className = "play"; 
music_two.pause();       
pButton_two.className = "";
pButton_two.className = "play";  

// pButton_two.addEventListener("click", play);
// pButton_three.addEventListener("click", play);

// timeupdate event listener
music_three.addEventListener("timeupdate", timeUpdate_three, false);
// alert(music_two.duration);
// makes timeline clickable
timeline_three.addEventListener("click", function(event) {
    moveplayhead_three(event);
    music_three.currentTime = duration_three * clickPercent_three(event);
}, false);

});
// returns click as decimal (.77) of the total timelineWidth
function clickPercent(event) {
    return (event.clientX - getPosition(timeline)) / timelineWidth;
}

function clickPercent_two(event) {
    return (event.clientX - getPosition(timeline_two)) / timelineWidth_two;
}
function clickPercent_three(event) {
    return (event.clientX - getPosition(timeline_three)) / timelineWidth_three;
}


// mousemove EventListener
// Moves playhead as user drags
function moveplayhead(event) {
    var newMargLeft = event.clientX - getPosition(timeline);

    if (newMargLeft >= 0 && newMargLeft <= timelineWidth) {
        playhead.style.marginLeft = newMargLeft + "px";
    }
    if (newMargLeft < 0) {
        playhead.style.marginLeft = "0px";
    }
    if (newMargLeft > timelineWidth) {
        playhead.style.marginLeft = timelineWidth + "px";
    }
}

function moveplayhead_two(event) {
    var newMargLeft = event.clientX - getPosition(timeline_two);

    if (newMargLeft >= 0 && newMargLeft <= timelineWidth_two) {
        playhead_two.style.marginLeft = newMargLeft + "px";
    }
    if (newMargLeft < 0) {
        playhead_two.style.marginLeft = "0px";
    }
    if (newMargLeft > timelineWidth) {
        playhead_two.style.marginLeft = timelineWidth_two + "px";
    }
}

function moveplayhead_three(event) {
    var newMargLeft = event.clientX - getPosition(timeline_three);

    if (newMargLeft >= 0 && newMargLeft <= timelineWidth_three) {
        playhead_three.style.marginLeft = newMargLeft + "px";
    }
    if (newMargLeft < 0) {
        playhead_three.style.marginLeft = "0px";
    }
    if (newMargLeft > timelineWidth_three) {
        playhead_three.style.marginLeft = timelineWidth_three + "px";
    }
}

// timeUpdate
// Synchronizes playhead position with current point in audio
function timeUpdate() {
    var playPercent = timelineWidth * (music.currentTime / duration);
    playhead.style.marginLeft = playPercent + "px";
    if (music.currentTime == duration) {
        pButton.className = "";
        pButton.className = "play";
     
    }
}

// timeUpdate
// Synchronizes playhead position with current point in audio
function timeUpdate_two() {
    var playPercent = timelineWidth_two * (music_two.currentTime / duration_two);
    playhead_two.style.marginLeft = playPercent + "px";
    if (music_two.currentTime == duration_two) {
        pButton_two.className = "";
        pButton_two.className = "play";
     
    }
}

function timeUpdate_three() {
    var playPercent = timelineWidth_three * (music_three.currentTime / duration_three);
    playhead_three.style.marginLeft = playPercent + "px";
    if (music_three.currentTime == duration_three) {
        pButton_three.className = "";
        pButton_three.className = "play";
     
    }
}

//Play and Pause
function play(music,pButton) {
    // start music
    if (music.paused) {
        music.play();
        // remove play, add pause
        pButton.className = "";
        pButton.className = "pause";
    
    } else { // pause music
        music.pause();
        // remove pause, add play
        pButton.className = "";
        pButton.className = "play";
    
    }
}

// Gets audio file duration
music.addEventListener("canplaythrough", function() {
    duration = music.duration;
}, false);
// Gets audio file duration
music_two.addEventListener("canplaythrough", function() {
    duration_two = music_two.duration;
}, false);
music_three.addEventListener("canplaythrough", function() {
    duration_three = music_three.duration;
}, false);

// getPosition
// Returns elements left position relative to top-left of viewport
function getPosition(el) {
    return el.getBoundingClientRect().left;
}

</script>
<script>
$(window).scroll(function() {

    if ($(this).scrollTop()>0)
     {
        $('.emr-ico').fadeOut();
     }
    else
     {
      $('.emr-ico').fadeIn();
     }
 });
 
 </script>
<script>
 $(window).scroll(function(){
        var wscroll = $(this).scrollTop();
        if(wscroll > 50){
         $(".navbar").addClass("shrink-nav");
        }
        else{
          $(".navbar").removeClass("shrink-nav");
        }
      });
 
  </script>
   <?php if(!empty($this->request->query['page'])){?>
        <script type="text/javascript">
          $('html, body').animate({
                 
                scrollTop: $(".mytabs").offset().top
            }, 1000);

        </script>
        <?php } ?>


<?php if(!empty($this->request->session()->read("whatIsGetStarted")) && $this->request->session()->read("whatIsGetStarted") == 1){  ?>
    <script type="text/javascript">
    $(document).ready(function(){   
   $('html, body').animate({
                 
                scrollTop: $(".product-listing").offset().top-30
            }, 1000);
              
            // }
        });

     

    </script>
    <?php } ?>
