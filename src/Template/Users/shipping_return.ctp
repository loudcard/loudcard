

    <div class="cart-conatiner">
      <div class="links" style="text-align: justify;">
      <ul class="social-links social-circle-links"></ul>
<div class="basic-page__body">
      <h1>Orders, Shipping &amp; Return Policies</h1>
<h2>Delivery Speed</h2>
<h3><span style="color: #69cce6;">Tasty Cookbooks and Coffee</span></h3>
<p><span style="font-weight: 400;">Orders are created in 3 - 5 business days and delivery timing depends on shipping method selected during checkout.</span></p>
<h3><strong><span style="color: #69cce6;">Tasty One Top</span></strong></h3>
<p><span style="font-weight: 400;">Orders will be shipping in November 2017 and will be delivered on a first come, first serve basis.</span></p>
<h2>Domestic Shipping</h2>
<h3><span style="color: #69cce6;">Tasty Cookbooks and Coffee</span></h3>
<p><span style="font-weight: 400;">We ship within the United States and U.S. territories, including Alaska and Hawaii. We can ship to PO Boxes and APO/FPO. Expedited shipping services are not deliverable to PO Boxes or APO/FPO addresses.</span></p>
<p><span style="font-weight: 400;">Note: The timelines shown during checkout include the 3 - 5 business days required to create your order.</span></p>
<h3><span style="color: #69cce6;">Tasty One Top</span></h3>
<p><span style="font-weight: 400;">We are unable to ship to military or PO boxes addresses. Expedited shipping is not currently available.</span></p>
<h2>International Shipping</h2>
<h3><span style="color: #69cce6;">Tasty Cookbooks and Coffee</span></h3>
<p><span style="color: #000000;">For international orders shipping costs and delivery time vary by location that are both calculated during checkout. Each order will comes with a tracking number through delivery to your door.</span></p>
<p><span style="color: #000000;">As the recipient of record, import charges may be imposed upon you and you are responsible for paying these. All duties and taxes are calculated and charged by your local customs office and will vary from country to country. Packages will be returned to us if unclaimed and we will not be able to refund shipping in those cases.</span></p>
<p><span style="color: #000000;">We recommend reaching out to your local customs officials for additional information. If you have not received your order within the time frame above, please reach us by email at <a href="mailto:help@tastyshop.com">help@tastyshop.com</a>.</span></p>
<h3><span style="color: #000000;"><span style="color: #69cce6;">Tasty One Top</span><br></span></h3>
<p><span style="color: #000000;">We ship within the United States only.</span></p>
<meta charset="utf-8">
<h2>Cancellation &amp; Order Change Policy</h2>
<br>
<h3><span style="color: #69cce6;">Tasty Cookbooks and Coffee</span></h3>
<br>
<p>Once your order is submitted, we’re unable to cancel or make adjustments to it.</p>
<p><span>You can cancel an order&nbsp;</span><em>yourself<span>&nbsp;</span></em><span>within 4 hours of it being placed. That option is available on your order confirmation page which can be accessed in the link provided in your order confirmation email.&nbsp;</span></p>
<p>Please be sure to review your order&nbsp;prior to submission. Please note that we do not proof or spell check orders.</p>
<meta charset="utf-8"><br>
<h3><span style="color: #69cce6;">Tasty One Top</span></h3>
<p class="p1"><span class="s1">Orders may be canceled up until they are processing in our warehouse or have shipped. Once your order has shipped, you will be subject to the terms of the Return Policy listed below.&nbsp;</span></p>
<h2>Return Policy</h2>
<meta charset="utf-8"><meta charset="utf-8">
<h3><span style="color: #69cce6;">Tasty Cookbooks and Coffee</span></h3>
<p>Due to the personalized nature of the products, we are unable to cover returns for ordinary wear and tear or damage caused by improper use or accidents.<br><br>If your item has a manufacturing defect, damage or is printed differently than you ordered, you can return within sixty&nbsp;(60) days of purchase. For all return requests you may reach out to us at help@tastyshop.com.<br><br>Please retain all packaging material until you're completely satisfied with the condition and performance of your purchase. Doing so will make it easier to return the item if necessary.</p>
<h3><span style="color: #69cce6;">Tasty One Top</span></h3>
<p>If you aren’t completely satisfied with your purchase within 60 days of receiving the order, you may return the item(s) for a full refund of the merchandise cost. For any defective items or products that are damaged during shipping, return shipping costs will be covered at no expense to you. If you are returning the item(s) because you’ve simply changed your mind or prefer a different item, you will be responsible for the return shipping costs. <br><br>After your merchandise has been received in our warehouse, we will issue you a refund to your original form of payment within 10 business days. The refund may not show up on your credit card billing statement for up to 2 billing cycles.</p>
<h2>Lost or Stolen Packages</h2>
<p><strong>&#65279;</strong>We are not responsible for lost or stolen packages. Please contact the shipping carrier for more help on this matter.</p>
<p>We suggest that you&nbsp;check the immediate area; including any mailbox, garage, porch, front desk, leasing office, neighbor, or anyone that may have signed for or picked up the package.</p>
<p>Track your package online to see if there are any special instructions or updates listed. APO/FPO customers ONLY - check your Military Base mailroom</p>
    </div>
      </div>

  
      
    </div>
  </div>
</div>
