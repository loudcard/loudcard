    <div class="photo-frame">      

     

     <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-sm-offset-3">
            <div class="sign-up-col">
              <i class="fa fa-pencil sign-up-icon" aria-hidden="true"></i><h1>Reset your Password</h1>
              <?php echo $this->Flash->render();?>

              <?php echo $this->Form->create("/", ['templates' => ['inputContainer' => '{{content}}'], "id" => "reset_password", 'class' => 'm-t', 'role' => 'form', 'label' => false]);
?>
               
                <div class="form-group">
                  <?php echo $this->Form->input("password", array("class" => "form-control","type" => "password","placeholder" => "New password", 'id' => 'password', "label" => false)); ?>
                </div>

                <div class="form-group">
                  <?php echo $this->Form->input("password_confirm", array("class" => "form-control", "placeholder" => "Confirm password", "label" => false,"type" => "password")); ?>
                </div>
                <div class="form-group">
                <?php echo $this->Form->submit("Reset Password", array("class" => "btn btn-default submit-btn")); ?>
                </div>
              

               <?php
                echo $this->Form->end();
                ?>
            </div>
          </div>
         
         
        </div>





          </div>

        </div>

      </div>
    </div>
  </div>
</div>
