<style>
.basic-page__body ul li {

    margin-left: 19px;
    margin-top: 2px;
    padding-left: 10px;
}
</style>
    <div class="cart-conatiner">
      <div class="links" style="text-align: justify;">
      <ul class="social-links social-circle-links"></ul>
<div class="basic-page__body">
      <h1 class="center">Terms of Service</h1>


<ul class="basic-page__body" style="margin:0; padding:0;">
<p><b>TABLE OF CONTENT</b></p>
<li>Introduction  </li>
<li>License to use the Services and Products  </li>
<li>Access to the Platform  </li>
<li>Your Account  </li>
<li>Purchase  </li>
<li>Uploaded Content and Images  </li>
<li>Intellectual Property Rights  </li>
<li>Order and Delivery  </li>
<li>Price and Payment  </li>
<li>Payment on Account (Credit)  </li>
<li>Returns  </li>
<li>Indemnification  </li>
<li>Limitation of Liability  </li>
<li>Termination  </li>
<li>General  </li> 
</ul>


<p> <b> 1 INTRODUCTION </b> </p>
<p> 1.1 These terms of service (the “Terms”) govern your access to and use of www.lizicards.com and the LiziCards iPhone app (together the “Platform”) and any products and services of whatever nature (whether existing now or in the future) available through the Platform (the “Services and Products”). </p>
<p> 1.2 Please read these Terms carefully before using the Platform. If you do not agree with the Terms, you must not use the Platform. By accessing the Platform, you agree to comply with and to be bound by the Terms of Service (https://lizicards.com/users/terms), Privacy Policy (https://lizicards.com/users/privacy) which sets out how we will use personal information you provide to us. If there is any conflict between the Privacy Policy and these Terms then the Terms will prevail. </p>
<p> 1.3 The Services, and the Platform, are operated by Loudcards LLC with DBA Lizicards of 745 Atlantic Ave, Boston, MA 02111, U.S.A. (“Lizicards”, "Lizicard", "Loudcards", "Loudcard", “we” or “us”). We are incorporated in Massachusetts with Federal Tax ID 82-2761653. </p>
<p> 1.4 Only persons aged 13 years or over may access the Platform and use the Services and Products and all users of the Services and Products warrant that they are 13 years of age or over.
<p> 1.5 We amend these Terms from time to time and by accessing the Platform, you confirm that you agree to comply with and to be bound by the amended Terms. Every time you wish to use our Platform, please check these Terms to ensure you understand the Terms that apply at that time. We do not separately file the Terms entered into by users of our Platform when they access our Platform. Please make a durable copy of these Terms by printing and/or saving a downloaded copy on your own computer. These Terms are offered in English only. If you do not agree with any revised version of the Terms, please do not continue to use the Platform. These terms were most recently updated on Nov 15, 2017. </p>
<p> 1.6 The Services and Products are designed for your convenience. If you have any comments, problems or questions regarding any part of the Services and/or products and services featured in the Services and Products, please send an email to hello@lizicards.com. If you have experienced technical problems while using the Platform please also contact us. </p>
 

<p> <b> 2 LICENSE TO USE THE SERVICES AND PRODUCTS </b> </p>
<p> 2.1 Subject to your compliance with the Terms, and subject to paragraph 5 of these Terms, we hereby grant you a limited, non-exclusive, non-sublicensable, non-assignable, immediately (and without notice) revocable license to download, install and use the Services on the device on which you install or use the Services and Products for the sole purpose of your personal use of the Services and Products from that device. </p>
<p> 2.2 You agree not to copy, loan, sell, resell, assign, rent, lease, publish, redistribute, license, sublicense or otherwise transfer the Services and products.  You further agree not to undertake cause, permit or authorize the modification, creation of derivative works, translation, reverse-engineering, reverse-compiling, decompiling or disassembling of the Services and products (or any part of it or its underlying software) or make any attempt to access the source code of the Services and Products (or any part of it or its underlying software). </p>
<p> <b> 3 ACCESS TO THE PLATFORM </b> </p>
<p> 3.1 We do not guarantee that our Platform, or any content on it, will always be available or be uninterrupted. We reserve the right at our sole discretion to withdraw or vary the Platform and the Services and products and/or to suspend or terminate your access to the Platform and the Services and products at any time without notice and we shall not be liable to you if the Platform or the Services and products are unavailable, either in whole or part, at any time for any reason whatsoever. </p>
<p> 3.2 Information on the Platform may contain technical inaccuracies or typographical errors. We attempt to make its descriptions as accurate as possible, but we do not warrant that the content on the Platform is accurate, complete, reliable, current, or error-free. Under no circumstances will we be liable in any way for any content, including, but not limited to, any errors or omissions in any content, or any loss or damage of any kind incurred as a result of the use of, access to, or denial of access to any content on the Platform. </p>
<p> 3.3 You may access any part of the Platform provided that it is not password protected. Access to some parts of the Platform are only available if you have a valid password. You may not access these areas without a valid password. </p>
<p> <b> 4 YOUR ACCOUNT </b> </p>
<p> 4.1 Subject to these Terms you may open an account with us by completing the Registration Form. Once your registration has been accepted (and until your account is terminated) you will be able to upload images to the Platform, access parts of your account using the Services and Products and (subject to agreeing any additional terms which are applicable) request Services and Products to be supplied by us (such as the printing of uploaded images). You agree to: </p>
<p> 4.1.1 provide true, accurate, current and complete information about yourself (the “Registration Data”) as prompted by the relevant registration form; and </p>
<p> 4.1.2 maintain and promptly update the Registration Data through your account to keep it true, accurate, current and complete. </p>
<p> 4.2 During the registration process you will be asked to supply your email address. This will help us to verify your identity on future visits. Should you register using an invalid email address or an email address that belongs to someone else, we may terminate your account at any time without notice. </p>
<p> 4.3 You will receive confirmation emails, updates and marketing emails after you create a Lizicards account, at the email address you register with. You can opt out of updates and marketing emails at any time (unsubscribe) </p>
<p> 4.4 You will also need to provide a password in order to access your account which you must keep secure at all times. Note that we are entitled to treat anything done through your account as having been done by you; it is up to you to maintain the security of your account and you will be responsible for any damage or losses caused by unauthorized access resulting from your failure to keep your password secure. We strongly recommend that you choose a unique password and we accept no liability whatsoever where a third party accesses your account using your password. You agree to: </p>
<p> 4.4.1 immediately notify us if you become aware of any unauthorized use of your password or account or any other breach of security by contacting us at hello@lizicards.com; and  </p>
<p> 4.4.2 ensure that you exit from your account at the end of each session. </p>
<p> 4.5 You are also responsible for ensuring that all persons who access our Platform through your internet, or mobile, connection are aware of these Terms and other applicable Terms, and that they comply with them. </p>
<p> 4.6 You are advised to keep backups of all material provided to us. In particular, it is up to you to keep backup copies of images uploaded by you or emailed to us. We will not be responsible for keeping backups or for the loss of, deletion or corruption of any images or voice recordings or any other material used in connection with the Services and products. </p>
<p> 4.7 We have the right to disable your password at any time if in our reasonable opinion you have failed to comply with any of the provisions of these Terms. </p>
<p> 4.8 There is no charge for opening a Lizicards account although you may wish to purchase certain Services and products of our Platform which are only available for a fee (see below). When you place an order for Services and products, you will be notified of the applicable charges and you will need to supply us with your credit card or debit card and/or other personal details so that we may process the order for you. Any charges for the Services and products requested will be billed to your payment method once the order has been accepted by us. </p>



<p> <b> 5 PURCHASE  </b> </p>
<p>5.1 If you wish to purchase certain Services and products of our Platform which are only available for a fee (as a “Purchaser”), you must read these Terms and indicate your acceptance of them during the purchase process. </p>
<p>5.2 The Services and products available to Purchasers for a fee (the “Purchased service and product”) are subject to change but may include things such as additional artwork, free shipping, and free customized stamps. We reserve the right to change the Purchased Services and Products at any time at our sole discretion. </p>
<p>5.3 You agree that we reserve the right to charge a fee for the Purchased Services and Products or any part of them (the “Fees”) and to change any applicable Fees from time to time at our sole discretion. Information on the current Fees for the Purchased Services and Products will be posted on our site. </p>
<p>5.4 You agree to pay the Fees and any other charges that we have notified you of and you have agreed in connection with your use of the Purchased Services and Products or any part of them (including all and any applicable taxes) at the prices in effect when the Fees and/or other charges were incurred. The Fees will be charged at the beginning of your purchase. </p>
<p> <b> 6 UPLOADED CONTENT AND VOICE RECORDINGS AND IMAGES  </b> </p>
<p>6.1 You may upload or use digital images when using the Platform or connection with the Platform and the Services which must be in JPEG format. You may record and use digital voice message when using the Platform or connection with the Platform and the Services which may be MP3 format. </p>
<p>6.2 Although we prohibit the uploading of certain types of image and voice recordings to the Platform, we cannot control, nor do we monitor the use of the Services. It is possible that images or voice recordings or other material may appear on the Platform or in connection with the Services which are unlawful or offensive and contravene our restrictions on content. We are not responsible for such images or voice recordings or material but if you become aware of any such images or voice recordings or material please contact us without delay at: hello@lizicards.com </p>
<p>6.3 We may at our discretion contact law enforcement authorities if we are made aware that anything unlawful is occurring or has occurred in relation to the use of the Platform or the Services including the uploading or emailing of any images or voice recordings in breach of our restrictions on content. We may provide copies of any relevant images or voice recordings or material to the law enforcement authorities and in that connection may also give them access to any personal data that is held by us. </p>
<p>6.4 We may without notice and at our sole discretion delete or remove any image or voice recordings that has been uploaded, recorded, emailed or submitted for printing in breach of these Terms. </p>
<p>6.5 By uploading, posting, contributing, voice recordings or including any content or material in a personalized Service, you grant us a non-exclusive, royalty-free, irrevocable license (including the right to grant sub-licenses through multiple tiers) to use, reproduce, adapt, distribute and communicate to the public that content or material solely for the purpose of performing obligations and exercising rights under these Terms. Note that we may modify content or material in order to conform it to Lizicards or the requirements of the Service you have ordered (such as by cropping images or voice recordings). </p>
<p>6.6 You are not allowed to upload or order printed items which contain or use any images or voice recordings or other material including text based annotations and comments, which contain any of the following: </p>
<p>6.6.1 material which is defamatory of any person; </p>
<p>6.6.2 material which is pornographic, obscene, indecent or offensive; </p>
<p>6.6.3 material which promotes discrimination based on race, sex, religion, nationality, disability, sexual orientation or age; </p>
<p>6.6.4 material that is likely to incite hatred or violence against any person or group; </p>
<p>6.6.5 material that is likely to deceive any person; </p>
<p>6.6.6 material which concerns or relates to any criminal act; </p>
<p>6.6.7 material the use or inclusion of which infringes any copyright, trademark, database or other intellectual property right of any third party; </p>
<p>6.6.8 material made in breach of any legal duty owed to a third party, such as a contractual duty or a duty of confidence; material which promotes any illegal activity; </p>
<p>6.6.9 material which is threatening, abusive or invades another’s privacy, causes annoyance, inconvenience or needless anxiety; </p>
<p>6.6.10 material which is likely to harass, upset, embarrass, alarm or annoy any other person; </p>
<p>6.6.11 material used to impersonate any person, or to misrepresent your identity or affiliation with any person; </p>
<p>6.6.12 material which gives the impression that it emanates from us, if this is not the case; </p>
<p>6.6.13 material that advocates, promotes or assists any unlawful act such as (by way of example only) copyright infringement or computer misuse; </p>
<p>6.6.14 material that breaches any applicable laws or legislation. </p>
<p>6.7 You are not allowed to: </p>
<p>6.7.1 create a database (electronic or otherwise) that includes material downloaded or otherwise obtained from the Platform except where expressly permitted in connection with the Services; </p>
<p>6.7.2 interfere with or disrupt the Platform, the Services or the servers or networks connected to the Platform or Services; </p>
<p>6.7.3 disseminate unsolicited advertisements or for any other commercial purposes (which would include using the Services to promote or encourage the sale of your goods/services); </p>
<p>6.7.4 transmit or re-circulate any material obtained from the Services to any third party except where expressly permitted; </p>
<p>6.7.5 disseminate any unsolicited or unauthorized advertising, promotional materials, “junk mail,” “spam,” “chain letters,” “pyramid schemes,” or any other form of solicitation; </p>
<p>6.7.6 disseminate any material that contains software viruses or any other computer code, files or programs designed to interrupt, damage, destroy or limit the functionality of any computer software or hardware or telecommunications equipment; </p>
<p>6.7.7 use the Platform or the Services in any way that might infringe third party privacy or other rights, is unlawful or that might bring us into disrepute; </p>
<p>6.7.8 post link(s) that take users to material that contravenes any of the above restrictions; or </p>
<p>6.7.9 send (at our sole discretion) excessive numbers of cards to yourself or to other people as part of any promotion, including, but not limited to, sending more than 10 postcards or greeting cards to the same address. </p>
<p> <b> 7 INTELLECTUAL PROPERTY RIGHTS  </b> </p>
<p>7.1 All materials on this Platform and the functionality and operation of the Platform itself are protected by intellectual property rights. These materials and the rights in these materials are owned by us, or used with permission of their owners. Such materials include, but are not limited to, the photographs, images, illustrations, text, video clips, audio clips, designs, logos, trademarks, trade dress and other materials contained in this Platform, the software used in the design and development of this Platform and the functionality and operation of the Platform itself. All rights are reserved, worldwide. We are the owner (or the licensee) of all intellectual property rights in the Services. </p>
<p>7.2 You retain all intellectual property rights, including copyright, in those images or voice recordings that you have uploaded to the Platform, whilst using the Services, where you already own such rights. </p>
<p>7.3 We may display, modify, print, transmit or distribute any of the images or voice recordings that you upload or email to us, in order to provide any of the Services offered by us through the Platform subject to these Terms. </p>
<p>7.4 You warrant that you have the right to copy, upload or otherwise deal with those images or voice recordings in relation to the Services and to allow us to process and otherwise deal with those images in accordance with these Terms. </p>
<p>7.5 You may not upload, request us to print, or otherwise deal with, in relation to the Services any images or voice recordings or other material where you do not have the right to do so or allow us to use such images or voice recordings or other material if such use would infringe any existing third party intellectual property or contractual rights. </p>
<p>7.6 You are not allowed to remove any copyright, trade mark or other intellectual property notices contained on the Platform or in the Services or from any copies or printed items taken of material from the Services. </p>
<p>7.7 We have the right to disclose your identity to any third party who claims that any content posted or uploaded by you to our Platform constitutes a violation of their intellectual property rights, or of their right to privacy.  </p>
<p> <b> 8 ORDER AND DELIVERY  </b> </p>
<p>8.1 An order for any Service (including any order for printing services) will not be treated as having been accepted until you receive confirmation from us that this is the case. This confirmation may be provided by email or post.  </p>
<p>8.2 The decision as to whether to accept any order from you is at our sole discretion. From time to time we may reject an order, for example, if we do not have your chosen Service in stock or if your order breaches any of the requirements of these Terms. If the order is rejected, we will contact you to confirm this and arrange for reimbursement of your payment. In the event that the Services are produced and prior to dispatch are then discovered to be in breach of these Terms, we will not dispatch the Services. We may, at our sole discretion, refund all or a proportion of the payment made in respect of the order concerned but reserve the right to charge in full as if the Services had been dispatched.  </p>
<p>8.3 We have a policy of continuous product development and reserve the right to amend the specifications of any of the Services without prior notice. </p>
<p>8.4 We endeavor to display and describe as accurately as possible the printed colors of the Services but cannot undertake to give any assurance that the colors of the Services supplied will exactly match those displayed on a customer’s monitor, mobile phone or any other electronic device.  </p>
<p>8.5 We will generally notify you of the dispatch dates available and the expected timeframe for delivery of your ordered Services during the order process; however, we do not guarantee delivery dates or times. We will make you aware of any delivery charges before you place your order. The expected delivery times and charges will differ depending on the Service you order.  </p>
<p>8.6 Neither we, nor any delivery service that we use, shall be liable for any failure to perform our Services where such failure or delay results from any circumstances outside our reasonable control; these circumstances include but are not limited to adverse weather conditions (such as snow, flood and extreme winds), fire, explosion, accident, traffic congestion, obstruction of any private or public highway, riot, terrorism, act of God, or industrial dispute or strike.  </p>
<p>8.7 Please be aware that if your order consists of or includes a postcard, the postcard will be delivered without any packaging or envelope so the content of the postcard will be visible to those handling it.  </p>
<p> <b> 9 PRICE AND PAYMENT  </b> </p>
<p>9.1 Any price stated (if at all) on the Platform for Services will also carry if applicable additional Sales Tax at the then applicable rate, if appropriate. You shall be responsible for any other taxes applicable in the territory to which the Services are sent.  </p>
<p>9.2 Payment must be made by credit card, debit card or PayPal at the time of placing an order which is accepted by us. Payment in full will be taken at this time and a contract in relation to the purchase of that Service and product will be in force. </p>
<p>9.3 You warrant that all details provided to us for the purpose of your order and its delivery will be correct and that the chosen method of payment is your property and that sufficient funds or credit facilities are available to cover the full cost of the Services ordered. We reserve the right to obtain validation of your credit card, debit card or PayPal details before accepting your order. </p>
<p>9.4 We will take all reasonable care to keep the details of your payment secure. However, save where we have acted negligently we cannot be held liable and disclaim all liability howsoever arising for any loss you may suffer if a third party gains access to any data you provide when accessing or making a payment through the Platform.  </p>
<p>9.5 In the event that you supply invalid credit or debit card details or details of a credit or debit card that belongs to someone else, we reserve the right to terminate your account at any time without notice and provide your details to the relevant authorities.  </p>
<p> <b> 10 PAYMENT ON ACCOUNT (CREDIT)  </b> </p>
<p>10.1 We may from time to time offer a scheme whereby customers may make a payment on account as advance payment for Services in return for additional bonus credit which may be used to pay for applicable Services.  </p>
<p>10.2 The system of payment on account and bonus credit may be referred to as “Credits” for promotional purposes. We may change this name from time to time but these Terms shall apply to any scheme involving advance payment for applicable Services.  </p>
<p>10.3 Credits that have been purchased and bonus credit amounts awarded are separately recorded and the total balance of credit is shown in your account under “Credit”.  </p>
<p>10.4 Applicable Services means any printed Services manufactured by us. Non applicable Services will be offset entirely against the cash balance on the account. At the point of checkout our system will check your Credit balance. Any applicable Services will be paid for by drawing down the paid and bonus balances pro rata.  </p>
<p>10.5 Any payment made on account shall be deemed as a payment for Services and products to be ordered from us. Cash balances will not be returned except in cases where you have received defective services and have requested a refund of the unused prepaid cash balance held on the account.  </p>
<p>10.7 If you change your mind after making a payment on account you may request a refund by calling customer services within 14 days of the transaction. In such cases the amount refunded will be the sum of the original payment less the value of any Services bought on account. Refunds can only be made to the credit card, debit card or PayPal account used to make the original purchase.  </p>
<p> <b> 11 RETURNS  </b> </p>
<p>11.1 The majority of our items are personalized Services for which there is no right to cancel the contract and return the Service, however, you may reject a Service which is faulty or not as described. Where this is the case, we will ask you to return the Service to us within 3 days of receipt. If you are eligible for a refund, we will refund the price you paid onto the card that you paid with. Alternatively, we may offer to reprint (if applicable) and resend the item. Please note that we do not offer both a refund and a resend. We will not provide a refund if the fault is a result of your own actions such as Service misuse or if personalization is mis-spelt or if you have uploaded an image or voice recordings of a low resolution or size or quality.  </p>
<p> <b> 13 INDEMNIFICATION  </b> </p>
<p>13.1 You agree to indemnify and hold us, and any parents, subsidiaries, affiliates, associated companies or any of their respective directors, officers, limited liability company members, employees, agents or service providers, harmless against any loss, damage, claim, cost or liability that arises out of any claim asserted by a third party that involves, arises out of, relates to or concerns:  </p>
<p>13.1.1 any use by you of the Platform or the Services in breach of these Terms;  </p>
<p>13.1.2 any claim that the uploading or emailing of any materials by you or on your behalf is an infringement of any third party’s copyright, trade mark or other intellectual property rights; or  </p>
<p>13.1.3 any claim that the processing, printing or other dealing by us of any materials uploaded or emailed by you or on your behalf is an infringement of any third party’s copyright, trade mark or other intellectual property rights.  </p>
<p> <b> 14 LIMITATION OF LIABILITY  </b> </p>
<p>14.1 We use reasonable care and skill to provide Lizicards in accordance with our specifications for Lizicards but:  </p>
<p>14.1.1 the Platform and the Services are provided on an “as is” and “as available” basis; and  </p>
<p>14.1.2 we cannot and do not make any warranties, claims or representations with respect to the Services including, without limitation, quality, performance, non-infringement, merchantability or fitness for use for a particular purpose. We do not represent or warrant that availability or use of the Services will be uninterrupted, timely, secure, error-free or virus-free. Lizicards cannot accept responsibility or liability for a failure of your mobile network, or internet provider, or any losses or damage suffered as a result, as this is outside the control of Lizicards.  </p>
<p>14.2 We will use reasonable endeavors to ensure that the Platform and the Services do not contain or promulgate any viruses or other malicious code. However, it is recommended that you should virus check all materials used in connection with the Platform and the Services and regularly check for the presence of viruses and other malicious code. We exclude to the fullest extent permitted by applicable laws all liability in connection with any damage or loss caused by computer viruses or other malicious code originating or contracted from the Platform or the Services.  </p>
<p>14.3 You should note that the transmission of information via the internet is not completely secure. Although we take appropriate technical and organizational measures to guard against unauthorized or unlawful processing of your personal data and against loss or destruction of, or damage to, your personal data, we cannot guarantee the security of your personal data. Any transmission of personal data is at your own risk. We are not responsible for circumvention of any privacy settings or security measures contained on our Platform and we exclude to the fullest extent permitted by applicable laws all liability in connection with the circumvention of any privacy settings or security measures contained on our Platform.  </p>
<p>14.4 Lizicards may include links to other websites in the Services but we are not responsible for the content of any external website, the content of any advertiser’s website or the conduct of any business or individual advertising in the Services or related to any Equivalent Service.  </p>
<p>14.5 We will not be liable for faulty Service unless a claim is notified to us in writing (including the order confirmation number and details of the claim) within 28 days of receipt of the Service, or in the case of non-delivery, within a reasonable time after the Service was expected to arrive. In the case of a valid claim, we may choose to replace the Service (or the part in question) or refund to you the price paid for the Service (or an appropriate proportion of the price). We will have no further liability to you in respect of the matters referred to in this clause.  </p>
<p>14.6 We, our agents, directors, officers, shareholders, employees and subcontractors will not be liable to you or anyone else, whether in contract, tort (including negligence, breach of statutory duty or other tort) or otherwise:  </p>
<p>14.6.1 for any loss of revenue, data, sales or business, agreements or contracts, anticipated savings, profits, opportunity, goodwill or reputation, or for any business interruption;  </p>
<p>14.6.2 for any loss or corruption of data; or  </p>
<p>14.6.3 for any indirect, special or consequential loss, damage, costs or other claims,  </p>
<p>howsoever caused or arising, including where arising directly or indirectly from any failure or delay in performing any obligation under these Terms caused by matters beyond our reasonable control. Except as expressly stated elsewhere in these Terms, all representations, warranties, conditions and other terms, whether express or implied (by common law, statute, collaterally or otherwise) are hereby excluded, except in the case of fraud, or where such exclusion is not permitted by law.  </p>
<p>14.7 The total liability of Lizicards shall not exceed the aggregate net purchase price (excluding taxes and freight) for the relevant Services. xxx  </p>
<p>14.8 Nothing in these Terms shall restrict or exclude any liability that we have to any party which cannot be excluded by law and, in particular, our liability for death or personal injury caused by our negligence shall not be limited or excluded in any way.  </p>
<p> <b> 15 TERMINATION  </b> </p>
<p>15.1 Where we terminate your access to the Platform or the Services for any reason: (a) all licenses and rights to use the Services shall immediately terminate; (b) you will immediately cease any and all use of the Services and (c) you will destroy and/or delete all copies of the Services in your possession or within your control.  </p>
<p> <b> 16 GENERAL  </b> </p>
<p>16.1 In the event that any term of these Terms is held to be invalid or unenforceable, the remainder of these Terms shall remain valid and enforceable.  </p>
<p>16.3 We may assign or transfer any of our rights or sub contract any of our obligations under these Terms to any third party. You may not assign or transfer any of your rights or sub contract any of your obligations under these Terms except with our specific permission in writing.  </p>
<p>16.4 Force Majeure. Neither party will be responsible for any failure to perform due to causes beyond its reasonable control, including, but not limited to, acts of God, terrorism, war, riot, embargoes, fire, floods, earthquakes, or strikes (each a “Force Majeure Event”) provided that such party gives prompt written notice to the other party of the Force Majeure Event.  The time for performance will be extended for a period equal to the duration of the Force Majeure Event.   </p>

<p> 16.5 Governing Law and Disputes. The Terms and any dispute arising hereunder shall be governed by and interpreted and construed in accordance with the laws of the Commonwealth of Massachusetts, without regard to conflict of law principles, and shall be subject to the exclusive jurisdiction of the federal and state courts located in the Commonwealth of Massachusetts.  </p>
<br>
<p>  Effective Date of Terms: November 15, 2017.  </p>




    </div>
      </div>

  
      
    </div>
  </div>
</div>
