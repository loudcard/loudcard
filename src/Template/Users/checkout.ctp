   <?php
  // pr($this->request->session()->read());
   $quantity = 0;
   $shipping_cost = 0; 
   $sales_tax = 0;
   $total = 0;
   $grand_total = 0;
   $class = "col-lg-6";
   if(!empty($this->request->session()->read("card_data.send")) && $this->request->session()->read("card_data.send") == "to_me"){
    $quantity = $this->request->session()->read("card_data.quantity");
    $total = $template_data['price']*$quantity;
     if($this->request->session()->read("card_data.country_id")!=231){
      $shipping_cost+=5;
    }
    else{
      $shipping_cost = $this->request->session()->read("card_data.shipping");
        if($this->request->session()->read("card_data.state_id")==3943){ 
              
            $add_shipping = $total+$shipping_cost; 
                $sales_tax = $add_shipping*$sal_tax/100;                
               }
    }      
    
  

  }
  if(!empty($this->request->session()->read("card_data.send")) && $this->request->session()->read("card_data.send") == "to_receipent"){
    $quantity =  count($this->request->session()->read("card_data.data"));
    $total = $template_data['price']*$quantity;
    if($quantity==1){
      $class = "col-lg-12";
    }
    $get_quantity_for_saleTax=0;
    $calculate_ship_cost =0;
    foreach ($this->request->session()->read("card_data.data") as $key => $value) {
      if($value['country_id']!=231){
        $shipping_cost+=5;
      }else{
                // echo "hello";
        $shipping_cost += $value['shipping'];
        if($value['state_id']==3943){
               $get_quantity_for_saleTax+=1;
               $calculate_ship_cost+= $value['shipping'];
              
               $get_total = $template_data['price']*$get_quantity_for_saleTax+$calculate_ship_cost;
                 $sales_tax = $get_total*$sal_tax/100;   
               
               }


      }
    }
    
  }  


   $image_background = $SITEURL.'/img/template_images/'.$preview_image;
  //
  ?>

  <style type="text/css">
     .image_imposter_background_checkout{
       background: rgba(0, 0, 0, 0) url("<?php echo $image_background; ?>") no-repeat scroll center center / 90% auto;
width:110px;
}
.address_user_image {
    float: right;
    left: 23px;
    position: relative;
    width: 47px;
}

   </style>
  
  <h4 class="all-head"><i class="fa fa-credit-card" aria-hidden="true"></i>
 Payment options</h4>
  
  <p><a class="collapse-link" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-shopping-cart mycart" aria-hidden="true"></i> <span class="summary_text">Show Order Summary</span></a></p>
  <div class="collapse" id="collapseExample">
    <div class="master-cart__full">
      <div class="cart-item">
        <div class="cart-item__cell cart-item__cell--left image_imposter_background_checkout"> <?php echo $this->Html->image('user_images/'.$template_data['users_image']['user_image'],['alt' => "Product Name","class" => "address_user_image"]);?> </div>
        <div class="cart-item__cell cart-item__cell--right">
          <div class="cart-item__sub-cell cart-item__sub-cell--top">
            <div class="cart-item__sub-sub-cell cart-item__sub-sub-cell--title">
              <h3 class="cart-item__heading"><?php echo !empty($template_data['template']['name'])?$template_data['template']['name']:""?></h3>
              <p>Quantity</p>

              <!-- <p>Cooking is personal, and so is the Tasty cookbook. &nbsp; Printed just for you, with seven of your favorite recipe themes &amp; a personalized dedication page.</p> --> 
            </div>
            <div class="cart-item__sub-sub-cell cart-item__sub-sub-cell--price"> <span class="cart-item__price">$<?php echo !empty($template_data['price'])?$template_data['price']:"0.00"?></span> <span class="cart-item__quantity"><?php echo $quantity;?></span> </div>
          </div>
              <!-- <div class="cart-item__sub-cell cart-item__sub-cell--bottom">
                <div class="cart-item__sub-sub-cell cart-item__sub-sub-cell--recipes-edit">
                  <div class="cart-item__sub-sub-sub-cell cart-item__sub-sub-sub-cell--recipes left">
                    <h4 class="cart-item__sub-heading photo">Photo Uploaded</h4>
                  </div>
                  <div class="cart-item__sub-sub-sub-cell cart-item__sub-sub-sub-cell--recipes right">
                    <h4 class="cart-item__sub-heading photo-img"> <?php echo $this->Html->image("user_images/".$this->request->session()->read("GuestUser.image_name"),["height" => "50","width" => "50"]);?></h4>
                  </div>
                </div>
                <div class="cart-item__sub-sub-cell cart-item__sub-sub-cell--remove">
                  <div class="cart-item__sub-sub-sub-cell cart-item__sub-sub-sub-cell--recipes">
                    <h4 class="cart-item__sub-heading">Voice Recorded</h4>
                  </div>
                  <div class="cart-item__sub-sub-sub-cell cart-item__sub-sub-sub-cell--recipes">
                    <h4 class="cart-item__sub-heading">
                       <audio preload="auto" src="<?php echo $SITEURL."img/user_audio/".$this->request->session()->read("GuestUser.audio_name");?>" controls=""></audio>
                    </h4>
                  </div>
                </div>
              </div> -->
            </div>
			<div class="clear"></div>
          </div>
          <div class="cart-totals">
            <div class="cart-totals__row">
              <h4 class="cart-totals__heading">Items total:</h4>
              <span class="cart-totals__total">$<?php echo number_format($total,2);?></span> </div>
              <div class="cart-totals__row">
                <h4 class="cart-totals__heading">Shipping Fee:</h4>
                <span>$<?php echo number_format($shipping_cost,2);?></span> </div>
                <div class="cart-totals__row">
                  <h4 class="cart-totals__heading">Sales Tax:</h4>
                  <span>$<?php echo number_format($sales_tax,2);?></span> </div>
                </div> 	<div class="clear"></div>
                <?php $grand_total += $total+$shipping_cost+$sales_tax;?> 
                <div class="cart-totals grand">
                  <div class="cart-totals__row">
                    <h4 class="cart-totals__heading">Order Total:</h4>
                    <span class="cart-totals__total">$<?php echo number_format($grand_total,2);?></span> </div>
                  </div> 	<div class="clear"></div>
                </div>
              </div>
              <div class="clear"></div>

              <div class="row checkout-box">
              
                             <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="sign-up-col">
                    <h1></h1>

                    <?php echo $this->Flash->render(); ?>
                    <form role="form" id="paymentForm" method="post">
                      <div class="card-payment">
                       <div id="paymentSection">
                       <div class="form-group msg-div" style="font-weight: bold;">

                        </div>
                        <div class="form-group">
                        <input type="hidden" name="card_type" id="card_type" value=""/>
                           <?php //echo pr($this->request->session()); <?php echo !empty($this->request->session()->read('order_id')) ? $this->request->session()->read('order_id') : ''; ?>
                          <input type="hidden" name="order_id" value="<?php echo $template_data['id']; ?>" />

                          <input class="form-control icon-lock" style="border-radius:0px" id="card_number" placeholder="Card Number" name="card_number" type="text">
                          <input type="hidden" name="payable_amount" value="<?php echo number_format($grand_total,2);?>">
                                                    <!-- <small class="help">This payment method supports Visa, American Express, MasterCard </small>  -->

                        </div>
                       

                        <div class="form-group">

                          <!--input class="form-control month" style="border-radius:0px" id="expiry_month" placeholder="Month" type="text" name="expiry_month">
                           <input class="form-control month" style="border-radius:0px" id="expiry_year" placeholder="Year" type="text" name="expiry_year"-->
                          <!--month drop down ---->
                          <select class="form-control month month-dropdown pull-left" style="border-radius:0px" id="expiry_month" placeholder="Month" type="text" name="expiry_month">
                            <option value="">Month</option>
                              <?php                              
                              for($m=1;$m<=12;$m++){
                                  ?>
                              <option value="<?php echo ($m<=9) ? '0'.$m : $m ; ?>"><?php echo ($m<=9) ? '0'.$m : $m ; ?></option>
                              <?php
                              }
                              ?>
                          </select>
                          
                          <!---year dropdown------>
                          
                          <select class="form-control month month-dropdown pull-right" style="border-radius:0px" id="expiry_year" placeholder="Year" type="text" name="expiry_year">
                              <option value="">Year</option>
                              <?php  
                              $current_year = date('Y');
                              for($y=$current_year;$y<=($current_year+19);$y++){
                                  ?>
                              <option value="<?php echo $y ; ?>"><?php echo $y ; ?></option>
                              <?php
                              }
                              ?>
                          </select>
                          
                          <div class="clear"></div>
                        </div>

                        <div class="form-group">
                            <input class="form-control" maxlength="5" style="border-radius:0px" id="cvv" placeholder="CVV" type="text" name="cvv">
                          <!-- <a href="#"><i id="cvc" class="fa fa-question-circle"></i></a> -->
                        </div>

                        <div class="form-group">
                          <input class="form-control" name="name_on_card" style="border-radius:0px" id="name_on_card" placeholder="Name on Card" type="text">
                        </div>


                        <div class="form-group">
                          <button type="button" class="btn btn-default guest-btn payment-btn lead-btn" id="cardSubmitBtn">Complete Order</button>
                        </div>
                      </div>
                    </div>           
                  </form>

                </div>
              </div> 
              
              
             
              
              
                <div class="col-md-6 col-sm-6 col-xs-12">
<div class="border"> <div class="or">OR</div> </div>
                  <div class="sign-up-col payment guest">
				  <h4 class="ctext">Check out with </h4>
				  
                    <form action="<?php echo $SITEURL;?>users/payment" method="post">
                      <input type="hidden" name="item_number" value="<?php echo $template_data['id'];?>" >
                      <input type="hidden" name="item_name" value="<?php echo $template_data['template']['name'];?>" >
                      <input type="hidden" name="quantity" value="<?php echo $quantity;?>" >
                      <input type="hidden" name="shipping" value="<?php echo $shipping_cost;?>" >
                      <input type="hidden" name="tax" value="<?php echo $sales_tax;?>">
                      <input type="hidden" name="amount" value="<?php echo $template_data['price'];?>" >
                      <input type="hidden" name="no_shipping" value="1">

                      <!-- <p><a href="#"><?php echo $this->Html->image("apple-pay.png");?></a></p> -->
                      <p><button type="submit" class="paypal-button"><?php echo $this->Html->image("pay-pal.png");?></button></p>
                      <!-- <p><a href="#"><?php echo $this->Html->image("amazon-pay.png");?></a></p> -->
                    </form>



                  </div>
                </div>
            
 
              
              
              
            </div>

            <style type="text/css">
              audio {
                width: none!important;
              }
            </style>

             <?= $this->Html->script("jquery.creditCardValidator"); ?>
        <script type="text/javascript">
        $(document).on("click",".paypal-button",function(){
          $(document).find('.overlay').show();
        })
            function cardFormValidate() {
                var cardValid = 0;

                //Card validation
                $('#card_number').validateCreditCard(function (result) {
                    var cardType = (result.card_type == null) ? '' : result.card_type.name;
                    if (cardType == 'Visa') {
                        var backPosition = result.valid ? '2px -163px, 260px -87px' : '2px -163px, 260px -61px';
                    } else if (cardType == 'MasterCard') {
                        var backPosition = result.valid ? '2px -247px, 260px -87px' : '2px -247px, 260px -61px';
                    } else if (cardType == 'Maestro') {
                        var backPosition = result.valid ? '2px -289px, 260px -87px' : '2px -289px, 260px -61px';
                    } else if (cardType == 'Discover') {
                        var backPosition = result.valid ? '2px -331px, 260px -87px' : '2px -331px, 260px -61px';
                    } else if (cardType == 'Amex') {
                        var backPosition = result.valid ? '2px -121px, 260px -87px' : '2px -121px, 260px -61px';
                    } else {
                        var backPosition = result.valid ? '2px -121px, 260px -87px' : '2px -121px, 260px -61px';
                    }
                    $('#card_number').css("background-position", backPosition);
                    if (result.valid) {
                        $("#card_type").val(cardType);
                        $("#card_number").removeClass('required');
                        cardValid = 1;
                    } else {
                        $("#card_type").val('');
                        $("#card_number").addClass('required');
                        cardValid = 0;
                    }
                });

                //Form validation
                var cardName = $("#name_on_card").val();
                var expMonth = $("#expiry_month").val();
                var expYear = $("#expiry_year").val();
                var cvv = $("#cvv").val();
                var regName = /^[a-z ,.'-]+$/i;
                var regMonth = /^01|02|03|04|05|06|07|08|09|10|11|12$/;
                var regYear = /^2016|2017|2018|2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030|2031$/;
                var regCVV = /^[0-9]{3,4}$/;
                if (cardValid == 0) {
                    $("#card_number").addClass('required');
                    $("#card_number").focus();
                    return false;
                } else if (!regMonth.test(expMonth)) {
                    $("#card_number").removeClass('required');
                    $("#expiry_month").addClass('required');
                    $("#expiry_month").focus();
                    return false;
                } else if (!regYear.test(expYear)) {
                    $("#card_number").removeClass('required');
                    $("#expiry_month").removeClass('required');
                    $("#expiry_year").addClass('required');
                    $("#expiry_year").focus();
                    return false;
                } else if (!regCVV.test(cvv)) {
                    $("#card_number").removeClass('required');
                    $("#expiry_month").removeClass('required');
                    $("#expiry_year").removeClass('required');
                    $("#cvv").addClass('required');
                    $("#cvv").focus();
                    return false;
                } else if (!regName.test(cardName)) {
                    $("#card_number").removeClass('required');
                    $("#expiry_month").removeClass('required');
                    $("#expiry_year").removeClass('required');
                    $("#cvv").removeClass('required');
                    $("#name_on_card").addClass('required');
                    $("#name_on_card").focus();
                    return false;
                } else {
                    $("#card_number").removeClass('required');
                    $("#expiry_month").removeClass('required');
                    $("#expiry_year").removeClass('required');
                    $("#cvv").removeClass('required');
                    $("#name_on_card").removeClass('required');
                    $('#cardSubmitBtn').prop('disabled', false);
                    return true;
                }
            }

            $(document).ready(function () {
                //Demo card numbers
                $('.card-payment .numbers li').wrapInner('<a href="javascript:void(0);"></a>').click(function (e) {
                    e.preventDefault();
                    $('.card-payment .numbers').slideUp(100);
                    cardFormValidate()
                    return $('#card_number').val($(this).text()).trigger('input');
                });
                $('body').click(function () {
                    return $('.card-payment .numbers').slideUp(100);
                });
                $('#sample-numbers-trigger').click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    return $('.card-payment .numbers').slideDown(100);
                });

                //Card form validation on input fields
                /*$('#paymentForm input[type=text]').on('keyup', function () {
                    cardFormValidate();
                });*/

                //Submit card form
                $(document).on('click',"#cardSubmitBtn", function () {

                    if (cardFormValidate()) {                     
                        var formData = $('#paymentForm').serialize();
                        $("#cardSubmitBtn").attr("disabled", true);
                        $(document).find('.overlay').show();
                        $.ajax({
                            type: 'POST',
                            url: path + 'users/payment_process',
                            dataType: "json",
                            data: formData,
                            beforeSend: function () {
                                $(".msg-div").html("Processing payment, Please do not refresh or press the back button");
                                $("#cardSubmitBtn").html('Processing....');
                            },
                            success: function (data) {
                                if (data.status == 1) {
                                  fbq('track', 'Purchase');
                                    window.location.href = path + "users/thanks"
                                } else {
                                    $(".msg-div").html("");
                                    swal("Error!", "Invalid card details", "error");
                                    $("#cardSubmitBtn").attr("disabled", false);
                                    $("#cardSubmitBtn").html('Proceed');
				    $(document).find('.overlay').hide();
                                }
                            }
                        });
                    }
                });
            });

	$(document).on("click",".collapse-link",function(){
              var text = $(".summary_text").text();
              
              if(text=="Show Order Summary"){
                $(".summary_text").text("Hide Order Summary");
                // $(this).text("Hide Order Summary");
              }
              if(text=="Hide Order Summary"){
                $(".summary_text").text("Show Order Summary");
                // $(this).text("Hide Order Summary");
              }
            });
        </script>
