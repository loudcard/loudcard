    <div class="photo-frame">      
    
            <?php
            //pr($this->request->session()->read('GuestUser'));
            ?>
            <center><?php echo $this->Flash->render();?></center>
           

        <!-- Select sending type-->

        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <h2 class="all-head">Order List </h2><br />
             <table id="user_list" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Transaction Id</th>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Email</th>
                          <th>Template name</th>
                          <th>Total Amount</th>
                          <th>Send to</th> 
                          <th>Shipping Type</th> 
                          <th>Payment Time</th>                                                     
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                      <?php foreach ($template_data as $key => $detail) {  
                      $shipping_type ="N/A";
                      if($detail['master_shipping_type']=="outside_us_shipping"){
                        $shipping_type = "Outside US shipping";
                      }
                      if($detail['master_shipping_type']=="us_expedited_shipping"){
                        $shipping_type = "Expedited shipping (average 2-3 days)";
                      } 
                      if($detail['master_shipping_type']=="us_shipping"){
                        $shipping_type = "US shipping (average 4-5 days)";
                      } 
                      if($detail['master_shipping_type']=="mixed"){
                        $shipping_type = "Mixed shipping";
                      }                          
                          $send_to ="";
                          $shipping = "Normal";
                          if(!empty($detail['send_to'])){
                            if($detail['send_to'] == "to_me"){
                              $send_to = "send to me";
                            }else{
                              $send_to =  "directly to recipient";
                            }
                          }

                           
                          
                       ?>                       
                        <tr>
                          <td><?=!empty($detail['transaction_id'])?$detail['transaction_id']:"NA";?></td>
                           <td><?=!empty($detail['user']['first_name'])?$detail['user']['first_name']:"NA";?></td>
                            <td><?=!empty($detail['user']['last_name'])?$detail['user']['last_name']:"NA";?></td>
                             <td><?=!empty($detail['user']['email'])?$detail['user']['email']:"NA";?></td>
                          <td><?=!empty($detail['template']['name'])?$detail['template']['name']:"NA";?></td>
                          <td><?=!empty($detail['total_amount'])?number_format($detail['total_amount'],2):"0.00";?></td>
                           <td><?=$send_to;?></td> 
                           <td><?php echo $shipping_type;?></td>
                         <td><?php echo date("Y-m-d H:i A",strtotime($detail['created_on']));?></td>
                           <td><?=!empty($detail['status'])?$detail['status']:"N/A";?></td>                         
                          <td><?=$this->Html->link("View",['controller' =>'Users' , "action" => 'view_orders',$detail['id']],['class' => 'btn btn-primary']);?>
                          <!-- <?=$this->Html->link("Delete",'javascript:void(0)',['class' => 'delete_order btn btn-danger','id' => $detail['id']]);?>                          -->
                          </td>
                        </tr>
                        <?php } ?>                        
                      </tbody>
                    </table>

          </div>
          
        

         
        </div>





</div>

</div>

</div>
</div>
</div>
</div>
