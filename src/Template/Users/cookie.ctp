

    <div class="cart-conatiner">
      <h6>Lizicard Cookie Policy</h6>
      <div class="links" style="text-align: justify;">
      <ul class="social-links social-circle-links"></ul>
      
<div class="row page-wrap">

            <div class="col-sm-offset-1 col-sm-10">

                    <article id="post-3292" class="post-3292 page type-page status-publish hentry">
                    <p>&nbsp;</p>
<p>Our Platform uses cookies to distinguish you from other users of our Platform. This helps us to provide you with a good experience when you browse our Platform and also allows us to improve our Platform. By visiting or using this Platform with your browser set to allow cookies, you are consenting to our use of cookies for the purposes set out below.</p>
<p><strong>What is a cookie?</strong></p>
<p>A cookie is a small file of letters and numbers that we store on your browser or the hard drive of your computer. Cookies contain information that is transferred to your computer’s hard drive.</p>
<p><strong>Types of cookies</strong></p>
<p>We use the following cookies:</p>
<ul>
<li><strong>Strictly necessary cookies.</strong> These are cookies that are required for the operation of our Platform. They include, for example, cookies that enable you to log into secure areas of our Platform, use a shopping cart or make use of e-billing services.</li>
<li><strong>Analytical/performance cookies.</strong> We and third parties such as Google Analytics use cookies to allow us to recognise and count the number of visitors and to see how visitors move around our Platform when they are using it. This helps us to improve the way our Platform works, for example, by ensuring that users are finding what they are looking for easily.</li>
<li><strong>Functionality cookies.</strong> These are used to recognise you when you return to our Platform. This enables us to personalise our content for you, greet you by name and remember your preferences (for example, your choice of language or region).</li>
<li><strong>Targeting cookies</strong>. We and third parties such as Google AdWords use these cookies to record your visit to our Platform, the pages you have visited and the links you have followed. We will use this information to make our Platform and the advertising displayed on it more relevant to your interests. We may also share this information with third parties for this purpose.</li>
</ul>
<p>Please note that third parties (including, for example, advertising networks and providers of external services like web traffic analysis services) may also use cookies, over which we have no control. These cookies are likely to be analytical/performance cookies or targeting cookies.</p>
<p>You can block cookies by activating the setting on your browser that allows you to refuse the setting of all or some cookies. However, if you use your browser settings to block all cookies (including essential cookies) you may not be able to access all or parts of our Platform. Our Platform (like many other shopping platforms) will not work when cookies are blocked.</p>
                    </article>

            </div>
		</div>
      </div>

  
      
    </div>
  </div>
</div>
