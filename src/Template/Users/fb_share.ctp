<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Lizi Cards</title>
<meta property="fb:app_id"          content="663418910527497" /> 
<meta property="og:type"            content="article" /> 
<meta property="og:url"             content="http://newsblog.org/news/136756249803614" /> 
<meta property="og:title"           content="Introducing our New Site" /> 
<meta property="og:image"           content="https://scontent-sea1-1.xx.fbcdn.net/hphotos-xap1/t39.2178-6/851565_496755187057665_544240989_n.jpg" /> 
<meta property="og:description"    content="http://samples.ogp.me/390580850990722" />


<!-- <link rel="canonical" href="<?=!empty($SITEURL)?$SITEURL:"";?>" />
 <meta property="og:locale" content="en_US" /> 
 <meta property="og:type" content="article" />
<meta property="og:title" content="<?=!empty($template_name)?$template_name:"";?>" />
<meta property="og:description" content="I just designed greeting card with my voice message and photo on Loudcards.com"/>
<meta property="og:url" content="<?=!empty($SITEURL)?$SITEURL:"";?>" />
<meta property="og:image" content="<?=!empty($preview_image)?$SITEURL."img/template_images/".$preview_image:"";?>" /> -->
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

<!-- Bootstrap -->
<?php echo $this->Html->css('bootstrap.min'); ?>
   <?php echo $this->Html->css('font-awesome.min'); ?>
   <?php echo $this->Html->css('style'); ?>
   <?php echo $this->Html->css('responsive'); ?>
  <link href="<?php echo $this->request->webroot;?>fonts/stylesheet.css" rel="stylesheet" type="text/css">
        <?php echo $this->Html->css('animate'); ?>
         <?php echo $this->Html->css('sweetalert'); ?>        
     <?php echo $this->Html->css('cropper.min'); ?>
  <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700|Pacifico" rel="stylesheet"> 
   <?php echo $this->Html->css('custom'); ?> 
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header> 
  
  <!-- Navigation -->
  <section class="main-menu">
    <nav class="navbar navbar-fixed-top navigation-top inner-nav" role="navigation">
      <div class="container-fluid"> 
        
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand" href="<?php echo $SITEURL;?>"><?php echo $this->Html->image("logo.png",['alt' => 'loudcards']);?> </a> </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li class="">
              <div class="cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>2</span> </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </section>
</header>

