    <div class="photo-frame">      

      <?php 
            // pr($template_data);
      $quantity = 1;
      $shipping_cost = 0; 
      $total = 0;
      $grand_total = 0;
      $class = "col-lg-6";
      if(!empty($template_data['send_to']) && $template_data['send_to'] == "to_me"){
        $quantity = $template_data['quantity'];

      }
      if(!empty($template_data['send_to']) && $template_data['send_to'] == "to_receipent"){
        $quantity = $template_data['quantity'];
        if($quantity==1){
        $class = "col-lg-12";
        }


      }
      $total = $template_data['price']*$quantity;
      ?>

      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <h2 class="all-head">Order Detail </h2><br /> <br />
         <div class="cart-header">
          <div class="cart-header__cell cart-header__cell--item">
            <h3 class="cart-item__sub-heading">Item</h3>
          </div>
          <div class="cart-header__cell cart-header__cell--price">
            <h3 class="cart-item__sub-heading">Total</h3>
          </div>
        </div>
        <div class="cart-item">
          <div class="cart-item__cell cart-item__cell--left"> 
            <?php echo $this->Html->image("template_images/".$preview_image,['alt' => "Product Name"]);?>
          </div>
          <div class="cart-item__cell cart-item__cell--right">
            <div class="cart-item__sub-cell cart-item__sub-cell--top">
              <div class="cart-item__sub-sub-cell cart-item__sub-sub-cell--title">
                <h3 class="cart-item__heading"><?php echo !empty($template_data['template']['name'])?$template_data['template']['name']:""?></h3>
                <p>Quantity</p>
                
                <!-- <p>Cooking is personal, and so is the Tasty cookbook. &nbsp; Printed just for you, with seven of your favorite recipe themes &amp; a personalized dedication page.</p> --> 
              </div>
              <div class="cart-item__sub-sub-cell cart-item__sub-sub-cell--price"> <span class="cart-item__price">$<?php echo !empty($template_data['price'])?$template_data['price']:"0.00"?></span> <span class="cart-item__quantity"><?php echo $quantity;?></span> </div>
            </div>
            <div class="cart-item__sub-cell cart-item__sub-cell--bottom">
              <div class="cart-item__sub-sub-cell cart-item__sub-sub-cell--recipes-edit">
           
                <div class="cart-item-center">
				 <h4 class="cart-item__sub-heading">Photo Uploaded</h4>
                  <h4 class="cart-item__sub-heading photo-img">
                    <?php echo $this->Html->image("user_images/".$template_data['users_image']['user_image'],["height" => "","width" => "55"]);?>
                  </h4>
                  <h5> <a class="download_btn" href="<?php echo $SITEURL;?>img/user_images/<?php echo $template_data['users_image']['user_image'];?>" download>Download</a> </h5>
                </div>
              </div>
              <div class="cart-item__sub-sub-cell cart-item__sub-sub-cell--remove">
			    <div class="cart-item-center">
           
                  <h4 class="cart-item__sub-heading">Voice Recorded</h4>
               
                <div class="cart-item__sub-sub-sub-cell cart-item__sub-sub-sub-cell--recipes">
                  <h4 class="cart-item__sub-heading">
                    <audio preload="auto" class="audio_recorded" src="<?php echo $SITEURL."img/user_audio/".$template_data['users_audio']['user_audio'];?>" controls=""></audio>

                  </h4>
                  <h5> <a class="download_btn" href="<?php echo $SITEURL;?>img/user_audio/<?php echo $template_data['users_audio']['user_audio'];?>" download>Download</a> </h5>
                </div>
              </div>    </div>
            </div>
          </div>
        </div>
        <div class="row">
          <?php 
          $shipping_cost+=$template_data['shipping_cost'];


            //   $state = $this->Location->state_name($template['shipping_cost']);

            // $country = $this->Location->country_name($this->request->session()->read("card_data.country_id"));



          
          $i=1; foreach ($get_address as $key => $value) {
           
          $state = $this->Location->state_name($value['state_id']);

          $country = $this->Location->country_name($value['country']);         



          ?>

          <div class="<?php echo $class;?>">
            <div class="shipping-address">
              <div class="cross-icon">
                <p>Address <?php echo $i;?></p>
              </div>
              <div class="adress-text">
                <p><?php echo $value['first_name']." ".$value['last_name'];?></p>
                <p><?php echo $value['address_one']." ".@$value['address_two'];?></p>
                <p><?php echo $value['city'];?>, <?php echo $state['name'];?></p>
                <p><?php echo $country['name'];?>, <?php echo $value['zip'];?></p>
              </div>
            </div>
          </div>
          <?php $i++; }  ?>
        </div>
        <div class="cart-totals">
          <div class="cart-totals__row">
            <h4 class="cart-totals__heading">Items total:</h4>
            <span class="cart-totals__total">$<?php echo number_format($total,2);?></span> </div>
            <div class="cart-totals__row">
              <h4 class="cart-totals__heading">Shipping:</h4>
              <span>$<?php echo number_format($shipping_cost,2);?></span> </div>
              <div class="cart-totals__row">
                <h4 class="cart-totals__heading">Taxes:</h4>
                <span>$<?php echo number_format($template_data['total_sales_tax'],2);?></span> </div>
              </div>
              <?php $grand_total += $total+$shipping_cost+$template_data['total_sales_tax'];?> 
              <div class="cart-totals grand">
                <div class="cart-totals__row">
                  <h4 class="cart-totals__heading">Order Total:</h4>
                  <span class="cart-totals__total">$<?php echo number_format($grand_total,2);?></span> </div>
                </div>
                <div class="clear"></div>
                

              </div>




            </div>





          </div>

        </div>

      </div>
    </div>
  </div>
</div>
