<hr class="show_line more_receipent<?php echo $count;?>">
<div class="more_receipent<?php echo $count;?> more_receipent">
<div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12"> 
          <div class="form-group">
            <input class="form-control first_name" name="data<?php echo "[$count]";?>[first_name]" style="border-radius:0px" id="" placeholder="First Name" type="text">
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12"> <div class="form-group">
          <input class="form-control last_name" name="data<?php echo "[$count]";?>[last_name]" style="border-radius:0px" id="" placeholder="Last Name" type="text">
        </div> </div>    
      </div>

      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12"> 
          <div class="form-group">
                <input class="form-control address_one" name="data<?php echo "[$count]";?>[address_one]" style="border-radius:0px" id="" placeholder="Address 1" type="text">
              </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">  <div class="form-group">
                <input class="form-control" name="data<?php echo "[$count]";?>[address_two]" style="border-radius:0px" id="" placeholder="Address 2 (Optional)" type="text">
              </div> </div>    
      </div>

      

      <div class="row">

      <div class="col-md-6 col-sm-6 col-xs-12"> 
           <div class="form-group">
            <input class="form-control city" name="data<?php echo "[$count]";?>[city]" style="border-radius:0px" id="" placeholder="City" type="text">
           <!-- <?php echo $this->Form->input('data['.$count.'][city]', ['class' => 'form-control city', 'label' => false,'options' => $city_list]); ?> -->
               
              </div>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12"> 
          <div class="form-group">
                <input class="form-control zip" name="data<?php echo "[$count]";?>[zip]" style="border-radius:0px" data-id="<?php echo $count;?>" placeholder="Zip" type="text" id="zips<?php echo $count;?>">
      <label id="zips<?php echo $count;?>-error" class="error" for="zips<?php echo $count;?>" style="display: none;">Enter valid zipcode</label>

              </div>
        </div>
          
      </div>

      <div class="row" >
        <div class="col-md-6 col-sm-6 col-xs-12">   <div class="form-group">
               <?php echo $this->Form->input('data['.$count.'][country_id]', ['class' => 'form-control country_fees multiple_country'.$count.'', 'id' => $count, 'label' => false,'options' => $country_list,'default' => 231]); ?>
              </div> </div> 

        
        <div class="col-md-6 col-sm-6 col-xs-12">   <div class="form-group show_state_list<?php echo $count;?>">
       <?php echo $this->Form->input('data['.$count.'][state_id]', ['class' => 'form-control state_me'.$count.' state_id', 'label' => false,'options' => $state_list,"empty" => "--Select state--"]); ?>
               
              </div> </div>    
      </div>

      <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12" id='country_fees<?php echo $count;?>'>
        <div class="radio-button show_shiiping_option<?php echo $count;?>">
        <h4 class="shipm">Shipping method</h4>
          <input checked="checked" class="radio-shipping" type="radio" name="data<?php echo "[$count]";?>[shipping]" value="1"> First Class shipping: <strong>$1</strong><br>
          <input class="radio-shipping" type="radio" name="data<?php echo "[$count]";?>[shipping]" value="7"> Expedited shipping: <strong>$7</strong><br>
          <!--input class="radio-shipping" type="radio" name="data<?php echo "[$count]";?>[shipping]" value="5"> Next day rush order (next day delivery if ordered before 12pm) : <strong>$5</strong-->
        </div>
        </div>
         <div class="col-md-6 col-sm-6 col-xs-12" id="remove_receipent">
        <a href="javascript:void(0)" id="<?php echo $count;?>" class="btn btn-danger remove_receipent pull-right">Remove this Recipient</a>
         
        </div>
      </div>

      
       

<style type="text/css">
  hr{
    display: block !important;
  }
</style>