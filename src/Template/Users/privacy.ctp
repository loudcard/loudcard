<div class="cart-conatiner">
      <div class="links" style="text-align: justify;">
      <ul class="social-links social-circle-links"></ul>
	  
	  <div class="basic-page__body">
      <meta charset="utf-8"><meta charset="utf-8">


      <h1>Privacy Policy </h1>


<ul class="basic-page__body">
<p><b>INTRODUCTION</b></p>
<li> At Lizicards, we believe that as our valued customer you have a fundamental right to privacy and to control your personal information. We are grateful that you have chosen to be our customer and we take the responsibility of not only printing and posting your cards, but also of being guardians of the personal information that you give us, extremely seriously. </li>
<li> This privacy policy (https://lizicards.com/users/privacy) sets out how Loudcards LLC with DBA Lizicards of 745 Atlantic Ave, Boston, MA 02111, U.S.A. (“Lizicards”, "Lizicard", "Loudcards", "Loudcard", “we” or “us”) will use any personal data we collect from you, or you provide to us, during your use of our products and services (whether existing now or in the future) (the “Services”) at www.lizicards.com or through our Lizicards mobile app (together our “Platform”). Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. By visiting the Platform you accept and consent to the practices described in this policy. </li>
<li> Data controller is Loudcards LLC with DBA Lizicards of 745 Atlantic Ave, Boston, MA 02111, U.S.A. (“Lizicards”, "Lizicard", "Loudcards", "Loudcard", “we” or “us”). We may from time to time use data processors, who are not employees, agents or otherwise connected with us, with whom we have an agreement to process your data. </li>
<li> We will protect your information consistent with the principles of the Privacy Act of 1974, the E-Government Act of 2002, and the Federal Records Act. </li>

</ul>


<ul class="basic-page__body">
<p><b>INFORMATION WE COLLECT FROM YOU</b> <br> We will collect and process the following data about you:</p>
<li>Information you give us. This is information about you that you give us by filling in forms on our Platform or by corresponding with us by phone, e-mail or otherwise. It includes information you provide when you register to use our Platform, or use our Services, place an order on our Platform, enter a competition, promotion or survey on the Platform and when you report a problem with our Platform. The information you give us may include your name, address, e-mail address and phone number, financial and credit card information. In order to take advantage of some of our Services, you may need to supply us with the personal details of a third party (for example, their name and address if you wish to send them some of our products). We will not use this information for anything other than providing the Service for which the information was supplied. </li>
<li>
<ul class="inner-basic-page__body">

 <strong> Information we collect about you. With regard to each of your visits to our Platform we will automatically collect the following information: </strong> <br>
<li>technical information, including the Internet protocol (IP) address used to connect your computer to the Internet, your login information (when you sign in as a customer), browser type and version, and the type of device used to access the Platform; </li>
<li>information about your visit, including the clickstream to, through and from our Platform (including date and time), products you viewed or searched for, page response times, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), and any phone number used to call our customer service number. </li>

</ul>
</li>
<li>Information we receive from other sources. We work with third parties (including, for example, business partners, sub-contractors in technical, payment and delivery services, advertising networks, analytics providers, search information providers) and may receive some information that is not personal information because it does not identify you or anyone else. For example, we may collect anonymous aggregated information about how users use our services.  </li>
<li>Cookies. Our website uses cookies to distinguish you from other users of our website. This helps us to provide you with a good experience when you browse our website and also allows us to improve our site.  </li>
<li>We do not knowingly solicit or collect any information about users who may be under the age of 13, and will delete any information provided by such individuals as soon as possible. Please do not provide us with any personal information if you are under the age of 13.  While we do allow users under the age of 13 to use the Platform, any users under the age of 18 should discuss the use of the Platform with their parents before they submit or upload any content to the Platform.
 </li>
</ul>


<ul class="basic-page__body">
<p><b>USES MADE OF THE INFORMATION</b> <br> We use information held about you in the following ways:</p>

<li>
<ul class="inner-basic-page__body"> 

 <strong> Information you give to us. We will use this information: </strong> <br>
<li> to carry out our obligations arising from any contracts entered into between you and us and to provide you with the information, products and Services that you request from us; </li>
<li> to provide you with information about other products and services we offer that are similar to those that you have already purchased or enquired about;</li>
<li>to provide you with information about products or services we feel may interest you. If you are an existing customer, we will only contact you by electronic means (e-mail or SMS) with information about products and services similar to those which were the subject of a previous sale to you. If you are a new customer, we will contact you by electronic means only if you have consented to this. If you do not want us to use your data in this way please tick the relevant box situated on the form on which we collect your data (for example, the order form or registration form) or contacting us at <a href="mailto:hello@lizicards.com" target="_top">hello@lizicards.com</a></li>
<li>to carry out market research and the tracking of sales;</li>
<li>to notify you about changes to our Services;</li>
<li>to ensure that content from our Platform is presented in the most effective manner for you and for your computer. </li>
</ul>
</li>

<li>
<ul class="inner-basic-page__body"> 

 <strong> Information we collect about you. We will use this information: </strong> <br>
 
 
<li>to administer our Platform and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes; </li>
<li>to improve our Platform to ensure that content is presented in the most effective and safe manner for you and for your computer; </li>
<li>to allow you to participate in interactive features of our Services, when you choose to do so; </li>
<li>to measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you; </li>
<li>to make suggestions and recommendations to you and other users of our Platform about goods or services that may interest you or them. </li>
 
</ul>
</li>


</ul>




<ul class="basic-page__body">
<p><b>DISCLOSURE OF YOUR INFORMATION</b> <br> You agree that we have the right to share your personal information in the following ways:</p>

<li>
<ul class="inner-basic-page__body"> 
 <strong> With selected third parties including: </strong> <br>
<li> business partners, suppliers and sub-contractors for the performance of any contract we enter into with them or you; </li>
<li> analytics and search engine providers that assist us in the improvement and optimization of our Platform.</li>
</ul>

<ul class="inner-basic-page__body"> 
 <strong> With third parties: </strong> <br>
<li> In the event that we sell or buy any business or assets, in which case we will disclose your personal data to the prospective seller or buyer of such business or assets.</li>
<li> If Loudcards LLC or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets.</li>
<li> where a third party claims that any content posted or uploaded by you to our Platform constitutes a violation of their intellectual property rights, or of their right to privacy.</li>
</ul>

</li>

<li> If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our terms of use (https://lizicards.com/users/privacy) and other agreements; or to protect the rights, property, or safety of Loudcards LLC, our customers, or others. This includes exchanging information with other companies and organizations for the purposes of fraud protection and credit risk reduction.</li>

</ul>



<ul class="basic-page__body">
<p><b>WHERE WE STORE YOUR PERSONAL DATA</b> <br> </p>

<li>All information you provide to us is stored on our secure servers in the United States of America. Any payment transactions will be encrypted using SSL technology. Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share your password with anyone.</li>
<li>Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorized access. </li>

</ul>


<ul class="basic-page__body">
<p><b>RETENTION OF YOUR PERSONAL DATA</b> <br> </p>

<li>We will retain your personal data for so long as is necessary for the purposes stated in this Privacy Policy and to address any claims or issues that may arise concerning your use of our Services.</li>
<li>After that, we may continue to hold data that relates to you and your use of our Platform for research and statistical analysis, as currently permitted under Data Protection Act. However, during the time you may contact us to request that we delete personal data we hold about you. </li>

</ul>



<ul class="basic-page__body">
<p><b>YOUR RIGHTS</b> <br> </p>

<li>You have the right to ask us to erase your personal data or not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by checking certain boxes on the forms we use to collect your data. You can also exercise the right at any time by contacting us at <a href="mailto:hello@lizicards.com" target="_top"> hello@lizicards.com</a> </li>
<li>If at any point you believe the information we process on you is incorrect you may ask to see this information and have it corrected or deleted. You also have the right to ask that we restrict the processing of your personal data and to object to our processing of your personal data. You also have the right to obtain from us, and reuse, the personal data we maintain from you, for your own purposes. </li>
<li>If you wish to lodge a complaint over the way in which we have handled your personal data, you can contact us at <a href="mailto:hello@lizicards.com" target="_top">hello@lizicards.com</a> </li>
<li>Our site may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies.  Please check these policies before you submit any personal data to these websites. </li>


</ul>



<ul class="basic-page__body">
<p><b>CARD TRANSACTIONS</b> <br> </p>

<li>Your full card details are not recorded or stored in our database. We only collect and store the final four numbers of your credit or debit card together with the expiry date and cardholder name in order for you to be able to select that card when making a future purchase. The details are encrypted and transferred securely to one or more third party payment service providers. </li>

</ul>



<ul class="basic-page__body">
<p><b> CALIFORNIA PRIVACY RIGHTS </b> <br> </p>

<p>Under California Civil Code sections 1798.83-1798.84, California residents are entitled to ask us for a notice identifying the categories of personal customer information which we share with our affiliates and/or third parties for marketing purposes, and providing contact information for such affiliates and/or third parties. If you are a California resident and would like a copy of this notice, please submit a written request to the following address: 111 E. 18th Street, 13th Floor, New York, NY 10003.</p>

</ul>


<ul class="basic-page__body">
<p><b>CHANGES TO OUR PRIVACY POLICY</b> <br> </p>

<li>Any changes we make to our privacy policy in the future will be posted on this page and, where appropriate, notified to you by e-mail. Please check back frequently to see any updates or changes to our privacy policy.</li>

</ul>



<ul class="basic-page__body">
<p><b>CONTACT</b> <br> </p>

<li>Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to <a href="mailto:hello@lizicards.com" target="_top">hello@lizicards.com </a></li>

</ul>
<br> 

<p> Effective Date of Privacy Policy: November 15, 2017. </p>

<br>

  
      </div>

  
      
    </div>
  </div>
</div>
