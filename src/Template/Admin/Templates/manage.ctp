<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_right">
               
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Template List </h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <?=$this->Flash->render();?>
                    <table id="user_list" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Image</th>
                          <th>Code</th>
                          <th>Tag</th>
                          <th>Template name</th>
                          <th>Price</th>                                                 
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                      <?php foreach ($templates as $key => $template_detail) {?>                       
                        <tr>
                          <td><?php echo $this->Html->image("templates/".$template_detail['templates'],["height" => "100", "width" => "100"]);?></td>
                           <td><?php echo !empty($template_detail['code'])?$template_detail['code']:"N/A";?></td>
                          <td><?php echo $template_detail['tag']['name'];?></td>
                          <td><?php echo $template_detail['name'];?></td>
                           <td>$ <?php echo number_format($template_detail['price'],2);?></td>                           
                          <td><?=$this->Html->link("Edit",['controller' =>'templates' , "action" => 'edit',$template_detail['id']],['class' => 'btn btn-primary']);?>
                          <?=$this->Html->link("Delete",'javascript:void(0)',['class' => 'delete_template btn btn-danger','id' => $template_detail['id']]);?></td>                         
                        </tr>
                        <?php } ?>                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

           
              
            </div>
          </div>
        </div>

<script>
    $(document).ready(function () {
        $('#user_list').DataTable();
    });
</script>
