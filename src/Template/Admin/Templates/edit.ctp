 <?php
 echo $this->Html->script('ckeditor/ckeditor.js');
 ?>
 <div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel x_panel_one">
          <div class="x_title">
            <h2>Add Template<small></small></h2>

            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo $this->Flash->render(); ?>
              </div>
            </div>

          </div>
          <?php  

          echo $this->Form->create($get_template_data, ['class' => 'form-horizontal form-bordered', 'label' => false, "id" => "edit_template","type" => 'file']);
          ?>
          <input type="hidden" name="id" id="template_ids" value="<?php echo @$id;?>">
           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Template Code <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php echo $this->Form->input('code', ['class' => 'form-control', 'label' => false]); ?>
            </div>
          </div>

          <div class="form-group">
            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Select tag <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php echo $this->Form->input('tag_id', ['class' => 'form-control', 'label' => false,'options' => $tag_list,'default' => $templates['tag']['id']]); ?>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Template Name <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php echo $this->Form->input('name', ['class' => 'form-control', 'label' => false]); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
             <img id="blah" src="<?php echo $SITEURL;?>img/templates/<?php echo $get_template_data['templates'];?>" alt="your image" style="width:150px;height:150px; "/>
           </div>
         </div>
         <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Template image <span class="required">*</span>
          </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo $this->Form->input('templates', ['class' => 'form-control images_template images_template_one', 'label' => false, "type" => "file",'onchange' => "readURL(this);"]); ?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Price <span class="required">*</span></label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo $this->Form->input('price', ['class' => 'form-control', 'label' => false, "maxlength" => "50",'type' => 'text']); ?>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Description <span class="required">*</span></label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo $this->Form->input('cover_text', ['class' => 'form-control ckeditor', 'label' => false, 'type' => 'textarea',"height" => "200"]); ?>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Artist <span class="required">*</span></label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo $this->Form->input('inside_text', ['class' => 'form-control ckeditor', 'label' => false, 'type' => 'textarea',"style" => "height:200px;"]); ?>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Card Specs <span class="required">*</span></label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo $this->Form->input('card_details', ['class' => 'form-control ckeditor', 'label' => false, 'type' => 'textarea',"height" => "200"]); ?>
          </div>
        </div>
        <div class="show_more_img_data">
          <?php $count=0; if(!empty($templates_images)){

            foreach ($templates_images as $key => $value) {  ?>
            <div class="show_more_images<?php echo $count;?>" id="<?php echo $value['id'];?>">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                 <img id="blah<?php echo $count;?>" src="<?php echo $SITEURL;?>img/template_images/<?php echo $value['images'];?>" alt="your image" style="width:150px;height:150px; "/>

               </div>
             </div>
             <div class="form-group">
              <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Image</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
               <?php echo $this->Form->input('images['.$count.']', ['class' => 'form-control images_template images_template'.$count.'', 'id' => $count, 'label' => false, "type" => "file",'onchange' => "readmoreURL(this);"]); ?>
               <input type="hidden" name="tempid[<?php echo $count;?>]" value="<?php echo $value['id']; ?>" />
             </div>
           </div>
           <div class="form-group">
            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Customize Image</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
             <input type="checkbox" onchange="triggerImageValidation(this);" class="show_preview_checkbox mypreviewcheckbox_<?php echo $count;?>" <?php if($value['show_preview']==1){echo "checked";} else{echo "disabled";}?> id="<?php echo $count;?>" name='show_preview<?php echo "[$count]";?>' value="1">
           </div>
         </div>
       </div>

       <?php $count++;  }
     }?>
   </div>

   <?php if($count > 0 && $count!=3){

    $display = "inline";
    $show_rm = "inline";
  }

  else if($count == 3){

    $display = "none";
    $show_rm = "inline";
  }
  else if($count ==0){

    $display = "inline";
    $show_rm = "none";
  }

  ?>

  <div class="ln_solid"></div>

  <div class="form-group add_template_submit_button">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
     <?php echo $this->Form->button('Submit', ['type' => 'submit', 'class' => 'btn btn-success']); ?>
     <?php echo $this->Html->link('Cancel',['controller' => 'Templates','action' => 'manage'],['class' => 'btn btn-warning']); ?>
     <?php $this->Form->button('Reset', ['type' => 'reset', 'class' => 'btn btn-primary']); ?> 
     <?php echo $this->Form->button('Add template images', ['type' => 'button', 'class' => 'btn btn-success add_images_bun','style' => 'display:'.$display.'']); ?>
     <?php echo $this->Form->button('Remove template image',['type' => 'button','class' => 'btn btn-danger remove_img','style' => 'display:'.$show_rm.'']); ?>

   </div>
 </div>          



</form>

</div>
</div>


</div>
</div>


</div>
</div>

<script type="text/javascript">
  var counter = <?php echo $count;?>;
// $(document).ready(function(){
// alert(counter);
// });



$(document).on("click",".add_images_bun",function(){   
    // alert(1);
    $.ajax({
            url: path + "admin/templates/add_more_images", //this is the submit URL
            type: 'POST', 
            data:{counter:counter},                     
            success: function (data) {
             $(".add_images_bun").text("Add more template images");
             if(counter==2){
              $(".add_images_bun").hide();
            }

            $(".remove_img").show();
            $(".show_more_img_data").append(data);
            if($(".show_preview_checkbox:checkbox:checked").length == 1){
              $('.show_preview_checkbox:not(:checked)').attr('disabled', 'disabled');
            }else{
             $('.show_preview_checkbox').removeAttr('disabled'); 
           }

           counter += 1;

         }
       });
  });



$(document).on("click",".remove_img",function(){ 
  counter-=1;

  swal({
    title: "Are you sure?",
    text: "You will not be able to recover the image!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete it!",
    cancelButtonText: "No, cancel plz!",
    closeOnConfirm: false,
    closeOnCancel: false,
  },
  function (isConfirm) {
    if (isConfirm) {
      var id = $(".show_more_images"+counter+"").attr("id");
      $.ajax({
                        url: path + "admin/templates/delete_template_image", //this is the submit URL
                        type: 'POST', //or POST  
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {

                          if (data.status != "" && data.status == "success") {

                            swal("Deleted!", data.msg, "success");
                            $(".show_more_images"+counter+"").remove();
                            if(counter > 0){
                             $(".add_images_bun").text("Add more template images"); 
                           }
                           $(".show_more_images" + counter + "").remove();
                           $(".add_images_bun").show();
                           if(counter==0){
                            $(".remove_img").hide();
                            $(".add_images_bun").show();
                            $(".add_images_bun").text("Add template images")
                            counter=0;
                          }
                        } else {
                          swal("fail!", data.msg, "error");
                        }
                      }
                    });
    } else {
      swal("Cancelled", "Your image is safe :)", "error");
    }
  });

});




$(document).on("click",".show_preview_checkbox",function(){
  if($(".show_preview_checkbox:checkbox:checked").length == 1){
    $('.show_preview_checkbox:not(:checked)').attr('disabled', 'disabled');
    var id = $(this).attr("id");
    
    // if (!$(".img_temp").hasClass(".images_template_img"+id+"")) {
    //   $(".img_temp").val('');

    // }
  }else{

    $('.show_preview_checkbox').removeAttr('disabled');
  }
});


function readURL(input) {
  var value = $(input).val();
  var Extension = value.substring(
    value.lastIndexOf('.') + 1).toLowerCase();
  if (Extension == "jpeg" || Extension == "jpg") {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#blah').attr('src', e.target.result);
        $("#blah").show();
      }

      reader.readAsDataURL(input.files[0]);
    }
  }else{
    $(".images_template_one").val('');
    swal("Fail!", "Photo only allows file types of JPG and JPEG. ", "error");
  }
}

/*function readmoreURL(input) {
          var id= $(input).attr('id');
          var value = $(input).val();
          var Extension = value.substring(
            value.lastIndexOf('.') + 1).toLowerCase();
          if (Extension == "jpeg" || Extension == "jpg") {
            if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.onload = function (e) {
                var img = new Image;
                img.onload = function() {
                  if(img.width !=1017 || img.height!=808) {
                   swal("Fail!", "Your picture must have 1,017x808 pixels.","error");
                   $(".images_template"+id+"").val('');
                   return false;
                 }else{

                  $('#blah'+id+'').attr('src', e.target.result);
                  $('#blah'+id+'').show();
                  return true;
                }
              }; img.src = reader.result;
              
            }

            reader.readAsDataURL(input.files[0]);
          }
        }else{
          $(".images_template"+id+"").val('');
          swal("Fail!", "Photo only allows file types of JPG and JPEG. ", "error");
        }
  }*/
    
    function readmoreURL(input) {        
          var id= $(input).attr('id');
          var value = $(input).val();          
          var mypreviewcheckbox = $('.mypreviewcheckbox_'+id).prop('checked');
          var Extension = value.substring(
            value.lastIndexOf('.') + 1).toLowerCase();
          if (Extension == "jpeg" || Extension == "jpg") {
            if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.onload = function (e) {
                var img = new Image;
                img.onload = function() {
                    if(mypreviewcheckbox==true) {                    
                        if(img.width !=1017 || img.height!=808){
                         swal("Fail!", "Your picture must have 1,017x808 pixels.","error");
                         $(".images_template"+id+"").val('');
                         return false;
                       }else{
                        $('#blah'+id+'').attr('src', e.target.result);
                        $('#blah'+id+'').show();
                        return true;
                      }
                  }else {
                    $('#blah'+id+'').attr('src', e.target.result);
                    $('#blah'+id+'').show();
                    return true;
                  }  
              }; img.src = reader.result;
              
            }

            reader.readAsDataURL(input.files[0]);
          }
        }else{
          $(".images_template"+id+"").val('');
          swal("Fail!", "Photo only allows file types of JPG and JPEG. ", "error");
          return false;
        }
      }
      
      function triggerImageValidation(input) {
          var id= $(input).attr('id');          
          var mypreviewcheckbox = $(input).prop('checked');
          if(mypreviewcheckbox==true) {
            $('.images_template'+id).trigger("change");
          }
      }
      
      /*function triggerImageForAllValidation() {                    
          $( ".show_preview_checkbox" ).each(function( index ) {
                var my_input = $(this);
                triggerImageValidation(my_input);
          });
          
      }*/
    
    


$("#edit_template").on("submit", function (e) {
    e.preventDefault();
    if ($("#edit_template").valid())
    {
      for ( instance in CKEDITOR.instances ) {
        CKEDITOR.instances[instance].updateElement();
    }
    
    //triggerImageForAllValidation();
     
        $.ajax({
            url: path + "admin/templates/edit", //this is the submit URL
            type: 'POST', //or POST
            dataType: 'json',
            data: new FormData(this),
            contentType: false,
            processData: false,           
            success: function (data) {  
            if (data != "" && data.status == "error"){
            swal("Fail!", data.msg, "error");
            
          }   
          else if (data != "" && data.status == "success"){
            swal("Great!", data.msg, "success");
            setTimeout(function(){ document.location.href = path+"admin/templates/manage";  }, 2000);
            
          }             
              
            }
        });
    }
});


</script>
