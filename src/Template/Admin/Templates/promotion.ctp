<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_right">
               
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Promotion Code </h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <?=$this->Flash->render();?>
                    <table id="user_list" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Text</th>
                          <th>Code</th> 
                          <th>Start date</th>
                           <th>End date</th>
                            <th>Status</th>                                                                          
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                           <?php if(!empty($promotion)){ foreach($promotion as $code){?>                  
                        <tr>
                          <td><?php echo $code['text'];?></td>
                          <td><?php echo $code['code'];?></td>
                          <td><?php echo date("Y-m-d",strtotime($code['start_date']));?></td>
                          <td><?php echo date("Y-m-d",strtotime($code['end_date']));?></td>
                          <td><?php echo ($code['status']==0)?"Inactive":"Active";?></td>
                          <td><?=$this->Html->link("Edit",['controller' =>'templates' , "action" => 'edit_code',$code['id']],['class' => 'btn btn-primary']);?>
                           <?=$this->Html->link("Delete",'javascript:void(0)',['class' => 'delete_promotion btn btn-danger','id' => $code['id']]);?></td>   
                         </td>                         
                        </tr>
                          <?php } }?>
                                    
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>

        <script>
    $(document).ready(function () {
        $('#user_list').DataTable();
    });
</script>


