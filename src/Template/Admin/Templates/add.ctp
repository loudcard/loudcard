 <?php
 echo $this->Html->script('ckeditor/ckeditor.js');
 ?>
 <div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel x_panel_one">
          <div class="x_title">
            <h2>Add Template<small></small></h2>

            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo $this->Flash->render(); ?>
              </div>
            </div>

          </div>
          <?php           
          echo $this->Form->create('/', ['class' => 'form-horizontal form-bordered', 'label' => false, "id" => "add_template","type" => 'file']);
          ?>

           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Template Code <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php echo $this->Form->input('code', ['class' => 'form-control', 'label' => false]); ?>
            </div>
          </div>

          <div class="form-group">
            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Select tag <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php echo $this->Form->input('tag_id', ['class' => 'form-control', 'label' => false,'options' => $tag_list]); ?>
            </div>
          </div>
         
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Template Name <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php echo $this->Form->input('name', ['class' => 'form-control', 'label' => false]); ?>
            </div>
          </div>
           <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
             <img id="blah" src="" alt="your image" style="display:none;width:150px;height:150px; "/>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Template image <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php echo $this->Form->input('templates', ['class' => 'form-control images_template images_template_one', 'label' => false, "type" => "file",'onchange' => "readURL(this);"]); ?>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Price <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php echo $this->Form->input('price', ['class' => 'form-control', 'label' => false, "maxlength" => "50"]); ?>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Description <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php echo $this->Form->input('cover_text', ['class' => 'form-control ckeditor', 'label' => false, 'type' => 'textarea',"height" => "200"]); ?>
            </div>
          </div>
         
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Artist <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php echo $this->Form->input('inside_text', ['class' => 'form-control ckeditor', 'label' => false, 'type' => 'textarea',"style" => "height:200px;"]); ?>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Card specs <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php echo $this->Form->input('card_details', ['class' => 'form-control ckeditor', 'label' => false, 'type' => 'textarea',"height" => "200"]); ?>
            </div>
          </div>
          <div class="show_more_img_data">
          </div>

          <div class="ln_solid"></div>
         
          <div class="form-group add_template_submit_button">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
             <?php echo $this->Form->button('Submit', ['type' => 'submit', 'class' => 'btn btn-success']); ?>
              <?php echo $this->Html->link('Cancel',['controller' => 'Templates','action' => 'manage'],['class' => 'btn btn-warning']); ?>
              <?php $this->Form->button('Reset', ['type' => 'reset', 'class' => 'btn btn-primary']); ?> 
               <?php echo $this->Form->button('Add template images', ['type' => 'button', 'class' => 'btn btn-success add_images_bun']); ?>
               <?php echo $this->Form->button('Remove template image',['type' => 'button','class' => 'btn btn-danger remove_img',"style" => 'display:none;']); ?>

            </div>
          </div>          
          
        

          </form>

        </div>
      </div>


</div>
</div>


</div>
</div>

<script type="text/javascript">
  var counter = 0;
  
    $(document).on("click",".add_images_bun",function(){   
    // alert(1);
     $.ajax({
            url: path + "admin/templates/add_more_images", //this is the submit URL
            type: 'POST', 
            data:{counter:counter},                     
            success: function (data) {
             $(".add_images_bun").text("Add more template images");
              if(counter==2){
                $(".add_images_bun").hide();
              }

              $(".remove_img").show();
              $(".show_more_img_data").append(data);
              if($(".show_preview_checkbox:checkbox:checked").length == 1){
                $('.show_preview_checkbox:not(:checked)').attr('disabled', 'disabled');
              }else{
               $('.show_preview_checkbox').removeAttr('disabled'); 
              }
            
  counter += 1;

            }
          });
   });

 

     $(document).on("click",".remove_img",function(){ 
      counter-=1;
     
     $(".show_more_images" + counter + "").remove();
     $(".add_images_bun").show();
     if(counter==0){
      $(".remove_img").hide();
       $(".add_images_bun").show();
       $(".add_images_bun").text("Add template images")
      counter=0;
     }
   });



$("#add_template").on("submit", function (e) {
    e.preventDefault();
    if ($("#add_template").valid())
    {
      for ( instance in CKEDITOR.instances ) {
        CKEDITOR.instances[instance].updateElement();
      }
     
        $.ajax({
            url: path + "admin/templates/add", //this is the submit URL
            type: 'POST', //or POST
            dataType: 'json',
            data: new FormData(this),
            contentType: false,
            processData: false,           
            success: function (data) {  
            if (data != "" && data.status == "error"){
            swal("Fail!", data.msg, "error");
            
          }   
          else if (data != "" && data.status == "success"){
            swal("Great!", data.msg, "success");
            setTimeout(function(){ document.location.href = path+"admin/templates/manage";  }, 2000);
            
          }             
              
            }
        });
    }
});

$(document).on("click",".show_preview_checkbox",function(){
  if($(".show_preview_checkbox:checkbox:checked").length == 1){
    $('.show_preview_checkbox:not(:checked)').attr('disabled', 'disabled');
    var id = $(this).attr("id");
    
    // if (!$(".img_temp").hasClass(".images_template_img"+id+"")) {
    //   $(".img_temp").val('');

    // }
  }else{

    $('.show_preview_checkbox').removeAttr('disabled');
  }
});


 function readURL(input) {
        var value = $(input).val();
         var Extension = value.substring(
                    value.lastIndexOf('.') + 1).toLowerCase();
         if (Extension == "jpeg" || Extension == "jpg") {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $("#blah").show();
                }

                reader.readAsDataURL(input.files[0]);
            }
             }else{
            $(".images_template_one").val('');
            swal("Fail!", "Photo only allows file types of JPG and JPEG. ", "error");
          }
        }

        function readmoreURL(input) {
          var id= $(input).attr('id');
          var value = $(input).val();          
          var mypreviewcheckbox = $('.mypreviewcheckbox_'+id).prop('checked');
          var Extension = value.substring(
            value.lastIndexOf('.') + 1).toLowerCase();
          if (Extension == "jpeg" || Extension == "jpg") {
            if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.onload = function (e) {
                var img = new Image;
                img.onload = function() {
                    if(mypreviewcheckbox==true) {                    
                        if(img.width !=1017 || img.height!=808){
                         swal("Fail!", "Your picture must have 1,017x808 pixels.","error");
                         $(".images_template"+id+"").val('');
                         return false;
                       }else{
                        $('#blah'+id+'').attr('src', e.target.result);
                        $('#blah'+id+'').show();
                        return true;
                      }
                  }else {
                    $('#blah'+id+'').attr('src', e.target.result);
                    $('#blah'+id+'').show();
                    return true;
                  }  
              }; img.src = reader.result;
              
            }

            reader.readAsDataURL(input.files[0]);
          }
        }else{
          $(".images_template"+id+"").val('');
          swal("Fail!", "Photo only allows file types of JPG and JPEG. ", "error");
          return false;
        }
      }
      
      function triggerImageValidation(input) {
          var id= $(input).attr('id');          
          var mypreviewcheckbox = $(input).prop('checked');
          if(mypreviewcheckbox==true) {
            $('.images_template'+id).trigger("change");
          }
      }





</script>
