 <?php
 echo $this->Html->script('ckeditor/ckeditor.js');
 ?>
 <div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel x_panel_one">
          <div class="x_title">
            <h2>Add Template<small></small></h2>

            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo $this->Flash->render(); ?>
              </div>
            </div>

          </div>
          <?php  

          echo $this->Form->create('/', ['class' => 'form-horizontal form-bordered', 'label' => false, "id" => "add_promotion","type" => 'file']);
          ?>
          

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Promotion text <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php echo $this->Form->input('text', ['class' => 'form-control', 'label' => false]); ?>
            </div>
          </div>
         
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Promotion code <span class="required">*</span></label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo $this->Form->input('code', ['class' => 'form-control', 'label' => false, "maxlength" => "50",'type' => 'text']); ?>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Start Date <span class="required">*</span></label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo $this->Form->input('start_date', ['class' => 'form-control','id' =>'start_date', 'label' => false, 'type' => 'text']); ?>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">End Date <span class="required">*</span></label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo $this->Form->input('end_date', ['class' => 'form-control','id' =>'end_date', 'label' => false,'type' => 'text']); ?>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Active </label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="checkbox" class="show_preview_checkbox" name='status' value="1">
          </div>
        </div>




  <div class="ln_solid"></div>

  <div class="form-group add_template_submit_button">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
     <?php echo $this->Form->button('Submit', ['type' => 'submit', 'class' => 'btn btn-success']); ?>
     <?php echo $this->Html->link('Cancel',['controller' => 'Templates','action' => 'promotion'],['class' => 'btn btn-warning']); ?>
     <?php $this->Form->button('Reset', ['type' => 'reset', 'class' => 'btn btn-primary']); ?> 
   

   </div>
 </div>          



</form>

</div>
</div>


</div>
</div>


</div>
</div>

<script type="text/javascript">

 $( function() {
   $("#start_date").datepicker({
        dateFormat: 'yy-mm-dd',minDate: 0,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#end_date").datepicker("option", "minDate", dt);
        }
    });

   $("#end_date").datepicker({
        dateFormat: 'yy-mm-dd',minDate: 0,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#start_date").datepicker("option", "maxDate", dt);
        }
    });
    // $( "#start_date,#end_date" ).datepicker({ dateFormat: 'yy-mm-dd',minDate: 0 });
  } );

$("#add_promotion").on("submit", function (e) {
    e.preventDefault();
    if ($("#add_promotion").valid())
    {
     
        $.ajax({
            url: path + "admin/templates/add_code", //this is the submit URL
            type: 'POST', //or POST
            dataType: 'json',
            data: new FormData(this),
            contentType: false,
            processData: false,           
            success: function (data) {  
            if (data != "" && data.status == "error"){
            swal("Fail!", data.msg, "error");
            
          }   
          else if (data != "" && data.status == "success"){
            swal("Great!", data.msg, "success");
            setTimeout(function(){ document.location.href = path+"admin/templates/promotion";  }, 2000);
            
          }             
              
            }
        });
    }
});


</script>
