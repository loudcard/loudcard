 <div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Edit User<small></small></h2>

            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
             <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <?php echo $this->Flash->render(); ?>
              </div>
          </div>

             </div>
            <?php
            if(!empty($id)){
              $fill_data = $get_user_data;
            }else{
              $fill_data = '/';
            }
            echo $this->Form->create($get_user_data, ['class' => 'form-horizontal form-bordered', 'label' => false, "id" => "add_user"]);
            ?>
            <?php echo $this->Form->input('id', ['value' => @$id, 'id' => 'user_id' , 'type' => 'hidden']); ?>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <?php echo $this->Form->input('first_name', ['class' => 'form-control', 'label' => false, "maxlength" => "50"]); ?>
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php echo $this->Form->input('last_name', ['class' => 'form-control', 'label' => false, "maxlength" => "50"]); ?>
          </div>
      </div>
      <div class="form-group">
        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Email Address <span class="required">*</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <?php echo $this->Form->input('email', ['class' => 'form-control', 'label' => false, "maxlength" => "50"]); ?>
      </div>
  </div>
 


<div class="ln_solid"></div>
<div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
    <?php echo $this->Form->button('Submit', ['type' => 'submit', 'class' => 'btn btn-success']); ?>
      <?php echo $this->Html->link('Cancel',['controller' => 'Users','action' => 'manage'],['class' => 'btn btn-warning']); ?>
      
  </div>
</div>

</form>
</div>
</div>
</div>
</div>


</div>
</div>

