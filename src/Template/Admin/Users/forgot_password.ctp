<?php
           
          
echo $this->Form->create("/", ['templates' => ['inputContainer' => '{{content}}'], "id" => "forgot_password", 'class' => 'm-t', 'role' => 'form', 'label' => false]);
?>
<h4 class="nomargin">Forgot Password</h4>
<p class="mt5 mb20">Enter your email address and reset link will be emailed to you.</p>
    <div class="forgot_pass_msg_div" style="color:#000; text-align: center;"></div>
 <div class="form-group">
                        <?= $this->Form->input("email", array("class" => "form-control","type" => "email", "placeholder" => "Enter your email", "label" => false)); ?>
                    </div>
                               


                    <div class="form-group">
                        <?= $this->Form->submit("Send Reset Password Link", array("class" => "btn login_btn btn-primary")); ?>
                        
                    </div>

                    <div class="modal-sub-btn-links" style="text-align:center; color:#fff">

                        <a href="login" class=" pull-left">Back to Login</a>

                    </div>
                <?php
                echo $this->Form->end();
                ?>

