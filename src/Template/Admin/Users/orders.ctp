<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_right">
               
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Order List </h2>
                    
                    <div class="clearfix"></div>
                  </div>

                  <!-- <div class="x_content">
                    <div class="row">
                    <form action="export_csv" method="post" id="export_csv">
                    <div class="col-sm-2 col-sm-offset-2">  
                    <input type="text" name="from_date" class="form-control" id="from_date" placeholder="From date">

                    </div>
                    <div class="col-sm-2">  
                    <input type="text" name="to_date" class="form-control" id="to_date" placeholder="To date">

                    </div>
                    <div class="col-sm-2">  
                    <select name="status" class="form-control">
                    <option value="" selected="selected" disabled="disabled">---Select Status---</option>
                    <option value="completed">Completed</option>
                    <option value="pending">Pending</option>
                    </select>

                    </div>
                     <div class="col-sm-2">  
                    <button type="submit" class="btn btn-primary">Export Order</button>

                    </div>
                    </form>
                  </div>
                  </div> -->




                  <div class="x_content">
                    <?=$this->Flash->render();?>
                    <table id="user_list" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>Transaction Id</th>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Email</th>
			   <th>Card Id</th>
                          <th>Template name</th>
                          <th>Total Amount</th>
                          <th>Send to</th> 
                          <th>Shipping Type</th> 
                         <th>Order From</th>
                          <th>Payment Time</th>                                                     
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                      <?php foreach ($template_data as $key => $detail) {   
                        $shipping_type ="N/A";
                      if($detail['master_shipping_type']=="outside_us_shipping"){
                        $shipping_type = "Outside US shipping";
                      }
                      if($detail['master_shipping_type']=="us_expedited_shipping"){
                        $shipping_type = "Expedited shipping (average 2-3 days)";
                      } 
                      if($detail['master_shipping_type']=="us_shipping"){
                        $shipping_type = "US shipping (average 4-5 days)";
                      } 
                      if($detail['master_shipping_type']=="mixed"){
                        $shipping_type = "Mixed shipping";
                      }                    
                          $send_to ="";
                          $shipping = "Normal";
                          if(!empty($detail['send_to'])){
                            if($detail['send_to'] == "to_me"){
                              $send_to = "send to me";
                            }else{
                              $send_to =  "directly to recipient";
                            }
                          }

                           
                          
                       ?>                       
                        <tr>
                          <td><?=!empty($detail['transaction_id'])?$detail['transaction_id']:"NA";?></td>
                           <td><?=!empty($detail['user']['first_name'])?$detail['user']['first_name']:"NA";?></td>
                            <td><?=!empty($detail['user']['last_name'])?$detail['user']['last_name']:"NA";?></td>
                             <td><?=!empty($detail['user']['email'])?$detail['user']['email']:"NA";?></td>
			  <td><?=!empty($detail['template']['code'])?$detail['template']['code']:"NA";?></td>
                          <td><?=!empty($detail['template']['name'])?$detail['template']['name']:"NA";?></td>
                          <td><?=!empty($detail['total_amount'])?number_format($detail['total_amount'],2):"0.00";?></td>
                           <td><?=$send_to;?></td> 
                           <td><?php echo $shipping_type;?></td>
				<td><?=!empty($detail['order_created_from'])?$detail['order_created_from']:"N/A";?></td>
                         <td><?php echo date("Y-m-d H:i A",strtotime($detail['created_on']));?></td>
                           <td><?=!empty($detail['status'])?$detail['status']:"N/A";?></td>                         
                          <td><?=$this->Html->link("View",['controller' =>'Users' , "action" => 'view_orders',$detail['id']],['class' => 'btn btn-primary']);?>
                          <!-- <?=$this->Html->link("Delete",'javascript:void(0)',['class' => 'delete_order btn btn-danger','id' => $detail['id']]);?>                          -->
                          </td>
                        </tr>
                        <?php } ?>                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

           
              
            </div>
          </div>
        </div>

<script>
    $(document).ready(function () {
        $('#user_list').DataTable();

         $( "#from_date,#to_date" ).datepicker({ dateFormat: 'yy-mm-dd'});

         $(document).on("keypress","#from_date,#to_date",function(e){
          e.preventDefault();
         });
         
    });
</script>
