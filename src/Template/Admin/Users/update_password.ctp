 <div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Change Password <small></small></h2>

            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
             <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <?php echo $this->Flash->render(); ?>
              </div>
          </div>

             </div>
           <?php
           echo $this->Form->create($User, ['url' => ['action' => 'updatePassword'], 'class' => 'form-horizontal', 'label' => false, 'enctype' => 'multipart/form-data', 'novalidate', 'id' => 'updatePassword','method' => 'post']);
                                                echo $this->Form->input('id', ['value' => $this->request->session()->read('Auth.User.id') , 'type' => 'hidden']);
                                                ?>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Old Password <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                   <?php echo $this->Form->input('old_password', ['class' => 'form-control', 'required' => true, 'label' => false,'placeholder' => 'Old Password','type' => 'password']); ?>
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">New Password <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
               <?php echo $this->Form->input('password', ['class' => 'form-control', 'required' => true, 'label' => false,'placeholder' => 'New Password','type' => 'password']);
                                                                    ?>
          </div>
      </div>
      <div class="form-group">
        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password <span class="required">*</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <?php echo $this->Form->input('confirm_password', ['class' => 'form-control', 'required' => true, 'label' => false,'placeholder' => 'Confirm Password','type' => 'password']); ?>
      </div>
  </div>
 

<div class="ln_solid"></div>
<div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
    <?php echo $this->Form->button('Save', ['type' => 'submit', 'class' => 'btn btn-success']); ?>
      
      
  </div>
</div>

</form>
</div>
</div>
</div>
</div>








</div>
</div>