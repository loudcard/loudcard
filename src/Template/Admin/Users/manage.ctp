<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_right">
               
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Users List </h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <?=$this->Flash->render();?>
                    <table id="user_list" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Email Address</th>
                          <th>Registered as</th> 
                          <th>Registered date</th> 
                          <th>Status</th>                                                 
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                      <?php $type = ""; foreach ($get_userdetail as $key => $user_detail) {
                        if($user_detail['type']==1){
                          $type = "Normal User";
                        }else if($user_detail['type']==2){
                          $type = "Guest User";
                        }
                        else if($user_detail['type']==3){
                          $type = "Facebook User";
                        }

                        if($user_detail['status']==1){
                          $status= "Activate";
                        }else{
                          $status = "Deactivate";
                        }

                       ?>                       
                        <tr>
                          <td><?=$user_detail['first_name'];?></td>
                          <td><?=$user_detail['last_name'];?></td>
                          <td><?=$user_detail['email'];?></td>     
                          <td><?=$type;?></td>   
                          <td><?=date("Y-m-d H:i:s",strtotime($user_detail['created_on']));?></td>
                          <td><span style="cursor: pointer;" class="user_status" data-status="<?php echo $user_detail['status'];?>" id="<?php echo $user_detail['id'];?>"><?=$status;?></span></td>                          
                          <td><!-- <?=$this->Html->link("Edit",['controller' =>'Users' , "action" => 'edit',$user_detail['id']],['class' => 'btn btn-primary']);?> -->
                          <?=$this->Html->link("View",['controller' =>'Users' , "action" => 'view',$user_detail['id']],['class' => 'btn btn-primary']);?>
                          <?=$this->Html->link("Delete",'javascript:void(0)',['class' => 'delete_user btn btn-danger','id' => $user_detail['id']]);?></td>                         
                        </tr>
                        <?php } ?>                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

           
              
            </div>
          </div>
        </div>

<script>
    $(document).ready(function () {
        $('#user_list').DataTable();
    });
</script>
