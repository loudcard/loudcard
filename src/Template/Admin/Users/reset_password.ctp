<?php          
          
echo $this->Form->create("/", ['templates' => ['inputContainer' => '{{content}}'], "id" => "reset_password", 'class' => 'm-t', 'role' => 'form', 'label' => false]);
?>
<h4 class="nomargin">Reset your Password</h4>

    <div class="forgot_pass_msg_div" style="color:#000; text-align: center;"><?php echo  $this->Flash->render(); ?></div>
 <div class="form-group">
                        <?= $this->Form->input("password", array("class" => "form-control","type" => "password","placeholder" => "New password", 'id' => 'password', "label" => false)); ?>
                    </div>
                               


                    <div class="form-group">
                        <?= $this->Form->input("password_confirm", array("class" => "form-control", "placeholder" => "Confirm password", "label" => false,"type" => "password")); ?>
                    </div>

                   <div class="form-group">
                        <?= $this->Form->submit("Reset Password", array("class" => "btn-more form-control")); ?>
                        
                    </div>
                <?php
                echo $this->Form->end();
                ?>
