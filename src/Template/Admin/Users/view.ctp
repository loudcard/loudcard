 <div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>View User<small></small></h2>

            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
             <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <?php echo $this->Flash->render(); ?>
              </div>
          </div>

             </div>
            <?php
            if($get_user_data['type']==1){
                          $type = "Normal User";
                        }else if($get_user_data['type']==2){
                          $type = "Guest User";
                        }
                        else if($get_user_data['type']==3){
                          $type = "Facebook User";
                        }

                        if($get_user_data['status']==1){
                          $status= "Activate";
                        }else{
                          $status = "Deactivate";
                        }
            echo $this->Form->create($get_user_data, ['class' => 'form-horizontal form-bordered', 'label' => false, "id" => "add_user"]);
            ?>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name: </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <?php echo $get_user_data['first_name'];?>
              </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name: </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php echo $get_user_data['last_name'];?>
          </div>
      </div>
      <div class="form-group">
        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Email Address:</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
         <?php echo $get_user_data['email'];?>
      </div>
  </div>
   <div class="form-group">
        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Registered as:</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
         <?php echo $type;?>
      </div>
  </div>

   <div class="form-group">
        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Registered date:</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
         <?php echo date("Y-m-d H:i:s",strtotime($get_user_data['created_on']));?>
      </div>
  </div>
   <div class="form-group">
        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Status:</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
         <?php echo $status;?>
      </div>
  </div>
 


<div class="ln_solid"></div>


</form>
</div>
</div>
</div>
</div>


</div>
</div>

