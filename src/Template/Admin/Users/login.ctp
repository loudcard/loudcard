<?php
            if (isset($remembered_data)) {
                $username = $remembered_data['username'];
                $password = $remembered_data['password'];
                $checked = 'checked="checked"';
            } else {
                $checked = $password = $username = '';
            }
          
echo $this->Form->create(NULL, ['templates' => ['inputContainer' => '{{content}}'], 'url' => ['action' => 'login'], 'class' => 'm-t', 'role' => 'form', 'label' => false]);
?>
<h4 class="nomargin">Sign In</h4>
<p class="mt5 mb20">Login to access your account.</p>
    <div class="form-group"><?php echo  $this->Flash->render(); ?></div>
 <div class="form-group">
                        <?= $this->Form->input("email", array('value' => @$username, "class" => "form-control", "type" => "email", "placeholder" => "Enter your email", "label" => false)); ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->input("password", array('value' => @$password, "class" => "form-control", "name" => "password", "placeholder" => "Enter your password", "label" => false)); ?>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->input('remember_me', array('label' => ' Remember Me', 'value' => '1', 'type' => 'checkbox', 'div' => false, $checked,'class' => 'remember_me')); ?>
                    </div>             


                    <div class="form-group">
                        <?= $this->Form->submit("Log in", array("class" => "btn btn-primary login_btn")); ?>
                        <?= $this->Html->image("loader.gif", array("width" => "50", "style" => "display:none", "class" => "loader")); ?>
                    </div>

                    <div class="modal-sub-btn-links" style="text-align:center; color:#fff">

                        <a href="<?php echo $SITEURL;?>admin/users/forgot_password" class=" pull-left forgot_pass">Forgot Password</a>

                    </div>
                <?php
                echo $this->Form->end();
                ?>


