<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="images/favicon.png" type="image/png">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"> 
        <title><?= $pageDescription ?></title>
	<?php
echo $this->Html->meta('icon','img/favicon.png');
?>

         <?php echo $this->Html->css('style.default'); ?>

        <script type="text/javascript">
            var path = '<?= $SITEURL; ?>';
          
        </script>

    </head>

    <body class="signin">

   

        <section>

            <div class="signinpanel">

                <div class="row">

                    

                        <div class="signin-info">
                            <div class="logopanel">
                                
                            </div><!-- logopanel -->

                            <div class="mb20"></div>


                            <div class="mb20"></div>

                        </div><!-- signin0-info -->

                    <!-- col-sm-7 -->

                    <?php echo $this->Flash->render(); ?>

                   
                        <?= $this->fetch('content') ?>
                    <!-- col-sm-5 -->

                </div><!-- row -->

                <div class="signup-footer">
                    <div class="pull-left">
                        <?php echo $Project_Name; ?> &copy; <?php echo date('Y'); ?>. All Rights Reserved.
                    </div>
                    <div class="pull-right">
                        Created By: <a href="http://teqmavens.com/" target="_blank">TeqMavens</a>
                    </div>
                </div>

            </div><!-- signin -->

        </section>


        <?php
         echo $this->Html->Script('jquery-1.10.2.min');
        // echo $this->Html->Script('jquery-migrate-1.2.1.min');
        // echo $this->Html->Script('bootstrap.min');
        // // echo $this->Html->Script('modernizr.min');
        // echo $this->Html->Script('retina.min');
        echo $this->Html->Script('custom');
         echo $this->Html->Script('jquery.validate.min');
        ?>

    </body>
</html>
