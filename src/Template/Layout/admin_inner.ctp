<?php 
    echo $this->element('admin_header'); 
    echo $this->element('sidebar'); 
    echo $this->Html->css('developer.css?v='.date('His'));
    echo $this->Html->script('common.js?'.date('s'));
    echo $this->element('top_navigation'); 
    echo $this->fetch('content');
    echo $this->element('admin_footer'); 
?>