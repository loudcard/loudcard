    <div class="row">
        <div class="col-lg-12">
        <?php if(!empty($tag_list)) { ?>
              <ul class="nav nav-tabs mytabs">
        <?php $active =""; $i=1; foreach ($tag_list as $key => $tag) { 
         
          if($tag['name'] == $click_on){
            $active = "active";
          }else{
            $active = "";
          }

          ?>
           
             
    <!-- <li class="show_cards <?php echo $active;?>" data-id="<?php echo $tag['id'];?>"><?php echo $this->Html->link($tag['name'],'javascript:void(0)');?></li> -->
    <li class="category_listing <?php echo $active;?>" data-id="<?php echo $tag['id'];?>"><a href="<?php echo $SITEURL.$tag['name'];?>"><?php echo $tag['name'];?></a></li>
    <?php $i++; } ?>
  </ul>
  <?php } ?>


  
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active search_result">
    
      <div class="row main_template">
        <?php if(!empty($templates)): $i=1; $j=1; foreach($templates as $templ): ?>
        <div class="col-md-6 col-sm-6 col-xs-12 product-column">
        <div class="product_bg">
        <div class="product_img"><?php echo $this->Html->image("templates/".$templ['templates'], ["alt" => $templ['name'],'class'=>'image view_detail_img_click','id' => $templ['id'],'data-counter'=>$j]);?>
      <div class="middle">
    <button type="button" class="btn btn-default mybutton view_detail" id="<?php echo $templ['id'];?>" data-counter="<?php echo $j;?>">View Details</button>
    <?php echo $this->Html->link("Select Card",['action' => 'upload_photo',$templ['id']],['class' => "btn btn-default mybutton selected"]);?>
        </div> 
  </div>
        <div class="product_text">
        <h1><?php echo $templ['name'];?></h1>
        <!--  <span><?php echo $templ['tag']['name'];?></span> -->
        <h2 class="price">$<?php echo $templ['price'];?></h2>
        </div>
        </div>

            <div class="show_card_detail<?php echo $templ['id'];?>"></div>
        </div>
        <?php if($i%2==0){ echo "</div><div class='row main_template'>"; $j++;}
       $i++; endforeach; else: echo "No template available"; endif; ?>
      
    
      </div>
    </div>
   
  </div>
        </div>
    </div>
   
  </div>
<!--   
  <div class="pagination">

                            <?= $this->Paginator->prev('« Previous') ?>
                             <?= $this->Paginator->first('< first'); ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->last('> last'); ?>
                            <?= $this->Paginator->next('Next »') ?>

                        </div> -->
 



  
  </section>
  
