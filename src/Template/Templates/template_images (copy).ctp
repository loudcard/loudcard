  <div class="modal-header">
          <h4 class="modal-title"><?php echo $this->request->data['name'];?></h4>
        </div>
        <div class="modal-body">
          <div class="row">
          <form action="" method="post" enctype="multipart/form-data" id="user_image_upload">
          <?php if(!empty($templates_images)): $i=1; foreach($templates_images as $images): if($i ==3){$class="templates_images";}else{$class="simple_images";}?>
          <div class="col-sm-4"><?php echo $this->Html->image("template_images/".$images['images'],['class' => $class]);?></div>
          <?php $i++; endforeach; else:?>  <div class="col-sm-4"></div><div class="col-sm-4"><?php echo $this->Html->image("templates/".$this->request->data['image_name'],['class' => $templates_images]);?></div> <?php endif;  ?>
          </div>
          <div class="show_user_img" style="display: none;"><?php echo $this->Html->image("templates/".$this->request->data['image_name'],['class' => "show_image"]);?></div>
          
          <div class="row">
          <div class="col-sm-6"></div>
          <input type="file" class="set_uer_image" style="display:none;">
          <div class="col-sm-6"><button type="submit" class="btn btn-primary pull-right upload_img">Upload Image</button></div>
          </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>