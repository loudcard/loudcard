



<div class='row show_template_data' id="template_detail<?php echo $this->request->data['data_counter'];?>">
  <div class="row">

<div><?php //echo $this->Html->image("arrow.png");?></div>
  <div class="product-detail-expand">
  <div class="cross-icon product-detail-exp">
  <!-- <button type="button" data-counter="<?php echo $this->request->data['data_counter'];?>" class="close close_detail_div " aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
  <a href="<?php echo $SITEURL;?>/templates/upload_photo/<?php echo $templates_images['id'];?>" class="btn btn-lg btn-default lead-btn select_btn">Select </a> 
  </div> <div class="clear"></div>
    <div class="col-sm-6">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
            <?php if(!empty($templates_images['templates_images'])){ $i=1; foreach ($templates_images['templates_images'] as $key => $templ_images) {?>
                
            <?php if($i==1){?>
            <div class="active">

                   <?php echo $this->Html->image("template_images/".$templ_images['images'],['class' => 'show_large_image']);?>
                </div>
                <?php } ?>

               <!--  <div class="item <?php if($i==1){echo "active";}?>">

                   <?php echo $this->Html->image("template_images/".$templ_images['images']);?>
                </div> -->
               
               <?php $i++; } }?>
               
            </div>
        </div> 
    <div class="clearfix">
        <div id="thumbcarousel" class="carousel slide" data-interval="false">
            <div class="carousel-inner">
                <div class="item active">
                 <?php if(!empty($templates_images['templates_images'])){ $j=0; foreach ($templates_images['templates_images'] as $key => $templ_images) { ?>
                    <div data-target="#carousel" data-slide-to="<?php echo $j;?>" class="thumb"><?php echo $this->Html->image("template_images/".$templ_images['images'],['class' => 'thumb_images']);?></div>
                   
                    <?php $j++; } } ?>
                   
                </div><!-- /item -->
                <!-- /item -->
            </div><!-- /carousel-inner -->
           <!--  <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a> -->
        </div> <!-- /thumbcarousel -->
    </div><!-- /clearfix -->
    </div> <!-- /col-sm-6 -->
    <div class="col-sm-6">
    <div class="product-detail-exp">
        <h2><?php if(!empty($templates_images['name'])){echo $templates_images['name'];}?></h2>
        <span>$<?php echo $templates_images['price'];?></span>
         <!-- <h3><?php if(!empty($templates_images['tag']['name'])){echo $templates_images['tag']['name'];}?></h3> -->
        
        <ul class="card-details">
<li class="details-item clearfix">
<!-- <span class="details-label float-left">Description</span> -->
<div class="details-desc"><?php if(!empty($templates_images['cover_text'])){echo $templates_images['cover_text'];} else{echo "N/A";} ?>

<br></div>
</li>
<li class="details-item clearfix">
<span class="details-label float-left">Artist</span>
<div class="details-desc"><?php if(!empty($templates_images['inside_text'])){echo $templates_images['inside_text'];} else{echo "N/A";} ?></div>
</li>
<li class="details-divider"></li>
<li class="details-item clearfix">
<span class="details-label float-left">Card Specs</span>
<div class="details-desc">
<ul class="simple-list">
<?php if(!empty($templates_images['card_details'])){echo $templates_images['card_details'];} else{echo "N/A";} ?>
</ul>
</div>
</li>
</ul>

<a href="<?php echo $SITEURL;?>/templates/upload_photo/<?php echo $templates_images['id'];?>" class="btn btn-lg btn-default lead-btn select_btn">Select </a>

    </div>
    </div> <!-- /col-sm-6 -->
    </div>
  </div>
</div>
