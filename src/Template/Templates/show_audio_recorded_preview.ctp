 <?php $user_audio   = 'img/user_audio/'.$this->request->session()->read('GuestUser.audio_name'); ?>
 <div class="row">
                 <div class="playbtn-left">
                    <button id="pButton_preview" class="play"></button>
                  </div>
                  <div class="playbtn-right">
                   
                    <audio id="music_preview" preload="auto" src="<?php echo $SITEURL.$user_audio;?>" controls="" style="display: none;"></audio>
                    <div id="audioplayer">
                      <div id="timeline_preview">
                        <div id="playhead_preview"></div>
                      </div>
                      <span class="play_pause_btn_preview">Play</span>
                    </div>
                     </div>  <div class="clear"></div> 
                </div>
                


  <script type="text/javascript">
    var music_preview = document.getElementById('music_preview'); // id for audio element
var duration_preview = music_preview.duration; // Duration of audio clip, calculated here for embedding purposes
music_preview.addEventListener('durationchange', function(e) {
    // alert(e.target.duration); //FIRST 0, THEN REAL DURATION
    duration_preview = e.target.duration;
});
var pButton_preview = document.getElementById('pButton_preview'); // play button

var playhead_preview = document.getElementById('playhead_preview'); // playhead
var timeline_preview = document.getElementById('timeline_preview'); // timeline
var timelineWidth_preview = timeline_preview.offsetWidth - playhead_preview.offsetWidth;
$(document).on("click","#pButton_preview",function(){
 
  play_preview(music_preview,pButton_preview);

  music_preview.addEventListener("timeupdate", timeUpdate_preview, false);

  timeline_preview.addEventListener("click", function(event) {
    moveplayhead_preview(event);
    music_preview.currentTime = duration_preview * clickPercent_preview(event);
  }, false);
        
      // } else if(d=="pause"){
      //   alert(2);
      //  music_audio.pause();
      //   // remove pause, add play
      //   pButton_audio.className = "";
      //   pButton_audio.className = "play";
      // }
   
// alert(d);
  //     if(d=="play"){
        // alert(1);
        e.stopPropagation();
        e.preventDefault();
        return false;
});

music_preview.addEventListener("canplaythrough", function() {
    duration_preview = music_preview.duration;
}, false);

// setTimeout(function(){
//  $(document).find("#pButton_audio").trigger("click");
  
// },150);
function clickPercent_preview(event) {
    return (event.clientX - getPosition(timeline_preview)) / timelineWidth_preview;
}
function moveplayhead_preview(event) {
    var newMargLeft = event.clientX - getPosition(timeline_preview);

    if (newMargLeft >= 0 && newMargLeft <= timelineWidth_preview) {
        playhead_preview.style.marginLeft = newMargLeft + "px";
    }
    if (newMargLeft < 0) {
        playhead_preview.style.marginLeft = "0px";
    }
    if (newMargLeft > timelineWidth_preview) {
        playhead_preview.style.marginLeft = timelineWidth_preview + "px";
    }
}
function timeUpdate_preview() {
    var playPercent = timelineWidth_preview * (music_preview.currentTime / duration_preview);
    // alert(playPercent);
    playhead_preview.style.marginLeft = playPercent + "px";
    if (music_preview.currentTime == duration_preview) {
        pButton_preview.className = "";
        pButton_preview.className = "play";
        $(document).find(".play_pause_btn_preview").html("Play");
     
    }
}
function play_preview(music,pButton) {
     // start music
    // alert(pButton_preview.className);
    if(pButton_preview.className=="play"){
    
    if (music_preview.paused) {
        music_preview.play();
        // remove play, add pause
        pButton_preview.className = "";
        pButton_preview.className = "pause";
        $(document).find(".play_pause_btn_preview").html("Pause");
    
    } 

return false;

  }


    if(pButton_preview.className=="pause"){// pause music
     
        music_preview.pause();
        // remove pause, add play
        pButton_preview.className = "";
        pButton_preview.className = "play";
        $(document).find(".play_pause_btn_preview").html("Play");
    return false;
    }

    //  else { // pause music
    //     music_audio.pause();
    //     // remove pause, add play
    //     pButton_audio.className = "";
    //     pButton_audio.className = "play";
    
    // }
    
}

function getPosition(el) {
    return el.getBoundingClientRect().left;
}


  </script>          