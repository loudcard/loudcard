 <?php echo $this->Html->css('font-awesome.min'); ?>
<link href="https://fonts.googleapis.com/css?family=Quicksand:400,700" rel="stylesheet">
 <div class="container-fluid voice2">
    <div class="row">
      <div class="col-md-7 col-sm-12 col-xs-12 right-sps">
        <div class="newsltr">
          <div class="home-newsletter">
            <h4> Sample Voice Messages to Help You Come up with Your Very Own Message </h4>
            <div class="voice-record">
              <div class="voice-post">
                <div class="row">
                  <div class="col-md-1">
                    <button id="pButton" class="play"></button>
                  </div>
                  <div class="col-md-11">
                    <p>Happy Fathers Day dad! We sure do miss you. Josh keeps on talking about the fun you two had at zoo. Please give all of our love from Boston to you and mom!

 </p>
                    <audio id="music" preload="true">
                      <source src="<?php echo $SITEURL;?>img/audio/sample_recording_N1.wav">
                    </audio>
                    <div id="audioplayer">
                      <div id="timeline">
                        <div id="playhead"></div>
                      </div>
                    </div>
                    <span class="posted-name">Scott whishing Happy Fathers Day to his Dad.</span> </div>
                </div>
              </div>
              <div class="voice-post">
                <div class="row">
                  <div class="col-md-1"><button id="pButton_two" class="play"></button></div>
                  <div class="col-md-11">
                    <p>Hi Mom and Dad, We wish you a Merry Christmas. We wish you a Merry Christmas. We wish you a Merry Christmas. And a happy New Year. We love you and can't wait to see you next Summer.

</p>
                    <audio id="music_two" preload="true">
                      <source src="<?php echo $SITEURL;?>img/audio/sample_recording_N2.wav">
                    </audio>
                    <div id="audioplayer">
                      <div id="timeline_two">
                        <div id="playhead_two"></div>
                      </div>
                    </div>
                    <span class="posted-name">Alexa's voice message to her parents from her husband, her daughter and herself.</span> </div>
                </div>
              </div>
              <div class="voice-post">
                <div class="row">
                  <div class="col-md-1"><button id="pButton_three" class="play"></button></div>
                  <div class="col-md-11">
                    <p>Ten little fingers and ten little toes. Oh my how our family does grow. We are thrilled to announce the arrival of Samantha Elle Thompson, our daughter, on May 2nd.

  </p>
                    <audio id="music_three" preload="true">
                      <source src="<?php echo $SITEURL;?>img/audio/sample_recording_N3.wav">
                    </audio>
                    <div id="audioplayer">
                      <div id="timeline_three">
                        <div id="playhead_three"></div>
                      </div>
                    </div>
                    <span class="posted-name">Jessica announcing the birth of her baby to multiple recipients.</span> </div>
                </div>
              </div>
            </div>
            <a class="btn btn-lg btn-default lead-btn scrollto_homedesign" href="javaScript:void(0)">Get Started</a> </div>
        </div>
      </div>
    </div>
  </div>

  <script>
var music = document.getElementById('music'); // id for audio element
var music_two = document.getElementById('music_two');
var music_three = document.getElementById('music_three');
var duration = music.duration; // Duration of audio clip, calculated here for embedding purposes
var duration_two = music_two.duration;
var duration_three = music_three.duration;

var pButton = document.getElementById('pButton'); // play button
var pButton_two = document.getElementById('pButton_two');
var pButton_three = document.getElementById('pButton_three');
var playhead = document.getElementById('playhead'); // playhead
var playhead_two = document.getElementById('playhead_two');
var playhead_three = document.getElementById('playhead_three');
var timeline = document.getElementById('timeline'); // timeline
var timeline_two = document.getElementById('timeline_two');
var timeline_three = document.getElementById('timeline_three');

// timeline width adjusted for playhead
var timelineWidth = timeline.offsetWidth - playhead.offsetWidth;
var timelineWidth_two = timeline_two.offsetWidth - playhead_two.offsetWidth;
var timelineWidth_three = timeline_three.offsetWidth - playhead_three.offsetWidth;

$(document).on("click","#pButton",function(){
// play button event listenter
play(music,pButton);
music_two.pause();       
pButton_two.className = "";
pButton_two.className = "play";  
music_three.pause();       
pButton_three.className = "";
pButton_three.className = "play";   
       
    
   
// pButton_two.addEventListener("click", play);
// pButton_three.addEventListener("click", play);

// timeupdate event listener
music.addEventListener("timeupdate", timeUpdate, false);
// makes timeline clickable
timeline.addEventListener("click", function(event) {
    moveplayhead(event);
    music.currentTime = duration * clickPercent(event);
}, false);

});

$(document).on("click","#pButton_two",function(){
// play button event listenter
play(music_two,pButton_two);
music.pause();       
pButton.className = "";
pButton.className = "play";  
music_three.pause();       
pButton_three.className = "";
pButton_three.className = "play"; 
// pButton_two.addEventListener("click", play);
// pButton_three.addEventListener("click", play);

// timeupdate event listener
music_two.addEventListener("timeupdate", timeUpdate_two, false);
// alert(music_two.duration);
// makes timeline clickable
timeline_two.addEventListener("click", function(event) {
    moveplayhead_two(event);
    music_two.currentTime = duration_two * clickPercent_two(event);
}, false);

});

$(document).on("click","#pButton_three",function(){
// play button event listenter
play(music_three,pButton_three);
music.pause();       
pButton.className = "";
pButton.className = "play"; 
music_two.pause();       
pButton_two.className = "";
pButton_two.className = "play";  

// pButton_two.addEventListener("click", play);
// pButton_three.addEventListener("click", play);

// timeupdate event listener
music_three.addEventListener("timeupdate", timeUpdate_three, false);
// alert(music_two.duration);
// makes timeline clickable
timeline_three.addEventListener("click", function(event) {
    moveplayhead_three(event);
    music_three.currentTime = duration_three * clickPercent_three(event);
}, false);

});
// returns click as decimal (.77) of the total timelineWidth
function clickPercent(event) {
    return (event.clientX - getPosition(timeline)) / timelineWidth;
}

function clickPercent_two(event) {
    return (event.clientX - getPosition(timeline_two)) / timelineWidth_two;
}
function clickPercent_three(event) {
    return (event.clientX - getPosition(timeline_three)) / timelineWidth_three;
}


// mousemove EventListener
// Moves playhead as user drags
function moveplayhead(event) {
    var newMargLeft = event.clientX - getPosition(timeline);

    if (newMargLeft >= 0 && newMargLeft <= timelineWidth) {
        playhead.style.marginLeft = newMargLeft + "px";
    }
    if (newMargLeft < 0) {
        playhead.style.marginLeft = "0px";
    }
    if (newMargLeft > timelineWidth) {
        playhead.style.marginLeft = timelineWidth + "px";
    }
}

function moveplayhead_two(event) {
    var newMargLeft = event.clientX - getPosition(timeline_two);

    if (newMargLeft >= 0 && newMargLeft <= timelineWidth_two) {
        playhead_two.style.marginLeft = newMargLeft + "px";
    }
    if (newMargLeft < 0) {
        playhead_two.style.marginLeft = "0px";
    }
    if (newMargLeft > timelineWidth) {
        playhead_two.style.marginLeft = timelineWidth_two + "px";
    }
}

function moveplayhead_three(event) {
    var newMargLeft = event.clientX - getPosition(timeline_three);

    if (newMargLeft >= 0 && newMargLeft <= timelineWidth_three) {
        playhead_three.style.marginLeft = newMargLeft + "px";
    }
    if (newMargLeft < 0) {
        playhead_three.style.marginLeft = "0px";
    }
    if (newMargLeft > timelineWidth_three) {
        playhead_three.style.marginLeft = timelineWidth_three + "px";
    }
}

// timeUpdate
// Synchronizes playhead position with current point in audio
function timeUpdate() {
    var playPercent = timelineWidth * (music.currentTime / duration);
    playhead.style.marginLeft = playPercent + "px";
    if (music.currentTime == duration) {
        pButton.className = "";
        pButton.className = "play";
     
    }
}

// timeUpdate
// Synchronizes playhead position with current point in audio
function timeUpdate_two() {
    var playPercent = timelineWidth_two * (music_two.currentTime / duration_two);
    playhead_two.style.marginLeft = playPercent + "px";
    if (music_two.currentTime == duration_two) {
        pButton_two.className = "";
        pButton_two.className = "play";
     
    }
}

function timeUpdate_three() {
    var playPercent = timelineWidth_three * (music_three.currentTime / duration_three);
    playhead_three.style.marginLeft = playPercent + "px";
    if (music_three.currentTime == duration_three) {
        pButton_three.className = "";
        pButton_three.className = "play";
     
    }
}

//Play and Pause
function play(music,pButton) {
    // start music
    if (music.paused) {
        music.play();
        // remove play, add pause
        pButton.className = "";
        pButton.className = "pause";
    
    } else { // pause music
        music.pause();
        // remove pause, add play
        pButton.className = "";
        pButton.className = "play";
    
    }
}

// Gets audio file duration
music.addEventListener("canplaythrough", function() {
    duration = music.duration;
}, false);
// Gets audio file duration
music_two.addEventListener("canplaythrough", function() {
    duration_two = music_two.duration;
}, false);
music_three.addEventListener("canplaythrough", function() {
    duration_three = music_three.duration;
}, false);

// getPosition
// Returns elements left position relative to top-left of viewport
function getPosition(el) {
    return el.getBoundingClientRect().left;
}

</script>
