                <?php if(!empty($templates)): foreach($templates as $templ): ?>
                    <div class="col-md-4">
                        <div class="box-simple show_rotating_thumbnail" id="<?php echo $templ['id'];?>">
                            <div>
                                <?php echo $this->Html->image("templates/".$templ['templates']);?>
                            </div>
                            <h3><?php echo ucfirst($templ['name']);?></h3>
                            <p><strong>$ <?php echo number_format($templ['price'],2);?></strong></p>
                        </div>
                    </div>
                <?php endforeach; else: echo "<center><h3>No template available</h3></center>"; endif;?>
                   
    