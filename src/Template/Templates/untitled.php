<section class="bar background-white">
    <div class="container">
        <div class="col-md-12">


            <div class="row search_result">
                <?php if(!empty($templates)): foreach($templates as $templ): ?>
                    <div class="col-md-4">
                        <div class="box-simple show_rotating_thumbnail" id="<?php echo $templ['id'];?>" data-name="<?php echo ucfirst($templ['name']);?>" data-image_name="<?php echo $templ['templates'];?>">
                            <div>
                                <?php echo $this->Html->link($this->Html->image("templates/".$templ['templates'], ["alt" => $templ['name'],'class'=>'temp_img']),'javascript:void(0)', ['escape' => false]);?>
                            </div>
                            <h3><?php echo ucfirst($templ['name']);?></h3>
                            <p><strong>$ <?php echo number_format($templ['price'],2);?></strong></p>
                        </div>
                    </div>
                <?php endforeach; else: echo "No template available"; endif;?>
                
            </div>

            
        </div>
        <div class="template-images-content">
         
        </div>
    </div>
</section>