-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 19, 2017 at 05:02 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `loudcard`
--

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1:active,0:inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `status`) VALUES
(1, 'Christmas', 1),
(2, 'Birthday', 1),
(3, 'New Year', 1);

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `templates` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `user_upload` varchar(255) DEFAULT NULL,
  `audio_message` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '0:deleted,1:active',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`id`, `tag_id`, `name`, `templates`, `price`, `user_upload`, `audio_message`, `status`, `created`) VALUES
(1, 1, 'Birthday 1', 'birthday1.jpg', 10.00, NULL, '', 1, '2017-08-30 06:00:01'),
(2, 1, 'Birthday 2', 'birthday2.jpg', 12.00, NULL, '', 1, '2017-08-30 06:00:01'),
(3, 2, 'Birthday 3', 'birthday3.jpg', 15.00, NULL, '', 1, '2017-08-30 06:00:01'),
(4, 2, 'Mom 1', 'mom1.jpg', 25.00, '', '', 1, '2017-08-30 06:00:01'),
(5, 3, 'Mom 2', 'mom2.jpg', 20.00, '59bce8e65b0ed150555261442622635-Closeup-portrait-of-young-couple-in-green-park-Girl-looks-at-the-man-with-eyes-full-of-love-and-smil-Stock-Photo.jpg', '7756156469697318.wav', 1, '2017-08-30 06:00:01'),
(6, 3, 'Veletinecard 1', 'veletinecard1.jpg', 30.00, '59bbaf460582715054723263.jpg', '', 1, '2017-08-30 06:00:01'),
(7, 1, 'Christmas1', '4.jpeg', 10.00, NULL, 'Christmas1', 1, '2017-09-16 08:12:16'),
(8, 1, 'Christmas2', '5.jpeg', 20.00, '59bcf2adb3fc2150555511742622635-Closeup-portrait-of-young-couple-in-green-park-Girl-looks-at-the-man-with-eyes-full-of-love-and-smil-Stock-Photo.jpg', '', 1, '2017-09-16 08:12:16'),
(9, 1, 'Christmas3', '6.jpeg', 30.00, '59bf98b189b121505728689Roelia.jpg', '3669658551876601.wav', 1, '2017-09-16 08:12:16');

-- --------------------------------------------------------

--
-- Table structure for table `templates_images`
--

CREATE TABLE IF NOT EXISTS `templates_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `images` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `show_preview` int(11) NOT NULL DEFAULT '0' COMMENT '1:Yes,0:No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `templates_images`
--

INSERT INTO `templates_images` (`id`, `template_id`, `images`, `status`, `show_preview`) VALUES
(1, 1, '4.jpg', 1, 0),
(2, 1, '5.jpg', 1, 0),
(3, 1, '7.jpg', 1, 1),
(4, 2, '7.jpg', 1, 1),
(5, 2, '5.jpg', 1, 0),
(6, 2, '6.jpg', 1, 0),
(7, 3, '4.jpg', 1, 0),
(8, 3, '7.jpg', 1, 1),
(9, 3, '6.jpg', 1, 0),
(10, 4, '7.jpg', 1, 1),
(11, 4, '5.jpg', 1, 0),
(12, 4, '6.jpg', 1, 0),
(13, 5, '4.jpg', 1, 0),
(14, 5, '7.jpg', 1, 1),
(15, 5, '6.jpg', 1, 0),
(16, 6, '4.jpg', 1, 0),
(17, 6, '5.jpg', 1, 0),
(18, 6, '7.jpg', 1, 1),
(19, 7, '4.jpg', 1, 0),
(20, 7, '6.jpg', 1, 0),
(21, 7, '7.jpg', 1, 1),
(22, 8, '6.jpg', 1, 0),
(23, 8, '4.jpg', 1, 0),
(24, 8, '7.jpg', 1, 1),
(25, 9, '7.jpg', 1, 1),
(26, 9, '6.jpg', 1, 0),
(27, 9, '4.jpg', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `templates_images_old`
--

CREATE TABLE IF NOT EXISTS `templates_images_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `images` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `templates_images_old`
--

INSERT INTO `templates_images_old` (`id`, `template_id`, `images`, `status`) VALUES
(1, 1, 'image.jpg', 1),
(2, 6, 'image(1).jpg', 1),
(3, 2, 'image(2).jpg', 1),
(4, 5, 'image(3).jpg', 1),
(5, 5, 'image(4).jpg', 1),
(6, 5, 'image(5).jpg', 1),
(7, 4, 'image(6).jpg', 1),
(8, 4, 'image(7).jpg', 1),
(9, 4, 'image(8).jpg', 1),
(10, 3, 'image(9).jpg', 1),
(11, 3, 'image(10).jpg', 1),
(12, 3, 'image(11).jpg', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
