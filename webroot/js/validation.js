//var path = 'http://localhost/momstars/';
jQuery(document).ready(function () {
     $.validator.addMethod(
            "lettersOnly",
            function (value, element) {
                return value.match(/^[A-Za-z ]+$/);
            },
            "Enter only letters"
            );

    $.validator.addMethod(
            "validateEmail",
            function (value, element) {
                return value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
            },
            "Invalid Email"
            );

    $.validator.addMethod(
            "validate_webpath",
            function (value, element) {
              //  return value.match(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/);
                return value.match(/(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/);
            },
            "Invalid website url"
            );
    
    $.validator.addMethod(
            "number_andDecimal",
            function (value, element) {
                return value.match(/^[0-9]\d*(\.\d+)?$/);
            },
            "Enter valid price"
            );
    

    $.validator.addMethod(
            "zip_code",
            function (value, element) {
                return value.match(/^\d{5}(?:[-\s]\d{4})?$/);
            },
            "Enter valid zipcode"
            );



     $.validator.addMethod(
            "number_andDecimal_for_discount",
            function (value, element) {
                return value.match(/^[1-9]\d*(\.\d+)?$/);
            },
            "Enter valid discount value"
            );
    
    /******Add User form validation**********/
    jQuery("#normal_user_form").validate({
        // Specify the validation rules
        rules: {
	    "first_name": {
                required: true,
                lettersOnly: true,
     
            },
            "last_name": {
                required: true,
                lettersOnly: true,
           
            },
            "email": {
                required: true,
                validateEmail: true,
               remote: path+"Users/checkemailuniqueness"

            },  
             "password": {
                required: true,
            
            },        
        },
        // Specify the validation error messages
        messages: {
            "first_name": {
                required: "Please enter first name"
            },
            "last_name": {
                required: "Please enter last name"

            },
            "email": {
                required: "Please enter email id",
               remote: "Email Id already exists"
               
            },  
             "password": {
                required: "Please enter password",
            
            },         
        }
    });

     /******Add User form validation**********/
    jQuery("#guest_user_form").validate({
        // Specify the validation rules
        rules: {
        "first_name": {
                required: true,
                lettersOnly: true,
     
            },
            "last_name": {
                required: true,
                lettersOnly: true,
           
            },
            "email": {
                required: true,
                validateEmail: true,
                //remote: path+"Users/checkemailuniqueness"

            },  
                  
        },
        // Specify the validation error messages
        messages: {
            "first_name": {
                required: "Please enter first name"
            },
            "last_name": {
                required: "Please enter last name"

            },
            "email": {
                required: "Please enter email id",
               // remote: "Email Id already exists"
               
            },  
                   
        }
    });
    
    /******Shipping Address to me**********/
    /******Add User form validation**********/
    jQuery("#card_to_me_form").validate({
        // Specify the validation rules
        rules: {
        "first_name": {
                required: true,
                // lettersOnly: true,
     
            },
            "last_name": {
                required: true,
                // lettersOnly: true,
           
            },
            "address_one": {
                required: true,               
                //remote: path+"Users/checkemailuniqueness"

            },           

            "city": {
                required: true, 
                lettersOnly: true,              
                //remote: path+"Users/checkemailuniqueness"

            }, 
            "state_id": {
                required: true, 
                          
                //remote: path+"Users/checkemailuniqueness"

            },  

            "zip": {
                required: true, 
                // zip_code: true,              
                //remote: path+"Users/checkemailuniqueness"

            },  

           
                  
        },
        // Specify the validation error messages
        messages: {
            "first_name": {
                required: "Please enter first name"
            },
            "last_name": {
                required: "Please enter last name"

            },
            "address_one": {
                required: "Please enter address",
               // remote: "Email Id already exists"
               
            },
            
            "city": {
                required: "Please enter city",
               // remote: "Email Id already exists"
               
            }, 
            "state_id": {
                required: "Please select state", 
                          
                //remote: path+"Users/checkemailuniqueness"

            },   

            "zip": {
                required: "Enter valid zipcode",
               // remote: "Email Id already exists"
               
            },  
                   
        }
    });
    
    
    /******Shipping address to receipent**********/
    $('#card_to_receipent_form').validate({
       
    });

    $('.first_name').each(function() {
        $(this).rules('add', {
            required: true,
           // lettersOnly: true,            
            messages: {
                required: "Please enter first name",
                
            }
        });
    });

    $('.last_name').each(function() {
        $(this).rules('add', {
            required: true,
           // lettersOnly: true,            
            messages: {
                required: "Please enter last name",
                
            }
        });
    });

    $('.address_one').each(function() {
        $(this).rules('add', {
            required: true,                   
            messages: {
                required: "Please enter address",
                
            }
        });
    });

    $('.city').each(function() {
        $(this).rules('add', {
            required: true,                   
            messages: {
                required: "Please enter city",
                
            }
        });
    });

    $('.state_id').each(function() {
        $(this).rules('add', {
            required: true,                   
            messages: {
                required: "Please select state",
                
            }
        });
    });

     $('.zip').each(function() {
        $(this).rules('add', {
            required: true, 
            // zip_code: true,                      
            messages: {
                required: "Enter valid zipcode",
                
            }
        });
    });
    
    
    
   /******Add User form validation**********/
    jQuery("#normal_user_sign_in_form").validate({
        // Specify the validation rules
        rules: {
        
            "email": {
                required: true,
                validateEmail: true,              

            },  
             "password": {
                required: true,
            
            },        
        },
        // Specify the validation error messages
        messages: {
           
            "email": {
                required: "Please enter email id",           
               
            },  
             "password": {
                required: "Please enter password",
            
            },         
        }
    });
    
    
     /******Add User form validation**********/
    jQuery("#forgot_password_user").validate({
        // Specify the validation rules
        rules: {
        
            "email": {
                required: true,
                validateEmail: true,              

            },  
                  
        },
        // Specify the validation error messages
        messages: {
           
            "email": {
                required: "Please enter email id",           
               
            },  
                  
        }
    });
    
    
    /******Contact form validation**********/
    jQuery("#contact").validate({
        // Specify the validation rules
        rules: {
        "name": {
                required: true,
                lettersOnly: true,
            },
            "email": {
                required: true,
                validateEmail: true,
            },
             "subject": {
                required: true,
         
            },
            "message": {
                required: true,
            }
        },
        // Specify the validation error messages
        messages: {
            "name": {
                required: "Please enter name"
            },
            "email": {
                required: "Please enter email"

            },
            "subject": {
                required: "Please enter subject"

            },
            "message": {
                required: "Please enter message"
            }         
        }
    });
    
    
    /******Add/Edit deal form validation**********/
    jQuery("#add_edit_deal").validate({
        // Specify the validation rules
        rules: {
	    "title": {
                required: true,
                lettersOnly: true,
            },
            "expiry_date": {
                required: true,
                
            },
            "description": {
                required: true,
                
            },
         
        },
        // Specify the validation error messages
        messages: {
            "title": {
                required: "Please enter deal title"
            },
             "expiry_date": {
                required: "Please enter deal expiry date"
            },
             "description": {
                required: "Please enter deal description"
            },
           
        }
    });
    
    /******Add/Edit deal form validation**********/
    jQuery("#edit_deal_card").validate({
        // Specify the validation rules
        rules: {
	    "title": {
                required: true,
               // lettersOnly: true,
            },
            "expiry_date": {
                required: true,
                
            },
            "description": {
                required: true,
                
            },
            "price": {
                required: true,
                number_andDecimal:true
               
            },
            
	   
        },
        // Specify the validation error messages
        messages: {
            "title": {
                required: "Please enter deal card title"
            },
             "expiry_date": {
                required: "Please enter deal card expiry date"
            },
             "description": {
                required: "Please enter deal card description"
            },
            "price": {
                required: "Please enter deal card price"
            }
	      
        }
    });


	 /******Add Edit category form validation**********/
    jQuery("#category_discount").validate({
        // Specify the validation rules
        rules: {
	    "total_category": {
                required: true,
               
            },
	   "discount_given": {
                required: true,
                number_andDecimal_for_discount:true
                
            }
        },
        // Specify the validation error messages
        messages: {
            "total_category": {
                required: "Please select total number of category"
            },
	    "discount_given": {
                required: "Please enter discount given to total category",
                
            }       
        }
    });

	/*
     * validate email to basic and business vendors/users
     */
    /******Add category form validation**********/
   jQuery("#mailto_business").validate({
        // Specify the validation rules
        rules: {
	    "subject": {
                required: true,
               
            },
	   "message": {
                required: true,
                
            }
        },
        // Specify the validation error messages
        messages: {
            "subject": {
                required: "Please enter subject"
            },
	    "message": {
                required: "Please enter message",
                
            }       
        }
    });
    
   jQuery("#invitation").validate({
        // Specify the validation rules
        rules: {
	    
            "username": {
                required: true,
                validateEmail: true,
                remote: path+"admin/Users/checkemailuniqueness"

            }          
        },
        // Specify the validation error messages
        messages: {
           
            "username": {
                required: "Please enter email id",
                remote: "Email Id already exists"
               
            }           
        }
    });

});


$(document).on("keyup",".web_url",function(){
    var val = $(this).val();
    if(val!="" && !val.match(/(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/)){
    $(this).removeClass("website");
    $("#add_vendor_form,#edit_vendor_form").validate().element(this);
}   
    else{
      $(this).addClass("website");
      $("#website-error").html('');
  }
    
});
