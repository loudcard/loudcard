//var path = 'http://localhost/momstars/';
jQuery(document).ready(function () {

// HIde sucess error mesage after 3 second
// $(".success_message,.error_message").delay(3000).hide("slow");


     $.validator.addMethod(
            "lettersOnly",
            function (value, element) {
                return value.match(/^[A-Za-z ]+$/);
            },
            "Enter only letters"
            );

    $.validator.addMethod(
            "validateEmail",
            function (value, element) {
                return value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
            },
            "Invalid Email"
            );
    $.validator.addMethod(
            "validatePhone",
            function (value, element) {
                return value.match(/^[+-]?\d+$/);
            },
            "Invalid Phone Number"
            );

     $.validator.addMethod(
            "number_andDecimal",
            function (value, element) {
                return value.match(/^[0-9]\d*(\.\d+)?$/);
            },
            "Enter valid price"
            );




     jQuery("#add_template").validate({
        // Specify the validation rules
        rules: {
           "code": {
                required: true,
                remote: path+"admin/Templates/checkcodeuniqueness"
                // lettersOnly: true,
     
            },  
        "name": {
                required: true,
                // lettersOnly: true,
     
            },
            "templates": {
                required: true,
                
           
            },
            "price": {
                required: true, 
                number_andDecimal: true,              
                //remote: path+"Users/checkemailuniqueness"

            },     
     
        },
        // Specify the validation error messages
        messages: {
            "code": {
                required: "Please enter template unique code",
                remote: "Template code already exists"
                // lettersOnly: true,
     
            }, 
            "name": {
                required: "Please enter template name"
            },
            "templates": {
                required: "Please upload template image"

            },
            "price": {
                required: "Please enter template Price",
               // remote: "Email Id already exists"
               
            },
                      
                   
        }
    });


     jQuery("#edit_template").validate({
        // Specify the validation rules
        rules: {
          "code": {
                required: true,
                remote: {
                    url: path+"admin/Templates/checkcodeuniqueness_edit", async: false,
                    type: "POST",
                    data: {id: $('#template_ids').val()}
                }
                // lettersOnly: true,
     
            },  
        "name": {
                required: true,
                // lettersOnly: true,
     
            },
           
            "price": {
                required: true, 
                number_andDecimal: true,              
                //remote: path+"Users/checkemailuniqueness"

            },     
     
        },
        // Specify the validation error messages
        messages: {
             "code": {
                required: "Please enter template unique code",
                remote: "Template code already exists"
                // lettersOnly: true,
     
            }, 
            "name": {
                required: "Please enter template name"
            },
           
            "price": {
                required: "Please enter template Price",
               // remote: "Email Id already exists"
               
            },
                      
                   
        }
    });


     jQuery("#edit_promotion").validate({
        // Specify the validation rules
        rules: {
        "text": {
                required: true,
                
     
            },
           
            "code": {
                required: true, 
                             
                //remote: path+"Users/checkemailuniqueness"

            }, 

            "start_date": {
                required: true, 
                             
                //remote: path+"Users/checkemailuniqueness"

            }, 
            "end_date": {
                required: true, 
                             
                //remote: path+"Users/checkemailuniqueness"

            },     
     
        },
        // Specify the validation error messages
        messages: {
            "text": {
                required: "Please enter promotion code short description"
            },
           
            "code": {
                required: "Please enter promotion code",
               // remote: "Email Id already exists"
               
            },

             "start_date": {
                required: "Please enter promotion code start date ",
               // remote: "Email Id already exists"
               
            },

             "end_date": {
                required: "Please enter promotion code end date",
               // remote: "Email Id already exists"
               
            },
                      
                   
        }
    });


     jQuery("#add_promotion").validate({
        // Specify the validation rules
        rules: {
        "text": {
                required: true,
                
     
            },
           
            "code": {
                required: true, 
                             
                //remote: path+"Users/checkemailuniqueness"

            }, 

            "start_date": {
                required: true, 
                             
                //remote: path+"Users/checkemailuniqueness"

            }, 
            "end_date": {
                required: true, 
                             
                //remote: path+"Users/checkemailuniqueness"

            },     
     
        },
        // Specify the validation error messages
        messages: {
            "text": {
                required: "Please enter promotion code short description"
            },
           
            "code": {
                required: "Please enter promotion code",
               // remote: "Email Id already exists"
               
            },

             "start_date": {
                required: "Please enter promotion code start date ",
               // remote: "Email Id already exists"
               
            },

             "end_date": {
                required: "Please enter promotion code end date",
               // remote: "Email Id already exists"
               
            },
                      
                   
        }
    });


    
    /******Add User form validation**********/
    jQuery("#add_user").validate({
        // Specify the validation rules
        rules: {
	    "first_name": {
                required: true,
                lettersOnly: true,
     
            },
            "last_name": {
                required: true,
                lettersOnly: true,
           
            },
            "email": {
                required: true,
                validateEmail: true,
                remote:{
                url: path+"admin/Users/checkemailuniqueness", async: false,
                    type: "POST",
                    data: {id: $('#user_id').val()}
                }
            },  
                   
        },
        // Specify the validation error messages
        messages: {
            "first_name": {
                required: "Please enter first name"
            },
            "last_name": {
                required: "Please enter last name"

            },
            "email": {
                required: "Please enter user email address",
                remote: "Email Id already exists"
               
            }, 
                   
        }
    });


	$('#updatePassword').validate({
        ignore: [],
        rules: {
            old_password: {
                required : true,
            },
            password: {
                required : true,
            },
            confirm_password: {
                required : true,
                equalTo: "#password",
            }
        },
        messages: {
            old_password: {
                required: "Please enter current password",
            },
            password: {
                required: "Please enter new password",
            },
            confirm_password: {
                equalTo : "Confirm password not match", 
                required: "Please enter Confirm password",
            }
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        }
    });

});
    
    
   
    
    
    
    
    


