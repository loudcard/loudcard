/*
 * search templates
 */
 $(document).on("submit","#search_template", function (e) {
    e.preventDefault();
    var val = $(".search_template").val();
    if(val==""){
        swal("Fail!", "Please enter template name to search", "error");
    }
    else{
        $(document).find('.overlay').show();
        $.ajax({
        cache: false,  
        url: path + "templates/search_template", //this is the submit URL
        type: 'POST', //or POST
        data: new FormData(this),
        contentType: false,
        processData: false,
        success: function (data) {
            setTimeout(function () {
                $(document).find('.overlay').hide();
                $('.search_result').html(data);
            }, 1000);
            
        }
    });
    }
});


/*
 * show rotating thumbnail in modal
 */
 $(document).on("click",".show_rotating_thumbnail",function(){
    var id= $(this).attr("id");
    var name = $(this).data("name");    
    var image_name = $(this).data("image_name");      
    $.ajax({
      cache: false,
        url: path + "templates/template_images", //this is the submit URL
        type: 'POST', //or POST
        data: {id:id,name:name,image_name:image_name},
        success: function (data) {               
               // / $('#template-images').modal('show');
               $('.template-images-content').html(data);
               $(".templates_images").each(function(){
                // alert($(this).attr("data-preview"));
              if($(this).attr("data-preview")==1 && $(this).attr("data-counter")==1){
                 $(".user_thumb_image_second").addClass("user_thumb_image");
                $(".user_thumb_image_second").removeClass("user_thumb_image_second");
                 $(".user_thumb_image").show(); 
              }else if($(this).attr("data-preview")==1 && $(this).attr("data-counter")==2){
                 $(".user_thumb_image").addClass("user_thumb_image_second");
                 $(".user_thumb_image").removeClass("user_thumb_image");
                $(".user_thumb_image_second").show();  
              }

              else{
                $(".user_thumb_image").hide();
                $(".user_thumb_image_second").hide(); 
              }
                });  
               var preview = $(".simple_images").data("preview");
               if(preview==1){
                   $(".show_upload_button").show();
               }
               $('html, body').animate({
                 
                scrollTop: $(".template-images-content").offset().top
            }, 500);
               
           }
       });
});


 /*
 * search templates
 */
 $(document).on("click",".upload_img", function () {

    $(".set_uer_image").trigger('click');
});

/*
 * Upload user image
 */

 // $(document).on("change",".set_uer_image", function () { 
 // alert(1);  
 //    // $("#upload_final_image").show();
 //    readURL(this);
    // var current = this;    
    // var img= $(this).prop("files")[0];
    // var image_id = $(".image_id").val();   
    // // console.log(img.name);
    //     var form_data = new FormData();                  // Creating object of FormData class
    //     form_data.append("set_uer_image", img);
    //     form_data.append("id", image_id)  
    //     $.ajax({
    //     url: path + "templates/upload_img", //this is the submit URL
    //     type: 'POST', //or POST
    //     cache: false,
    //     contentType: false,
    //     processData: false,
    //     data: form_data, 
    //     dataType: 'json',  
    //     success: function (data) {
    //         console.log(data);
    //         if (data.status != "" && data.status == "error") {                               
    //             swal("Error!", data.msg, "error");
    //         }
    //         else{
    //             // $(".show_user_img").attr("src","");
    //             // alert($(".show_user_img").attr('src'));
    //             // // alert(1);
    //             // $(".show_user_img").show();
    //             // // $(".show_user_thumbimg").show();
    //             // $(".show_user_img").attr('src',path + '/img/user_images/' + data.image_name);
    //             // // alert($(".show_user_img").attr('src'));
    //             // wheelzoom(document.querySelector('img.show_user_img')); 
    //             // $(".show_user_thumbimg").attr('src',path + '/img/user_images/' + data.image_name);   
    //         }
            
    //     }
    // });
    // });



 

     $(document).on("change",".set_uer_image", function () { 
        $("#upload_final_image").show();
        readURL(this);
    });
   

 $(document).on("click", ".templates_images", function () {    
    var id= $(this).attr('id');        
    var src = $(this).attr('src');      
    var check_thumbnail_preview = $(this).attr("data-preview");          
    var mega_image_id = $(".simple_images").attr('id');
    var update_thumbnail_update = $(".simple_images").attr('src');
    var check_mega_image_preview = $(".simple_images").attr("data-preview");  
    var update_mega_image=$(".simple_images").attr('src',src);
    var update_mage_image_id = $(".simple_images").attr("id",id); 
    var data_counter = $(this).attr("data-counter");
    var check_user_image = $(".show_user_img").attr("data-img"); 
    $(".simple_images").attr("data-preview",check_thumbnail_preview);
    $(this).attr("data-preview",check_mega_image_preview);      
    $(this).attr('id',mega_image_id);
    $(this).attr('src',update_thumbnail_update);
    $(".image_id").val(id);
    // alert(check_thumbnail_preview);
    if(check_thumbnail_preview==0){
       $(".show_user_img").hide(); 
       $(".show_upload_button").hide();
    }else{
        if(check_user_image !=""){

        $(".show_user_img").show();
        wheelzoom(document.querySelector('img.show_user_img'));
        }else{
            $(".show_user_img").hide();
            
        } 
       $(".show_upload_button").show();
    }

    
    
});

 /*
 Show cards by tags
 */
 $(document).on("click",".show_cards",function(){  
    var id = $(this).attr("data-id");
    $(this).siblings().removeClass("active");
    $(this).addClass("active");


      $.ajax({
        cache: false,
        url: path + "templates/show_cards", //this is the submit URL
        type: 'POST', //or POST
        data: {id:id},
        success: function (data) {               
               $('.search_result').html(data);
             
               $('html, body').animate({
                 
                scrollTop: $(".product-listing").offset().top
            }, 1000);
               
           }
       });

 });

 $(document).on("click",".view_detail",function(){  
    var id = $(this).attr("id");
   var data_counter = $(this).attr("data-counter");
   var insertdata = $(this).parent().parent().parent().parent().parent().find(".product-column:last");
   $(".show_template_data").detach();

      $.ajax({
        cache: false,
        url: path + "templates/view_detail", //this is the submit URL
        type: 'POST', //or POST
        data: {id:id,data_counter:data_counter},
        success: function (data) { 

            fbq('track', 'ViewContent', {
              currency: 'USD',
              content_ids: id,
              content_type: 'Greeting Card',
            });

          $(data).insertAfter($(insertdata));
          $('html, body').animate({
                 
                scrollTop: $(".product-detail-expand").offset().top-120
            }, 1000);
         
        // $('.show_template_data'+data_counter+'').slideDown(2000);    
               // $('.show_template_data'+data_counter+'').html(data);
             
            //    $('html, body').animate({
                 
            //     scrollTop: $('.show_card_detail'+id'').offset().top
            // }, 1000);
               
           }
       });

 });


 $(document).on("click",".view_detail_img_click",function(){  
    var id = $(this).attr("id");
   var data_counter = $(this).attr("data-counter");
   var insertdata = $(this).parent().parent().parent().parent().find(".product-column:last");
   $(".show_template_data").detach();

      $.ajax({
        cache: false,
        url: path + "templates/view_detail", //this is the submit URL
        type: 'POST', //or POST
        data: {id:id,data_counter:data_counter},
        success: function (data) {
        fbq('track', 'ViewContent', {
              currency: 'USD',
              content_ids: id,
              content_type: 'Greeting Card',
            }); 
          $(data).insertAfter($(insertdata));
          $('html, body').animate({
                 
                scrollTop: $(".product-detail-expand").offset().top-120
            }, 1000);
         
        // $('.show_template_data'+data_counter+'').slideDown(2000);    
               // $('.show_template_data'+data_counter+'').html(data);
             
            //    $('html, body').animate({
                 
            //     scrollTop: $('.show_card_detail'+id'').offset().top
            // }, 1000);
               
           }
       });

 });


 $(document).on("click",".close_detail_div",function(){  
  // alert(1);
   var data_counter = $(this).attr("data-counter");
   // alert(data_counter);
   $('#template_detail'+data_counter+'').remove();;

 });

 

// var page_number = 2;
 $(document).on("click",".next_btn",function() {

  var options = {direction:'left',duration:500};    
  var page_number = $(this).attr("data-page_number");
  var curr = $(this);
  if(page_number == 9){
    page_number = 2;
    return false;
  }
  
  $.ajax({
       cache: false,
        url: path + "templates/setpage_number",
        type:'POST', //this is the submit URL
        data: {page_number:page_number,next_btn:1},
        dataType: 'json', 
        success: function (data) { 
          console.log(data);
          if (data != "" && data.status == "error"){
            swal("Fail!", data.msg, "error");
            return false;
          }
          curr.attr("data-page_number",data.page_number);
         $(".prev_btn").attr("data-page_number",data.page_number);
          var p_num = curr.attr("data-page_number"); 
          alert(p_num);        
         if(p_num==3){

          fbq('track', 'ViewContent', {
                content_type: 'Record Message',
              });

             stopAlreadyPlayingAudio();
             
          if(data.audio!="" && data.audio=="yes"){
             // alert(data.audio);
            curr.show();
          }else{
            curr.hide();
          }
          $(".hide_rest").hide();
          $(".step-two > a").css("font-weight","normal");
          $(".step-two").addClass('done');
          $("#prog").css("width","66.6667%");
          // $(".next_btn_div").removeClass("col-lg-12");
          // $(".next_btn_div").addClass("col-lg-6");
           $(".sidebar_prev_btn").show();
           $(".sidebar_prev_btn").addClass("prev_btn");
            $(".sidebar_prev_btn").attr("data-page_number",data.page_number);
           $(".sidebar_prev_btn").text('Back');
           $(".sidebar_prev_btn").attr("href","javascript:void(0)");
           // curr.hide();
            $(".show_main_background").hide();    
            $(".add_audio_btn").toggle('slide',options);  
              
        $(".step-three").addClass("active");
        // $(".my-add-audio").css("display","block");
        $(".cus-Support-audio").show();
        

          $.ajax({
        cache: false,
        url: path + "templates/show_sample_videos", //this is the submit URL
        type: 'POST', //or POST        
        success: function (data) { 
        $(".cus-Support-audio").show();  
        $(".cus-Support-audio").html(data);         
           }
       }); 

          var phot_frame_div_height = $(".photo-frame").height();   
                $(".fix-col-left").css("height",phot_frame_div_height+"px");
             $('html, body').animate({
                 
                scrollTop: $(".container-wizard").offset().top
            }, 1000);    
         }
          if(p_num==4){  

             fbq('track', 'ViewContent', {
                content_type: 'Review your LiziCard',
              });
              stopAlreadyPlayingAudio();
              
            $(".hide_rest").show();
          $(".add_audio_btn").hide();     
          $(".customize_preview").toggle('slide',options);
           $.ajax({
                  cache: false,
                  url: path + "templates/show_audio_recorded_preview", //this is the submit URL
                  type: 'POST', //or POST        
                  success: function (result) {  
                  
                   $(".audio_media").html(result); 
                    
              if(browserName=="Chrome"){
              $(document).find("#timeline_audios,#timeline_preview").hide();
              $(document).find(".playbtn-left").removeClass("col-md-2");
              $(document).find(".playbtn-left").addClass("col-md-12");
             // $(document).find(".playbtn-left").css("padding","0% 45%");
              $(document).find(".playbtn-left").removeClass("playbtn-left");
             
              }       
                        
                                      
                  }
                });
          
          $(".sidebar_prev_btn").text("Back"); 
           $('html, body').animate({
                 
                scrollTop: $(".container-wizard").offset().top
            }, 1000);  
          
              var phot_frame_div_height = $(".photo-frame").height();   
                $(".fix-col-left").css("height",phot_frame_div_height+"px");
          }

          if(p_num==5){ 
            fbq('track', 'ViewContent', {
                content_type: 'Register with lizicard',
              });
              stopAlreadyPlayingAudio();
	           curr.hide();
            $(".customize_preview,.container-wizard").hide();           
          $(".template_sidebar,.rowdata").hide();         
            $(".main_container_div").removeClass("col-sm-9");
            $(".main_container_div").addClass("col-sm-12");
             if(data.form_type!="" && data.form_type=="sign_in"){
              $(".registration_form").hide();
              $(".sign_in_form_with_forgot_pass").hide();
              $(".sign_in_form_with_forgot_pass").toggle('slide',options);    
             }
             if(data.form_type!="" && data.form_type=="sign_up"){
              
              $(".sign_in_form_with_forgot_pass").hide();  
              $(".registration_form").hide();
              $(".registration_form").toggle('slide',options); 
             }
             if(data.form_type==null){
               $(".registration_form").toggle('slide',options);
            $(".step-four").addClass("active");
            
             }
             $(".cus-Support-audio").hide();
            $(".prev_btn").show();

                        
           

          //   curr.hide();
          // $(".step-three").addClass('done');
          // $("#prog").css("width","100%"); 
          //   $(".template_sidebar,.rowdata").hide();
          //   $(".main_container_div").removeClass("col-sm-9");
          //   $(".main_container_div").addClass("col-sm-12");
          //   $(".registration_form").hide();                         
          //   $(".sending_type").toggle('slide',options);
          //   $(".step-four").addClass("active");
          //   $(".cus-Support-audio").hide();
            
            
           
          }

          if(p_num==6){  
            fbq('track', 'ViewContent', {
                content_type: 'Select Mail type',
              });
              stopAlreadyPlayingAudio();

            curr.hide();
            $(".prev_btn,.step-wizard").show();
            $(".step-three > a").css("font-weight","normal");
             $(".step-three").addClass('done');
            $("#prog").css("width","100%");  
         
            $(".customize_preview,.template_sidebar,.rowdata").hide();
            $(".main_container_div").removeClass("col-sm-9");
            $(".main_container_div").addClass("col-sm-12");
            $(".registration_form").hide();                         
            $(".sending_type").toggle('slide',options);
            $(".step-four").addClass("active");
            $(".cus-Support-audio").hide();
            
            
           
          }

           }
       });
    
    
 });


 $(document).on("click",".prev_btn",function() {
   var options = {direction:'left',duration:500};    
  var page_number = $(this).attr("data-page_number");
  var curr = $(this);
 if(page_number == 2){
  page_number = 2;
   $(".next_btn").attr("data-page_number",2);
   
   return false;
  }
 
  $.ajax({
        cache: false,
        url: path + "templates/setpage_number",
        type:'POST', //this is the submit URL
        data: {page_number:page_number,prev_btn:1},
        dataType: 'json', 
        success: function (data) {
          if (data != "" && data.status == "error"){
            swal("Fail!", data.msg, "error");
            return false;
          }
          curr.attr("data-page_number",data.page_number);          
          $(".next_btn").attr("data-page_number",data.page_number);
          $(".prev_btn").attr("data-page_number",data.page_number);
          var p_num = curr.attr("data-page_number");
          alert(p_num);
         if(p_num==4){
          fbq('track', 'ViewContent', {
            
              content_type: 'Review your LiziCard',
            });
             stopAlreadyPlayingAudio();
          curr.hide();
          $(".main_container_div").removeClass("col-sm-12");
            $(".main_container_div").addClass("col-sm-9");
            $(".registration_form,.sign_in_form_with_forgot_pass,.sending_type").hide(); 
            $(".step-three").addClass('active');
          $(".step-three").removeClass('done');
          $("#prog").css("width","66.6667%");
          $(".step-four").removeClass('active');  
          $(".step-three > a").css("font-weight","bold");     
          setTimeout(function(){

          $(".next_btn_div,.container-wizard").show();          
            $(".template_sidebar").toggle('slide',options);
            $(".next_btn").show();
            $(".sidebar_prev_btn").addClass("prev_btn");
            $(".sidebar_prev_btn").attr("data-page_number",data.page_number);
           $(".sidebar_prev_btn").text('Back');
           $(".sidebar_prev_btn").attr("href","javascript:void(0)");            
            $(".rowdata").toggle('slide',options);
            $(".customize_preview").toggle('slide',options);
          },100);
        
        // $(".cus-Support-audio").show();
        
        
         $('html, body').animate({
                 
                scrollTop: $(".container-wizard").offset().top
            }, 1000);

          // $(".sending_type").hide();
          // $(".registration_form").toggle('slide',options);
          //  $(".next_btn").show();                
       
         }

         if(p_num==8){
          fbq('track', 'InitiateCheckout');
          var options = {direction:'right',duration:500}; 
          $(".chackout_page").hide();
                $(".cart-conatiner").toggle('slide',options);  
                $(".next_btn").attr("data-page_number",8);
                $(".prev_btn").attr("data-page_number",8);
                // $(".prev_btn").attr("data-type","to_receipent");
        $.ajax({
            url: path + "users/address", //this is the submit URL
            type: 'POST', //or POST                    
            success: function (data) {
              $(".container-wizard").hide(); 
              $(".master-cart__full").html(data);
               
             
              
            }
        });
         }

         // if(p_num==5){
          
         //  $(".row card_to_me_text").hide();
         //  $(".row card_to_me").hide();
         //  $(".sending_type").toggle('slide',options);
         // $(".card_to_me_text,.card_to_me").hide();  
         //   $(".next_btn").hide(); 
         //   $(".mail_receipent").hide(); 
         //   $(".card_to_me_text,.card_to_me").hide();           
       
         // }

         if(p_num==6){

          fbq('track', 'ViewContent', {
            
              content_type: 'Select Mailing type',
            });
             stopAlreadyPlayingAudio();

          $(".row card_to_me_text,.card_to_me_form").hide();
          $(".row card_to_me").hide();
          $(".sending_type").toggle('slide',options);
         $(".card_to_me_text,.card_to_me,.card_to_me_form").hide();  
           $(".next_btn").hide(); 
           $(".mail_receipent,.card_to_receipent_form").hide(); 
           $(".card_to_me_text,.card_to_me,.card_to_me_form").hide(); 



          
         
       
         }

         if(p_num==7){
          fbq('track', 'Lead');
          $(".step-wizard").show();
          var data_type= curr.attr("data-type");
          if(data_type=="to_me"){
            
            $(".sending_type").hide();
            $(".cart-conatiner").hide(); 
            $(".card_to_me_text,.card_to_me,.card_to_me_form").toggle('slide',options);
            
          }
          else if(data_type=="to_receipent"){
            $(".sending_type").hide();
            $(".cart-conatiner").hide();             
            $(".card_to_me_text,.card_to_me,.card_to_me_form").hide();
            $(".mail_receipent,.card_to_receipent_form").toggle('slide',options);
          }
         }


         if(p_num==3){
              fbq('track', 'ViewContent', {
            
              content_type: 'Record Message',
            });

             stopAlreadyPlayingAudio();

          curr.hide();
          $(".hide_rest").hide();
          $(".customize_preview").hide();
          $(".rowdata").show();
          $(".add_audio_btn").toggle('slide',options);
          $(".sidebar_prev_btn").text("Back");
          $(".sidebar_prev_btn").show();
           $('html, body').animate({
                 
                scrollTop: $(".container-wizard").offset().top
            }, 1000);

          var phot_frame_div_height = $(".photo-frame").height();   
                $(".fix-col-left").css("height",phot_frame_div_height+"px");
        
         }

         if(p_num==2){  

          fbq('track', 'ViewContent', {
            
              content_type: 'Upload photo',
            });           
             stopAlreadyPlayingAudio();
             
          $(".hide_rest").show();
          $(".step-two").removeClass('done');
          $(".step-two").addClass('active');
          $("#prog").css("width","33.3333%");
          $(".step-three").removeClass('done');
          $(".step-three").removeClass('active');
          $(".step-four").removeClass('active');
          $(".step-three > a").css("font-weight","normal");  
          $(".step-two > a").css("font-weight","bold");  
          $(".next_btn_div").show();
          $(".next_btn").show();
                   
          
          curr.hide();
          $(".sidebar_prev_btn").show();
          $(".sidebar_prev_btn").removeClass("prev_btn");
          $(".sidebar_prev_btn").attr("href",path);
          $(".sidebar_prev_btn").text("Back");
           $(".add_audio_btn").hide();    
            $(".show_main_background").toggle('slide',options);
                 
            
        $(".cus-Support-audio").hide();
         $('html, body').animate({
                 
                scrollTop: $(".container-wizard").offset().top
            }, 1000);

        var phot_frame_div_height = $(".photo-frame").height();   
                $(".fix-col-left").css("height",phot_frame_div_height+"px");
          
         }
          
           }
       });
  
 });


$(document).on("click",".scrollto_homedesign",function(){  
     $('html, body').animate({
                 
                scrollTop: $(".product-listing").offset().top-30
            }, 1000);  

 });

/*
Normal user formm submit*/
// Register normal user  
$("#normal_user_form").on("submit", function (e) {
    e.preventDefault();
    if ($("#normal_user_form").valid())
    {
        $.ajax({
            cache: false,
            url: path + "users/register", //this is the submit URL
            type: 'POST', //or POST
            data: new FormData(this),
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
              fbq('track', 'CompleteRegistration');
                $(".normal_user_submit").attr("disabled",true);
                $(".normal_user_submit").html("Please wait.....");
                if (data != "" && data.status == "success") {
                 var options = {direction:'left',duration:500};
               setTimeout(function(){ 
               $(".after_login_menu,.container-wizard").show();         
                 $(".next_btn").hide();
                 $(".step-three > a").css("font-weight","normal");
          $(".step-three").addClass('done');
          $("#prog").css("width","100%"); 
            $(".template_sidebar,.rowdata").hide();
            $(".main_container_div").removeClass("col-sm-9");
            $(".main_container_div").addClass("col-sm-12");
            $(".registration_form").hide();                         
            $(".sending_type").toggle('slide',options);
            $(".step-four").addClass("active");
            $(".cus-Support-audio").hide();
            
              },1000) ;
                } else if (data != "" && data.status == "error") {
                  setTimeout(function(){
                 swal("Fail!", data.msg, "error");
                 $(".normal_user_submit").attr("disabled",false);
                $(".normal_user_submit").html("Join Now");
              },1000) ;
              
                }
              
            }
        });
    }
});

// Register guest user

// Register normal user  
$("#guest_user_form").on("submit", function (e) {
  
    e.preventDefault();
    if ($("#guest_user_form").valid())
    {

        $.ajax({
            cache: false,
            url: path + "users/register", //this is the submit URL
            type: 'POST', //or POST
            data: new FormData(this),
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (data) {

            fbq('track', 'CompleteRegistration');

                $(".guest-btn").attr("disabled",true);
                $(".guest-btn").html("Please wait.....");
                if (data != "" && data.status == "success") {
                  var options = {direction:'left',duration:500};
               setTimeout(function(){
                $(".after_login_menu,.step-wizard").show();
                $(".next_btn").hide();
                $(".step-three > a").css("font-weight","normal");
          $(".step-three").addClass('done');
          $("#prog").css("width","100%"); 
            $(".template_sidebar,.rowdata").hide();
            $(".main_container_div").removeClass("col-sm-9");
            $(".main_container_div").addClass("col-sm-12");
            $(".registration_form").hide();                         
            $(".sending_type").toggle('slide',options);
            $(".step-four").addClass("active");
            $(".cus-Support-audio").hide();
            $(".after_login_menu").show();
              },1000) ;
                } else if (data != "" && data.status == "error") {
                  setTimeout(function(){
                 swal("Fail!", data.msg, "error");
                 $(".guest-btn").attr("disabled",false);
                $(".guest-btn").html("Continue as Guest");
              },1000) ;
              
                }
              
            }
        });
    }
});

  $(document).ready(function(){
    window.fbAsyncInit = function () {
        // init the FB JS SDK
        FB.init({
            appId: fb_id, // App ID from the App Dashboard
            status: true, // check the login status upon init?
            cookie: true, // set sessions cookies to allow your server to access the session?
            xfbml: true, // parse XFBML tags on this page?
            oauth: true
        });
    };

    // Load the SDK's source Asynchronously
    // Note that the debug version is being actively developed and might 
    // contain some type checks that are overly strict. 
    // Please report such bugs using the bugs tool.
    (function (d, debug) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all" + (debug ? "/debug" : "") + ".js";
        ref.parentNode.insertBefore(js, ref);
    }(document, /*debug*/ false));

});

function fblogin() {
 
   // $(".loader").show();
    $(".guest-btn").attr("disabled",true);
    $(".normal_user_submit").attr("disabled",true);
    FB.login(function (response) {
        if (response.authResponse) {
            console.log('Welcome!  Fetching your information.... ');
            FB.api('/me', {fields: 'name,first_name,last_name, email,gender,verified,picture'},
                    function (response) {
                        $.ajax({
                            cache: false,
                            url: path + "users/facebook_login", //this is the submit URL
                            type: 'POST', //or POST
                            data: {userdata: response},
                            dataType: 'json',
                            success: function (data) {
                                console.log(data);
                               if (data != "" && data.status == "success") {
                                fbq('track', 'CompleteRegistration');
                                var options = {direction:'left',duration:500};
                             setTimeout(function(){
                              $(".after_login_menu,.container-wizard").show();      
                              $(".next_btn").hide();
                              $(".step-three > a").css("font-weight","normal");
                            $(".step-three").addClass('done');
                            $("#prog").css("width","100%"); 
                              $(".template_sidebar,.rowdata").hide();
                              $(".main_container_div").removeClass("col-sm-9");
                              $(".main_container_div").addClass("col-sm-12");
                              $(".registration_form,.sign_in_form_with_forgot_pass").hide();                         
                              $(".sending_type").toggle('slide',options);
                              $(".step-four").addClass("active");
                              $(".cus-Support-audio").hide();  
                              $(".after_login_menu").show();                           
                            },1000) ;
                                $(".guest-btn").attr("disabled",false);
                                $(".normal_user_submit").attr("disabled",false);//                              
                            }else if (data != "" && data.status == "error") {
                              setTimeout(function(){
                             swal("Fail!", data.msg, "error");
                             $(".guest-btn").attr("disabled",false);
                            $(".normal_user_submit").attr("disabled",false);//  
                          },1000) ;
              
                            }
                          }
                        });
                    });
        } else {
            console.log('User cancelled login or did not fully authorize.');
        }
    }, {scope: 'email'}
    );
}

$(document).on("click",".mail-card-reciept-receipent",function(){
  var options = {direction:'right',duration:500};  
  $.ajax({
        cache: false,
        url: path + "templates/setpage_number",
        type:'POST', //this is the submit URL
        data: {page_number:6,next_btn:1,type:'to_receipent'},
        dataType: 'json', 
        success: function (data) {   

         fbq('track', 'Lead');
           $(".send_to_receipent").val(data.type);
        $(".sending_type").hide();
        $(".mail_receipent,.card_to_receipent_form").toggle('slide',options);
      $(".card_to_me_text,.card_to_me,.card_to_me_form").hide();
        // $(".next_btn").show();
          console.log(data);
       $(".next_btn").attr("data-page_number",data.page_number);
       $(".prev_btn").attr("data-page_number",data.page_number);       
       

           }
       });
});

$(document).on("click",".mail-card-reciept-me",function(){
  var options = {direction:'right',duration:500}; 
 
  $.ajax({
        cache: false,
        url: path + "templates/setpage_number",
        type:'POST', //this is the submit URL
        data: {page_number:6,next_btn:1,type:'to_me'},
        dataType: 'json', 
        success: function (data) { 
          fbq('track', 'Lead');
          $(".send_to_me").val(data.type);
       $(".next_btn").attr("data-page_number",data.page_number);
       $(".prev_btn").attr("data-page_number",data.page_number);
        // $(".next_btn").show();  
        $(".sending_type").hide();
        $(".mail_receipent,.card_to_receipent_form").hide();
        $(".card_to_me_text,.card_to_me,.card_to_me_form").toggle('slide',options); 
	$(".user_first_name").val(data.user_first_name);
        $(".user_last_name").val(data.user_last_name);   
         
         // alert(data.page_number);

           }
       });
});

$(document).on("click",".thumb_images",function(){
var src = $(this).attr('src');
$(".show_large_image").attr("src",src);
$(this).fadeIn(2000);
});


$(document).on("click",".increase_btn",function(){
var val = $(".quantity").val();
var incre = "";
val = parseInt(val)+1;
$(".quantity").val(val);

});

$(document).on("click",".decrease_btn",function(){
var val = $(".quantity").val();
var incre = "";
val = parseInt(val)-1;
if(val==0){
  $(".quantity").val(1);
  return false;
}else{
  $(".quantity").val(val);
}
});

/*
Address form to me submit*/
 
$("#card_to_me_form").on("submit", function (e) {
  var options = {direction:'right',duration:500}; 
    e.preventDefault();
      var v= $("#country_name_list option:selected").val();
      // alert(v);
   if(v!=231){
   $('#zips').rules('remove', 'required');
    
   }else{
     var zip_val = $('#zips').val();
      var validate_zip = /^[0-9]{4,5}$/;
  if(!validate_zip.test(zip_val)){
 setTimeout(function(){
  $("#zips-error").show();
    $("#zips-error").html("Enter valid zipcode");
    },.1);
    return false;
  }
    $('#zips').rules('add', {
    required: true   // set a new rule
});
   }
    if ($("#card_to_me_form").valid())
    {   
            fbq('track', 'InitiateCheckout');
                $(".card_to_me_text,.card_to_me,.card_to_me_form").hide();
                $(".cart-conatiner").toggle('slide',options);  
                $(".next_btn").attr("data-page_number",8);
                $(".prev_btn").attr("data-page_number",8);
                $(".prev_btn").attr("data-type","to_me");
        $.ajax({
            cache: false,
            url: path + "users/address", //this is the submit URL
            type: 'POST', //or POST
            data: new FormData(this),
            contentType: false,
            processData: false,            
            success: function (data) {
              
              $(".container-wizard").hide(); 
              $(".master-cart__full").html(data);
               
              
             
              
              //   $(".normal_user_submit").attr("disabled",true);
              //   $(".normal_user_submit").html("Please wait.....");
              //   if (data != "" && data.status == "success") {
              //  setTimeout(function(){
              //   swal("Success!", data.msg, "success");
              //   $(".normal_user_submit").attr("disabled",false);
              //   $(".normal_user_submit").html("Join Now");
              // },2000) ;
              //   } else if (data != "" && data.status == "error") {
              //     setTimeout(function(){
              //    swal("Fail!", data.msg, "error");
              //    $(".normal_user_submit").attr("disabled",false);
              //   $(".normal_user_submit").html("Join Now");
              // },2000) ;
              
                }
              
            // }
        });
    }
});

 var count = 0;
$(document).on("click",".add_more_receipent",function(){
 count += 1;
    $.ajax({
            cache: false,
            url: path + "Users/add_more_receipent",//this is the submit URL    
            type:"POST",
            data:{count:count},         
            success: function (result) {
              $(".show_more_receipent").append(result);
              $(".remove_receipent").show();
              // $(".remove_receipent").attr("id",count);

              $.validator.addMethod(
            "lettersOnly",
            function (value, element) {
                return value.match(/^[A-Za-z ]+$/);
            },
            "Enter only letters"
            );

               $.validator.addMethod(
            "zip_code",
            function (value, element) {
                return value.match(/^\d{5}(?:[-\s]\d{4})?$/);
            },
            "Enter valid zipcode"
            );

              $('#card_to_receipent_form').validate({
       
             });

     $('.first_name').each(function() {
        $(this).rules('add', {
            required: true,
            //lettersOnly: true,            
            messages: {
                required: "Please enter first name",
                
            }
        });
    });

    $('.last_name').each(function() {
        $(this).rules('add', {
            required: true,
            //lettersOnly: true,            
            messages: {
                required: "Please enter last name",
                
            }
        });
    });

    $('.address_one').each(function() {
        $(this).rules('add', {
            required: true,                 
            messages: {
                required: "Please enter address",
                
            }
        });
    });

    $('.city').each(function() {
        $(this).rules('add', {
            required: true,                   
            messages: {
                required: "Please enter city",
                
            }
        });
    });
  $('.state_id').each(function() {
        $(this).rules('add', {
            required: true,                   
            messages: {
                required: "Please select state",
                
            }
        });
    });

     $('.zip').each(function() {
        $(this).rules('add', {
            required: true, 
            // zip_code: true,                       
            messages: {
                required: "Please enter zip code",
                
            }
        });
    });
              // $(".add_more_receipent").html("Remove this Recipient");
              // $(".add_more_receipent").addClass("remove_receipent");
              // $(".add_more_receipent").removeClass("add_more_receipent");
          
            
          }
        });
});


$(document).on('click', ".remove_receipent", function () {
    var id = $(this).attr("id");   
    if(id==1){
      $(this).hide();
     
    }
    // alert(count);
    $(".more_receipent" + id + "").remove();
    count-=1;
    $(this).attr("id",count);
  });

/*
Address form to me submit*/
 
$("#card_to_receipent_form").on("submit", function (e) {
    e.preventDefault();
     

    $('.zip').each(function(index) {    
      var zip_val = $(this).val();
      var id = $(this).attr("data-id");
      var v = $(".multiple_country"+index+" option:selected").val();
      
      if(v!=231){
       $('#zips'+id+'').rules('remove', 'required');

     }else{
    
      $('#zips'+id+'').rules('add', {
       required: true,
         // set a new rule
  });
      var zip_val = $('#zips'+id+'').val();
      var validate_zip = /^[0-9]{4,5}$/;
  if(!validate_zip.test(zip_val)){
 setTimeout(function(){
$("#zips"+id+"-error").show();
    $("#zips"+id+"-error").html("Enter valid zipcode");
    },.1);
    return false;
  }
       
    } 
  });
    if ($("#card_to_receipent_form").valid())
    { 
        fbq('track', 'InitiateCheckout');
      var options = {direction:'right',duration:500}; 
      $(".mail_receipent,.card_to_receipent_form").hide();
                $(".cart-conatiner").toggle('slide',options);  
                $(".next_btn").attr("data-page_number",8);
                $(".prev_btn").attr("data-page_number",8);
                $(".prev_btn").attr("data-type","to_receipent");
        $.ajax({
            cache: false,
            url: path + "users/address", //this is the submit URL
            type: 'POST', //or POST
            data: new FormData(this),
            contentType: false,
            processData: false,           
            success: function (data) {
              $(".container-wizard").hide(); 
              $(".master-cart__full").html(data);
              
              
            }
        });
    }
});

$(document).on("submit","#checkout_form", function (e) {

  var options = {direction:'right',duration:500}; 
    e.preventDefault();   
        $(".cart-conatiner").hide();
         $(".next_btn").attr("data-page_number",9);
          $(".prev_btn").attr("data-page_number",9);      
        $.ajax({
            cache: false,
            url: path + "users/checkout", //this is the submit URL
            type: 'POST', //or POST
            data: new FormData(this),
            contentType: false,
            processData: false,            
            success: function (data) {

          fbq('track', 'AddPaymentInfo');

              $(".chackout_page").toggle('slide',options);
              $(".container-wizard").hide();  
            $(".chackout_page").html(data);
              
              
              
            }
        });
    
});

$(document).on("change","#country_name_list",function(){
var val = $(this).val();
if(val==231){
  $(document).find(".show_shiiping_option").siblings().remove();
     $.ajax({
            cache: false,
            url: path + "users/shiiping_option", //this is the submit URL
            type: 'POST',                      
            success: function (data) {
             $(data).insertAfter($("#country_name_lists"));
              
              setTimeout(function(){
                $(".state_me").show();
                $(document).find(".state_me option").attr("disabled",false);
                $(document).find(".state_me").attr("name","state_id");
              },500);
            }
        });
}else if(val==38 || val==101 || val==166 || val==181 || val==44 || val==13){
 setTimeout(function(){
                $(".state_me").show();
                $(document).find(".state_me option").attr("disabled",false);
                $(document).find(".state_me").attr("name","state_id");
		$(document).find(".show_shiiping_option").remove();
              },500);
}

else{

setTimeout(function(){
  $(".state_me,#state-id-error").hide();
                $(document).find(".state_me option").attr("disabled",true);
                $(document).find(".state_me").attr("name","");
              },500);
   $(document).find(".show_shiiping_option").remove();
}
});


$(document).on("change",".country_fees",function(){
  var id = $(this).attr("id");
  var val = $(this).val();
 
  if(val==231){
     $("#remove_receipent").removeClass("col-md-12 col-sm-12 col-xs-12");

  $("#remove_receipent").addClass("col-md-6 col-sm-6 col-xs-12");
     $.ajax({
            cache: false,
            url: path + "users/shiiping_option_more", //this is the submit URL
            type: 'POST', 
            data:{count:id},                     
            success: function (data) {
               $(document).find("#country_fees"+id+"").html(data);
             // $(data).insertAfter($("#country_fees"+id+""));

             setTimeout(function(){
                $(".state_me"+id+"").show();
                $(document).find(".state_me"+id+" option").attr("disabled",false);
                $(document).find(".state_me"+id+"").attr("name","data["+id+"][state_id]");
              },500);
              
              
            }
        });
}
else if(val==38 || val==101 || val==166 || val==181 || val==44 || val==13){
 setTimeout(function(){
                $(".state_me"+id+"").show();
                $(document).find(".state_me"+id+" option").attr("disabled",false);
                $(document).find(".state_me"+id+"").attr("name","data["+id+"][state_id]");
		$(document).find(".show_shiiping_option"+id+"").remove();
              },500);
}
else{
  // alert(1);
   setTimeout(function(){ 
            $("#data-"+id+"-state-id-error").hide();
                $(".state_me"+id+"").hide();
                $(document).find(".state_me"+id+" option").attr("disabled",true);
                $(document).find(".state_me"+id+"").attr("name","");
              },500);
  $("#remove_receipent").removeClass("col-md-6 col-sm-6 col-xs-12");
  $("#remove_receipent").addClass("col-md-12 col-sm-12 col-xs-12");
// alert(count);
   $(document).find(".show_shiiping_option"+id+"").remove();
}
});

$(document).on("change",".country_name_list_action",function(){
var val = $(this).val();
if(val==231 || val==38 || val==101 || val==166 || val==181 || val==44 || val==13){
   $.ajax({
            cache: false,
            url: path + "users/show_country", //this is the submit URL
            type: 'POST', 
            data:{val:val},                      
            success: function (data) {
              $(".show_state_list").html(data);
             
              
              
            }
        });
 }

});


$(document).on("change",".country_fees",function(){
  var val = $(this).val();
  var id = $(this).attr("id");  
 
 if(val==231 || val==38 || val==101 || val==166 || val==181 || val==44 || val==13){ 
     $.ajax({
            cache: false,
            url: path + "users/show_country_more", //this is the submit URL
            type: 'POST', 
            data:{count:id,val:val},                     
            success: function (data) {
             $(".show_state_list"+id+"").html(data);
              
              
            }
        });
   }

});


// Delete users

$(document).on("click", ".delete_user", function () {
    var curr = $(this);
    var id = $(this).attr("id");  

    swal({
        title: "Are you sure?",
        text: "Are you sure you want to delete this user?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false,
    },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        cache: false,
                        url: path+"admin/Users/delete_user", //this is the submit URL
                        type: 'POST', //or POST  
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {

                            if (data.status != "" && data.status == "success") {
                                // curr.closest("tr").remove();
                                // swal("Deleted!", data.msg, "success");
                                document.location.reload();
                            } else {
                                swal("fail!", data.msg, "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "Your data is safe :)", "error");
                }
            });
});


// Change users status

$(document).on("click", ".user_status", function () {
    var curr = $(this);
    var id = $(this).attr("id");  
    var status = $(this).attr("data-status");
    swal({
        title: "Are you sure?",
        text: "Are you sure you want to change the status?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false,
    },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        cache: false,
                        url: path+"admin/Users/change_status", //this is the submit URL
                        type: 'POST', //or POST  
                        data: {id: id,status:status},
                        dataType: 'json',
                        success: function (data) {
                            if (data.status != "" && data.status == "success") {
                                curr.attr('data-status',data.new_status);
                                if(data.new_status==1){
                                  curr.html("Activate");
                                }else{
                                  curr.html("Deactivate");
                                }
                                 swal("Success!", data.msg, "success");
                            } else {
                                swal("fail!", data.msg, "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "", "error");
                }
            });
});


// Delete template

$(document).on("click", ".delete_template", function () {
    var curr = $(this);
    var id = $(this).attr("id");  

    swal({
        title: "Are you sure?",
        text: "Are you sure you want to delete this template?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false,
    },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        cache: false,
                        url: path+"admin/templates/delete_template", //this is the submit URL
                        type: 'POST', //or POST  
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {

                            if (data.status != "" && data.status == "success") {
                                // curr.closest("tr").remove();
                                // swal("Deleted!", data.msg, "success");
                                document.location.reload();
                            } else {
                                swal("fail!", data.msg, "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "Your data is safe :)", "error");
                }
            });
});


// Delete promotion code

$(document).on("click", ".delete_promotion", function () {
    var curr = $(this);
    var id = $(this).attr("id");  

    swal({
        title: "Are you sure?",
        text: "Are you sure you want to delete this promotion code?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false,
    },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        cache: false,
                        url: path+"admin/templates/delete_promotion", //this is the submit URL
                        type: 'POST', //or POST  
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {

                            if (data.status != "" && data.status == "success") {
                                // curr.closest("tr").remove();
                                // swal("Deleted!", data.msg, "success");
                                document.location.reload();
                            } else {
                                swal("fail!", data.msg, "error");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "Your data is safe :)", "error");
                }
            });
});

$("#forgot_password").on("submit", function (e) {
    // alert(path); return false;
    e.preventDefault();

    if ($("#forgot_password").valid())
    {
        // $(".login_btn").attr("disabled", true);
        // $(".loader").show();

        $.ajax({
            cache: false,
            url: path + "admin/users/forgot_password", //this is the submit URL
            type: 'POST', //or POST
            data: new FormData(this),
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                
                $(".forgot_pass_msg_div").show();
                if (data != "" && data.status == "success") {
                    
                    $(".forgot_pass_msg_div").html(data.response)
                   
                        $(".forgot_pass_msg_div").html(data.response)
                        // setTimeout(function () {
                        // $("#vendor_user_forgot_passwordModal").modal('hide');
                        $("#forgot_password").find("input[type=email]").val('');
                        $("#forgot_password").find("label.error").remove();
                        // $(".forgot_pass_msg_div").html('');
                        $(".login_btn").attr("disabled", false);
                        // }, 3000);
                   
                } else if (data.status == "error") {
                    $(".forgot_pass_msg_div").html(data.response);
                    $(".login_btn").attr("disabled", false);
                  //$(".loader").hide()
                }

                setTimeout(function () {
                    $(".forgot_pass_msg_div").hide('slow');
                }, 5000);

            }
        });
    }
});

$(document).on("click",".show_sign_in_form",function(){
  var options = {direction:'left',duration:500};  
  $.ajax({
        cache: false,
        url: path + "templates/setpage_number",
        type:'POST', //this is the submit URL
        data: {page_number:4,form_type:"sign_in",next_btn:1},
        dataType: 'json', 
        success: function (data) { 
          console.log(data); 
               $(".registration_form").hide();
          $(".sign_in_form_with_forgot_pass").toggle('slide',options);    
      

           }
       });  
 
});
$(document).on("click",".sign_up_form",function(){
  var options = {direction:'left',duration:500};
 $.ajax({
        cache: false,
        url: path + "templates/setpage_number",
        type:'POST', //this is the submit URL
        data: {page_number:4,form_type:"sign_up",next_btn:1},
        dataType: 'json', 
        success: function (data) { 
          console.log(data); 
         $(".sign_in_form_with_forgot_pass").hide();
  $(".registration_form").toggle('slide',options);
      

           }
       });  

 
  
});

/*
Normal user formm submit*/
// Register normal user  
$("#normal_user_sign_in_form").on("submit", function (e) {
    e.preventDefault();
    if ($("#normal_user_sign_in_form").valid())
    {
        $.ajax({
            cache: false,
            url: path + "users/login", //this is the submit URL
            type: 'POST', //or POST
            data: new FormData(this),
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                $(".normal_user_sign_in_submit").attr("disabled",true);
                $(".normal_user_sign_in_submit").html("Please wait.....");
                if (data != "" && data.status == "success") {
                 var options = {direction:'left',duration:500};
               setTimeout(function(){                
                 $(".next_btn").hide();
                 $(".step-three > a").css("font-weight","normal");
          $(".step-three").addClass('done');
          $("#prog").css("width","100%"); 
            $(".template_sidebar,.rowdata").hide();
            $(".main_container_div").removeClass("col-sm-9");
            $(".main_container_div").addClass("col-sm-12");
            $(".sign_in_form_with_forgot_pass").hide();                         
            $(".sending_type").toggle('slide',options);
            $(".step-four").addClass("active");
            $(".cus-Support-audio").hide();
            $(".after_login_menu,.container-wizard").show();
              },1000) ;
                } else if (data != "" && data.status == "error") {
                  setTimeout(function(){
                 swal("Fail!", data.msg, "error");
                 $(".normal_user_sign_in_submit").attr("disabled",false);
                $(".normal_user_sign_in_submit").html("Join Now");
              },1000) ;
              
                }
              
            }
        });
    }
});

$("#forgot_password_user").on("submit", function (e) {
    // alert(path); return false;
    e.preventDefault();

    if ($("#forgot_password_user").valid())
    {
        // $(".login_btn").attr("disabled", true);
        // $(".loader").show();

        $.ajax({
            cache: false,
            url: path + "users/forgot_password", //this is the submit URL
            type: 'POST', //or POST
            data: new FormData(this),
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (data) {
                
                $(".forgot_pass_msg_div").show();
                if (data != "" && data.status == "success") {
                    
                    $(".forgot_pass_msg_div").html(data.response)
                   
                        $(".forgot_pass_msg_div").html(data.response)
                        // setTimeout(function () {
                        // $("#vendor_user_forgot_passwordModal").modal('hide');
                        $("#forgot_password_user").find("input[type=email]").val('');
                        $("#forgot_password_user").find("label.error").remove();
                        // $(".forgot_pass_msg_div").html('');
                        $(".login_btn").attr("disabled", false);
                        // }, 3000);
                   
                } else if (data.status == "error") {
                    $(".forgot_pass_msg_div").html(data.response);
                  //$(".loader").hide()
                }

                setTimeout(function () {
                    $(".forgot_pass_msg_div").hide('slow');
                }, 5000);

            }
        });
    }
});

$(document).ready(function(){
    setTimeout(function(){
	$(".error_message,.success_message").hide("slow")
    },5000);
});

$(document).on("click",".checkout_header",function(){
$(".checkout_submit").trigger("click");
});


$(document).on("keyup","#zips",function(){
 var v= $("#country_name_list option:selected").val();
 if(v==231){
  var zip_val = $(this).val();
  var validate_zip = /^[0-9]{4,5}$/;
  if(!validate_zip.test(zip_val)){
    $("#zips-error").show();
    $("#zips-error").html("Enter valid zipcode");
    return false;
  }else{
    $("#zips-error").hide();
    $("#zips-error").html("");
  }

}

});

$(document).on("keyup blur",".zip",function(){
  var id  = $(this).attr("data-id");
  // alert(id);
 var v= $(".multiple_country"+id+" option:selected").val();
  
 if(v==231){
  var zip_val = $(this).val();
  var validate_zip = /^[0-9]{4,5}$/;
  if(!validate_zip.test(zip_val)){
 setTimeout(function(){
$("#zips"+id+"-error").show();
    $("#zips"+id+"-error").html("Enter valid zipcode");
    },.1);
    return false;
  }
else{
 
    $("#zips"+id+"-error").hide();
    $("#zips"+id+"-error").html("");
  }

}

});

$(document).on("click",".tips_from_pro",function(){
$('html, body').animate({
                 
                scrollTop: $(".right-sps").offset().top
            }, 500);
});

function stopAlreadyPlayingAudio() {
    var playing_music_audio = document.getElementById('music_audio');
    var button_music_audio  = document.getElementById('pButton_audio');
    var text_button_music_audio  = $('.play_pause_btn_audio');
    
    var playing_music_preview = document.getElementById('music_preview'); 
    var button_music_preview  = document.getElementById('pButton_preview');  
    var text_button_music_preview  = $('.play_pause_btn_preview');
    
    var playing_music_audios = document.getElementById('music_audios');
    var button_music_audios  = document.getElementById('pButton_audios');
    var text_button_music_audios  = $('.play_pause_btn_audios');
    
    if(typeof playing_music_audio === 'object' && playing_music_audio!=null) {        
        playing_music_audio.pause();
        if(typeof button_music_audio === 'object' && button_music_audio!=null) {
            button_music_audio.className = "";
            button_music_audio.className = "play";
            if(typeof text_button_music_audio === 'object' && text_button_music_audio!=null) {
                text_button_music_audio.text('Play');
            }
            
        }
    }
    if(typeof playing_music_preview === 'object' && playing_music_preview!=null) {
        playing_music_preview.pause();
        if(typeof button_music_preview === 'object' && button_music_preview!=null) {
            button_music_preview.className = "";
            button_music_preview.className = "play";
            if(typeof text_button_music_preview === 'object' && text_button_music_preview!=null) {
                text_button_music_preview.text('Play');
            }
        }
    }
    
    if(typeof playing_music_audios === 'object' && playing_music_audios!=null) {
        playing_music_audios.pause();
        if(typeof button_music_audios === 'object' && button_music_audios != null) {
            button_music_audios.className = "";
            button_music_audios.className = "play";            
            if(typeof text_button_music_audios === 'object' && text_button_music_audios!=null) {
                text_button_music_audios.text('Play');
            }
            
            
        }
    }
    
}


$(document).on("click",".what_is_get_started",function(){
 $.ajax({
        cache: false,  
        url: path + "users/what_is_get_started", //this is the submit URL
        type: 'POST', //or POST       
        success: function (data) {
          window.location.href = path;
            
        }
    });
});


$(document).ready(function(){
    setTimeout(function(){
      $.ajax({
        cache: false,  
        url: path + "users/whatis", //this is the submit URL
        type: 'POST', //or POST       
        success: function (data) {          
            
        }
    });
    },2000);
});




$(document).on("click",".mybutton.selected",function(){
  var href = $(this).attr("href");
  var id = href.substring(href.lastIndexOf("/") + 1);
  alert(get_id);
   fbq('track', 'ViewContent', {
              currency: 'USD',
              content_ids: id,
              content_type: 'Select Card',
            });
});  

 
